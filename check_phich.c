/*
 * gcc -Wall -o check_phich check_phich.c -Wno-unused-but-set-variable
 *
 * textlog with ENB_PHY_PHICH on
 */
#include <stdio.h>
#include <stdlib.h>

void get(char *s)
{
  int c;
  int n = 0;
  while (1) {
    c = getchar(); if (c == EOF || c == '\n') break;
    s[n] = c;
    n++;
    if (n == 4000) abort();
  }
  s[n] = 0;
}

int main(void)
{
  int same = 1;
  int lines = 0;
  int _;
  int eNB_ID, f, sf, ue, rnti, harq, NGROUP, NSF, ngroup, nseq, ACK,
      first_rb, n_DMRS;
  int old_eNB_ID, old_f=-1, old_sf=-1, old_ue, old_rnti, old_harq, old_NGROUP,
      old_NSF, old_ngroup, old_nseq, old_ACK, old_first_rb, old_n_DMRS;
  char s[4096];
  while (1) {
    get(s);
    if (s[0] == 0) break;
    lines++;
    if (sscanf(s, "%d:%d:%d.%d: ENB_PHY_PHICH eNB_ID %d frame %d "
                  "subframe %d UE_id %d rnti %d harq_pid %d NGROUP %d "
                  "NSF %d ngroup %d nseq %d ACK %d first_rb %d n_DMRS %d",
               &_, &_, &_, &_,
               &eNB_ID, &f, &sf, &ue, &rnti, &harq, &NGROUP, &NSF, &ngroup,
               &nseq, &ACK, &first_rb, &n_DMRS) != 17) {
      printf("bad line '%s'\n", s);
      continue;
    }
    if (f == old_f && sf == old_sf) {
      if (old_nseq == nseq && old_ngroup == ngroup)
        printf("bad PHICH line %d\n", lines);
      same++;
      if (same > 2)
        printf("around line %d, 3 or more PHICH at same time\n", lines);
    } else same = 1;
    old_eNB_ID = eNB_ID; old_f = f; old_sf = sf; old_ue = ue; old_rnti = rnti;
    old_harq = harq; old_NGROUP = NGROUP; old_NSF = NSF; old_ngroup = ngroup;
    old_nseq = nseq; old_ACK = ACK; old_first_rb = first_rb;
    old_n_DMRS = n_DMRS;
  }
  printf("processed %d lines\n", lines);
  return 0;
}
