#ifndef _GOLD_H_
#define _GOLD_H_

#include <stdint.h>

typedef struct {
  unsigned char *x;
  int length;
} bit_vector;

typedef struct {
  bit_vector x1, x2;
} gold;

int gold_c(gold *g, int n);
void init_gold(gold *g, uint32_t c_init);
void free_gold(gold *g);

#endif /* _GOLD_H_ */
