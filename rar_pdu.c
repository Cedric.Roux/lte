#include <stdio.h>
#include <stdlib.h>

int main(void)
{
  unsigned char buf[256];
  int len = 0;
  while (1) {
    int c = getchar();
    if (c == EOF) break;
    if (len == 256) abort();
    buf[len] = c;
    len++;
  }

  /* TODO: more than 1 RAR */
  while (1) {
    int e = (buf[0] >> 7) & 1;
    int t = (buf[0] >> 6) & 1;
    if (e) { printf("more than 1 header in RAR PDU not handled\n"); return 1; }
    if (!t) { printf("backoff not handled\n"); return 1; }
    int rapid = buf[0] & 0x3f;

    int ta = ((buf[1] & 0x7f) << 4) |
             ((buf[2] & 0xf0) >> 4);
    int ul_grant = ((buf[2] & 0x0f) << 16) |
                   ((buf[3] & 0xff) << 8) |
                   ((buf[4] & 0xff));
    int rnti = ((buf[5] & 0xff) << 8) |
               ((buf[6] & 0xff));

    /* UL grant (36.213 6.2) */
    int hop      = (ul_grant >> 19) & 0x01;
    int rb_alloc = (ul_grant >> 9)  & 0x3f;
    int mcs      = (ul_grant >> 5)  & 0x0f;
    int tpc      = (ul_grant >> 2)  & 0x07;
    int ul_delay = (ul_grant >> 1)  & 0x01;
    int csi_req  = (ul_grant >> 1)  & 0x01;

    printf("RAPID                  = %d\n", rapid);
    printf("Timing Advance Command = %d\n", ta);
    printf("UL Grant               = %d\n", ul_grant);
    printf("    Hopping flag  = %d\n", hop);
    printf("    RB assignment = %d\n", rb_alloc);
    printf("    MCS           = %d\n", mcs);
    printf("    TPC for PUSCH = %d\n", tpc);
    printf("    UL delay      = %d\n", ul_delay);
    printf("    CSI request   = %d\n", csi_req);
    printf("Temporary C-RNTI       = %d\n", rnti);

    break;
  }
}
