/* kdf function defined in 33.220, annex B */
/* kdf parameters (fc, p0, ..., pn) defined in 33.401 annex A */

/* gcc -Wall kdf.c -lcrypto */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/hmac.h>
#include <openssl/evp.h>

void usage(void)
{
  printf("gimme, all hex-encoded: key fc p0 ... pn\n");
  printf("\n");
  printf("for kNASenc: fc = 15  p0 = 01  p1 = 00 (for eea0, inc. for others)\n");
  printf("for kNASint: fc = 15  p0 = 02  p1 = 00 (for eia0, inc. for others)\n");
  printf("       for kNASenc, kNASint, key is kASME\n");
  printf("for kRRCenc: fc = 15  p0 = 03  p1 = 00 (for eea0, inc. for others)\n");
  printf("for kRRCint: fc = 15  p0 = 04  p1 = 00 (for eia0, inc. for others)\n");
  printf("for kUPenc:  fc = 15  p0 = 05  p1 = 00 (for eea0, inc. for others)\n");
  printf("for kUPint:  fc = 15  p0 = 06  p1 = 00 (for eia0, inc. for others)\n");
  printf("       for kRRCenc, kRRCint, kUPenc, kUPint, key is kENB\n");
  printf("for kASME:   fc = 10  p0 = SN id  p1 = SQN xor AK\n");
  printf("       SN id = MCC2|MCC1  MNC3|MCC3  MNC2|MNC1\n");
  printf("       (if mnc is 2 digits xy use xyF, ie. MNC3=F)\n");
  printf("       key is CK|IK\n");
  printf("for kENB:    fc = 11  p0 = NAS COUNT (init value of NAS COUNT is 0)\n");
  printf("                           NAS COUNT is 4 bytes, eg. 00000000\n");
  printf("       key is kASME\n");
  printf("for 5G/NR kRRCenc: fc = 69  p0 = 03  p1 = 00 (for nea0, inc. for others)\n");
  printf("for 5G/NR kRRCint: fc = 69  p0 = 04  p1 = 00 (for nia0, inc. for others)\n");
  printf("for 5G/NR kUPenc:  fc = 69  p0 = 05  p1 = 00 (for nea0, inc. for others)\n");
  printf("for 5G/NR kUPint:  fc = 69  p0 = 06  p1 = 00 (for nia0, inc. for others)\n");
  exit(0);
}

int ishex(int v)
{
  return (v >= '0' && v <= '9') ||
         (v >= 'a' && v <= 'f') ||
         (v >= 'A' && v <= 'F');
}

int unhex(int v)
{
  if (v >= '0' && v <= '9') return v - '0';
  if (v >= 'a' && v <= 'f') return v + 10 - 'a';
  return v + 10 - 'A';
}

unsigned char *add(char *_in, unsigned char *out, int *outlen)
{
printf("adding %s\n", _in);
  unsigned char *in = (unsigned char *)_in;
  while (*in) {
    if (!ishex(in[0]) || !ishex(in[1]))
      { printf("bad input %s\n", _in); exit(1); }
    *outlen = *outlen + 1;
    out = realloc(out, *outlen); if (out == NULL) abort();
    out[*outlen-1] = unhex(in[0]) * 16 + unhex(in[1]);
    in += 2;
  }
  return out;
}

int main(int n, char **v)
{
  char *key = NULL;
  char *fc = NULL;
  char *l[n];
  int nl = 0;
  int i;

  unsigned char *k = NULL;
  int klen = 0;
  unsigned char out[256/8];
  unsigned char *data = NULL;
  int datalen = 0;

  for (i = 1; i < n; i++) {
    if (!strcmp(v[i], "-h") || !strcmp(v[i], "--help")) usage();
    if (key == NULL) { key = v[i]; continue; }
    if (fc == NULL) { fc = v[i]; continue; }
    l[nl] = v[i];
    nl++;
  }

  if (key == NULL || fc == NULL || nl == 0) usage();

  k = add(key, k, &klen);

  data = add(fc, data, &datalen);
  for (i = 0; i < nl; i++) {
    char *h = "0123456789abcdef";
    char pp[5];
    int p;
    data = add(l[i], data, &datalen);
    p = strlen(l[i]) / 2;
    if (p > 65535) { printf("too long\n"); exit(1); }
    pp[0] = h[(p >> (3*4)) & 0x0f];
    pp[1] = h[(p >> (2*4)) & 0x0f];
    pp[2] = h[(p >> (1*4)) & 0x0f];
    pp[3] = h[(p >> (0*4)) & 0x0f];
    pp[4] = 0;
    data = add(pp, data, &datalen);
  }

  HMAC(EVP_sha256(), k, klen, data, datalen, out, NULL);

printf("klen %d datalen %d\n", klen, datalen);

  printf("k: ");
  for (i = 0; i < klen; i++) {
    printf("%2.2x", k[i]);
  }
  printf("\n");

  printf("low 128 bits, for 128 bits key: ");
  for (i = 128/8; i < 256/8; i++) {
    printf("%2.2x", out[i]);
  }
  printf("\n");

  for (i = 0; i < 256/8; i++) {
    printf("%2.2x", out[i]);
  }
  printf("\n");

  return 0;
}
