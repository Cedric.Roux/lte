/* gcc -Wall -o opc opc.c -lcrypto */
#include <stdio.h>
#include <openssl/aes.h>
#include <string.h>
#include <stdlib.h>

void unhex(char *in, unsigned char *out)
{
  int i;
  for (i = 0; i < 16; i++) {
    int a1 = in[i*2];
    int a2 = in[i*2+1];
    if (a1 >= '0' && a1 <= '9') a1 -= '0';
    else if (a1 >= 'A' && a1 <= 'F') a1 -= 'A'-10;
    else if (a1 >= 'a' && a1 <= 'f') a1 -= 'a'-10;
    else { printf("bad hex string '%s'\n", in); exit(1); }
    if (a2 >= '0' && a2 <= '9') a2 -= '0';
    else if (a2 >= 'A' && a2 <= 'F') a2 -= 'A'-10;
    else if (a2 >= 'a' && a2 <= 'f') a2 -= 'a'-10;
    else { printf("bad hex string '%s'\n", in); exit(1); }
    out[i] = a1 * 16 + a2;
  }
}

int main(int n, char **v)
{
  unsigned char k[16], op[16], opc[16];
  char *op_in, *k_in;
  AES_KEY key;
  int i;
  char hex[16] = "0123456789abcdef";

  if (n != 3 || strlen(v[1]) != 128/8*2 ||strlen(v[2]) != 128/8*2)
    { printf("gimme <op> <key>\n"); return 1; }

  op_in = v[1];
  k_in  = v[2];

  unhex(op_in, op);
  unhex(k_in, k);

  AES_set_encrypt_key(k, 128, &key);
  AES_encrypt(op, opc, &key);

  printf("AES(op, k):             ");
  for (i = 0; i < 16; i++) printf("%c%c", hex[opc[i]>>4], hex[opc[i]&15]);
  printf("\n");

  for (i = 0; i < 16; i++) opc[i] ^= op[i];

  printf("OPC[=aes(op,k) xor op]: ");
  for (i = 0; i < 16; i++) printf("%c%c", hex[opc[i]>>4], hex[opc[i]&15]);
  printf("\n");

  return 0;
}
