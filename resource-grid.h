#ifndef _RESOURCE_GRID_
#define _RESOURCE_GRID_

#define REFSIG   1
#define PSS      2
#define SSS      3
#define BCH      4
#define PCFICH   5
#define PHICH    6
#define PDCCH    7
#define RESERVED 8

#define GRID(g, ns, l, k) \
    (ns) * (g)->N_RB_DL * (g)->N_sc_RB * (g)->N_symb_DL + \
    (l) * (g)->N_RB_DL * (g)->N_sc_RB + \
    (k)

typedef enum {
  NG_1_6, NG_1_2, NG_1, NG_2
} ng_t;

typedef struct {
  int *grid;

  /* parameters (set them before calling generate_resource_grid) */
  int N_RB_DL;
  int CP;                         /* normal CP = 0, extended CP = 1 */
  int N_ID_cell;                  /* in [0 .. 503] */
  int n_ports;                    /* 1, 2 or 4 */
  int port;                       /* in [0 .. n_ports-1] */
  ng_t ng;
  int phich_duration;             /* normal = 0, extended = 1 */

  /* constants */
  //int N_RB_max_DL = 110;
  int N_sc_RB; //     = 12;

  /* variables (value depends on constants and parameters) */
  int N_symb_DL;
} resource_grid_t;

void generate_resource_grid(resource_grid_t *g);
void dump_grid(resource_grid_t *g);

#endif /* _RESOURCE_GRID_ */
