/* only normal prefix handled */
/* only one antenna port */

#include "pcfich.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <fftw3.h>

#include "gold.h"
#include "utils.h"
#include "resource-grid.h"

static int cfi0[]={0,1,1,0,1,1,0,1,1,0,1,1,0,1,1,0,1,1,0,1,1,0,1,1,0,1,1,0,1,1,0,1};
static int cfi1[]={1,0,1,1,0,1,1,0,1,1,0,1,1,0,1,1,0,1,1,0,1,1,0,1,1,0,1,1,0,1,1,0};
static int cfi2[]={1,1,0,1,1,0,1,1,0,1,1,0,1,1,0,1,1,0,1,1,0,1,1,0,1,1,0,1,1,0,1,1};

int pcfich(int subframe, float *re, resource_grid_t *g)
{
  int i;
  int rec[32];
  gold gd;

  memset(&gd, 0, sizeof(gold));

  /* see 36.211 6.7.1 */
  init_gold(&gd, (subframe + 1) * (2 * g->N_ID_cell + 1) * 512 + g->N_ID_cell);

  int out = 0;
  for (i = 0; i < 4; i++) {
    int ik;
    /* see 36.211 6.7.4 */
    int k_ = (12/2) * (g->N_ID_cell % (2 * g->N_RB_DL));
    int k = k_ + ((i * g->N_RB_DL)/2) * (12/2);
    k = k % (g->N_RB_DL * 12);
    for (ik = 0; ik < 6; ik++) {
      int re_i = k + ik;
      int a, b;
      if (g->grid[GRID(g, subframe*2, 0, re_i)] != PCFICH) continue;
      qpsk(re[(re_i)*2], re[(re_i)*2+1], &a, &b);
      a = a ^ gold_c(&gd, out);
      b = b ^ gold_c(&gd, out+1);
      rec[out] = a;
      rec[out+1] = b;
      out += 2;
      printf(" %d %d", a, b);
    }
  }
  printf("\n");

  int d[3] = { 0, 0, 0 };
  for (i = 0; i < 32; i++) {
    if (cfi0[i] != rec[i]) d[0]++;
    if (cfi1[i] != rec[i]) d[1]++;
    if (cfi2[i] != rec[i]) d[2]++;
  }
  i = 0;
  if (d[1] < d[i]) i = 1;
  if (d[2] < d[i]) i = 2;
  printf("CFI value = %d (errors %d)\n", i, d[i]);

  free_gold(&gd);

  return i;
}

#ifndef NO_MAIN

int main(int n, char **v)
{
  resource_grid_t g;
  char *in_name;
  FILE *fin;
  int16_t *in;
  float *re;
  int div;
  int i;

  /* parameters */
  int subframe = 5;
  int nb_rb = 50;
  int N_cell_id = 0;

  /* constants (may be changed at runtime because of parameters) */
  int cp0 = 160;
  int cp  = 144;
  int fft = 2048;
  int sf_length = 7680 * 4;

  if (n != 4) { printf("gimme nb_rb subframe file\n"); exit(1); }
  nb_rb = atoi(v[1]);
  subframe = atoi(v[2]);
  in_name = v[3];

  switch (nb_rb) {
  default: printf("unhandled # rb %d\n", nb_rb); exit(1);
  case 50: div = 2; break;
  case 25: div = 4; break;
  }

  cp0 /= div;
  cp  /= div;
  fft /= div;
  sf_length /= div;

  in = malloc(sf_length * 4); if (in == NULL) abort();
  re = malloc(14 * nb_rb * 12 * 2 * sizeof(float)); if (re == NULL) abort();

  fin = fopen(in_name, "r"); if (fin == NULL) { perror(in_name); return 1; }
  if (fread(in, sf_length * 4, 1, fin) != 1) abort();
  fclose(fin);

  for (i = 0; i < 14; i++)
    do_symbol(in, i, re + i * nb_rb * 12 * 2, cp0, cp, fft, nb_rb);

  for (i = 0; i < nb_rb * 12; i++)
    printf("symbol 0 re %d %g %g\n", i, re[i*2], re[i*2+1]);

  /* resource grid */
  /* constant */
  g.N_sc_RB        = 12;

  /* parameters */
  g.N_RB_DL        = nb_rb;
  g.CP             = 0;
  g.N_ID_cell      = N_cell_id;
  g.n_ports        = 1;
  g.port           = 0;
  g.ng             = NG_1;
  g.phich_duration = 0;

  generate_resource_grid(&g);

  printf("CFI value %d\n", pcfich(subframe, re, &g));

  return 0;
}

#endif /* NO_MAIN */
