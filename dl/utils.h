#ifndef _UTILS_H_
#define _UTILS_H_

#include <stdint.h>

void do_symbol(int16_t *ins, int l, float *re, int cp0, int cp, int fft, int nb_rb);
void qpsk(float I, float Q, int *a, int *b);
void qam16(float I, float Q, int *a0, int *a1, int *a2, int *a3);
void qam64(float I, float Q, int *a0, int *a1, int *a2, int *a3, int *a4, int *a5);

uint32_t get_bits(unsigned char *buffer, int start, int len);

#endif /* _UTILS_H_ */
