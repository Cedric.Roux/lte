#include "pdcch.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "resource-grid.h"
#include "utils.h"
#include "pcfich.h"
#include "gold.h"
#include "viterbi.h"
#include "crc.h"

/* format 0:
 * - carrier indicator      0 or 3 bits
 * - format 0/format 1a     1 bit (0 = format 0, 1 = format 1a)
 * - frequnecy hoping flag  1 bit
 * - rb assignment          ceil(log2(N_RB_UL * (N_RB_UL + 1) / 2)) bits
 * - mcs                    5 bits
 * - ndi                    1 bit
 * - tpc                    2 bits
 * - cyclic shift           3 bits
 * - ul index (for TDD)     2 bits  (0 bit in FDD)
 * - DAI (for TDD)          2 bits  (0 bit in FDD)
 * - CSI req                1 bit   (2 bits for > 1 DL cell and DCI in UE spec. search space
 * - SRS req                0 bit   (1 bit in some cases)
 * - rat                    1 bit   (if N_BR_UL <= N_RB_DL, 0 bit if not)
 *
 * rb assignment:
 * 6  RBs      5 bits
 * 15 RBs      7 bits
 * 25 RBs      9 bits         (0+1+1+9+5+1+2+3+0+0+1+0+1 = 24 bits = 15+9)
 * 50 RBs      11 bits
 * 75 RBs      12 bits
 * 100 RBs     13 bits
 *
 *-----------------------------------------
 * format 1A
 * - carrier indicator      0 or 3 bits
 * - format 0/format 1a     1 bit (0 = format 0, 1 = format 1a)
 * - local/distrib. VRB flg 1 bit
 * - rb assignment          ceil(log2(N_RB_DL * (N_RB_DL + 1) / 2)) bits
 * - mcs                    5 bits
 * - harq pid               3 bits (fdd) 4 bits (tdd)
 * - ndi                    1 bit
 * - rv                     2 bits
 * - tpc for pucch          2 bits
 * - dai (for tdd)          2 bits
 * - srs req.               0 bit (1 bit in some cases)
 *
 * rb assignment:
 * 6  RBs      5 bits
 * 15 RBs      7 bits
 * 25 RBs      9 bits         (0+1+1+9+5+3+1+2+2+0+0 = 24 bits = 15+9)
 * 50 RBs      11 bits
 * 75 RBs      12 bits
 * 100 RBs     13 bits
 *
 *-----------------------------------------
 * format 1
 * - carrier indicator      0 or 3 bits
 * - resource alloc. header 0 or 1 bit (0 for DL_NB_RB <= 10)
 * - ressource alloc.       ceil(N_RB_DL / P) bits (P is RBG stuff, 2 for 25 RBs)
 * - mcs                    5 bits
 * - harq pid               3 bits (fdd) 4 bits (tdd)
 * - ndi                    1 bit
 * - rv                     2 bits
 * - tpc for pucch          2 bits
 * - dai (for tdd)          2 bits
 *
 * resource alloc.: (P defined in 32.213 table 7.1.6.1-1)
 * 6  RBs      6 bits         P = 1
 * 15 RBs      8 bits         P = 2
 * 25 RBs     13 bits         P = 2
 * 50 RBs     17 bits         P = 3
 * 75 RBs     19 bits         P = 4
 * 100 RBs    25 bits         P = 4
 *
 *-----------------------------------------
 * Table 5.3.3.1.2-1: Ambiguous Sizes of Information Bits.
 * {12, 14, 16 ,20, 24, 26, 32, 40, 44, 56}
 * if format 0 or 1a size is one of these, add 1 bit
 * format 0 and 1a must have same size (kind of, read 36.212 5.3.3.1.3)
 */

typedef struct {
  int l;
  int k[4];
} reg_t;

reg_t reg[100*3*3];

int *interleave(int Nreg)
{
  /* 36.212 5.1.4.2.1 table 5.1.4-2 */
  int P[32] = { 1, 17, 9, 25, 5, 21, 13, 29, 3, 19, 11, 27, 7, 23, 15, 31,
                0, 16, 8, 24, 4, 20, 12, 28, 2, 18, 10, 26, 6, 22, 14, 30 };
  int *ret = calloc(Nreg, sizeof(int)); if (ret == NULL) abort();
  int R = (Nreg + 31) / 32;
  int Nd = R * 32 - Nreg;
  int col, row;
  int start;
  int out = 0;

  int r0[32], r1[32];
  for (int i = 0; i < 32; i++) r0[i] = i-Nd;
  for (int i = 0; i < 32; i++) r1[i] = P[i]-Nd;//r0[P[i]];

  (void)r0; /* shutup gcc */

  for (int i = 0; i < 32; i++) printf(" %d", r1[i]);
  printf("\n");
//  exit(1);
  for (col = 0; col < 32; col++) {
    for (start = -Nd, row = 0; row < R; row++, start+=32) {
      if (start + P[col] >= 0) {
        ret[start + P[col]] = out;
        out++;
      }
    }
  }

  return ret;
}

/* returns number of REGs available for PDCCH */
int get_reg_k_l(reg_t *r, resource_grid_t *g, int L, int ns)
{
  int k;
  int l;
  int m;
  int i;
  int j;

  m = 0;

  for (k = 0; k < g->N_RB_DL * g->N_sc_RB; k++) {
    for (l = 0; l < L; l++) {
      /* is k the start of a PDCCH REG */
      int regsize;
      int free_re;
      int reserved;
      if (k % 4 == 0) {
        regsize = 4;
        free_re = 0;
        for (i = 0; i < 4; i++) {
          if (!g->grid[GRID(g, ns, l, k+i)])
            free_re++;
        }
        if (free_re == 4) goto pdcch_reg;
      }
      if (k % 6 == 0)
      {
        regsize = 6;
        free_re = 0;
        reserved = 0;
        for (i = 0; i < 6; i++) {
          if (!g->grid[GRID(g, ns, l, k+i)])
            free_re++;
          if (g->grid[GRID(g, ns, l, k+i)] == RESERVED
              || g->grid[GRID(g, ns, l, k+i)] == REFSIG)
            reserved++;
        }
        if (free_re == 4 && reserved == 2) goto pdcch_reg;
      }
      /* at this point, k is either not a start of REG or not a free REG */
      continue;
pdcch_reg:
      r[m].l = l;
      j = 0;
      for (i = 0; i < regsize; i++)
        if (!g->grid[GRID(g, ns, l, k+i)]) {
          g->grid[GRID(g, ns, l, k+i)] = PDCCH;
          r[m].k[j] = k+i;
          j++;
        }
      m++;
    }
  }

  return m;
}

int get_dci(dci_t *ret, int cce, int cce_count, int dci_bits, int *pdcch_bits,
            dci_type_t type)
{
  int i;
  int computed_crc;
  int received_crc;
  int rnti;
  int *xb = interleave(dci_bits + 16);
  int dci_coded[(dci_bits + 16) * 3];
  char dci[(dci_bits + 16 + 7) / 8];
  memset(dci, 0, sizeof(dci));
  printf("seek dci at cce %d agg %d\n", cce, cce_count);
  /* check that there is no 2 in the data */
  for (i = 0; i < cce_count * 72; i++) {
    if (pdcch_bits[cce * 72 + i] == 2) return 0;
  }
  /* collect bits */
  for (i = 0; i < (dci_bits + 16) * 3; i++) {
    int bloc = i % 3;
    int bit = i / 3;
    int pos = bloc * (dci_bits + 16) + xb[bit];
    if (pos < cce_count * 72)
      dci_coded[i] = pdcch_bits[cce * 72 + pos];
    else
      dci_coded[i] = -128;
    printf("%d[%d]", dci_coded[i], pos);
  }
  printf("\n");
  int poly[3] = { 0133, 0171, 0165 };
  int nerr = tail_biting_viterbi_decode(dci, dci_coded, (dci_bits + 16) * 3,
                                        7, poly, 3, 2);
  printf("viterbi gives %d errors\n", nerr);
  if (nerr) return 0;
  for (i = (dci_bits + 16) * 3; i < cce_count * 72; i++) {
    if (pdcch_bits[cce * 72 + i] !=
        pdcch_bits[cce * 72 + i - (dci_bits + 16) * 3]) {
      printf("error: bad repetition of bits\n");
      return 0;
    }
  }
  free(xb);

  printf("cce %d agg level %d dci (%d bits + 16 bits crc xored with rnti):",
         cce, cce_count, dci_bits);
  for (i = 0; i < (dci_bits + 16 + 7) / 8; i++) printf(" %2.2x", (unsigned char)dci[i]);
  printf("\n");

  received_crc = get_bits((unsigned char *)dci, dci_bits, 16);
  computed_crc = crc(dci, dci_bits, 0x11021, 16, 0, CRC_NO_COMPLEMENT);
  rnti = received_crc ^ computed_crc;
  printf("received crc %4.4x computed crc %4.4x\n", received_crc, computed_crc);
  printf("rnti %4.4x (%d)\n", rnti, rnti);

  ret->type = type;
  memcpy(ret->dci, dci, (dci_bits + 16 + 7) / 8);
  ret->dci_bits = dci_bits;
  ret->rnti = rnti;
  ret->cce = cce;
  ret->aggregation_level = cce_count;

  return 1;
}

int get_dcis(dci_t *ret, int dci_bits, int cce_count, int *pdcch_bits,
             char *free_cce, dci_type_t type)
{
  int agg_level[] = { 8, 4, 2, 1 };
  int a;
  int agg;
  int cce;
  int dci_count = 0;
  int found;
  int i;

  /* try aggregation level in decreasing order */
  for (a = 0; a < 4; a++) {
    agg = agg_level[a];
    for (cce = 0; cce <= cce_count - agg; cce += agg) {
      /* check that all requested cces are free */
      for (i = 0; i < agg; i++)
        if (!free_cce[cce+i]) break;
      if (i != agg) continue;
      /* try to get dci */
      found = get_dci(ret, cce, agg, dci_bits, pdcch_bits, type);
      if (!found) continue;
      dci_count++;
      ret++;
      /* mark the cces as used */
      for (i = 0; i < agg; i++)
        free_cce[cce+i] = 0;
    }
  }

  return dci_count;
}

static char *type_name(dci_type_t t)
{
  switch (t) {
  case DCI_0_OR_1A: return "DCI_0_OR_1A";
  case DCI_1: return "DCI_1";
  }
  abort();
}

/* L: number of symbols for PDCCH */
dci_t *pdcch(resource_grid_t *g, float *re, int subframe, int L, int *ret_count)
{
  int i;
  int m;
  int ns;
  gold gd;

  /* see 36.211 6.8.2 */
  init_gold(&gd, (subframe << 9) + g->N_ID_cell);

  m = get_reg_k_l(reg, g, L, 0);
  printf("%d REGs for PDCCH\n", m);

  /* a little check - same number of PDCCH REGs for all subframes */
  for (ns = 2; ns < 20; ns += 2)
    if (get_reg_k_l(reg, g, L, ns) != m) abort();

  int *x = interleave(m);
  for (i = 0; i < m; i++) printf(" %d", x[i]);
  printf("\n");

  int pdcch_bits[72*m/9];

  int n_cce = m/9;

  int out = 0;
  int cce;
  for (cce = 0; cce < n_cce; cce++) {
    printf("cce %d\n", cce);
    for (i = 0; i < 9; i++) {
      int m_quad = m;
      int quad = x[cce * 9 + i];
      int re_i;
      /* remove cyclic shift */
      quad = (quad + m_quad * 504 - g->N_ID_cell) % m_quad;
      printf("    reg %d [l %d k %d %d %d %d]\n", i,
             reg[quad].l,
             reg[quad].k[0],
             reg[quad].k[1],
             reg[quad].k[2],
             reg[quad].k[3]);
      for (int j = 0; j < 4; j++) {
        int a, b;
        re_i = reg[quad].l * g->N_RB_DL * 12 + reg[quad].k[j];
        a = b = 2;
        if (fabs(re[re_i*2]) > .5 && fabs(re[re_i*2+1]) > .5) {
          qpsk(re[re_i*2], re[re_i*2+1], &a, &b);
          /* unscramble */
          a ^= gold_c(&gd, out);
          b ^= gold_c(&gd, out+1);
        }
        printf("    bits %d %d", a, b);
        pdcch_bits[out] = a;
        pdcch_bits[out+1] = b;
        out += 2;
      }
      printf("\n");
    }
  }

  int format_0_1a_bits;
  int format_1_bits;
  switch (g->N_RB_DL) {
  default: printf("unhandled # rb %d\n", g->N_RB_DL); exit(1);
  case 25:
    format_0_1a_bits = 25;
    format_1_bits = 27;
    break;
  }
  dci_t d[n_cce];
  int n_dci = 0;
  char free_cce[n_cce];
  for (i = 0; i < n_cce; i++)
    free_cce[i] = 1;
  n_dci += get_dcis(d+n_dci, format_0_1a_bits, n_cce, pdcch_bits, free_cce, DCI_0_OR_1A);
  n_dci += get_dcis(d+n_dci, format_1_bits, n_cce, pdcch_bits, free_cce, DCI_1);

  printf("got %d dci\n", n_dci);

  for (i = 0; i < n_dci; i++) {
    int j;
    printf("dci %s of size %d bits for rnti %4.4x (%d) ",
           type_name(d[i].type), d[i].dci_bits, d[i].rnti, d[i].rnti);
    for (j = 0; j < d[i].dci_bits; j++)
      printf("%d", get_bits((unsigned char *)d[i].dci, j, 1));
    printf(" cce %d aggregation level %d\n", d[i].cce, d[i].aggregation_level);
  }

  free(x);
  free_gold(&gd);

  dci_t *dd = NULL;
  if (n_dci) {
    dd = calloc(n_dci, sizeof(dci_t)); if (dd == NULL) abort();
    memcpy(dd, &d, n_dci * sizeof(dci_t));
  }
  *ret_count = n_dci;
  return dd;
}

#ifndef NO_MAIN

void usage(void)
{
  printf("gimme <input file> <nb rb> <subframe>\n");
  exit(1);
}

int main(int n, char **v)
{
  char *in_name = NULL;
  FILE *fin;
  int nb_rb = -1;
  int subframe = -1;
  resource_grid_t g;
  int i;
  int L;
  int cp0 = 160;
  int cp  = 144;
  int fft = 2048;
  int sf_length = 7680 * 4;
  int div;
  int16_t *in;
  float *re;

  for (i = 1; i < n; i++) {
    if (in_name == NULL) { in_name = v[i]; continue; }
    if (nb_rb == -1) { nb_rb = atoi(v[i]); continue; }
    if (subframe == -1) { subframe = atoi(v[i]); continue; }
    usage();
  }
  if (in_name == NULL || nb_rb == -1
      || subframe < 0 || subframe > 9) usage();
  if (nb_rb != 25) { printf("%d RBs not handled\n", nb_rb); exit(1); }

  /* constant */
  g.N_sc_RB        = 12;

  /* parameters */
  g.N_RB_DL        = nb_rb;
  g.CP             = 0;
  g.N_ID_cell      = 0;
  g.n_ports        = 1;
  g.port           = 0;
  g.ng             = NG_1_6;
  g.phich_duration = 0;

  switch (nb_rb) {
  default: printf("unhandled # rb %d\n", nb_rb); exit(1);
  case 50: div = 2; break;
  case 25: div = 4; break;
  }

  cp0 /= div;
  cp  /= div;
  fft /= div;
  sf_length /= div;

  in = malloc(sf_length * 4); if (in == NULL) abort();
  re = malloc(14 * nb_rb * 12 * 2 * sizeof(float)); if (re == NULL) abort();

  fin = fopen(in_name, "r"); if (fin == NULL) { perror(in_name); return 1; }
  if (fread(in, sf_length * 4, 1, fin) != 1) abort();
  fclose(fin);

  for (i = 0; i < 14; i++)
    do_symbol(in, i, re + i * nb_rb * 12 * 2, cp0, cp, fft, nb_rb);

  for (i = 0; i < nb_rb * 12; i++)
    printf("symbol 0 re %d %g %g\n", i, re[i*2], re[i*2+1]);

  generate_resource_grid(&g);

  /* number of symbols for PDCCH */
  L = pcfich(subframe, re, &g) + 1;
  printf("L %d\n", L);

  int n_dci;
  dci_t *d = pdcch(&g, re, subframe, L, &n_dci);
  free(d);
  printf("%d DCI\n", n_dci);

  return 0;
}

#endif /* NO_MAIN */
