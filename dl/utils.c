#include "utils.h"

#include <stdlib.h>
#include <stdint.h>
#include <fftw3.h>
#include <math.h>

void do_symbol(int16_t *ins, int l, float *re, int cp0, int cp, int fft, int nb_rb)
{
  int i;
  int skip = cp;
  if (l % 7 == 0) skip = cp0;

  for (i = 0; i < l; i++)
    if (i == 0 || i == 7) ins += 2 * (cp0 + fft);
    else                  ins += 2 * (cp + fft);

  fftw_complex *in, *out;
  fftw_plan p;
  in = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * fft);
  out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * fft);
  p = fftw_plan_dft_1d(fft, in, out, FFTW_FORWARD, FFTW_ESTIMATE);

  for (i = 0; i < fft; i++) {
    in[i][0] = ins[(i+skip)*2] / 2048.;
    in[i][1] = ins[(i+skip)*2+1] / 2048.;
  }

  fftw_execute(p);

  for (i = 0; i < nb_rb * 12; i++) {
    int i_fft;
    /* i_fft is the index of RE number i in the FFT array */
    if (i <= (nb_rb*12) / 2 - 1) i_fft = fft + i - (nb_rb*12) / 2;
    else                         i_fft = i - (nb_rb*12) / 2 + 1;
    re[i*2] = out[i_fft][0];
    re[i*2+1] = out[i_fft][1];
  }

  fftw_destroy_plan(p);
  fftw_free(in); fftw_free(out);
}

void qpsk(float I, float Q, int *a, int *b)
{
  if      (I > 0 && Q > 0) { *a = 0; *b = 0; }
  else if (I > 0 && Q < 0) { *a = 0; *b = 1; }
  else if (I < 0 && Q > 0) { *a = 1; *b = 0; }
  else if (I < 0 && Q < 0) { *a = 1; *b = 1; }
  else abort();
}

#define range(x, a0, a1, b0, b1) \
  rint((b0) + ((x) - (a0)) / ((a1) - (a0)) * ((b1) - (b0)))
/*
  a0   b0
  x    y
  a1   b1
(x - a0) / (a1 - a0) = (y - b0) / (b1 - b0)
y = b0 + (x - a0) / (a1 - a0) * (b1 - b0)
*/

void qam16(float I, float Q, int *a0, int *a1, int *a2, int *a3)
{
  int map[4*4][4] = {
/* 36.211 table 7.1.3-1 */
{ 1, 1, 1, 1 },   /* -3 -3 */
{ 1, 1, 0, 1 },   /* -1 -3 */
{ 0, 1, 0, 1 },   /*  1 -3 */
{ 0, 1, 1, 1 },   /*  3 -3 */
{ 1, 1, 1, 0 },   /* -3 -1 */
{ 1, 1, 0, 0 },   /* -1 -1 */
{ 0, 1, 0, 0 },   /*  1 -1 */
{ 0, 1, 1, 0 },   /*  3 -1 */
{ 1, 0, 1, 0 },   /* -3  1 */
{ 1, 0, 0, 0 },   /* -1  1 */
{ 0, 0, 0, 0 },   /*  1  1 */
{ 0, 0, 1, 0 },   /*  3  1 */
{ 1, 0, 1, 1 },   /* -3  3 */
{ 1, 0, 0, 1 },   /* -1  3 */
{ 0, 0, 0, 1 },   /*  1  3 */
{ 0, 0, 1, 1 },   /*  3  3 */
  };

  int i = range(I, -4.4, 4.4, 0, 3);
  int q = range(Q, -4.4, 4.4, 0, 3);

  if (!(i >= 0 && i <= 3 && q >= 0 && q <= 3)) { printf("QAM64: bad IQ, fatal\n"); abort(); }

  *a0 = map[i+q*4][0];
  *a1 = map[i+q*4][1];
  *a2 = map[i+q*4][2];
  *a3 = map[i+q*4][3];
}

void qam64(float I, float Q, int *a0, int *a1, int *a2, int *a3, int *a4, int *a5)
{
  int map[8*8][6] = {
/* 36.211 table 7.1.4-1 */
{ 1, 1, 1, 1, 1, 1 },   /* -7 -7 */
{ 1, 1, 1, 1, 0, 1 },   /* -5 -7 */
{ 1, 1, 0, 1, 0, 1 },   /* -3 -7 */
{ 1, 1, 0, 1, 1, 1 },   /* -1 -7 */
{ 0, 1, 0, 1, 1, 1 },   /*  1 -7 */
{ 0, 1, 0, 1, 0, 1 },   /*  3 -7 */
{ 0, 1, 1, 1, 0, 1 },   /*  5 -7 */
{ 0, 1, 1, 1, 1, 1 },   /*  7 -7 */
{ 1, 1, 1, 1, 1, 0 },   /* -7 -5 */
{ 1, 1, 1, 1, 0, 0 },   /* -5 -5 */
{ 1, 1, 0, 1, 0, 0 },   /* -3 -5 */
{ 1, 1, 0, 1, 1, 0 },   /* -1 -5 */
{ 0, 1, 0, 1, 1, 0 },   /*  1 -5 */
{ 0, 1, 0, 1, 0, 0 },   /*  3 -5 */
{ 0, 1, 1, 1, 0, 0 },   /*  5 -5 */
{ 0, 1, 1, 1, 1, 0 },   /*  7 -5 */
{ 1, 1, 1, 0, 1, 0 },   /* -7 -3 */
{ 1, 1, 1, 0, 0, 0 },   /* -5 -3 */
{ 1, 1, 0, 0, 0, 0 },   /* -3 -3 */
{ 1, 1, 0, 0, 1, 0 },   /* -1 -3 */
{ 0, 1, 0, 0, 1, 0 },   /*  1 -3 */
{ 0, 1, 0, 0, 0, 0 },   /*  3 -3 */
{ 0, 1, 1, 0, 0, 0 },   /*  5 -3 */
{ 0, 1, 1, 0, 1, 0 },   /*  7 -3 */
{ 1, 1, 1, 0, 1, 1 },   /* -7 -1 */
{ 1, 1, 1, 0, 0, 1 },   /* -5 -1 */
{ 1, 1, 0, 0, 0, 1 },   /* -3 -1 */
{ 1, 1, 0, 0, 1, 1 },   /* -1 -1 */
{ 0, 1, 0, 0, 1, 1 },   /*  1 -1 */
{ 0, 1, 0, 0, 0, 1 },   /*  3 -1 */
{ 0, 1, 1, 0, 0, 1 },   /*  5 -1 */
{ 0, 1, 1, 0, 1, 1 },   /*  7 -1 */
{ 1, 0, 1, 0, 1, 1 },   /* -7  1 */
{ 1, 0, 1, 0, 0, 1 },   /* -5  1 */
{ 1, 0, 0, 0, 0, 1 },   /* -3  1 */
{ 1, 0, 0, 0, 1, 1 },   /* -1  1 */
{ 0, 0, 0, 0, 1, 1 },   /*  1  1 */
{ 0, 0, 0, 0, 0, 1 },   /*  3  1 */
{ 0, 0, 1, 0, 0, 1 },   /*  5  1 */
{ 0, 0, 1, 0, 1, 1 },   /*  7  1 */
{ 1, 0, 1, 0, 1, 0 },   /* -7  3 */
{ 1, 0, 1, 0, 0, 0 },   /* -5  3 */
{ 1, 0, 0, 0, 0, 0 },   /* -3  3 */
{ 1, 0, 0, 0, 1, 0 },   /* -1  3 */
{ 0, 0, 0, 0, 1, 0 },   /*  1  3 */
{ 0, 0, 0, 0, 0, 0 },   /*  3  3 */
{ 0, 0, 1, 0, 0, 0 },   /*  5  3 */
{ 0, 0, 1, 0, 1, 0 },   /*  7  3 */
{ 1, 0, 1, 1, 1, 0 },   /* -7  5 */
{ 1, 0, 1, 1, 0, 0 },   /* -5  5 */
{ 1, 0, 0, 1, 0, 0 },   /* -3  5 */
{ 1, 0, 0, 1, 1, 0 },   /* -1  5 */
{ 0, 0, 0, 1, 1, 0 },   /*  1  5 */
{ 0, 0, 0, 1, 0, 0 },   /*  3  5 */
{ 0, 0, 1, 1, 0, 0 },   /*  5  5 */
{ 0, 0, 1, 1, 1, 0 },   /*  7  5 */
{ 1, 0, 1, 1, 1, 1 },   /* -7  7 */
{ 1, 0, 1, 1, 0, 1 },   /* -5  7 */
{ 1, 0, 0, 1, 0, 1 },   /* -3  7 */
{ 1, 0, 0, 1, 1, 1 },   /* -1  7 */
{ 0, 0, 0, 1, 1, 1 },   /*  1  7 */
{ 0, 0, 0, 1, 0, 1 },   /*  3  7 */
{ 0, 0, 1, 1, 0, 1 },   /*  5  7 */
{ 0, 0, 1, 1, 1, 1 },   /*  7  7 */
  };

  int i = range(I, -6.10, 6.10, 0, 7);
  int q = range(Q, -6.10, 6.10, 0, 7);

  if (!(i >= 0 && i <= 7 && q >= 0 && q <= 7)) { printf("QAM64: bad IQ, fatal\n"); abort(); }

  *a0 = map[i+q*8][0];
  *a1 = map[i+q*8][1];
  *a2 = map[i+q*8][2];
  *a3 = map[i+q*8][3];
  *a4 = map[i+q*8][4];
  *a5 = map[i+q*8][5];
printf("IQ iq %g %g  %d %d | %d%d%d%d%d%d\n", I, Q, i, q, *a0, *a1, *a2, *a3, *a4, *a5);
}

uint32_t get_bits(unsigned char *buffer, int start, int len)
{
  int byte, bit;
  byte = start / 8;
  bit = start % 8;
  uint32_t ret = 0;
  while (len) {
    ret <<= 1;
    ret |= (buffer[byte] >> (7 - bit)) & 1;
    bit++;
    if (bit == 8) {
      bit = 0;
      byte++;
    }
    len--;
  }
  return ret;
}
