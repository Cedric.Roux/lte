#ifndef _PDCCH_H_
#define _PDCCH_H_

#include "resource-grid.h"

typedef enum {
  DCI_0_OR_1A, DCI_1
} dci_type_t;

typedef struct {
  dci_type_t type;
  char dci[32/4+2];
  int dci_bits;
  int rnti;
  int cce;
  int aggregation_level;
} dci_t;

dci_t *pdcch(resource_grid_t *g, float *re, int subframe, int L, int *ret_count);

#endif /* _PDCCH_H_ */
