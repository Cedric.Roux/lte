#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <fftw3.h>

int nb_rb = 50;

int cp0 = 160;
int cp  = 144;

int fft = 2048;

int sf_length = 7680 * 4;

void do_symbol(int16_t *ins, int l)
{
  int i;
  int skip = cp;
  if (l % 7 == 0) skip = cp0;
//  printf("l %d skip %d\n", l, skip);

  for (i = 0; i < l; i++)
    if (i == 0 || i == 7) ins += 2 * (cp0 + fft);
    else                  ins += 2 * (cp + fft);

  fftw_complex *in, *out;
  fftw_plan p;
  in = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * fft);
  out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * fft);
  p = fftw_plan_dft_1d(fft, in, out, FFTW_FORWARD, FFTW_ESTIMATE);

  for (i = 0; i < fft; i++) {
    in[i][0] = ins[(i+skip)*2] / 2048.;
    in[i][1] = ins[(i+skip)*2+1] / 2048.;
  }

  fftw_execute(p); /* repeat as needed */

  for (i = 0; i < fft; i++) {
    //printf("%4d: %12g %12g   %g %g\n", i, in[i][0], in[i][1], out[i][0], out[i][1]);
    //printf("%d %g %g\n", i, out[i][0], out[i][1]);
    printf("%g %g\n", out[i][0], out[i][1]);
  }

  fftw_destroy_plan(p);
  fftw_free(in); fftw_free(out);
}

int main(int n, char **v)
{
  char *in_name;
  FILE *fin;
  int16_t *in;
  int div;
  int i;

  if (n != 2) { printf("gimme subframe file\n"); exit(1); }
  in_name = v[1];

  switch (nb_rb) {
  default: printf("unhandled # rb %d\n", nb_rb); exit(1);
  case 50: div = 2; break;
  }

  cp0 /= div;
  cp  /= div;
  fft /= div;
  sf_length /= div;

  in = malloc(sf_length * 4); if (in == NULL) abort();

  fin = fopen(in_name, "r"); if (fin == NULL) { perror(in_name); return 1; }
  if (fread(in, sf_length * 4, 1, fin) != 1) abort();
  fclose(fin);

  for (i = 0; i < 14; i++)
    do_symbol(in, i);

  return 0;
}
