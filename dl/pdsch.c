#include "pdsch.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include "table_36212_5_1_3_3.h"
#include "resource-grid.h"
#include "utils.h"
#include "gold.h"
#include "crc.h"
#include "turbo.h"

#define min(a, b) ((a)<(b)?(a):(b))

int get_nir(int ue_category, int tm_mode, int mdlharq, int max_spatial_layers)
{
  /* see 36.306 version 16.4.0 Table 4.1-1 */
  int nsoft_table[12] = {
    250368, 1237248, 1237248, 1827072, 3667200, 3654144,
    3654144, 35982720, 5481216, 5481216, 7308288, 7308288
  };
  int nir;
  int nsoft;
  int kc;
  int mlimit;
  int kmimo;

  if (ue_category < 1 || ue_category > 12) abort();

  if (tm_mode == 3 || tm_mode == 4 || tm_mode == 8 || tm_mode == 9)
    kmimo = 2;
  else
    kmimo = 1;

  /* 36.212 5.1.4.1.2 */
  nsoft = nsoft_table[ue_category - 1];
  if (nsoft == 35982720) kc = 5;
  else {
    if (max_spatial_layers <= 2)
      kc = 2;
    else
      kc = 1;
  }
  mlimit = 8;
  nir = nsoft / (kc * kmimo * min(mdlharq, mlimit));

  return nir;
}

int compute_Qm(int mcs)
{
  int Qm;

  if (mcs < 10) Qm = 2;
  else if (mcs < 17) Qm = 4;
  else if (mcs < 29) Qm = 6;
  else if (mcs == 29) Qm = 2;
  else if (mcs == 30) Qm = 4;
  else if (mcs == 31) Qm = 6;
  else abort();

  return Qm;
}

int compute_G(int *dl, resource_grid_t *g, int mcs, int subframe)
{
  int i;
  int ret = 0;

  for (i = 0; i < g->N_RB_DL; i++) {
    int ns, k, l;
    if (dl[i] == 0) continue;
    for (ns = 0; ns < 2; ns++)
      for (l = 0; l < g->N_symb_DL; l++)
        for (k = 0; k < g->N_sc_RB; k++)
          if (g->grid[GRID(g, ns + subframe * 2, l, k + i * 12)] == 0)
            ret++;
  }

  return ret * compute_Qm(mcs);
}

void interleave1(int *v, int R_TC, int C_TC, int D)
{
  /* 36.212 table 5.1.4-1 */
  int p[32] = {
    0, 16, 8, 24, 4, 20, 12, 28, 2, 18, 10, 26, 6, 22, 14, 30,
    1, 17, 9, 25, 5, 21, 13, 29, 3, 19, 11, 27, 7, 23, 15, 31
  };
  int *y = calloc(R_TC * C_TC, sizeof(int)); if (y == NULL) abort();
  int ND = R_TC * C_TC - D;
  int k;
  int r, c;

  /* put NULL bits */
  for (k = 0; k < ND; k++)
    y[k] = -1;

  /* then index k of dk */
  for (k = 0; k < D; k++)
    y[ND + k] = k;

  k = 0;
  for (c = 0; c < C_TC; c++)
    for (r = 0; r < R_TC; r++) {
      v[k] = y[p[c] + C_TC * r];
      k++;
    }

  free(y);
}

void interleave2(int *v, int R_TC, int C_TC, int D)
{
  /* 36.212 table 5.1.4-1 */
  int p[32] = {
    0, 16, 8, 24, 4, 20, 12, 28, 2, 18, 10, 26, 6, 22, 14, 30,
    1, 17, 9, 25, 5, 21, 13, 29, 3, 19, 11, 27, 7, 23, 15, 31
  };
  int K_PI = R_TC * C_TC;
  int *y = calloc(R_TC * C_TC, sizeof(int)); if (y == NULL) abort();
  int ND = R_TC * C_TC - D;
  int k;
  int r, c;

  /* put NULL bits */
  for (k = 0; k < ND; k++)
    y[k] = -1;

  /* then index k of dk */
  for (k = 0; k < D; k++)
    y[ND + k] = k;

  k = 0;
  for (c = 0; c < C_TC; c++)
    for (r = 0; r < R_TC; r++) {
      int pi_k = (p[k / R_TC] + C_TC * (k % R_TC) + 1) % K_PI;
      v[k] = y[pi_k];
      k++;
    }

  free(y);
}

/* returns NULL if a failure happened (of any kind)
 * the decoded data if success, allocated with calloc,
 * so don't forget to free it at some point
 *
 * sets also *_tbs
 */
unsigned char *pdsch(int tbs, int mcs, int *dl, float *re, int rv_index,
                     int subframe, int rnti, resource_grid_t *g)
{
  int fail = 0;
  unsigned char *ret = NULL;

  int i;

  printf("mcs %d tbs %d\n", mcs, tbs);

  int *a = calloc(tbs, sizeof(int));
  int *b = calloc(tbs+24, sizeof(int));
  int **c;

  int A = tbs;
  int Z = 6144;
  int B = A + 24;
  int Bp;
  int L;
  int C;
  int Kplus;
  int Kminus;
  int Cplus;
  int Cminus;
  int F;
  int G;
  int Gp;
  int NL;
  int Qm;
  int gamma;
  int r;
  int E;
  int D;
  int R_TC;
  int C_TC = 32;
  int K_PI;
  int ns;
  int l;
  int k;
  int s;
  int Kr;
  int j;
  int k0;
  int Nir;
  int Ncb;
  int *f;
  int b_pos;

  if (B <= Z) {
    L = 0;
    C = 1;
    Bp = B;
  } else {
    L = 24;
    C = (B + Z - L - 1) / (Z - L); /* this is ceil(B / (Z-L)) */
    Bp = B + C * L;
  }

  /* compute K+ and K- */
  for (i = 0; i < sizeof(table_36212_5_1_3_3)/sizeof(table_36212_5_1_3_3[0]); i++)
    if (C * table_36212_5_1_3_3[i].K >= Bp) break;
  if (i == sizeof(table_36212_5_1_3_3)/sizeof(table_36212_5_1_3_3[0])) abort();
  Kplus = table_36212_5_1_3_3[i].K;
  if (C == 1) {
    Cplus = 1;
    Kminus = 0;
    Cminus = 0;
  } else {
    Kminus = table_36212_5_1_3_3[i-1].K;
    Cminus = (C * Kplus - Bp) / (Kplus - Kminus);
    Cplus = C - Cminus;
  }

  F = Cplus * Kplus + Cminus * Kminus - Bp;

  printf("B' %d C %d K+ %d K- %d C+ %d C- %d F %d\n", Bp, C, Kplus, Kminus,
         Cplus, Cminus, F);

  /* allocate c_rk, first C- arrays of size K- then the rest size K+ */
  c = calloc(C, sizeof(int *)); if (c == NULL) abort();
  for (i = 0; i < C; i++) {
    c[i] = calloc(i < Cminus ? Kminus : Kplus, sizeof(int));
    if (c[i] == NULL) abort();
  }

  G = compute_G(dl, g, mcs, subframe);

  NL = 1;   /* number of layers (think: antennas) */
  Qm = compute_Qm(mcs);

  Gp = G / (NL * Qm);

  gamma = Gp % C;

  printf("G %d G' %d gamma %d\n", G, Gp, gamma);

  /* feed f from re */
  f = calloc(G, sizeof(int)); if (f == NULL) abort();
  i = 0;
  for (ns = 0; ns < 2; ns++) {
    for (l = 0; l < g->N_symb_DL; l++) {
      for (k = 0; k < 12 * g->N_RB_DL; k++) {
        int re_i;
        float I, Q;
        int rb = k / 12;
        if (!dl[rb]) continue;
        if (g->grid[GRID(g, subframe * 2 + ns, l, k)]) continue;
        re_i = ns * g->N_RB_DL * 12 * g->N_symb_DL + l * g->N_RB_DL * 12 + k;
        I = re[re_i * 2];
        Q = re[re_i * 2 + 1];
        switch (Qm) {
        case 2: qpsk(I, Q, &f[i], &f[i+1]); break;
        case 4: qam16(I, Q, &f[i], &f[i+1], &f[i+2], &f[i+3]); break;
        case 6: qam64(I, Q, &f[i], &f[i+1], &f[i+2],
                      &f[i+3], &f[i+4], &f[i+5]); break;
        }
        i += Qm;
//printf("IQ %g %g\n", I, Q);
      }
    }
  }
  if (i != G) abort();
  printf("f[%d]: ", G);
  for (i = 0; i < G; i++) printf("%d", f[i]);
  printf("\n");

  /* unscramble f */
  int c_init;
  gold gg;
  c_init = (rnti << 14) + (0 /* q */ << 13) + (subframe << 9) + g->N_ID_cell;
  init_gold(&gg, c_init);
  for (i = 0; i < G; i++) f[i] ^= gold_c(&gg, i);
  free_gold(&gg);
  printf("f unscrambled[%d]: ", G);
  for (i = 0; i < G; i++) printf("%d", f[i]);
  printf("\n");

  /* fill c_rk with index s of bs, that is instead of c_rk = bs, do c_rk = s
   * and also set -1 for filler bits and 'Kr + crc bit' for CRC bits
   */
  for (k = 0; k < F; k++)
    c[0][k] = -1;
  k = F;
  s = 0;
  for (r = 0; r < C; r++) {
    if (r < Cminus) Kr = Kminus;
    else            Kr = Kplus;
    while (k < Kr - L) {
      c[r][k] = s;
      k++;
      s++;
    }
    if (C > 1) {
      while (k < Kr) {
        c[r][k] = Kr + (k + L - Kr);
        k++;
      }
    }
    k = 0;
  }

  int f_start = 0;

  /* not sure if the code is working for F != 0, let's crash for the moment */
  if (F) { printf("TODO: F = %d, not null, check processing\n", F); exit(1); }

  b_pos = 0;

  /* let's process turbo blocks one by one */
  for (r = 0; r < C; r++) {
    int *d[3];
    int di;
    int *v[3];
    int *w;
    int *e;

    if (r < Cminus) Kr = Kminus;
    else            Kr = Kplus;

    D = Kr + 4;

    if (r <= C - gamma - 1)
      E = NL * Qm * Gp / C;
    else
      E = NL * Qm * (Gp + C - 1) / C;   /* this is for ceil(G'/C) */

    R_TC = (D + C_TC - 1) / C_TC;
    K_PI = C_TC * R_TC;

    printf("coded block %d\n", r);
    printf("    D %d r %d E %d R_TC %d K_PI %d\n", D, r, E, R_TC, K_PI);

    d[0] = calloc(D, sizeof(int)); if (d[0] == NULL) abort();
    d[1] = calloc(D, sizeof(int)); if (d[1] == NULL) abort();
    d[2] = calloc(D, sizeof(int)); if (d[2] == NULL) abort();
    /* do dk = k for k in [0..K-1]
     * and dk = '-2 - trellis bit x' for trellis termination, x in [0..3]
     * don't forget the F NULL bits (set as -1) for d0k and d1k when r==0
     */
    for (di = 0; di < 3; di++) {
      for (k = 0; k < Kr; k++)
        if (r == 0 && (di == 0 || di == 1) && k < F)
          d[di][k] = -1;
        else
          d[di][k] = k;
      for (; k < D; k++)
        d[di][k] = -2 - (k - Kr);
    }

    /* vk = -1 for NULL, index k of dk otherwise */
    v[0] = calloc(K_PI, sizeof(int)); if (v[0] == NULL) abort();
    v[1] = calloc(K_PI, sizeof(int)); if (v[1] == NULL) abort();
    v[2] = calloc(K_PI, sizeof(int)); if (v[2] == NULL) abort();

    interleave1(v[0], R_TC, C_TC, D);
    interleave1(v[1], R_TC, C_TC, D);
    interleave2(v[2], R_TC, C_TC, D);

    /* compute w
     * put index k of v as: k for v0, k+K_PI for v1, k+2*K_PI for v2
     */
    w = calloc(3 * K_PI, sizeof(int)); if (w == NULL) abort();
    for (k = 0; k < K_PI; k++) {
      w[k] = k;
      w[K_PI + 2 * k] = K_PI + k;
      w[K_PI + 2 * k + 1] = 2 * K_PI + k;
    }

    /* compute e */
    e = calloc(E, sizeof(int));

    Nir = get_nir(4 /* ue_category */, 1 /* tm_mode */, 8 /* mdlharq */, 1);
    Ncb = min(Nir / C, 3 * K_PI);

    /* (Ncb + 8 * R_TC - 1) / (8 * R_TC) is ceil(Ncb / R_TC) */
    k0 = R_TC * (2 * ((Ncb + 8 * R_TC - 1) / (8 * R_TC)) * rv_index + 2);

    k = 0;
    j = 0;
    /* cb array to check correct rate matching */
    int *cb = calloc(Ncb, sizeof(int)); if (cb == NULL) abort();
    for (i = 0; i < Ncb; i++) cb[i] = -1;
    while (k < E) {
      /* check NULL bits (complex, can be in v, d, or c) */
      int wi = (k0 + j) % Ncb;
      int vi = w[wi] / K_PI;
      int vj = w[wi] % K_PI;
      if (v[vi][vj] == -1) goto null_bit;
      int di = vi;
      int dj = v[vi][vj];
      if (d[di][dj] == -1) goto null_bit;
      /* parity bits are negative, but they are valid */
      if (d[di][dj] < 0) goto not_null_bit;
      int ci = r;
      int cj = d[di][dj];
      if (c[ci][cj] == -1) goto null_bit;
      /* we good */
not_null_bit:
       e[k] = wi;
       /* check that corresponding bit in f is correct (always the same) */
       /* code not tested on real input, may be wrong */
       if (cb[wi] == -1) cb[wi] = f[f_start + k];
       if (cb[wi] != f[f_start + k]) { printf("error bad f[%d]\n", f_start + k); fail = 1; }
       k++;
null_bit:
      j++;
    }
    free(cb);

    /* from this point, d[][] is reused to put actual data into it */
    /* put bits from e (actually f) to d, passing through w and v */
    /* let's initialize to -1 */
    for (k = 0; k < D; k++) {
      d[0][k] = -1;
      d[1][k] = -1;
      d[2][k] = -1;
    }
    for (k = 0; k < E; k++) {
      int wi = e[k];
      int vi = w[wi] / K_PI;
      int vj = w[wi] % K_PI;
      int di = vi;
      int dj = v[vi][vj];
      d[di][dj] = f[f_start + k];
    }
    for (i = 0; i < 3; i++) {
      printf("d[%d][%d]: ", i, D);
      for (k = 0; k < D; k++) if (d[i][k] == -1) printf("."); else printf("%d", d[i][k]);
      printf("\n");
    }

    /* recover missing d0 bits by simulating the turbo coder (this is weird) */
    /* get interleaver parameters (for 2nd encoder, not used for the moment) */
    int tf1 = -1, tf2 = -1;
    for (i = 0; i < sizeof(table_36212_5_1_3_3) / sizeof(table_36212_5_1_3_3[0]); i++) {
      if (table_36212_5_1_3_3[i].K == Kr) {
        tf1 = table_36212_5_1_3_3[i].f1;
        tf2 = table_36212_5_1_3_3[i].f2;
        break;
      }
    }

    int s0 = 0;
    int s1 = 0;
    for (i = 0; i < Kr; i++) {
      int zk0, zk, next;
      /* first encoder */
      if (d[0][i] == -1) {
        if (d[1][i] == -1) {
          printf("both systematic and redundant bit are unkown, don't know what to do\n");
          exit(1);
        }
        /* we don't know the bit, let's take the one generating zk */
        /* compute zk as if d0k is 0 */
        zk0 = (0 + ((s0 >> 2) & 1) + ((s0 >> 1) & 1)) & 1;
        /* computed value is same as received value? if yes then d0k is 0 */
        if (d[1][i] == zk0)
          d[0][i] = 0;
        else
          d[0][i] = 1;
      }
      zk = (d[0][i] + ((s0 >> 2) & 1) + ((s0 >> 1) & 1)) & 1;
      if (d[1][i] != -1 && zk != d[1][i]) {
        printf("fail at i %d zk %d d[1][%d] %d\n", i, zk, i, d[1][i]);
        fail = 1;
      }
      next = (d[0][i] + ((s0 >> 1) & 1) + (s0 & 1)) & 1;
      s0 = (s0 >> 1) | (next << 2);

      /* second encoder (to be done if needed) */
      int ti = (tf1 * i + tf2 * i * i) % Kr;
      (void)ti;
      (void)s1;
    }

    printf("d[0](%d):", i);
    for (k = 0; k < Kr; k+=8) {
      int v = 0;
      for (int b = 0; b < 8; b++) {
        v <<= 1;
        v |= d[0][k+b];
      }
      printf(" %2.2x", v);
    }
    printf("\n");

    /* check CRC for C > 1 */
    if (C > 1) {
      /* get d0 as bytes */
      unsigned char *d_0 = calloc(Kr/8, sizeof(unsigned char)); if (d_0 == NULL) abort();
      for (k = 0; k < Kr; k+=8) {
        int v = 0;
        for (int b = 0; b < 8; b++) {
          v <<= 1;
          v |= d[0][k+b];
        }
        d_0[k/8] = v;
      }
      int computed_crc = crc((char *)d_0, Kr-24, 0x1800063, 24, 0, CRC_NO_COMPLEMENT);
      int received_crc = d_0[Kr/8-3] * 256 * 256 + d_0[Kr/8-2] * 256 + d_0[Kr/8-1];
      printf("c[%d]: crc computed %6.6x received %6.6x\n", r, computed_crc, received_crc);
      if (computed_crc != received_crc) {
        printf("error: bad crc for c[%d]\n", r);
        fail = 1;
      }
      free(d_0);
    }

    /* turbo encode data and check that it's the same as what is received */
    int *td[3];
    td[0] = calloc(D, sizeof(int)); if (d[0] == NULL) abort();
    td[1] = calloc(D, sizeof(int)); if (d[1] == NULL) abort();
    td[2] = calloc(D, sizeof(int)); if (d[2] == NULL) abort();
    turbo_encode(d[0], td, Kr);
    /* check bits */
    for (i = 0; i < D; i++) {
      if (d[0][i] != -1 && td[0][i] != d[0][i]) { printf("error: bit %d different\n", i); fail = 1; }
      if (d[1][i] != -1 && td[1][i] != d[1][i]) { printf("error: zk %d different\n", i); fail = 1; }
      if (d[2][i] != -1 && td[2][i] != d[2][i]) { printf("error: zk' %d different\n", i); fail = 1; }
    }
    free(td[0]);
    free(td[1]);
    free(td[2]);

    /* collect data into b */
    if (r == 0) k = F; else k = 0;
    while (k < Kr - L) {
      b[b_pos] = d[0][k];
      k++;
      b_pos++;
    }

    free(e);
    free(w);
    free(v[0]); free(v[1]); free(v[2]);
    free(d[0]); free(d[1]); free(d[2]);

    f_start += E;
  }

  if (f_start != G) abort();
  if (b_pos != B) abort();

  /* get b as bytes */
  unsigned char *bb = calloc(B/8, sizeof(unsigned char)); if (bb == NULL) abort();
  for (k = 0; k < B; k+=8) {
    int v = 0;
    for (int bi = 0; bi < 8; bi++) {
      v <<= 1;
      v |= b[k+bi];
    }
    bb[k/8] = v;
  }
  int computed_crc = crc((char *)bb, B-24, 0x1864cfb, 24, 0, CRC_NO_COMPLEMENT);
  int received_crc = bb[B/8-3] * 256 * 256 + bb[B/8-2] * 256 + bb[B/8-1];
  printf("b: crc computed %6.6x received %6.6x\n", computed_crc, received_crc);
  if (computed_crc != received_crc) {
    printf("error: bad crc for b\n");
    fail = 1;
  }

  if (!fail) {
    ret = calloc(A/8, sizeof(unsigned char)); if (ret == NULL) abort();
    memcpy(ret, bb, A/8);
  }

  free(bb);

  free(a);
  free(b);
  for (i = 0; i < C; i++) free(c[i]);
  free(c);
  free(f);

  return ret;
}

#ifndef NO_MAIN

void usage(void)
{
  printf("gimme <input file> <nb rb> <subframe> <rnti>\n");
  exit(1);
}

int main(int n, char **v)
{
  resource_grid_t g;
  char *in_name = NULL;
  FILE *fin;
  int nb_rb = -1;
  int subframe = -1;
  int rnti = -1;
  int dl[100];
  int i;
  int mcs;
  int n_rb;
  int16_t *in;
  float *re;
  int cp0 = 160;
  int cp  = 144;
  int fft = 2048;
  int sf_length = 7680 * 4;
  int div;
  unsigned char *data;

  for (i = 1; i < n; i++) {
    if (in_name == NULL) { in_name = v[i]; continue; }
    if (nb_rb == -1) { nb_rb = atoi(v[i]); continue; }
    if (subframe == -1) { subframe = atoi(v[i]); continue; }
    if (rnti == -1) { rnti = atoi(v[i]); continue; }
    usage();
  }
  if (in_name == NULL || nb_rb == -1
      || subframe < 0 || subframe > 9 || rnti == -1) usage();
  if (nb_rb != 25) { printf("%d RBs not handled\n", nb_rb); exit(1); }

  switch (nb_rb) {
  default: printf("unhandled # rb %d\n", nb_rb); exit(1);
  case 50: div = 2; break;
  case 25: div = 4; break;
  }

  cp0 /= div;
  cp  /= div;
  fft /= div;
  sf_length /= div;

  /* constant */
  g.N_sc_RB        = 12;

  /* parameters */
  g.N_RB_DL        = nb_rb;
  g.CP             = 0;
  g.N_ID_cell      = 0;
  g.n_ports        = 1;
  g.port           = 0;
  g.ng             = NG_1_6;
  g.phich_duration = 0;

  generate_resource_grid(&g);

  int pdcch_width = 1;

  /* mark PDCCH */
  for (i = 0; i < nb_rb; i++) {
    int ns, l, k;
    for (ns = 0; ns < 20; ns += 2)
      for (l = 0; l < pdcch_width; l++)
        for (k = 0; k < g.N_sc_RB; k++)
          if (g.grid[GRID(&g, ns, l, k + i * 12)] == 0)
            g.grid[GRID(&g, ns, l, k + i * 12)] = PDCCH;
  }

  dump_grid(&g);

  in = malloc(sf_length * 4); if (in == NULL) abort();
  re = malloc(14 * nb_rb * 12 * 2 * sizeof(float)); if (re == NULL) abort();

  fin = fopen(in_name, "r"); if (fin == NULL) { perror(in_name); return 1; }
  if (fread(in, sf_length * 4, 1, fin) != 1) abort();
  fclose(fin);

  for (i = 0; i < 14; i++)
    do_symbol(in, i, re + i * nb_rb * 12 * 2, cp0, cp, fft, nb_rb);

  for (n_rb = 1; n_rb <= 25; n_rb++) {
    for (i = 0; i < n_rb; i++) dl[i] = 1;
    for (; i < 25; i++) dl[i] = 0;
    for (mcs = 0; mcs <= 28; mcs++) {
if (mcs == 28 && n_rb == 25)
{
(void)dl;
int dd[] = {
1, 1, 1, 1,
1, 1, 1, 1,
1, 1, 1, 1,
1, 1, 1, 1,
1, 1, 1, 1,
1, 1, 1, 1,
1,
};
//memset(dd, 0, sizeof(dd));
//dd[0] = 1;
      int tbs = 18336;
      data = pdsch(tbs, mcs, dd, re, 0 /* rv_index */, subframe, rnti, &g);
      printf("pdsch for tbs %d #rb %d mcs %d\n", tbs, n_rb, mcs);
      if (data == NULL) printf("pdsch decoding failed\n");
      else {
        int b;
        printf("pdsch[%d]:", tbs/8);
        for (b = 0; b < tbs/8; b++) printf(" %2.2x", data[b]);
        printf("\n");
        free(data);
      }
}
    }
  }

  printf("nir %d\n", get_nir(4 /* ue_category */, 1 /* tm_mode */, 8 /* mdlharq */, 1));

  free(g.grid);

  return 0;
}

#endif /* NO_MAIN */
