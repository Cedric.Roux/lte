#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "utils.h"
#include "pcfich.h"
#include "pdcch.h"
#include "pdsch.h"
#include "table_36213_7_1_7_2_1_1.h"

void usage(void)
{
  printf("gimme <nb rb>, and pipe stdin a T textlog trace\n");
  exit(1);
}

int dl(int nb_rb, void *in, int subframe)
{
  resource_grid_t g;
  int i;
  int L;
  int cp0 = 160;
  int cp  = 144;
  int fft = 2048;
  int div;
  float *re;
  int count = 0;
  int success = 0;

  /* 36.213 table 7.1.7.1-1 */
  int m2t[] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 9, 10, 11, 12, 13, 14, 15, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26 };

  if (nb_rb != 25) { printf("%d RBs not handled\n", nb_rb); exit(1); }

  /* constant */
  g.N_sc_RB        = 12;

  /* parameters */
  g.N_RB_DL        = nb_rb;
  g.CP             = 0;
  g.N_ID_cell      = 0;
  g.n_ports        = 1;
  g.port           = 0;
  g.ng             = NG_1_6;
  g.phich_duration = 0;

  switch (nb_rb) {
  default: printf("unhandled # rb %d\n", nb_rb); exit(1);
  case 50: div = 2; break;
  case 25: div = 4; break;
  }

  cp0 /= div;
  cp  /= div;
  fft /= div;

  re = malloc(14 * nb_rb * 12 * 2 * sizeof(float)); if (re == NULL) abort();

  for (i = 0; i < 14; i++)
    do_symbol(in, i, re + i * nb_rb * 12 * 2, cp0, cp, fft, nb_rb);

  for (i = 0; i < nb_rb * 12; i++)
    printf("symbol 0 re %d %g %g\n", i, re[i*2], re[i*2+1]);

  generate_resource_grid(&g);

  /* PCFICH */

  /* number of symbols for PDCCH */
  L = pcfich(subframe, re, &g) + 1;
  printf("L %d\n", L);

  /* PDCCH */

  int n_dci;
  dci_t *d = pdcch(&g, re, subframe, L, &n_dci);
  printf("%d DCI\n", n_dci);

  /* PDSCH */
  int curbit;
#define B(x, l) do { \
    (x) = get_bits(b, curbit, l); \
    curbit += l; \
  } while (0)

  int dl[g.N_RB_DL];
  int n_rbg;
  int ralloc_bits, P;

  switch(g.N_RB_DL) {
  case   6: ralloc_bits =  6; P = 1; break;
  case  15: ralloc_bits =  8; P = 2; break;
  case  25: ralloc_bits = 13; P = 2; break;
  case  50: ralloc_bits = 17; P = 3; break;
  case  75: ralloc_bits = 19; P = 4; break;
  case 100: ralloc_bits = 25; P = 4; break;
  }

  n_rbg = ralloc_bits;

  int rb_assign_bits;

  switch(g.N_RB_DL) {
  case   6: rb_assign_bits =  5; break;
  case  15: rb_assign_bits =  7; break;
  case  25: rb_assign_bits =  9; break;
  case  50: rb_assign_bits = 11; break;
  case  75: rb_assign_bits = 12; break;
  case 100: rb_assign_bits = 13; break;
  }

  for (i = 0; i < n_dci; i++) {
    unsigned char *b = (unsigned char *)d[i].dci;
    int tbs;
    int n_rb;
    switch (d[i].type) {
    case DCI_1: {
      int ralloc;
      int ralloc_header;
      int has_ralloc_header;
      int mcs;
      int harq;
      int ndi;
      int rv;
      int tpc;
      int k;
      int rbg;
      curbit = 0;
      ralloc_header = 0;
      if (g.N_RB_DL > 10) has_ralloc_header = 1; else has_ralloc_header = 0;
      if (has_ralloc_header) B(ralloc_header, 1);
      if (ralloc_header) { printf("alloc type 1 not done\n"); exit(1); }
      B(ralloc, ralloc_bits);
      B(mcs, 5);
      B(harq, 3);
      B(ndi, 1);
      B(rv, 2);
      B(tpc, 2);
      printf("DCI 1 ralloc %x mcs %d harq %d ndi %d rv %d tpc %d\n",
             ralloc, mcs, harq, ndi, rv, tpc);
      memset(dl, 0, sizeof(dl));
      n_rb = 0;
      for (rbg = 0; rbg < n_rbg; rbg++)
        if (ralloc & (1 << (n_rbg - 1 - rbg)))
          for (k = 0; k < P && rbg * P + k < g.N_RB_DL; k++) {
            dl[rbg * P + k] = 1;
            n_rb++;
          }
      tbs = table_36213_7_1_7_2_1_1[n_rb-1][m2t[mcs]];
      unsigned char *data = pdsch(tbs, mcs, dl, re, rv, subframe, d[i].rnti, &g);
      count++;
      if (data != NULL) success++;
      if (data == NULL) { printf("error decoding PDSCH\n"); break; }
      printf("data[%d]", tbs/8);
      for (k = 0; k < tbs/8; k++) printf(" %2.2x", data[k]);
      printf("\n");
      free(data);
      break;
    }
    case DCI_0_OR_1A: {
      int format_1a;
      int distributed_vrb;
      int rb_assign;
      int mcs;
      int harq;
      int ndi;
      int rv;
      int tpc;
      int start, len;
      int k;
      curbit = 0;
      B(format_1a, 1);
      if (format_1a == 0) break;
      B(distributed_vrb, 1);
      B(rb_assign, rb_assign_bits);
      B(mcs, 5);
      B(harq, 3);
      B(ndi, 1);
      B(rv, 2);
      B(tpc, 2);
      printf("DCI 1A rb_assign %x mcs %d harq %d ndi %d rv %d tpc %d\n",
             rb_assign, mcs, harq, ndi, rv, tpc);
      if (distributed_vrb) { printf("error: distributed vrb not supported\n"); exit(1); }
      memset(dl, 0, sizeof(dl));
      start = rb_assign % g.N_RB_DL;
      len   = rb_assign / g.N_RB_DL + 1;
      if (start + len > g.N_RB_DL) {
        start = g.N_RB_DL - 1 - start;
        len   = g.N_RB_DL + 1 - len + 1;
      }
      for (k = start; k < start + len; k++) dl[k] = 1;
      printf("dl[start %d len %d]: ", start, len);
      for (k = 0; k < g.N_RB_DL; k++) { printf("%d", dl[g.N_RB_DL - 1 - k]); if ((g.N_RB_DL - 1 - k) % 4 == 0) printf(" "); }
      printf("\n");
      n_rb = len;
      /* 36.212 5.3.3.1.3: special case for SI-RNTI
       * (we should check RA-RNTI and P-RNTI)
       */
      if (d[i].rnti == 0xffff) {
        if (tpc & 1) n_rb = 3; else n_rb = 2;
      }
      tbs = table_36213_7_1_7_2_1_1[n_rb-1][m2t[mcs]];
      unsigned char *data = pdsch(tbs, mcs, dl, re, rv, subframe, d[i].rnti, &g);
      count++;
      if (data != NULL) success++;
      if (data == NULL) { printf("error decoding PDSCH\n"); break; }
      printf("data[%d]", tbs/8);
      for (k = 0; k < tbs/8; k++) printf(" %2.2x", data[k]);
      printf("\n");
      free(data);
      break;
    }}
  }

  free(d);
  free(re);
  free(g.grid);

  printf("%d PDSCH processed, %d successfully\n", count, success);
  if (count != success) { printf("ERROR: some pdsch failed\n"); return 1; }
  return 0;
}

char *l;
int lsize;
int lmaxsize;

void put(int c)
{
  if (lsize == lmaxsize) {
    lmaxsize += 65536;
    l = realloc(l, lmaxsize);
    if (l == NULL) abort();
  }
  l[lsize] = c;
  lsize++;
}

void get_line(void)
{
  int c;

  lsize = 0;
  while (1) {
    c = getchar();
    if (c == '\n') { put(0); break; }
    if (c == EOF) {
      if (lsize) put(0);
      break;
    }
    put(c);
  }
}

int a2h(char x)
{
  if (x >= '0' && x <= '9') return x - '0';
  if (x >= 'a' && x <= 'f') return x - 'a' + 10;
  if (x >= 'A' && x <= 'F') return x - 'A' + 10;
  abort();
}

int main(int n, char **v)
{
  int nb_rb = -1;
  unsigned char *in;
  int pos;
  int i;
  int div;
  int sf_length = 7680 * 4;
  int subframe;

  for (i = 1; i < n; i++) {
    if (nb_rb == -1) { nb_rb = atoi(v[i]); continue; }
    usage();
  }
  if (nb_rb == -1) usage();
  if (nb_rb != 25) { printf("%d RBs not handled\n", nb_rb); exit(1); }

  switch (nb_rb) {
  default: printf("unhandled # rb %d\n", nb_rb); exit(1);
  case 50: div = 2; break;
  case 25: div = 4; break;
  }

  sf_length /= div;

  in = malloc(sf_length * 4); if (in == NULL) abort();

  while (1) {
    char *sf;
    get_line();
    if (lsize == 0) break;
    if (strstr(l, ": ENB_PHY_OUTPUT_SIGNAL") == NULL) continue;
    sf = strstr(l, "subframe "); if (sf == NULL) abort();
    if (sscanf(sf, "subframe %d", &subframe) != 1) abort();
    pos = 0;
    while (pos != lsize && l[pos] != '[') pos++;
    pos++;
    if (pos >= lsize) abort();
    for (i = 0; i < sf_length * 4; i++) {
      while (pos < lsize && isspace(l[pos])) pos++;
      if (pos > lsize - 2) abort();
      in[i] = a2h(l[pos]) * 16 + a2h(l[pos+1]);
      pos += 2;
    }
    dl(nb_rb, in, subframe);
  }

  return 0;
}
