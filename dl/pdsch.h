#ifndef _PDSCH_H_
#define _PDSCH_H_

#include "resource-grid.h"

unsigned char *pdsch(int tbs, int mcs, int *dl, float *re, int rv_index,
                     int subframe, int rnti, resource_grid_t *g);

#endif /* _PDSCH_H_ */
