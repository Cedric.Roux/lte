#ifndef _VITERBI_H_
#define _VITERBI_H_

int tail_biting_viterbi_decode(char *out, int *m, int len, int k,
                               int *poly, int n_poly, int n_loops);

#endif /* _VITERBI_H_ */
