#include "turbo.h"

#include <stdlib.h>

#include "table_36212_5_1_3_3.h"

void turbo_encode(int *in, int **out, int K)
{
  int i;
  int f1 = -1, f2 = -1;
  int s0, s1;
  int d;
  int d0, d1, d2, zk, next;
  int x[3], xp[3], z[3], zp[3];

  /* get interleaver parameters */
  for (i = 0; i < sizeof(table_36212_5_1_3_3) / sizeof(table_36212_5_1_3_3[0]); i++) {
    if (table_36212_5_1_3_3[i].K == K) {
      f1 = table_36212_5_1_3_3[i].f1;
      f2 = table_36212_5_1_3_3[i].f2;
      break;
    }
  }
  if (f1 == -1) abort();

  s0 = s1 = 0;
  for (i = 0; i < K; i++) {
    /* first turbo */
    d = in[i];
    d0 = s0 & 1;
    d1 = (s0 >> 1) & 1;
    d2 = (s0 >> 2) & 1;
    zk = (d + d1 + d2) & 1;
    next = (d + d0 + d1) & 1;
    s0 = (s0 >> 1) | (next << 2);

    out[0][i] = d;
    out[1][i] = zk;

    /* second turbo */
    d = in[(f1 * i % K + f2 * i % K * i % K) % K];
    d0 = s1 & 1;
    d1 = (s1 >> 1) & 1;
    d2 = (s1 >> 2) & 1;
    zk = (d + d1 + d2) & 1;
    next = (d + d0 + d1) & 1;
    s1 = (s1 >> 1) | (next << 2);

    out[2][i] = zk;
  }

  /* termination */
  for (i = 0; i < 3; i++) {
    /* firt turbo */
    d0 = s0 & 1;
    d1 = (s0 >> 1) & 1;
    d2 = (s0 >> 2) & 1;
    z[i] = (d0 + d2) & 1;
    x[i] = (d0 + d1) & 1;
    s0 = s0 >> 1;

    /* second turbo */
    d0 = s1 & 1;
    d1 = (s1 >> 1) & 1;
    d2 = (s1 >> 2) & 1;
    zp[i] = (d0 + d2) & 1;
    xp[i] = (d0 + d1) & 1;
    s1 = s1 >> 1;
  }

  out[0][K]   = x[0];
  out[0][K+1] = z[1];
  out[0][K+2] = xp[0];
  out[0][K+3] = zp[1];

  out[1][K]   = z[0];
  out[1][K+1] = x[2];
  out[1][K+2] = zp[0];
  out[1][K+3] = xp[2];

  out[2][K]   = x[1];
  out[2][K+1] = z[2];
  out[2][K+2] = xp[1];
  out[2][K+3] = zp[2];
}

#ifndef NO_MAIN

#include <stdio.h>

int main(void)
{
  int c;
  int s0 = 0;
  while (1) {
    c = getchar();
    if (c != '0' && c != '1') break;
    int d = c - '0';
    int zk = (d + ((s0 >> 2) & 1) + (s0 & 1)) & 1;
int d0 = s0 & 1;
int d1 = (s0 >> 1) & 1;
int d2 = (s0 >> 2) & 1;
zk = (d2 + d + d1) & 1;

    int next = (d + d1 + d0) & 1;
    s0 = (s0 >> 1) | (next << 2);
    printf("%d", zk);
  }
  printf("\n");
  return 0;
}

#endif
