#ifndef _PCFICH_H_
#define _PCFICH_H_

#include "resource-grid.h"

int pcfich(int subframe, float *re, resource_grid_t *g);

#endif /* _PCFICH_H_ */
