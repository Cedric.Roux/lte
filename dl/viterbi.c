/* this code is very slow */
#include "viterbi.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static int compute_poly(int poly, int shift, int k)
{
  int v = poly & shift;
  int r = 0;
  while (k) {
    r ^= v;
    v >>= 1;
    k--;
  }
  return r & 1;
}

static int distance(int *expected, int *actual, int n)
{
  int ret = 0;
  int i;

  for (i = 0; i < n; i++)
    if (actual[i] != -128)
      ret += expected[i] != actual[i];

  return ret;
}

/* after reading
 * https://ir.nctu.edu.tw/bitstream/11536/16512/1/000305494800006.pdf
 * "The modified wrap-around Viterbi algorithm for convolutional tail-biting
 * codes" I think this function implements the WAVA (Wrap-Around Viterbi
 * Algorithm) algorithm, but not sure...
 */
/* returns number of errors */
int tail_biting_viterbi_decode(char *out, int *m, int len, int k,
                               int *poly, int n_poly, int n_loops)
{
  int shift = 0;
  int i;
  int s;
  int last_s;
  int start_s;
  int n_bits = len / n_poly;
  int n_states = 1 << (k-1);
  int error[n_bits+1][n_states];
  int prev[n_bits+1][n_states];
  unsigned char decoded[n_bits];
  int expected[n_poly];
  int actual[n_poly];
  int p;
  int next;
  int next_distance;
  int loop;
  int ret;
  int min_error;

  if (n_bits * n_poly != len) { printf("bad input\n"); exit(0); }

  memset(error, -1, sizeof(error));

  for (i = 0; i < n_states; i++)
    error[0][i] = 0;

  loop = 0;
next:
  for (i = 0; i < n_bits; i++) {
    for (p = 0; p < n_poly; p++) {
      int c = *m;
      m++;
      actual[p] = c;
    }

    for (s = 0; s < n_states; s++) {
      if (error[i][s] == -1) continue;

      /* input bit 0 */
      shift = s;
      for (p = 0; p < n_poly; p++)
        expected[p] = compute_poly(poly[p], shift, k);
      next = shift >> 1;
      next_distance = error[i][s] + distance(expected, actual, n_poly);
      if (error[i+1][next] == -1
         || next_distance < error[i+1][next]) {
        error[i+1][next] = next_distance;
        prev[i+1][next] = s;
      }

      /* input bit 1 */
      shift = s | (1 << (k -1));
      for (p = 0; p < n_poly; p++)
        expected[p] = compute_poly(poly[p], shift, k);
      next = shift >> 1;
      next_distance = error[i][s] + distance(expected, actual, n_poly);
      if (error[i+1][next] == -1
         || next_distance < error[i+1][next]) {
        error[i+1][next] = next_distance;
        prev[i+1][next] = s;
      }
    }
  }

  printf("first|last:");
  for (last_s = 0; last_s < n_states; last_s++) {
    s = last_s;
    for (i = n_bits; i > 0; i--) {
      s = prev[i][s];
    }
    printf(" %d|%d", s, last_s);
  }
  printf("\n");

  /* get minimal error */
  s = 0;
  min_error = error[n_bits][s];
  for (i = 1; i < n_states; i++)
    if (error[n_bits][i] < min_error) {
      min_error = error[n_bits][i];
      s = i;
    }

  /* check that first state is the same for all last states with minimal error,
   * that will give us the target state (this is very hypothetical)
   */
  /* first step: get start_s from any min error last state */
  last_s = s;
  for (i = n_bits; i > 0; i--) {
    s = prev[i][s];
  }
  start_s = s;
  /* second step: verify that all min error last state reach this start state */
  for (s = 0; s < n_states; s++) {
    int ss = s;
    if (error[n_bits][s] != min_error) continue;
    for (i = n_bits; i > 0; i--) {
      ss = prev[i][ss];
    }
    if (ss != start_s)
      printf("error(?): last state %d min error %d goes to start state %d instead of %d\n",
             s, min_error, ss, start_s);
  }
  /* third step: if not, I don't know... let's see later if there is
   * something to worry about or not...
   */
  /* fourth step: let's take for last state this start state and decode */
  last_s = start_s;
  if (error[n_bits][last_s] != min_error)
    printf("error: error of last state %d is %d (we want %d)\n", last_s,
           error[n_bits][last_s], min_error);
  s = last_s;
  for (i = n_bits; i > 0; i--) {
    decoded[i-1] = s >> (k - 2);
    s = prev[i][s];
  }

  printf("last error %d (initial s %d last s %d)\n",
         error[n_bits][last_s], s, last_s);
  printf("errors: ");
  for (i = 0; i < n_states; i++)
    printf(" %d", error[n_bits][i]);
  printf("\n");

  ret = error[n_bits][last_s];

  /* if last state is not init state, try again starting with last metric */
  if (last_s != s) {
    loop++;
    for (i = 0; i < n_states; i++)
      error[0][i] = error[n_bits][i];
    if (loop < n_loops)
      goto next;
  }

  /* output what we get, no matter what */
  int out_byte = 0;
  int out_bit = 7;
  for (i = 0; i < n_bits; i++) {
    out[out_byte] |= (decoded[i] << out_bit);
    out_bit--;
    if (out_bit < 0) { out_bit = 7; out_byte++; }
  }

  return ret;
}

#ifndef NO_MAIN

int main(void)
{
  int bits[] = {
01, 01, 00, 00, 01, 00, 00, 01, 01, 00, 00, 01, 01, 01, 00, 01, 01, 01, 01, 01, 01, 01, 00, 01, 00, 00, 01, 01, 01, 00, 00, 01, 00, 01, 00, 01, 00, 00, 00, 01, 00, 01, 01, 00, 00, 00, 01, 01, 01, 00, 00, 00, 00, 01, 01, 01, 01, 01, 00, 01, 00, 01, 01, 01, 01, 01, 00, 00, 00, 01, 01, 01, 00, 01, 01, 00, 00, 00, 01, 00, 01, 00, 00, 01, 01, 01, 00, 00, 01, 00, 01, 00, 01, 00, 00, 00, 01, 00, 01, 01, 00, 00, 01, 00, 00, 00, 00, 00, 01, 00, 01, 01, 01, 00, 01, 00, 01, 01, 00, 01, 00, 01, 01 };
  int in[123];
  char out[123];
  int poly[3] = { 0133, 0171, 0165 };

  int i;

  memset(out, 0, 123);

  for (i = 0; i < 123/3; i++) {
    in[3*i] = bits[i];
    in[3*i+1] = bits[i+123/3];
    in[3*i+2] = bits[i+123/3*2];
  }

  tail_biting_viterbi_decode(out, bits, 123, 7,
                             poly, 3, 10);

  for (i = 0; i < (123/3+7)/8; i++)
    printf(" %2.2x", (unsigned char)out[i]);
  printf("\n");

  return 0;
}

#endif /* NO_MAIN */
