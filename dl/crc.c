#include "crc.h"

/* to be optimized */
int crc(char *_data, int len_bits, int poly, int crcsize, int init_value,
        int do_complement)
{
  unsigned char *data = (unsigned char *)_data;
  int i;
  int j;
  int shift = init_value;
  int ret;
  int bits;

  poly >>= 1;

  for (bits = 0, i = 0; ; i++) {
    if (bits == len_bits) break;
    unsigned char d = data[i];
    for (j = 0; j < 8; j++, d<<=1) {
      if (bits == len_bits) break;
      int b = ((d >> 7) ^ (shift >> (crcsize-1))) & 1;
      shift = ((shift ^ (-b & poly)) << 1) | b;
      bits++;
    }
  }

  ret = shift & ((1 << crcsize) - 1);
  if (do_complement)
    ret = ~ret & ((1 << crcsize) - 1);

  return ret;
}

#ifndef NO_MAIN

#include <stdio.h>

int main(void)
{
  char in[] = { 0x8a, 0xc3, 0x01, 0x00 };
  int c = crc(in, 25, 0x11021, 16, 0, CRC_NO_COMPLEMENT);
  printf("crc = %2.2x\n", c);
  return 0;
}

#endif /* NO_MAIN */
