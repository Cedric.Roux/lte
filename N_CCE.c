/* returns the number of CCEs usable for DCI */
#include <stdio.h>
#include <stdlib.h>

/* n_reg: see 36.211 6.2.4 */
int n_reg(int N_RB_DL, int pdcch_size, int nb_antennas, int cp)
{
  int n = 0;

  /* first OFDM symbol */
  n = 2 * N_RB_DL;

  if (pdcch_size == 1) return n;

  /* second symbol */
  if (nb_antennas <= 2)
    n += 3 * N_RB_DL;
  else
    n += 2 * N_RB_DL;

  if (pdcch_size == 2) return n;

  /* third symbol */
  n += 3 * N_RB_DL;

  if (pdcch_size == 3) return n;

  /* fourth symbol */
  if (cp == 0) /* normal prefix */
    n += 3 * N_RB_DL;
  else
    n += 2 * N_RB_DL;

  return n;
}

/* return number of REGs used by PCFICH: see 36.211 6.7.4 */
int n_pcfich(void)
{
  return 4;
}

typedef enum {
  NG_1_over_6, NG_1_over_2, NG_1, NG_2
} ng_phich_t;


/* returns number of REGs used by PHICH: see 36.211 6.9 and 6.9.3 */
int n_phich(int N_RB_DL, ng_phich_t ng)
{
  /* we only consider normal cyclic prefix because
   * the factor 2 in 6.9 for extended prefix is
   * removed in 6.9.3
   */
  int N_PHICH_group = 0;

  switch (ng) {
  case NG_1_over_6: N_PHICH_group = (N_RB_DL + 8*6-1) / (8 * 6); break;
  case NG_1_over_2: N_PHICH_group = (N_RB_DL + 8*2-1) / (8 * 2); break;
  case NG_1:        N_PHICH_group = (N_RB_DL + 8-1) / 8; break;
  case NG_2:        N_PHICH_group = (2 * N_RB_DL + 8-1) / 8; break;
  }

  return N_PHICH_group * 3;
}

int main(int n, char **v)
{
  int N_RB_DL;
  int ng;
  int nb_antennas;
  int cp;
  int pdcch_size;

  if (n != 6) {
    printf("gimme <N_RB_DL> <ng> <nb antennas> <cp> <pdcch_size>\n");
    printf("    for ng give [0, 1, 2, 3] for [1/6, 1/2, 1, 2]\n");
    printf("    for cp give 0 for normal prefix, 1 for extended\n");
    return 1;
  }

  N_RB_DL     = atoi(v[1]);
  ng          = atoi(v[2]);
  nb_antennas = atoi(v[3]);
  cp          = atoi(v[4]);
  pdcch_size  = atoi(v[5]);

  printf("N_CCE = %d [n_reg = %d]\n",
      (n_reg(N_RB_DL, pdcch_size, nb_antennas, cp)
         -n_phich(N_RB_DL, ng)-n_pcfich())/9,
      n_reg(N_RB_DL, pdcch_size, nb_antennas, cp)
         -n_phich(N_RB_DL, ng)-n_pcfich());

  return 0;
}
