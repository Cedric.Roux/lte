#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <regex.h>

#include "options.h"

static char **name;
static char **value;
static int size;

int option_known(char *x)
{
  int i;
  for (i = 0; i < size; i++) if (!strcmp(x, name[i])) return 1;
  return 0;
}

void parse_options(char *filename)
{
  FILE *f = fopen(filename, "r"); if(f==NULL){perror(filename);exit(1);}
  char *line = NULL;
  regex_t empty, comment,option;
  regmatch_t match[3];
  int l = 0;

  if (regcomp(&empty,   "^ *$", REG_EXTENDED | REG_NEWLINE) ||
      regcomp(&comment, "^ *\\#.*$", REG_EXTENDED | REG_NEWLINE) ||
      regcomp(&option,  "^ *([^ ]+) *= *([^ ]+) *$", REG_EXTENDED | REG_NEWLINE)) {
    printf("error: options: regcomp fails\n");
    exit(1);
  }

  while (1) {
    size_t n;
    ssize_t s;
    if ((s = getline(&line, &n, f)) == -1) break;
    if (s && line[s-1] == '\n') line[s-1] = 0;
    l++;
    if (!regexec(&empty, line, 0, NULL, 0)) continue;
    if (!regexec(&comment, line, 0, NULL, 0)) continue;
    if (!regexec(&option, line, 3, match, 0)) {
      char *m1 = line + match[1].rm_so; line[match[1].rm_eo] = 0;
      char *m2 = line + match[2].rm_so; line[match[2].rm_eo] = 0;
      if (option_known(m1)) {
        printf("error: options: option %s defined multiple times\n", m1);
        exit(1);
      }
      size++;
      name = realloc(name, size * sizeof(char *)); if (name == NULL) abort();
      value = realloc(value, size * sizeof(char *)); if(value==NULL) abort();
      name[size-1] = strdup(m1); if (name[size-1]==NULL) abort();
      value[size-1] = strdup(m2); if (value[size-1]==NULL) abort();
      continue;
    }
    printf("error: options: bad line %d in config file\n", l);
    exit(1);
  }

  regfree(&empty);
  regfree(&comment);
  regfree(&option);

  free(line);

  fclose(f);
}

char *option(char *n)
{
  int i;
  for (i = 0; i < size; i++) if (!strcmp(name[i], n)) return value[i];
  printf("error: options: unknown option %s\n", n);
  exit(1);
}

/*#define TEST*/
#ifdef TEST
int main(int n, char **v)
{
  int i;
  parse_options("test.conf");
  for (i = 1; i < n; i++) printf("'%s'='%s'\n", v[i], option(v[i]));
  return 0;
}
#endif
