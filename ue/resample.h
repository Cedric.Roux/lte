#ifndef _RESAMPLE_H_
#define _RESAMPLE_H_

void resample_for_ue(void *in, void *out, int samples);
void resample_for_enb(void *in, void *out, int samples);

#endif /* _RESAMPLE_H_ */
