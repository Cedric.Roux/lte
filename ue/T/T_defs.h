#ifndef _T_defs_H_
#define _T_defs_H_

/* comment (and recompile everything) to not send time in events */
#define T_SEND_TIME

/* maximum number of arguments for the T macro */
#define T_MAX_ARGS 16

/* maximum size of a message - increase if needed */
#define T_BUFFER_MAX (1024*64)

/* size of the local cache for messages (must be pow(2,something)) */
#define T_CACHE_SIZE (8192 * 2)

/* maximum number of bytes a message can contain */
#ifdef T_SEND_TIME
#  define T_PAYLOAD_MAXSIZE (T_BUFFER_MAX-sizeof(int)-sizeof(struct timespec))
#else
#  define T_PAYLOAD_MAXSIZE (T_BUFFER_MAX-sizeof(int))
#endif

typedef struct {
  volatile int busy;
  char buffer[T_BUFFER_MAX];
  int length;
} T_cache_t;

#define T_SHM_FILENAME "/T_shm_segment"

#endif /* _T_defs_H_ */
