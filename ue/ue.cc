/* this code is C embedded in C++ because RRC library is in C++
 * don't expect clean C++ in there
 */

#include <srslte/srslte.h>
#include <srslte/asn1/liblte_rrc.h>
#include <string.h>

#include "srs/ue.h"

#include "T.h"

extern "C" {

typedef struct {
  int has_sib1;
  int has_sib2;
  LIBLTE_RRC_SYS_INFO_BLOCK_TYPE_1_STRUCT sib1;
  LIBLTE_RRC_SYS_INFO_BLOCK_TYPE_2_STRUCT sib2;
} ue_priv_t;

#include "ue.h"
#include "modem.h"
#include "resample.h"
#include "options.h"
#include "threading.h"

static void *ue_thread(void *_u)
{
  srsue::ue *u = (srsue::ue *)_u;
  srsue::phch_recv *r = &u->phy.sf_recv;

  while (1) {
    acquire_tick(u->id);
    acquire_token();

    T(T_ID((uintptr_t)T_ue0_run+u->id), T_INT(1));
    r->tick((u->ts/u->samples_per_subframe-4) % 10240, u->in, u->out);
    T(T_ID((uintptr_t)T_ue0_run+u->id), T_INT(0));

    release_token();
  }

  return 0;
}

void ue_bcch_dlsch(ue_t *_u, void *in, int nbytes)
{
  ue_priv_t *u = (ue_priv_t *)_u->priv;
  unsigned int i;
  LIBLTE_BIT_MSG_STRUCT msg;
  if (nbytes > 512) { printf("bcch too big\n"); exit(1); }
  srslte_bit_unpack_vector((unsigned char *)in, msg.msg, nbytes*8);
  msg.N_bits = nbytes*8;
  LIBLTE_RRC_BCCH_DLSCH_MSG_STRUCT r;
  liblte_rrc_unpack_bcch_dlsch_msg(&msg, &r);
printf("got %d sibs!\n", r.N_sibs);
  for (i = 0; i < r.N_sibs; i++) {
    if (r.sibs[i].sib_type == LIBLTE_RRC_SYS_INFO_BLOCK_TYPE_1) {
      u->sib1 = r.sibs[i].sib.sib1;
      u->has_sib1 = 1;
    }
    if (r.sibs[i].sib_type == LIBLTE_RRC_SYS_INFO_BLOCK_TYPE_2) {
      u->sib2 = r.sibs[i].sib.sib2;
      u->has_sib2 = 1;
    }
  }
}

int ue_has_sib1(ue_t *_u)
{
  ue_priv_t *u = (ue_priv_t *)_u->priv;
  return u->has_sib1;
}

int ue_has_sib2(ue_t *_u)
{
  ue_priv_t *u = (ue_priv_t *)_u->priv;
  return u->has_sib2;
}

static void init_ue(ue_t *u)
{
  memset(u, 0, sizeof(ue_t));
  u->priv = calloc(1, sizeof(ue_priv_t));

  /* gcc shut up */
  (void)liblte_rrc_dl_pathloss_change_num;
  (void)liblte_rrc_prohibit_phr_timer_num;
  (void)liblte_rrc_periodic_phr_timer_num;
}

ue_t *new_ue(int id, unsigned char *mib)
{
  ue_t *ret = (ue_t *)malloc(sizeof(ue_t)); if (ret == NULL) abort();
  init_ue(ret);

  char s0[128];

  srsue::all_args_t *args = new srsue::all_args_t;

  args->rf.dl_freq = 2680000000;
  args->rf.ul_freq = 2560000000;
  args->rf.rx_gain = -1;
  args->rf.tx_gain = -1;
  args->rf.nof_rx_ant = 1;
  args->rf.device_name = "auto";
  args->rf.device_args = "auto";
  args->rf.time_adv_nsamples = "auto";
  args->rf.burst_preamble = "auto";
  args->pcap.enable = false;
  args->pcap.filename = "ue.pcap";
  args->trace.enable = false;
  args->trace.phy_filename = "ue.phy_trace";
  args->gui.enable = false;
  args->log.phy_level = "info";
  args->log.phy_hex_limit = 32;
  args->log.mac_level = "info";
  args->log.mac_hex_limit = 32;
  args->log.rlc_level = "info";
  args->log.rlc_hex_limit = 32;
  args->log.pdcp_level = "info";
  args->log.pdcp_hex_limit = 32;
  args->log.rrc_level = "info";
  args->log.rrc_hex_limit = 32;
  args->log.gw_level = "info";
  args->log.gw_hex_limit = 32;
  args->log.nas_level = "info";
  args->log.nas_hex_limit = 32;
  args->log.usim_level = "info";
  args->log.usim_hex_limit = 32;

  args->log.all_level = "info";
  args->log.all_hex_limit = 32;

  args->usim.algo= "milenage";

  char n[128];
  sprintf(n, "ue%d.op", id);   args->usim.op      = option(n);
  sprintf(n, "ue%d.amf", id);  args->usim.amf     = option(n);
  sprintf(n, "ue%d.imsi", id); args->usim.imsi    = option(n);
  sprintf(n, "ue%d.imei", id); args->usim.imei    = option(n);
  sprintf(n, "ue%d.k", id);    args->usim.k       = option(n);
  sprintf(n, "ue%d.log", id);  args->log.filename = option(n);

  /* Expert section */
  args->expert.phy.worker_cpu_mask = -1;
  args->expert.phy.sync_cpu_affinity = -1;
  args->expert.ue_cateogry = 4;
  args->expert.metrics_period_secs = 1.0;
  args->expert.pregenerate_signals = false;
  args->expert.phy.rssi_sensor_enabled = true;
  args->expert.phy.prach_gain = -1.0;
  args->expert.phy.cqi_max = 15;
  args->expert.phy.cqi_fixed = -1;
  args->expert.phy.snr_ema_coeff = 0.1;
  args->expert.phy.snr_estim_alg = "refs";
  args->expert.phy.pdsch_max_its = 4;
  args->expert.phy.attach_enable_64qam = false;
  args->expert.phy.nof_phy_threads = 1;
  args->expert.phy.equalizer_mode = "mmse";
  args->expert.phy.cfo_integer_enabled = false;
  args->expert.phy.cfo_correct_tol_hz = 50.0;
  args->expert.phy.time_correct_period = 5;
  args->expert.phy.sfo_correct_disable = false;
  args->expert.phy.sss_algorithm = "full";
  args->expert.phy.estimator_fil_w = 0.1;


  args->rf_cal.tx_corr_dc_gain = 0.0;
  args->rf_cal.tx_corr_dc_phase = 0.0;
  args->rf_cal.tx_corr_iq_i = 0.0;
  args->rf_cal.tx_corr_iq_q = 0.0;

  srsue::ue *u = new srsue::ue(id);

  if (!u->init(args)) {
    printf("ue init error\n");
    exit(1);
  }

  srsue::phch_recv *r = &u->phy.sf_recv;
  //srsue::phch_recv *r2 = NULL;

  r->mib(mib);

  char name[128];
  sprintf(name, "ue %d", id);
  new_thread(ue_thread, u, name);

  ret->priv = u;
  return ret;
}

void ue_tick(ue_t *_u, unsigned long ts, int samples_per_subframe,
    float *in, float *out)
{
  srsue::ue *u = (srsue::ue *)_u->priv;
  u->ts = ts;
  u->samples_per_subframe = samples_per_subframe;
  u->in = in;
  u->out = out;
  release_tick(u->id);
}

/* this function is not absolutely necessary, but if the configuration file
 * does not contain a setting for an UE, it's better to quit early
 */
void ue_check_config_file(int n)
{
  int id;
  char s[128];
  for (id = 0; id < n; id++) {
    sprintf(s, "ue%d.op", id);   option(s);
    sprintf(s, "ue%d.amf", id);  option(s);
    sprintf(s, "ue%d.imsi", id); option(s);
    sprintf(s, "ue%d.imei", id); option(s);
    sprintf(s, "ue%d.k", id);    option(s);
  }
}

} /* extern "C" */
