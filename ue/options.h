#ifndef _OPTIONS_H_
#define _OPTIONS_H_

void parse_options(char *filename);
char *option(char *name);

#endif /* _OPTIONS_H_ */
