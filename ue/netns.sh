ip netns add srs
ip link set tun_srsue netns srs
ip netns exec srs ifconfig tun_srsue 192.172.0.2 up
ip netns exec srs route add default tun_srsue

ip netns add srs0
ip link set tun_srsue_0 netns srs0
ip netns exec srs0 ifconfig tun_srsue_0 192.172.0.2 up
ip netns exec srs0 route add default tun_srsue_0
ip netns exec srs0 ifconfig lo 127.0.0.1 up
