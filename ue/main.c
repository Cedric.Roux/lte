#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <srslte/srslte.h>

#include "modem.h"
#include "modem_queues.h"
#include "resample.h"
#include "ue.h"
#include "options.h"
#include "threading.h"

#include "T.h"

/****************************************************************************/
/* tti timer begin                                                          */
/****************************************************************************/

/* This is used by the RRC layer of SRS when searching for SIBx.
 * The original code uses usleep, but we may not be realtime,
 * so it's better to wait for actual TTI processing to be done
 * by the system, whatever (physical) time it takes.
 * tti_timer_wait is used in srs/upper/rrc.cc, function rrc::sib_search.
 */
#include <pthread.h>

static pthread_mutex_t tti_timer_mutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t tti_timer_cond = PTHREAD_COND_INITIALIZER;
static volatile uint64_t tti_timer_tti;

void tti_timer_tick(void)
{
  if (pthread_mutex_lock(&tti_timer_mutex)) abort();
  tti_timer_tti++;
  if (pthread_cond_broadcast(&tti_timer_cond)) abort();
  if (pthread_mutex_unlock(&tti_timer_mutex)) abort();
}

void tti_timer_wait(int n)
{
  if (pthread_mutex_lock(&tti_timer_mutex)) abort();
  uint64_t cur_tti = tti_timer_tti;
  while (cur_tti + n > tti_timer_tti)
    if (pthread_cond_wait(&tti_timer_cond, &tti_timer_mutex)) abort();
  if (pthread_mutex_unlock(&tti_timer_mutex)) abort();
}

/****************************************************************************/
/* tti timer end                                                            */
/****************************************************************************/


void usage(void)
{
  printf(
"<configuration file> [options]\n"
"options:\n"
"    -s <eNB IP address>\n"
"        connection to this address, default 127.0.0.1\n"
"    -n <number of UEs>\n"
"        run n UEs (default 1)\n"
"    -r <number of RBs>\n"
"        default 25 (5MHz cell)\n"
#if T_TRACER
"    --T_nowait\n"
"        don't wait for a tracer, run immediately\n"
"    --T_dont_fork\n"
"        don't fork (used for debugging with gdb)\n"
"    --T_port <port>\n"
"        default port is 2021\n"
#endif
  );
  exit(0);
}

/* variable incremented each time an UE is ready (ie. has its IP address) */
volatile int ue_ready;

#include <xmmintrin.h>

int main(int n, char **v)
{
  /* get rid of denormals (see http://carlh.net/plugins/denormals.php) */
  /* maybe not useful/necessary? */
  _MM_SET_FLUSH_ZERO_MODE(_MM_FLUSH_ZERO_ON);

  char *server = "127.0.0.1";
  ue_t **u;
  int ue_count = 1;   /* total # UEs we want */
  int ue_cur   = 0;   /* actual # UEs in the system */
  int i, j;
  int rb = 25;
  int samples_per_subframe;
  unsigned long ts = 0;
  char *conffile = NULL;
#if T_TRACER
  int T_wait = 1;
  int T_dont_fork = 0;
  int T_port = 2021;
#endif

  for (i = 1; i < n; i++) {
    if (!strcmp(v[i], "-h") || !strcmp(v[i], "--help")) usage();
    if (!strcmp(v[i], "-s")) { if(i>n-2)usage(); server = v[++i]; continue; }
    if (!strcmp(v[i], "-n")){if(i>n-2)usage();ue_count=atoi(v[++i]);continue;}
    if (!strcmp(v[i], "-r")){if(i>n-2)usage();rb=atoi(v[++i]);continue;}
#if T_TRACER
    if (!strcmp(v[i], "--T_nowait")) { T_wait = 0; continue; }
    if (!strcmp(v[i], "--T_dont_fork")) { T_dont_fork = 1; continue; }
    if (!strcmp(v[i], "--T_port"))
      { if(i>n-2)usage(); T_port = atoi(v[++i]); continue; }
#endif
    if (conffile == NULL) { conffile = v[i]; continue; }
    usage();
  }

#if T_TRACER
  T_init(T_port, T_wait, T_dont_fork);
#endif

  if (conffile == NULL) usage();
  parse_options(conffile);

  if (ue_count <= 0) usage();

  u = malloc(ue_count * sizeof(ue_t *)); if (u == NULL) abort();

  ue_check_config_file(ue_count);

  switch (rb) {
  case 25: samples_per_subframe = 7680; break;
  case 50: samples_per_subframe = 7680*2; break;
  case 100: samples_per_subframe = 7680*4; break;
  default: printf("unsupported # RBs (%d), only 25, 50 or 100\n",rb);exit(1);
  }

  srslte_use_standard_symbol_size(true);

  /* fft stuff */

  srslte_ofdm_t fft;
  cf_t *outfft;
  srslte_cp_t cp = SRSLTE_CP_NORM;

  /* *2 to use it for SIB, not only MIB */
  outfft = malloc(sizeof(cf_t) * SRSLTE_SLOT_LEN(srslte_symbol_sz(rb)) * 2);
  if (!outfft) { perror("malloc"); exit(-1); }
  if (srslte_ofdm_rx_init(&fft, cp, rb))
    { fprintf(stderr, "Error initializing FFT\n"); exit(-1); }
  srslte_dft_plan_set_norm(&fft.fft_plan, true);

  /* pbch stuff */

  uint8_t bch_payload_rx[SRSLTE_BCH_PAYLOAD_LEN];

  srslte_cell_t cell = {
    rb,           // nof_prb
    1,            // nof_ports
    0,            // cell_id
    SRSLTE_CP_NORM,       // cyclic prefix
    SRSLTE_PHICH_R_1,          // PHICH resources      
    SRSLTE_PHICH_NORM    // PHICH length
  };

  srslte_pbch_t pbch;

  int nof_re;
  cf_t *ce[SRSLTE_MAX_PORTS];
  cf_t *slot1_symbols[SRSLTE_MAX_PORTS];
  uint32_t nof_rx_ports = 1;

  nof_re = SRSLTE_SLOT_LEN_RE(cell.nof_prb, SRSLTE_CP_NORM); 

  for (i=0;i<cell.nof_ports;i++) {
    ce[i] = malloc(sizeof(cf_t) * nof_re);
    if (!ce[i]) {
      perror("malloc");
      exit(-1);
    }
    for (j=0;j<nof_re;j++) {
      ce[i][j] = 1;
    }
    slot1_symbols[i] = malloc(sizeof(cf_t) * nof_re);
    if (!slot1_symbols[i]) {
      perror("malloc");
      exit(-1);
    }
  }
  if (srslte_pbch_init(&pbch, cell)) {
    fprintf(stderr, "Error creating PBCH object\n");
    exit(-1);
  }
  srslte_pbch_decode_reset(&pbch);

  int sock = start_modem(server, samples_per_subframe, &ts);

  /* get the MIB */

  int mib_found = 0;
  while (!mib_found) {
    input_buffer b;
    //float out[5760*4*2]; /* max possible size (20MHz) */
    float out[7680*4*2]; /* max possible size (20MHz) */
    from_enb(sock, &b);
    short *x = (short *)b.data;
    for (i = 0; i < b.size / 2; i++) out[i] = x[i] / 32768.;

    //resample_for_ue(b.data, out, b.size/4);

    /* get slot1 */
    float *in = out;
    in += SRSLTE_SLOT_LEN(srslte_symbol_sz(rb))*2;
    for (j = 0; j < SRSLTE_SLOT_LEN(srslte_symbol_sz(rb)); j++)
      outfft[j] = in[j*2] + I * in[j*2+1];
    srslte_ofdm_rx_slot(&fft, outfft, slot1_symbols[0]);

    int sfn_offset;
    if (srslte_pbch_decode(&pbch, slot1_symbols[0], ce, 0, bch_payload_rx, &nof_rx_ports, &sfn_offset) == 1) {
      uint32_t sfn;
      srslte_pbch_mib_unpack(bch_payload_rx, &cell, &sfn);
      printf("decoding done rx port %d sfn %d offset %d\n", nof_rx_ports, sfn, sfn_offset);
      mib_found = 1;
    }

    memset(b.data, 0, samples_per_subframe * 4);
    b.size = samples_per_subframe * 4;
    b.timestamp = ts;
    to_enb(sock, &b);
    ts += samples_per_subframe;
  }

  /* threading init */
  init_tokens(atoi(option("threads.count")));
  init_ticks(ue_count);

  void *qstate = init_queues(sock, ue_count, samples_per_subframe);

  /* main loop */

  while (1) {
    /* if all current UEs are ready, add a new one if we have to */
    if (ue_cur != ue_count && ue_ready == ue_cur) {
      u[ue_cur] = new_ue(ue_cur, bch_payload_rx);
      ue_cur++;
    }

    T(T_from_enb, T_INT(1));
    int insize;
    float *in = dequeue_from_enb(qstate, &insize);
    T(T_from_enb, T_INT(0));

    T(T_from_enb, T_INT(1));
    float **out = enqueue_get_output_buffer(qstate);
    T(T_from_enb, T_INT(0));

    extern void tti_timer_tick();
    tti_timer_tick();

    /* start UEs */
    int k;
    for (k = 0; k < ue_cur; k++)
      ue_tick(u[k], ts, samples_per_subframe, in, out[k]);

    /* wait for UEs */
    wait_tokens(ue_cur);
    reset_tokens();

    dequeue_free_buffer(qstate, in);

    T(T_to_enb, T_INT(1));
    enqueue_to_enb(qstate, out, ts, ue_cur);
    T(T_to_enb, T_INT(0));
    ts += samples_per_subframe;
  }

  return 0;
}
