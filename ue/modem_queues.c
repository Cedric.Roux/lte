#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <pthread.h>

#include "modem_queues.h"
#include "modem.h"
#include "threading.h"

#include "T.h"

#define OUT_LIST_SIZE 32
#define SEND_LIST_SIZE 10
#define IN_LIST_SIZE 10

typedef struct {
  float **out;
  int ue_count;
  uint64_t timestamp;
} send_buffer_t;

typedef struct {
  int sock;
  int max_ue;
  int samples_per_subframe;

  /* the buffers to be given to the UEs to generate signal */
  float **output_buffer[OUT_LIST_SIZE];
  int output_free_size;
  int output_free_start;
  pthread_mutex_t output_mutex;

  /* list of buffers to be sent */
  send_buffer_t send_buffer[SEND_LIST_SIZE];
  volatile int send_start;
  volatile int send_size;
  pthread_mutex_t send_mutex;
  pthread_cond_t send_cond;

  /* input buffers from eNB */
  float in[IN_LIST_SIZE][7680*4*2];  /* enough room for 20MHz */
  volatile int in_start;
  volatile int in_size;
  pthread_mutex_t in_mutex;
  pthread_cond_t in_cond;
} queue_state_t;

//#include <volk/volk.h>

static void *send_thread(void *qstate)
{
  queue_state_t *q = qstate;
  send_buffer_t *out;
  input_buffer x;
  int i, j;

  while (1) {
    /* wait for some buffer */
    if (pthread_mutex_lock(&q->send_mutex)) abort();
    while (q->send_size == 0)
      if (pthread_cond_wait(&q->send_cond, &q->send_mutex)) abort();
    out = &q->send_buffer[q->send_start];
    if (pthread_mutex_unlock(&q->send_mutex)) abort();

    /* use it (without locks) */
    T(T_modem_send, T_INT(1));
    int16_t *b = (int16_t *)x.data;

    /* to be optimized, those loops are very slow (on massive) */
#if 1
    for (j = 1; j < out->ue_count; j++) {
      for (i = 0; i < q->samples_per_subframe * 2; i++) {
        out->out[0][i] += out->out[j][i];
        out->out[j][i] = 0;
      }
    }
    for (i = 0; i < q->samples_per_subframe * 2; i++) {
      if (out->out[0][i] > 32767) b[i] = 32767;
      else if (out->out[0][i] < -32768) b[i] = -32768;
      else b[i] = out->out[0][i];
      out->out[0][i] = 0;
    }
#else
    /* this does not seem to be faster (because of memset loop?) */
    for (j = 1; j < out->ue_count; j++)
      volk_32f_x2_add_32f(out->out[0], out->out[0], out->out[j],
                          q->samples_per_subframe * 2);

    volk_32f_s32f_convert_16i(b, out->out[0], 1, q->samples_per_subframe * 2);

    for (j = 0; j < out->ue_count; j++)
      memset(out->out[j], 0, q->samples_per_subframe * 2 * sizeof(float));
#endif

    x.timestamp = out->timestamp;
    x.size = q->samples_per_subframe * 2 * 2;

    T(T_modem_send, T_INT(0));
    T(T_modem_send, T_INT(1));

    to_enb(q->sock, &x);

    T(T_modem_send, T_INT(0));

    /* release it */
    if (pthread_mutex_lock(&q->send_mutex)) abort();
    q->send_start = (q->send_start+1) % SEND_LIST_SIZE;
    q->send_size--;
    if (pthread_mutex_unlock(&q->send_mutex)) abort();

    if (pthread_mutex_lock(&q->output_mutex)) abort();
    /* put back at head (for better reuse) */
    q->output_free_start=(q->output_free_start+OUT_LIST_SIZE-1)%OUT_LIST_SIZE;
    q->output_free_size++;
    q->output_buffer[q->output_free_start] = out->out;
    if (pthread_mutex_unlock(&q->output_mutex)) abort();
  }

  return 0;
}

static void *receive_thread(void *qstate)
{
  queue_state_t *q = qstate;
  float *f;
  int i;

  while (1) {
    if (pthread_mutex_lock(&q->in_mutex)) abort();
    /* no free buffer is an error */
    if (q->in_size == IN_LIST_SIZE)
      { printf("%s:%d: input full\n", __FILE__, __LINE__); exit(1); }
    int pos = (q->in_start + q->in_size) % IN_LIST_SIZE;
    f = q->in[pos];
    if (pthread_mutex_unlock(&q->in_mutex)) abort();

    /* get data from eNB */
    input_buffer b;
    from_enb(q->sock, &b);

    T(T_modem_recv, T_INT(1));

    /* feed buffer */
    if (b.size != q->samples_per_subframe * 2 * 2)
      { printf("bad input from enb\n"); exit(1); }
    int16_t *x = (int16_t *)b.data;
    for (i = 0; i < b.size / 2; i++) f[i] = x[i] / 32768.;

    T(T_modem_recv, T_INT(0));

    /* signal new buffer */
    if (pthread_mutex_lock(&q->in_mutex)) abort();
    q->in_size++;
    if (pthread_cond_signal(&q->in_cond)) abort();
    if (pthread_mutex_unlock(&q->in_mutex)) abort();
  }

  return 0;
}

void enqueue_to_enb(void *qstate, float **out, uint64_t timestamp,
    int ue_count)
{
  queue_state_t *q = qstate;
  if (pthread_mutex_lock(&q->send_mutex)) abort();

  /* full means error */
  if (q->send_size == SEND_LIST_SIZE) {
    printf("%s:%d: FULL!! increase SEND_LIST_SIZE\n", __FILE__, __LINE__);
    exit(1);
  }

  /* put buffer at the end of the send list */
  int pos = (q->send_start + q->send_size) % SEND_LIST_SIZE;

  q->send_buffer[pos].out       = out;
  q->send_buffer[pos].timestamp = timestamp;
  q->send_buffer[pos].ue_count  = ue_count;

  q->send_size++;

  /* signal a new buffer */
  if (pthread_cond_signal(&q->send_cond)) abort();

  if (pthread_mutex_unlock(&q->send_mutex)) abort();
}

float *dequeue_from_enb(void *qstate, int *size)
{
  queue_state_t *q = qstate;
  float *ret;

  if (pthread_mutex_lock(&q->in_mutex)) abort();
  while (q->in_size == 0)
    if (pthread_cond_wait(&q->in_cond, &q->in_mutex)) abort();

  ret = q->in[q->in_start];
  *size = q->samples_per_subframe * 2 * 4;

  if (pthread_mutex_unlock(&q->in_mutex)) abort();

  return ret;
}

void dequeue_free_buffer(void *qstate, float *f)
{
  queue_state_t *q = qstate;
  if (pthread_mutex_lock(&q->in_mutex)) abort();
  if (f != q->in[q->in_start])
    { printf("%s:%d: fatal\n", __FILE__, __LINE__); exit(1); }
  q->in_start = (q->in_start + 1) % IN_LIST_SIZE;
  q->in_size--;
  if (pthread_mutex_unlock(&q->in_mutex)) abort();
}

float **enqueue_get_output_buffer(void *qstate)
{
  queue_state_t *q = qstate;
  float **ret;

  if (pthread_mutex_lock(&q->output_mutex)) abort();

  /* no free buffer means error */
  if (q->output_free_size == 0)
    {printf("%s:%d: empty out list, possible?\n",__FILE__,__LINE__);exit(1);}

  ret = q->output_buffer[q->output_free_start];
  q->output_free_start = (q->output_free_start + 1) % OUT_LIST_SIZE;
  q->output_free_size--;

  if (pthread_mutex_unlock(&q->output_mutex)) abort();

  return ret;
}

void *init_queues(int sock, int ue_count, int samples_per_subframe)
{
  int i, l;
  queue_state_t *q = calloc(1, sizeof(queue_state_t)); if (q==NULL) abort();

  for (l = 0; l < OUT_LIST_SIZE; l++) {
    q->output_buffer[l] = malloc(ue_count * sizeof(float *));
    if (q->output_buffer[l] == NULL) abort();
    for (i = 0; i < ue_count; i++) {
      q->output_buffer[l][i] = calloc(1, 2*samples_per_subframe * sizeof(float));
      if (q->output_buffer[l][i] == NULL) abort();
    }
  }

  q->sock = sock;
  q->samples_per_subframe = samples_per_subframe;
  q->max_ue = ue_count;

  q->output_free_size = OUT_LIST_SIZE;

  if (pthread_mutex_init(&q->output_mutex, NULL)) abort();

  if (pthread_mutex_init(&q->send_mutex, NULL)) abort();
  if (pthread_cond_init(&q->send_cond, NULL)) abort();

  if (pthread_mutex_init(&q->in_mutex, NULL)) abort();
  if (pthread_cond_init(&q->in_cond, NULL)) abort();

  new_thread(send_thread, q, "modem send");
  new_thread(receive_thread, q, "modem receive");

  return q;
}
