#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#include "threading.h"

/* tokens */

static volatile int free_tokens;
static volatile int consumed_tokens;
static pthread_mutex_t tokens_mutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t tokens_cond = PTHREAD_COND_INITIALIZER;

void acquire_token(void)
{
  if (pthread_mutex_lock(&tokens_mutex)) abort();

  while (free_tokens == 0)
    if (pthread_cond_wait(&tokens_cond, &tokens_mutex)) abort();
  free_tokens--;

  if (pthread_mutex_unlock(&tokens_mutex)) abort();
}

void release_token(void)
{
  if (pthread_mutex_lock(&tokens_mutex)) abort();
  free_tokens++;
  consumed_tokens++;
  if (pthread_cond_broadcast(&tokens_cond)) abort();
  if (pthread_mutex_unlock(&tokens_mutex)) abort();
}

void init_tokens(int number_of_tokens)
{
  free_tokens = number_of_tokens;
  consumed_tokens = 0;
}

void wait_tokens(int number_of_runs)
{
  if (pthread_mutex_lock(&tokens_mutex)) abort();
  while (consumed_tokens != number_of_runs)
    if (pthread_cond_wait(&tokens_cond, &tokens_mutex)) abort();
  if (pthread_mutex_unlock(&tokens_mutex)) abort();
}

void reset_tokens(void)
{
  if (pthread_mutex_lock(&tokens_mutex)) abort();
  consumed_tokens = 0;
//printf("reset_tokens free tokens %d\n", free_tokens);
  if (pthread_mutex_unlock(&tokens_mutex)) abort();
}

/* ticks */

typedef struct {
  volatile int busy;
  pthread_mutex_t mutex;
  pthread_cond_t cond;
} tick_t;

static tick_t *ticks;

void init_ticks(int number_of_ticks)
{
  int i;
  ticks = malloc(number_of_ticks * sizeof(tick_t)); if(ticks==NULL)abort();
  for (i = 0; i < number_of_ticks; i++) {
    if (pthread_mutex_init(&ticks[i].mutex, NULL)) abort();
    if (pthread_cond_init(&ticks[i].cond, NULL)) abort();
    ticks[i].busy = 1;
  }
}

void acquire_tick(int id)
{
  if (pthread_mutex_lock(&ticks[id].mutex)) abort();

  while (ticks[id].busy)
    if (pthread_cond_wait(&ticks[id].cond, &ticks[id].mutex)) abort();

  ticks[id].busy = 1;

  if (pthread_mutex_unlock(&ticks[id].mutex)) abort();
}

void release_tick(int id)
{
  if (pthread_mutex_lock(&ticks[id].mutex)) abort();

  ticks[id].busy = 0;
  if (pthread_cond_signal(&ticks[id].cond)) abort();

  if (pthread_mutex_unlock(&ticks[id].mutex)) abort();
}

/* utils */

void new_thread(void *(*f)(void *), void *data, char *name)
{
  pthread_t t;
  pthread_attr_t att;

  if (pthread_attr_init(&att))
    { fprintf(stderr, "pthread_attr_init err\n"); exit(1); }
  if (pthread_attr_setdetachstate(&att, PTHREAD_CREATE_DETACHED))
    { fprintf(stderr, "pthread_attr_setdetachstate err\n"); exit(1); }
  if (pthread_attr_setstacksize(&att, 10000000))
    { fprintf(stderr, "pthread_attr_setstacksize err\n"); exit(1); }
  if (pthread_create(&t, &att, f, data))
    { fprintf(stderr, "pthread_create err\n"); exit(1); }
  if (pthread_attr_destroy(&att))
    { fprintf(stderr, "pthread_attr_destroy err\n"); exit(1); }

  if (pthread_setname_np(t, name)) abort();
}
