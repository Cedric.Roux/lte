#ifndef _THREADING_H_
#define _THREADING_H_

/***************************************************************************/
/* tokens: used to have a maximum number of UE threads running in parallel */
/***************************************************************************/

/* UE threads call those functions */
void acquire_token(void);
void release_token(void);

/* master thread calls those functions */
void init_tokens(int number_of_tokens);
void wait_tokens(int number_of_runs);
void reset_tokens(void);

/***************************************************************************/
/* ticks: used by one UE thread to wait for TTI tick                       */
/***************************************************************************/

void init_ticks(int number_of_ticks);
void acquire_tick(int id);
void release_tick(int id);

/* utils */
void new_thread(void *(*f)(void *), void *data, char *name);

#endif /* _THREADING_H_ */
