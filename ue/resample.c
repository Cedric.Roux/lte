#include <stdio.h>
#include <stdlib.h>
#include <samplerate.h>

#include "resample.h"

void resample_for_ue(void *_in, void *_out, int samples)
{
  float in[samples*2];
  src_short_to_float_array(_in, in, samples*2);

  SRC_DATA d = (SRC_DATA) {
    data_in: in,
    data_out: _out,
    input_frames: samples,
    output_frames: samples * 3/4,
    end_of_input: 1,
    src_ratio: 3./4.
  };
  //if (src_simple(&d, SRC_ZERO_ORDER_HOLD, 2))
  //if (src_simple(&d, SRC_SINC_FASTEST, 2))
  if (src_simple(&d, SRC_SINC_BEST_QUALITY, 2))
  //if (src_simple(&d, SRC_LINEAR, 2))
    { printf("error: samplerate fails\n"); abort(); }
  if (/*d.input_frames_used != samples ||*/
      d.output_frames_gen != samples * 3/4) {
    printf("error: wanted (in: %d out: %d) but got (in: %ld out: %ld)\n",
           samples, samples * 3/4,
           d.input_frames_used, d.output_frames_gen);
    abort();
  }
}

void resample_for_enb(void *_in, void *_out, int samples)
{
  float out[samples*4/3*2];

  SRC_DATA d = (SRC_DATA) {
    data_in: _in,
    data_out: out,
    input_frames: samples,
    output_frames: samples * 4/3,
    end_of_input: 1,
    src_ratio: 4./3.
  };
  //if (src_simple(&d, SRC_ZERO_ORDER_HOLD, 2))
  //if (src_simple(&d, SRC_SINC_FASTEST, 2))
  if (src_simple(&d, SRC_SINC_BEST_QUALITY, 2))
  //if (src_simple(&d, SRC_LINEAR, 2))
    { printf("error: samplerate fails\n"); abort(); }
  if (/*d.input_frames_used != samples ||*/
      d.output_frames_gen != samples * 4/3) {
    printf("error: wanted (in: %d out: %d) but got (in: %ld out: %ld)\n",
           samples, samples * 3/4,
           d.input_frames_used, d.output_frames_gen);
    abort();
  }

  int i;
  for (i = 0; i < samples*4/3*2; i++) out[i] *= 2048./32768.;

  src_float_to_short_array(out, _out, samples*4/3*2);
}
