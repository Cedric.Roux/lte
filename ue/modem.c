#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "modem.h"

int fullread(int fd, void *_buf, int count)
{
  char *buf = _buf;
  int ret = 0;
  int l;
  while (count) {
    l = read(fd, buf, count);
    if (l <= 0) return -1;
    count -= l;
    buf += l;
    ret += l;
  }
  return ret;
}

int fullwrite(int fd, void *_buf, int count)
{
  char *buf = _buf;
  int ret = 0;
  int l;
  while (count) {
    l = write(fd, buf, count);
    if (l <= 0) return -1;
    count -= l;
    buf += l;
    ret += l;
  }
  return ret;
}

void to_enb(int sock, input_buffer *b)
{
//printf("to_enb ts %ld [%ld]\n", b->timestamp, b->timestamp/7680);
  if (fullwrite(sock, &b->timestamp, sizeof(b->timestamp)) != sizeof(b->timestamp)) goto err;
  if (fullwrite(sock, &b->size, sizeof(b->size)) != sizeof(b->size)) goto err;
  if (fullwrite(sock, b->data, b->size) != b->size) goto err;
//printf("to_enb: press enter\n");
//getchar();
  return;

err:
  printf("to_enb fails\n");
  exit(1);
}

void from_enb(int sock, input_buffer *b)
{
  if (fullread(sock, &b->timestamp, sizeof(b->timestamp)) != sizeof(b->timestamp)) goto err;
  if (fullread(sock, &b->size, sizeof(b->size)) != sizeof(b->size)) goto err;
  if (fullread(sock, b->data, b->size) != b->size) goto err;
//printf("from_enb ts %ld [%ld] size %d\n", b->timestamp, b->timestamp/7680, b->size);
//printf("from_enb: press enter\n");
//getchar();
  return;

err:
  printf("from_enb fails\n");
  exit(1);
}

int start_modem(char *server, int samples_per_subframe, unsigned long *_ts)
{
  int i;
  int ts = 0;
  int port = 4042;
  int sock = socket(AF_INET, SOCK_STREAM, 0);
  if (sock == -1) { perror("socket"); exit(1); }
  struct sockaddr_in addr = {
    sin_family: AF_INET,
    sin_port: htons(port),
    sin_addr: { s_addr: /*INADDR_ANY*/ inet_addr(server) }
  };
  while (connect(sock, (struct sockaddr *)&addr, sizeof(addr))) {
    printf("connection to enb failed, try again in 1s\n");
    sleep(1);
  }
  /* write 10 dummy buffers */
  for (i = 0; i < 10; i++) {
    input_buffer b;
    memset(b.data, 0, samples_per_subframe * 4);
    b.size = samples_per_subframe * 4;
    b.timestamp = ts;
    to_enb(sock, &b);
    ts += samples_per_subframe;
  }
  /* discard all buffers ID <= 5 */
  while (1) {
    input_buffer b;
    from_enb(sock, &b);
    if (b.timestamp == samples_per_subframe * 5) break;
  }
  *_ts = ts;
  return sock;
}
