#ifndef _MODEM_H_
#define _MODEM_H_

#include <stdint.h>

typedef struct {
  int32_t data[30720]; /* max 20MHz */
  unsigned long timestamp;
  int size;
} input_buffer;

void to_enb(int sock, input_buffer *b);
void from_enb(int sock, input_buffer *b);

/* returns a socket and the next timestamp to use for sending */
int start_modem(char *server, int samples_per_subframe, unsigned long *_ts);

#endif /* _MODEM_H_ */
