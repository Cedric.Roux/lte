#ifndef _MODEM_QUEUES_H_
#define _MODEM_QUEUES_H_

#include <stdint.h>

/* call enqueue_get_output_buffer, use the buffer, call enqueue_to_enb
 * (enqueue_to_enb will free it)
 */
float **enqueue_get_output_buffer(void *qstate);
void enqueue_to_enb(void *qstate, float **out, uint64_t timestamp,
    int ue_count);

/* call dequeue_from_enb, use the buffer, call dequeue_free_buffer */
float *dequeue_from_enb(void *qstate, int *size);
void dequeue_free_buffer(void *qstate, float *f);

/* returns a queue state type, as opaque data structure */
void *init_queues(int sock, int ue_count, int samples_per_subframe);

#endif /* _MODEM_QUEUES_H_ */
