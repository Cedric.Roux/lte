#ifndef _UE_H_
#define _UE_H_

typedef struct {
  void *priv;
} ue_t;

void ue_bcch_dlsch(ue_t *u, void *in, int nbytes);

int ue_has_sib1(ue_t *u);
int ue_has_sib2(ue_t *u);

ue_t *new_ue(int id, unsigned char *mib);

void ue_tick(ue_t *u, unsigned long ts, int samples_per_subframe,
    float *in, float *out);

void ue_check_config_file(int n);

#endif /* _UE_H_ */
