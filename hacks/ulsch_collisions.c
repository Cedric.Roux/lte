/* this program parses lines like:
16:39:50.739903911: ENB_PHY_ULSCH_UE_DCI eNB_ID 0 frame 168 subframe 6 UE_id 2 rnti 28132 harq_pid 2 mcs 10 round 0 first_rb 1 nb_rb 6 TBS 1032
 * and checks that no collision occurs for a given TTI
 * 25 PRBs supposed
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <inttypes.h>

int check_rbs(int *rb, int first, int count)
{
  int i;
  for (i = first; i < count; i++) {
    if (rb[i]) return -1;
    rb[i] = 1;
  }
  if (i == 24 || first == 0) return -2;
  return 0;
}

char *l;
int lsize, lmaxsize;

void put(int c)
{
  if (lsize == lmaxsize) {
    lmaxsize += 512;
    l = realloc(l, lmaxsize);
    if (l == NULL) abort();
  }
  l[lsize] = c;
  lsize++;
}

void getl(void)
{
  int c;
  lsize = 0;
  while (1) {
    c = getchar();
    if (c == '\n' || c == EOF) break;
    put(c);
  }
  if (c == EOF && lsize == 0) return;
  put(0);
}

/* count[n] is how many time we have n UEs scheduled
 * in the same subframe
 */
int count[100];

int main(void)
{
  uint64_t h, m, s, ns;
  int frame, subframe;
  int ue_id;
  int rnti;
  int harq_pid;
  int mcs;
  int round;
  int first_rb;
  int nb_rb;
  int TBS;

  int curframe = -1, cursubframe = -1;
  uint64_t t, curt;

  int rb[25];
  memset(rb, 0, 13*sizeof(int));

  int n = 0;

  while (1) {
    getl(); if (lsize == 0) break;
    if (sscanf(l, "%"SCNu64":%"SCNu64":%"SCNu64".%"SCNu64": ENB_PHY_ULSCH_UE_DCI eNB_ID 0 frame %d subframe %d UE_id %d rnti %d harq_pid %d mcs %d round %d first_rb %d nb_rb %d TBS %d",
               &h, &m, &s, &ns, &frame, &subframe, &ue_id, &rnti, &harq_pid, &mcs, &round, &first_rb, &nb_rb, &TBS) != 14) continue;
    t = h * 60 * 60 * 1000000000 + m * 60 * 1000000000 + s * 1000000000 + ns;
    if (frame != curframe || subframe != cursubframe ||
        (frame == curframe && subframe == cursubframe && t - curt > 5 * 1000000)) {
      memset(rb, 0, 25*sizeof(int));
      curt = t;
      curframe = frame;
      cursubframe = subframe;
//printf("new %d.%d\n", frame, subframe);
      if (n >= 10) abort();
      count[n]++;
      n = 0;
    }
    int ret = check_rbs(rb, first_rb, nb_rb);
    if (ret == -1) printf("error\n");
    if (ret == -2) printf("WARN: %s\n", l);
    n++;
  }

  int i;
  for (i = 0; i < 10; i++) printf("%d ", count[i]); printf("\n");

  return 0;
}
