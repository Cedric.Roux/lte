/* this program parses lines like:
16:39:50.755979016: ENB_PHY_DLSCH_UE_DCI eNB_ID 0 frame 170 subframe 2 UE_id 3 rnti 22244 dci_format 1 harq_pid 6 mcs 28 TBS 6712 format 1 dci_pdu 2013763616
 * and checks that no collision occurs for a given TTI
 * the dci pdu is decoded, supposed to be for a 25 PRBs, SISO lte-softmodem
 * basic case
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <inttypes.h>

int check_rbs(int *rb, uint32_t pdu)
{
  /* we suppose no carrier indicator
   * we only support resource allocation type 0
   */
  int i;
  if (pdu & 0x80000000) abort();
  pdu <<= 1;
  for (i = 0; i < 13; i++, pdu <<= 1) {
    int bit = pdu & 0x80000000;
    if (!bit) continue;
    if (rb[i]) return -1;
    rb[i] = 1;
  }
  return 0;
}

char *l;
int lsize, lmaxsize;

void put(int c)
{
  if (lsize == lmaxsize) {
    lmaxsize += 512;
    l = realloc(l, lmaxsize);
    if (l == NULL) abort();
  }
  l[lsize] = c;
  lsize++;
}

void getl(void)
{
  int c;
  lsize = 0;
  while (1) {
    c = getchar();
    if (c == '\n' || c == EOF) break;
    put(c);
  }
  if (c == EOF && lsize == 0) return;
  put(0);
}

/* count[n] is how many time we have n UEs scheduled
 * in the same subframe
 */
int count[100];

int main(void)
{
  uint64_t h, m, s, ns;
  int frame, subframe;
  int ue_id;
  int rnti;
  int harq_pid;
  int mcs;
  int TBS;
  int32_t dci_pdu;

  int curframe = -1, cursubframe = -1;
  uint64_t t, curt;

  int rb[13];  /* not 25 because PRBs are grouped by 2 in 25MHz */
  memset(rb, 0, 13*sizeof(int));

  int n = 0;

  while (1) {
    getl(); if (lsize == 0) break;
    if (sscanf(l, "%"SCNu64":%"SCNu64":%"SCNu64".%"SCNu64": ENB_PHY_DLSCH_UE_DCI eNB_ID 0 frame %d subframe %d UE_id %d rnti %d dci_format 1 harq_pid %d mcs %d TBS %d format 1 dci_pdu %"SCNd32"",
               &h, &m, &s, &ns, &frame, &subframe, &ue_id, &rnti, &harq_pid, &mcs, &TBS, &dci_pdu) != 12) continue;
    t = h * 60 * 60 * 1000000000 + m * 60 * 1000000000 + s * 1000000000 + ns;
    if (frame != curframe || subframe != cursubframe ||
        (frame == curframe && subframe == cursubframe && t - curt > 5 * 1000000)) {
      memset(rb, 0, 13*sizeof(int));
      curt = t;
      curframe = frame;
      cursubframe = subframe;
//printf("new %d.%d\n", frame, subframe);
      if (n >= 10) abort();
      count[n]++;
      n = 0;
    }
    if (check_rbs(rb, dci_pdu) == -1) printf("error\n");
    n++;
  }

  int i;
  for (i = 0; i < 10; i++) printf("%d ", count[i]); printf("\n");

  return 0;
}
