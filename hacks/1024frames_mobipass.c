/* usage: ./1024frames_mobipass < /tmp/LOG.txt 2> /tmp/FRAMES.raw */

#include <stdio.h>
#include <stdlib.h>

#define LEN 256000

char line[LEN];
int size;

void get_line(void)
{
  size = 0;
  while (1) {
    int c = getchar(); if (c == '\n' || c == EOF) break;
    if (size == LEN-1) { printf("line too long\n"); exit(1); }
    line[size] = c;
    size++;
  }
  line[size] = 0;
}

int get_byte(char *line, int pos, int *v)
{
  int a, b;
  while (line[pos] && line[pos] == ' ') pos++; if (!line[pos]) abort();
  a = line[pos] >= '0' && line[pos] <= '9' ? line[pos] - '0' :
      line[pos] >= 'a' && line[pos] <= 'f' ? 10 + line[pos] - 'a' :
      -1;
  pos++; if (!line[pos]) abort();
  b = line[pos] >= '0' && line[pos] <= '9' ? line[pos] - '0' :
      line[pos] >= 'a' && line[pos] <= 'f' ? 10 + line[pos] - 'a' :
      -1;
  pos++; if (!line[pos]) abort();
  if (a == -1 || b == -1) abort();
  *v = (a << 4) | b;
  return pos;
}

int get_short(char *line, int pos, short *v)
{
  int a, b;
  pos = get_byte(line, pos, &a);
  pos = get_byte(line, pos, &b);
  *v = a | (b << 8);
  return pos;
}

int main(void)
{
  unsigned char sf[15360*2];
  int i;
  int cur = 0;
  while (1) {
    int _;
    get_line();
    cur++;
    if (size == 0) { printf("error\n"); return 1; }
    if (sscanf(line,
    "%d:%d:%d.%d: ENB_PHY_INPUT_SIGNAL eNB_ID 0 frame 0 subframe 0 antenna %d",
            &_, &_, &_, &_, &_) == 5) break;
  }
  printf("found fsf 0/0 line %d\n", cur);

  for (i = 0; i < 1024*10; i++) {
    int j;
    int pos = 0;
    if (i%50 == 0) printf("do %d\n", i);
    while (line[pos] && line[pos] != '[') pos++; if (!line[pos]) abort();
    pos++; if (!line[pos]) abort();
    for (j = 0; j < 15360*2; j++) {
      short v;
      pos = get_short(line, pos, &v);
      sf[j] = v >> 0;
    }
    fwrite(sf, 15360*2, 1, stderr);
    get_line(); if (size == 0) abort();
    cur++;
  }

  fflush(stderr);

  /* check that last line is fsf 0/0 */
  do {
    int _;
    if (size == 0) { printf("error\n"); return 1; }
    if (sscanf(line,
    "%d:%d:%d.%d: ENB_PHY_INPUT_SIGNAL eNB_ID 0 frame 0 subframe 0 antenna %d",
              &_, &_, &_, &_, &_) == 5) break;
    printf("error, not fsf 0\n");
    return 1;
  } while (0);
  printf("found fsf 0/0 line %d\n", cur);

  return 0;
}
