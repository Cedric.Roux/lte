/* export PATH=/roux/install/bin:$PATH
 * aarch64-linux-android-gcc -pie -fPIC -fPIE --sysroot=/roux/install/sysroot record.c
 */

/* sudo ./record tun5 192.172.0.2 8001 192.172.0.1 /tmp/ul.raw /tmp/dl.raw */

#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <linux/if_packet.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <linux/if_ether.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>

/* return 1: server -> client
 *        2: client -> server
 *        0: not wanted
 */
int wanted_packet(unsigned char *p, int l,
    uint32_t server_ip, uint16_t server_port,
    uint32_t client_ip)
{
  int direction = -1;  /* 0: server -> client, 1: client -> server */
  uint32_t source_ip, dest_ip;

  if ((p[0] & 0xf0) != 0x40) return 0; /* version = 4 */
  if (p[2]*256+p[3] != l)    return 0; /* length must match */
  if (p[9] != 6)             return 0; /* protocol = 6 */

  /* client -> server or server -> client or unwanted stuff */
  source_ip = ((uint32_t)p[12]<<24) + (p[13]<<16) + (p[14]<<8) + p[15];
  dest_ip   = ((uint32_t)p[16]<<24) + (p[17]<<16) + (p[18]<<8) + p[19];
  if (source_ip == server_ip && dest_ip == client_ip) direction = 0;
  if (source_ip == client_ip && dest_ip == server_ip) direction = 1;
  if (direction == -1) return 0;

  /* server port */
  if (p[20+direction*2]*256+p[21+direction*2] != server_port) return 0;

  /* check IP header length */
  if ((p[0] & 0x0f) != 0x05) {
    int i;
    printf("unhandled IP packet, header length not 5\n");
    for (i = 0; i < l; i++) printf(" %2.2x", p[i]);
    printf("\n"); fflush(stdout);
    exit(1);
  }

  return direction + 1;
}

void usage(void)
{
  printf("usage: [options] <interface name> <server IP> <server port>\n");
  printf("                 <client IP> <server to client filename>\n");
  printf("                 <client to server filename>\n");
  printf("options:\n");
  printf("        -v       be verbose\n");
  exit(1);
}

int main(int n, char **v)
{
  int                 s;
  struct ifconf       t;
  struct ifreq        *r;
  int                 i;
  char                *interface_name = NULL;
  char                *server_ip_name = NULL;
  int                 server_port     = -1;
  char                *client_ip_name = NULL;
  int                 if_id           = -1;
  struct sockaddr_ll  addr;
  int                 verbose         = 0;
  uint32_t            server_ip;
  uint32_t            client_ip;
  FILE                *client_to_server;
  FILE                *server_to_client;
  char                *c2s = NULL;
  char                *s2c = NULL;

  for (i = 1; i < n; i++) {
    if (!strcmp(v[i], "-v")) { verbose = 1; continue; }
    if (interface_name == NULL) { interface_name = v[i]; continue; }
    if (server_ip_name == NULL) { server_ip_name = v[i]; continue; }
    if (server_port == -1)
      { server_port = atoi(v[i]); if (server_port == -1) usage(); continue; }
    if (client_ip_name == NULL) { client_ip_name = v[i]; continue; }
    if (s2c == NULL) { s2c = v[i]; continue; }
    if (c2s == NULL) { c2s = v[i]; continue; }
    usage();
  }

  if (interface_name == NULL || server_ip_name == NULL ||
      server_port == -1 || client_ip_name == NULL ||
      c2s == NULL || s2c == NULL) usage();

  server_to_client = fopen(s2c, "w");
  if (server_to_client == NULL) { perror(s2c); return 1; }
  client_to_server = fopen(c2s, "w");
  if (client_to_server == NULL) { perror(c2s); return 1; }

  server_ip = ntohl(inet_addr(server_ip_name));
  client_ip = ntohl(inet_addr(client_ip_name));

  s = socket(AF_PACKET, SOCK_RAW, ETH_P_IP);
  if (s == -1) { perror("socket"); return 1; }

  t.ifc_req = NULL;
  if (ioctl(s, SIOCGIFCONF, &t) == -1) { perror("ioctl"); return 1; }

  r = malloc(sizeof(struct ifreq) * t.ifc_len);
  if (r == NULL) { perror("malloc"); return 1; }

  t.ifc_req = r;
  if (ioctl(s, SIOCGIFCONF, &t) == -1) { perror("ioctl"); return 1; }

  for (i = 0; i < t.ifc_len / sizeof(struct ifreq); i++) {
    if (verbose) printf("%s %d\n", r[i].ifr_name, r[i].ifr_ifindex);
    if (!strcmp(interface_name, r[i].ifr_name)) if_id = i;
  }

  if (if_id == -1)
    { printf("ERROR: interface %s not found\n", interface_name); return 1; }

  if (ioctl(s, SIOCGIFINDEX, &r[if_id]) == -1)
    { perror("ioctl SIOCGIFINDEX"); return 1; }

  if_id = r[if_id].ifr_ifindex;
  if (verbose) printf("index %d\n", if_id);

  free(r);

  memset(&addr, 0, sizeof(addr));
  addr.sll_family   = AF_PACKET;
  addr.sll_protocol = htons(ETH_P_ALL);
  addr.sll_ifindex  = if_id;

  if (bind(s, (struct sockaddr *)&addr, sizeof(addr)) == -1)
    { perror("bind"); return 1; }

  while (1) {
    unsigned char packet[65536];
    int wanted;
    int l = read(s, packet, 65536);
    if (l == -1) { perror("read"); return 1; }
    if (l == 0) { printf("socket dies?\n"); return 1; }
    if (verbose) {
      printf("packet [%5d]", l);
      for (i = 0; i < l; i++) printf(" %2.2x", packet[i]);
      printf("\n");
    }
    wanted = wanted_packet(packet, l, server_ip, server_port, client_ip);
    if (wanted == 1) {
      fwrite(packet, l, 1, server_to_client);
      fflush(server_to_client);
    } else if (wanted == 2) {
      fwrite(packet, l, 1, client_to_server);
      fflush(client_to_server);
    }
  }

  return 0;
}
