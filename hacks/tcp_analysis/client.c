#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <stdint.h>

int main(void)
{
  int s, s2;
  struct sockaddr_in addr = {
    .sin_family      = AF_INET,
    .sin_port        = htons(8001),
    .sin_addr.s_addr = 0,
  };
  int v;
  uint64_t x = 0, y;

  s = socket(AF_INET, SOCK_STREAM, 0);
  if (s == -1) { perror("socket"); return 1; }
  v = 1;
  if (setsockopt(s, SOL_SOCKET, SO_REUSEADDR, &v, sizeof(v)) == -1)
    { perror("setsockopt"); return -1; }
  if (bind(s, (struct sockaddr *)&addr, sizeof(addr)) == -1)
    { perror("bind"); return -1; }
  if (listen(s, 5) == -1) { perror("listen"); return 1; }

  s2 = accept(s, NULL, NULL);
  if (s2 == -1) { perror("accept"); return 1; }
  close(s);

  while (1) {
    if (read(s2, &y, 8) != 8) { perror("read"); return 1; }
    if (y != x) { printf("bad sequence!\n"); return 1; }
    x++;
  }

  return 0;
}
