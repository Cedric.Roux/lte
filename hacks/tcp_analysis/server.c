#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <stdint.h>
#include <arpa/inet.h>

int main(void)
{
  int s;
  struct sockaddr_in addr = {
    .sin_family      = AF_INET,
    .sin_port        = htons(8001),
    .sin_addr.s_addr = inet_addr("192.172.0.2"),
  };
  uint64_t x = 0;

  s = socket(AF_INET, SOCK_STREAM, 0);
  if (s == -1) { perror("socket"); return 1; }

  if (connect(s, (struct sockaddr *)&addr, sizeof(addr)) == -1)
    { perror("connect"); return 1; }

  while (1) {
    if (write(s, &x, 8) != 8) { perror("write"); return 1; }
    x++;
  }

  return 0;
}
