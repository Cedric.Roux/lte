/* gcc -Wall eia2-128.c -lcrypto */
/* usage: echo <data as hex (include pdcp SN)> | hex2bin | ./eia2-128 <key> */

#include <stdio.h>
#include <openssl/cmac.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>

void compute_t(unsigned char *t, uint32_t count, int bearer, int direction)
{
  t[0] = (count >> 24) & 255;
  t[1] = (count >> 16) & 255;
  t[2] = (count >>  8) & 255;
  t[3] = (count      ) & 255;
  t[4] = (bearer << 3) | (direction << 2);
  memset(&t[5], 0, 8-5);
}

int ishex(int v)
{
  return (v >= '0' && v <= '9') ||
         (v >= 'a' && v <= 'f') ||
         (v >= 'A' && v <= 'F');
}

int unhex(int v)
{
  if (v >= '0' && v <= '9') return v - '0';
  if (v >= 'a' && v <= 'f') return v + 10 - 'a';
  return v + 10 - 'A';
}

unsigned char *add(char *_in, unsigned char *out, int *outlen)
{
  unsigned char *in = (unsigned char *)_in;
  while (*in) {
    if (!ishex(in[0]) || !ishex(in[1]))
      { printf("bad input %s\n", _in); exit(1); }
    *outlen = *outlen + 1;
    out = realloc(out, *outlen); if (out == NULL) abort();
    out[*outlen-1] = unhex(in[0]) * 16 + unhex(in[1]);
    in += 2;
  }
  return out;
}

void usage(void)
{
  printf("gimme: key [bearer] [direction] [count]\n");
  printf("     key:       hex encoded, 16 bytes (i.e. 128 bits)\n");
  printf("     bearer:    RB identity - 1 (eg. for drb 1 pass 0)\n");
  printf("                default: 0\n");
  printf("     direction: 0 for uplink, 1 for downlink\n");
  printf("                default: 0\n");
  printf("     count:     pdcp sequence number (with HFN in front)\n");
  printf("                default: 0\n");
  exit(0);
}

int main(int n, char **v)
{
  unsigned char t[8];
  uint32_t count = 0;
  int bearer     = 0;  /* is "RB identity - 1" (RB identity is drb-Identity in RRCConnectionReconfiguration) */
  int direction  = 0;  /* 0: uplink, 1: downlink */
  char *_k         = NULL;
  unsigned char *k = NULL;
  int klen         = 0;
  int i;
  int o = 0;  /* counter for command line's options */
  CMAC_CTX *ctx;
  unsigned char mact[16];
  size_t mactlen;

  for (i = 1; i < n; i++) {
    if (!strcmp(v[i], "-h") || !strcmp(v[i], "--help")) usage();
    if (_k == NULL) { _k = v[i]; continue; }
    if (o == 0) { o++; bearer    = atoi(v[i]); continue; }
    if (o == 1) { o++; direction = atoi(v[i]); continue; }
    if (o == 2) { o++; count     = atoi(v[i]); continue; }
    usage();
  }

  if (_k == NULL) usage();

  k = add(_k, k, &klen);

  if (klen != 16) usage();

  compute_t(t, count, bearer, direction);

  ctx = CMAC_CTX_new(); if (ctx == NULL) abort();

  CMAC_Init(ctx, k, 16, EVP_aes_128_cbc(), NULL);

  CMAC_Update(ctx, t, 8);

  while (1) {
    int c = getchar();
    unsigned char cc = c;
    if (c == EOF) break;
    CMAC_Update(ctx, &cc, 1);
  }

  CMAC_Final(ctx, mact, &mactlen);

  printf("mac[%d] = ", (int)mactlen);
  for (i = 0; i < mactlen; i++) printf("%2.2x ", mact[i]);
  printf("\n");

  return 0;
}
