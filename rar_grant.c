#include <stdio.h>
#include <stdlib.h>

int main(int n, char **v)
{
  int grant;
  if (n != 2) { printf("gimme a grant (hex number)\n"); return 1; }
  if (sscanf(v[1], "%x", &grant) != 1) abort();
  printf("I got grant %d (%x)\n", grant, grant);
  printf("hopping flag: %d\n", (grant>>19) & 1);
  printf("rb alloc: %d\n",     (grant>>9) & 1023);
  printf("mcs: %d\n",          (grant>>5) & 15);
  printf("pusch tpc: %d\n",    (grant>>2) & 3);
  printf("ul delay: %d\n",     (grant>>1) & 1);
  printf("csi request: %d\n",  grant & 1);
  return 0;
}
