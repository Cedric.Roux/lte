#include <stdio.h>
#include <stdlib.h>

unsigned char pdu[65536];
int pdu_size;

int main(void)
{
  int reserved, fi, e, sn, li;
  int data_size, header_size;
  unsigned char *s;

  /* read pdu from stdin */
  while (1) {
    int c = getchar(); if (c == EOF) break;
    pdu[pdu_size] = c;
    pdu_size++;
  }

  reserved = (pdu[0] >> 5) & 7;
  fi = (pdu[0] >> 3) & 3;
  e = (pdu[0] >> 2) & 1;
  sn = ((pdu[0] & 3) << 8) | pdu[1];

  if (reserved) printf("WARNING: reserved bits not 0: 0x%x\n", reserved);
  printf("fi %d e %d sn %d\n", fi, e, sn);

  s = pdu+2;

  data_size = e == 1 ? 0 : pdu_size-2;

  while (e) {
    e = (*s >> 7) & 1;
    li = ((*s & 127) << 4) | ((s[1] >> 4) & 15);
    data_size += li;
    printf("e %d li %d\n", e, li);
    s += 2;
    if (!e) break;
    e = (s[-1] >> 3) & 1;
    li = ((s[-1] & 3) << 8) | s[0];
    data_size += li;
    printf("e %d li %d\n", e, li);
    s++;
  }

  header_size = s - pdu;

  printf("last packet size: %d\n", pdu_size-data_size-header_size);
  printf("header bytes in buffer: %d\n", header_size);
  printf("non-header bytes in packet: %d\n", pdu_size-header_size);
  printf("data bytes to read according to RLC headers: %d (without last)\n",
         data_size);
  if (data_size > pdu_size-header_size)
    printf("ERROR: not enough bytes in buffer!\n");
  return 0;
}
