#include <stdio.h>

int main(void)
{
  int i;

  for (i = 1; i <= 100; i++) {
    int n = i;
    while (n != 1) {
      if (n % 2 == 0) { n = n/2; continue; }
      if (n % 3 == 0) { n = n/3; continue; }
      if (n % 5 == 0) { n = n/5; continue; }
      break;
    }
    if (n == 1) printf("%d ", i);
  }
  printf("\n");
  return 0;
}
