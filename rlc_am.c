/* bug: we don't check size of data, live with that */

#include <stdio.h>
#include <stdlib.h>

unsigned char pdu[65536];
int pdu_size;

int getbit(int *cur, int *curbit)
{
  int ret = (pdu[*cur] >> (*curbit)) & 1;

  *curbit = (*curbit) - 1;
  if ((*curbit) < 0) { *curbit = 7; *cur = (*cur) + 1; }

  return ret;
}

void dump_control_pdu(void)
{
  int cpt, ack_sn, e1, nack_sn, e2, sostart, soend;
  int cur, curbit;
  int i;

#define BIT getbit(&cur, &curbit)

  cpt = (pdu[0] >> 4) & 7;
  if (cpt != 0) { printf("ERROR: control pdu with cpt %d\n", cpt); return; }

  ack_sn = ((pdu[0] & 15) << 6) | ((pdu[1] >> 2) & 63);
  e1 = (pdu[1] >> 1) & 1;

  printf("status pdu:\n");
  printf("    (ack_sn: all pdu lower than this one OK if not nacked below)\n");
  printf("ack_sn %d e1 %d\n", ack_sn, e1);

  cur = 1;
  curbit = 0;
  while (e1) {
     nack_sn = 0;
     for (i = 0; i < 10; i++) { nack_sn <<= 1; nack_sn |= BIT; }
     e1 = BIT;
     e2 = BIT;
     if (e2) {
       sostart = 0;
       for (i = 0; i < 15; i++) { sostart <<= 1; sostart |= BIT; }
       soend = 0;
       for (i = 0; i < 15; i++) { soend <<= 1; soend |= BIT; }
     }
     if (e2)
       printf("nack_sn %d e1 %d e2 %d sostart %d soend %d\n",
              nack_sn, e1, e2, sostart, soend);
     else
       printf("nack_sn %d e1 %d e2 %d\n", nack_sn, e1, e2);
  }

  printf("%d bytes consumed\n", cur + (curbit == 7 ? 0 : 1));
}

int main(void)
{
  int dc, rf, p, fi, e, sn, li;
  int lsf, so;
  int data_size, header_size;
  unsigned char *s;

  /* read pdu from stdin */
  while (1) {
    int c = getchar(); if (c == EOF) break;
    pdu[pdu_size] = c;
    pdu_size++;
  }

  dc = (pdu[0] >> 7) & 1;
  if (dc == 0) { dump_control_pdu(); return 0; }
  rf = (pdu[0] >> 6) & 1;
  p  = (pdu[0] >> 5) & 1;
  fi = (pdu[0] >> 3) & 3;
  e  = (pdu[0] >> 2) & 1;
  sn = ((pdu[0] & 3) << 8) | pdu[1];

  printf("dc: 0 control, 1 data\n");
  printf("rf: 0 am pdu, 1 am pdu segment\n");
  printf("p:  1 status report request\n");
  printf("fi: 0: full SDU, 1:begin, 2:end, 3:continue\n");
  printf("----------------------------------\n");
  printf("dc %d\nrf %d\np  %d\nfi %d\ne  %d\nsn %d\n", dc, rf, p, fi, e, sn);

  s = pdu + 2;

  if (rf) {
    lsf = (pdu[2] >> 7) & 1;
    so = ((pdu[2] & 0x7f) << 8) | pdu[3];
    printf("am pdu segment:\n");
    printf("  lsf: %d\n", lsf);
    printf("  so: %d\n", so);
    s += 2;
  }

  data_size = e == 1 ? 0 : pdu_size - 2 - 2 * rf;

  while (e) {
    e = (*s >> 7) & 1;
    li = ((*s & 127) << 4) | ((s[1] >> 4) & 15);
    data_size += li;
    printf("e %d li %d\n", e, li);
    s += 2;
    if (!e) break;
    e = (s[-1] >> 3) & 1;
    li = ((s[-1] & 3) << 8) | s[0];
    data_size += li;
    printf("e %d li %d\n", e, li);
    s++;
  }

  header_size = s - pdu;

  printf("last packet size: %d\n", pdu_size-data_size-header_size);
  printf("header bytes in buffer: %d\n", header_size);
  printf("non-header bytes in packet: %d\n", pdu_size-header_size);
  printf("data bytes to read according to RLC headers: %d (without last)\n",
         data_size);
  if (data_size > pdu_size-header_size)
    printf("ERROR: not enough bytes in buffer!\n");
  return 0;
}
