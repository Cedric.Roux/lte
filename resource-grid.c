/* this program marks REs used for (cell-specific) reference signals,
 * SSS, PSS and BCH
 * it leaves all other REs
 * only frame structure 1 (fdd) handled
 * (hopefully this is correct...)
 */
#include <stdio.h>
#include <stdlib.h>

#include "resource-grid.h"

/* 'only_symbol_0' is used to reserve REs in the first symbol of a subframe
 * for the case with only one antenna port. In this case, 36.211 6.2.4
 * says the REs used for reference symbol for antenna port 1 (we don't have
 * it, we only have antenna port 0) should be marked as reserved (the text
 * does not say it that exact way, but that's what it means)
 *
 * 'reserved' is to forcefully mark REs as, well, reserved
 */
static void mark_refsig(resource_grid_t *g, int p, int only_symbol_0, int reserved)
{
  int v, v_shift, ns, m, k, l;

  v_shift = g->N_ID_cell % 6;

  for (ns = 0; ns < 20; ns++)
  for (m = 0; m < 2 * g->N_RB_DL; m++)
  /* l is { 0, N_symb_DL - 3 } if p in { 0, 1 } and 1 if p in { 2, 3 } */
  /* in special case 'only_symbol_0' only l==0 is done */
  for (l = ((p == 0 || p == 1) ? 0 : 1);
       l <= (only_symbol_0 ? 0 : (p == 0 || p == 1) ? g->N_symb_DL - 3 : 1);
       l += ((p == 0 || p == 1) ? g->N_symb_DL - 3 : 1)) {
    if      (p == 0 && l == 0) v = 0;
    else if (p == 0 && l != 0) v = 3;
    else if (p == 1 && l == 0) v = 3;
    else if (p == 1 && l != 0) v = 0;
    else if (p == 2)           v = 3 * (ns % 2);
    else /*if (p == 3)*/       v = 3 + 3 * (ns % 2);
    k = 6 * m + (v + v_shift) % 6;
    /* don't do 2nd slot of subframe in mode 'only_symbol_0' */
    if (only_symbol_0 && ns & 1) continue;
    g->grid[GRID(g, ns, l, k)] = only_symbol_0 || reserved ? RESERVED : REFSIG;
  }
}

static void mark_pss(resource_grid_t *g)
{
  int k, l, n, ns;

  l = g->N_symb_DL-1;

  for (ns = 0; ns <= 10; ns += 10)
  /* actually used by pss: for (n = 0; n < 62; n++) */
  /* we also mark the 'reserved' REs at each end of the pss */
  for (n = -5; n <= 66; n++) {
    k = n - 31 + (g->N_RB_DL * g->N_sc_RB) / 2;
    g->grid[GRID(g, ns, l, k)] = n >= 0 && n <= 61 ? PSS : RESERVED;
  }
}

static void mark_sss(resource_grid_t *g)
{
  int k, l, n, ns;

  l = g->N_symb_DL-2;

  for (ns = 0; ns <= 10; ns += 10)
  /* actually used by sss: for (n = 0; n < 62; n++) */
  /* we also mark the 'reserved' REs at each end of the pss */
  for (n = -5; n <= 66; n++) {
    k = n - 31 + (g->N_RB_DL * g->N_sc_RB) / 2;
    g->grid[GRID(g, ns, l, k)] = n >= 0 && n <= 61 ? SSS : RESERVED;
  }
}

static void mark_bch(resource_grid_t *g)
{
  /* we must skip reference signals for antenna ports 0-3 even
   * if they are not present in the curren configuration in which
   * case we also have to mark them reserved
   */
  resource_grid_t full_grid = *g;
  int k, l, ns, k_prime, p;

  full_grid.grid = calloc(
                   /* vertical (frequency) */
                   g->N_RB_DL * g->N_sc_RB *
                   /* horizontal (time) */
                   g->N_symb_DL * 20, sizeof(int));
  if (full_grid.grid == NULL) abort();
  for (p = 0; p < 4; p++) mark_refsig(&full_grid, p, 0, 1);

  ns = 1;

  for (l = 0; l <= 3; l++)
  for (k_prime = 0; k_prime <= 71; k_prime++) {
    k = g->N_RB_DL * g->N_sc_RB / 2 - 36 + k_prime;
    if (full_grid.grid[GRID(g, ns, l, k)]) {
      if (!g->grid[GRID(g, ns, l, k)])
        g->grid[GRID(g, ns, l, k)] = RESERVED;
      continue;
    }
    g->grid[GRID(g, ns, l, k)] = BCH;
  }

  free(full_grid.grid);
}

static void mark_pcfich(resource_grid_t *g)
{
  int i;
  int ns;

  for (ns = 0; ns < 20; ns += 2) {
    for (i = 0; i < 4; i++) {
      int ik;
      /* see 36.211 6.7.4 */
      int k_ = (12/2) * (g->N_ID_cell % (2 * g->N_RB_DL));
      int k = k_ + ((i * g->N_RB_DL)/2) * (12/2);
      k = k % (g->N_RB_DL * 12);
      for (ik = 0; ik < 6; ik++) {
        int re_i = k + ik;
        /* skip reserved / refsig REs */
        if (g->grid[GRID(g, ns, 0, re_i)]) continue;
        g->grid[GRID(g, ns, 0, re_i)] = PCFICH;
      }
    }
  }
}

static int phich_reg_count(resource_grid_t *g, int l, int ns)
{
  int n;
  int k;
  int i;

  /* compute number of REGs in symbol l not assigned to PCFICH */
  n = 0;
  k = 0;
  while (k < g->N_RB_DL * g->N_sc_RB) {
    int busy;
    int reg_size = 4;
    /* if one RE is reserved/refsig, then REG size is 6, not 4 */
    for (i = 0; i < 4; i++)
      if (g->grid[GRID(g, ns, l, k+i)] == RESERVED
          || g->grid[GRID(g, ns, l, k+i)] == REFSIG)
        reg_size = 6;
    busy = 0;
    /* if the RE has some PCFICH inside, then it's busy, don't count it */
    for (i = 0; i < reg_size; i++)
      if (g->grid[GRID(g, ns, l, k+i)] == PCFICH)
        busy = 1;
    if (!busy) n++;
    k += reg_size;
  }
  return n;
}

static int phich_reg_get_k(resource_grid_t *g, int l, int ns, int reg)
{
  int k;
  int i;

  k = 0;
  while (k < g->N_RB_DL * g->N_sc_RB) {
    int busy;
    int reg_size = 4;
    /* if one RE is reserved/refsig, then REG size is 6, not 4 */
    for (i = 0; i < 4; i++)
      if (g->grid[GRID(g, ns, l, k+i)] == RESERVED
          || g->grid[GRID(g, ns, l, k+i)] == REFSIG)
        reg_size = 6;
    busy = 0;
    /* if the RE has some PCFICH inside, then skip it */
    for (i = 0; i < reg_size; i++)
      if (g->grid[GRID(g, ns, l, k+i)] == PCFICH)
        busy = 1;
    if (!busy) {
      reg--;
      if (reg == -1) break;
    }
    k += reg_size;
  }
  if (reg != -1) abort();
  return k;
}

static void mark_phich(resource_grid_t *g)
{
  int N_group;
  int n0;
  int nl;
  int ns;
  int ni;
  int k;
  int i;
  int j;
  int l;
  int m;
  int m_max;
  int reg_size;

  /* see 36.211 6.9 */
  switch (g->ng) {
  case NG_1_6: N_group = g->N_RB_DL / 8 / 6; if (N_group*8*6 != g->N_RB_DL) N_group++; break;
  case NG_1_2: N_group = g->N_RB_DL / 8 / 2; if (N_group*8*2 != g->N_RB_DL) N_group++; break;
  case NG_1:   N_group = g->N_RB_DL / 8;     if (N_group*8   != g->N_RB_DL) N_group++; break;
  case NG_2:   N_group = g->N_RB_DL / 4;     if (N_group*4   != g->N_RB_DL) N_group++; break;
  }

  if (g->CP) N_group *= 2;

  /* this is max for m' as defined in 36.211 6.9.3, only frame structure type 1 handled */
  if (g->CP == 0) {
    m_max = N_group - 1;
  } else {
    m_max = N_group / 2 - 1;
  }

  /* see 36.211 6.9.3 - this code is very inefficient, reg to k mapping
   * should be done out of the loop, reg count needs to be done also only
   * once; bah, no big deal
   */
  for (ns = 0; ns < 20; ns += 2) {
    n0 = phich_reg_count(g, 0, ns);
    for (m = 0; m <= m_max; m++) {
      for (i = 0; i <= 2; i++) {
        if (g->phich_duration == 0)
          l = 0;
        else
          l = i;
        nl = phich_reg_count(g, l, ns);
        ni = (g->N_ID_cell * nl / n0 + m + i * nl / 3) % nl;
        k = phich_reg_get_k(g, l, ns, ni);
        /* mark the REG as PHICH */
        reg_size = 4;
        /* if one RE is reserved/refsig, then REG size is 6, not 4 */
        for (j = 0; j < 4; j++)
          if (g->grid[GRID(g, ns, l, k+j)] == RESERVED
              || g->grid[GRID(g, ns, l, k+j)] == REFSIG)
            reg_size = 6;
        for (j = 0; j < reg_size; j++)
          if (!g->grid[GRID(g, ns, l, k+j)])
            g->grid[GRID(g, ns, l, k+j)] = PHICH;
      }
    }
  }
}

void generate_resource_grid(resource_grid_t *g)
{
  int p;

  g->N_sc_RB = 12;

  if (g->CP == 0) {
    g->N_symb_DL = 7;
  } else {
    g->N_symb_DL = 6;
  }

  g->grid = calloc(
               /* vertical (frequency) */
               g->N_RB_DL * g->N_sc_RB *
               /* horizontal (time) */
               g->N_symb_DL * 20, sizeof(int));
  if (g->grid == NULL) abort();

  /* mark as RESERVED the REs that are not for the wanted port */
  for (p = 0; p < g->n_ports; p++) mark_refsig(g, p, 0, p != g->port);

  /* special case for 1 antenna port: REGs in symbol 0 have to skip
   * REs that would be assigned to antenna port number 1 (second antenna port)
   * if there were two antenna ports
   * so let's mark them as well even if they wouldn't be used by a pdsch
   */
  if (g->n_ports == 1) mark_refsig(g, 1, 1, 1);

  mark_pss(g);
  mark_sss(g);
  mark_bch(g);
  mark_pcfich(g);
  mark_phich(g);
}

void dump_grid(resource_grid_t *g)
{
  int k, l, ns, z;

  for (k = g->N_RB_DL * g->N_sc_RB - 1; k >= 0; k--) {
    printf("%3d ", k / g->N_sc_RB);
    for (ns = 0; ns < 20; ns++) {
      printf(ns&1 ? "!" : "|");
      for (l = 0; l < g->N_symb_DL; l++)
        printf("%c", "-rpsbCH*R"[g->grid[GRID(g, ns, l, k)]]);
    }
    printf("|\n");
    if (k % g->N_sc_RB == 0) {
      printf("    +");
      for (z = 0; z < 20 * (g->N_symb_DL+1) - 1; z++) printf(" ");
      printf("+\n");
    }
  }
  printf("r: refsig    p: PSS    s: SSS    b: BCH    C: PCFICH    H: PHICH    *: PDCCH    R: reserved\n");
}

#ifndef NO_MAIN

void usage(void)
{
  printf("gimme <N_RB_DL>\n");
  exit(1);

}

int main(int n, char **v)
{
  resource_grid_t g;
  int i;

  /* constant */
  g.N_sc_RB        = 12;

  /* parameters */
  g.N_RB_DL        = -1;
  g.CP             = 0;
  g.N_ID_cell      = 0;
  g.n_ports        = 1;
  g.port           = 0;
  g.ng             = NG_1;
  g.phich_duration = 0;

  if (g.port >= g.n_ports) { printf("port out of range\n"); exit(1); }

  for (i = 1; i < n; i++) {
    if (g.N_RB_DL == -1) { g.N_RB_DL = atoi(v[i]); continue; }
    usage();
  }
  if (g.N_RB_DL == -1) usage();

  generate_resource_grid(&g);

  dump_grid(&g);

  free(g.grid);
}

#endif /* NO_MAIN */
