#include <stdio.h>
#include "liblte_rrc.h"

int main(void)
{
  LIBLTE_RRC_UL_CCCH_MSG_STRUCT ul;
  LIBLTE_BIT_MSG_STRUCT msg;
  LIBLTE_ERROR_ENUM err;
  unsigned int i;
  uint8 *b;

  ul.msg_type = LIBLTE_RRC_UL_CCCH_MSG_TYPE_RRC_CON_REQ;
  ul.msg.rrc_con_req.ue_id_type = LIBLTE_RRC_CON_REQ_UE_ID_TYPE_S_TMSI;
  ul.msg.rrc_con_req.ue_id.s_tmsi.m_tmsi = 0xaadead55;
  ul.msg.rrc_con_req.ue_id.s_tmsi.mmec = 0xdc;
  ul.msg.rrc_con_req.cause = LIBLTE_RRC_CON_REQ_EST_CAUSE_MO_SIGNALLING;
  err = liblte_rrc_pack_ul_ccch_msg(&ul, &msg);
  if (err != LIBLTE_SUCCESS) { printf("error\n"); exit(1); }
  printf("con req stmsi: %d bits\n", msg.N_bits);
  b = msg.msg; for (i = 0; i < (msg.N_bits+7) / 8; i++) { int v = 0; int j;
    for (j=0; j<8; j++) { v<<=1; v|=*b++; } printf("%2.2x ",v);} printf("\n");

  ul.msg_type = LIBLTE_RRC_UL_CCCH_MSG_TYPE_RRC_CON_REQ;
  ul.msg.rrc_con_req.ue_id_type = LIBLTE_RRC_CON_REQ_UE_ID_TYPE_RANDOM_VALUE;
  ul.msg.rrc_con_req.ue_id.random = 0xdeadbeef0101ffff;
  ul.msg.rrc_con_req.cause = LIBLTE_RRC_CON_REQ_EST_CAUSE_MO_DATA;
  err = liblte_rrc_pack_ul_ccch_msg(&ul, &msg);
  if (err != LIBLTE_SUCCESS) { printf("error\n"); exit(1); }
  printf("con req random: %d bits\n", msg.N_bits);
  b = msg.msg; for (i = 0; i < (msg.N_bits+7) / 8; i++) { int v = 0; int j;
    for (j=0; j<8; j++) { v<<=1; v|=*b++; } printf("%2.2x ",v);} printf("\n");

  ul.msg_type = LIBLTE_RRC_UL_CCCH_MSG_TYPE_RRC_CON_REEST_REQ;
  ul.msg.rrc_con_reest_req.ue_id.c_rnti = 0xdead;
  ul.msg.rrc_con_reest_req.ue_id.phys_cell_id = 0x5555;
  ul.msg.rrc_con_reest_req.ue_id.short_mac_i = 0xaaaa;
  ul.msg.rrc_con_reest_req.cause = LIBLTE_RRC_CON_REEST_REQ_CAUSE_OTHER_FAILURE;
  err = liblte_rrc_pack_ul_ccch_msg(&ul, &msg);
  if (err != LIBLTE_SUCCESS) { printf("error\n"); exit(1); }
  printf("con reest req: %d bits\n", msg.N_bits);
  b = msg.msg; for (i = 0; i < (msg.N_bits+7) / 8; i++) { int v = 0; int j;
    for (j=0; j<8; j++) { v<<=1; v|=*b++; } printf("%2.2x ",v);} printf("\n");

  return 0;

  (void)liblte_rrc_dl_pathloss_change_num;
  (void)liblte_rrc_prohibit_phr_timer_num;
  (void)liblte_rrc_periodic_phr_timer_num;
}
