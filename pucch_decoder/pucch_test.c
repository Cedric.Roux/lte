/* only for 50 PRBs, normal prefix */

/* plot "TOTO" using 1:2:3 with image */

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <complex.h>
#include "pucch_generate.h"

int main(int n, char **v)
{
  char **n1_pucch;
  if (n < 3) {
    printf("gimme <subframe> <n1_pucch 1> .. <n1_pucch n>\n");
    printf("  n1_pucch starts with '-' for NACK\n");
    return 1;
  }

  n -= 2;
  n1_pucch = &v[2];

  int subframe = atoi(v[1]);

  complex REs[14][50*12];
  complex z_p[14][12];
  int i, j;

  for (i = 0; i < 14; i++) for (j = 0; j < 50*12; j++) REs[i][j] = 0;

  int k;
  for (k = 0; k < n; k++) {
    pucch_compute(z_p, abs(atoi(n1_pucch[k])), subframe);
    for (int l = 0; l < 7; l++) for (i = 0; i < 12; i++) {
      int x = (int[]){-1,-1,1,1,1,-1,-1}[l];
      /* start n1_pucch by '-' means NACK, so send +1 */
      if (n1_pucch[k][0] == '-') x = 1;
      REs[l][i] += x * z_p[l][i];
      REs[7+l][i+600-12] += x * z_p[7+l][i];
    }
  }

#if 0
  for (int i = 0; i < 14; i++) for (j = 0; j < 600; j++)
    printf("%d %d %f\n", i, j, cabs(REs[i][j]));
exit(1);
#endif

  for (k = 0; k < n; k++) {
    pucch_compute(z_p, abs(atoi(n1_pucch[k])), subframe);

    /* compute channel estimate */

    complex chest0[12];  /* channel estimate for slot 0 */
    complex chest1[12];  /* channel estimate for slot 1 */

    for (i = 0; i < 12; i++) chest0[i] = chest1[i] = 0;
    for (int l = 2; l <= 4; l++) for (i = 0; i < 12; i++) {
      chest0[i] += REs[l][i] * conj(z_p[l][i]);
      chest1[i] += REs[7+l][i+600-12] * conj(z_p[7+l][i]);
    }

    /* apply channel to received signal and extract soft data bits */

    complex slot0[7][12];
    complex slot1[7][12];

    for (i = 0; i < 7; i++) for (j = 0; j < 12; j++) {
      slot0[i][j] = REs[i][j] * conj(chest0[j]) *  conj(z_p[i][j]);
      slot1[i][j] = REs[7+i][j+600-12] * conj(chest1[j]) *  conj(z_p[7+i][j]);
    }

    /* average soft data bits */

    complex p = 0;
    for (i = 0; i < 7; i++) if (!(i == 2 || i == 3 || i == 4))
      for (j = 0; j < 12; j++) {
        p += slot0[i][j];
        p += slot1[i][j];
      }

    printf("subframe %d n1_pucch %d [%s] p %f %f\n",
           subframe, abs(atoi(n1_pucch[k])),
           n1_pucch[k][0] == '-' ? "NACK" : "ACK",
           creal(p) / (4*12), cimag(p) / (4*12));
  }

  return 0;
}
