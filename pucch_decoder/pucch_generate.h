#ifndef _PUCCCH_GENERATE_H_
#define _PUCCCH_GENERATE_H_

void pucch_compute(complex out[14][12], int n1_pucch, int subframe);

#endif /* _PUCCCH_GENERATE_H_ */
