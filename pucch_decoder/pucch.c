/* only tested for 25/50 PRBs, normal prefix, some code only for 50 PRBs */

/* plot "TOTO" using 1:2:3 with image */

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <fftw3.h>
#include <complex.h>
#include "pucch_generate.h"

#include <stdint.h>
#if CHECK_7_5K
#include "kHz_7_5.h"
#endif

struct parameters {
  float samplerate;
  int cp0;
  int cp;
  int fft_size;
  int samples_per_tti;
  int nb_re_per_symbol;
};

void setup_parameters(struct parameters *p, int nb_rb)
{
  switch (nb_rb) {
  case 25:
    p->samplerate = 30.72e6/4;
    p->cp0 = 160/4;
    p->cp = 144/4;
    p->fft_size = 512;
    p->samples_per_tti = 30720/4;
    p->nb_re_per_symbol = 1200/4;
    break;
  case 50:
    p->samplerate = 30.72e6/2;
    p->cp0 = 160/2;
    p->cp = 144/2;
    p->fft_size = 1024;
    p->samples_per_tti = 30720/2;
    p->nb_re_per_symbol = 1200/2;
    break;
  default: printf("unsupported number of RB %d\n", nb_rb); exit(1);
  }
}

complex seq_7_5k[30720];

void generate_7_5k_symbol(complex *out, int prefix_length, struct parameters *p)
{
  int i;

  for (i = -prefix_length; i < p->fft_size; i++)
    *out++ = cexp(I * 2 * M_PI * i * -7.5e3 / p->samplerate);
}

void generate_7_5k(struct parameters *p)
{
  int cp0 = p->cp0;
  int cp = p->cp;
  complex *to = seq_7_5k;
  int i, j;

  for (i = 0; i < 2; i++) {
    generate_7_5k_symbol(to, cp0, p);
    to += cp0 + p->fft_size;
    for (j = 0; j < 6; j++) {
      generate_7_5k_symbol(to, cp, p);
      to += cp + p->fft_size;
    }
  }
}

void to_freq_symbol(complex *_in, complex *_out, struct parameters *p)
{
  int i;
  int fft = p->fft_size;
  fftw_complex *in, *out;
  fftw_plan plan;
  in = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * fft);
  out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * fft);
  plan = fftw_plan_dft_1d(fft, in, out, FFTW_FORWARD, FFTW_ESTIMATE);

  for (i = 0; i < fft; i++) {
    in[i][0] = creal(_in[i]);
    in[i][1] = cimag(_in[i]);
  }

  fftw_execute(plan);

  for (i = 0; i < fft; i++)
    _out[i] = out[i][0] + I * out[i][1];

  fftw_destroy_plan(plan);
  fftw_free(in); fftw_free(out);
}

void to_freq(complex *in, complex *out, struct parameters *p)
{
  int cp0 = p->cp0;
  int cp = p->cp;
  complex *from, *to;
  int i, j;

  from = in;
  to = out;

  for (i = 0; i < 2; i++) {
    to_freq_symbol(from + cp0, to, p);
    from += cp0 + p->fft_size;
    to += p->fft_size;
    for (j = 0; j < 6; j++) {
      to_freq_symbol(from + cp, to, p);
      from += cp + p->fft_size;
      to += p->fft_size;
    }
  }
}

int main(int n, char **v)
{
  struct parameters p;
  if (n != 5)
    { printf("gimme <nr rb> <file> <subframe> <n1_pucch>\n"); return 1; }

  FILE *file;
  int nb_rb = atoi(v[1]);
  char *filename = v[2];
  int subframe = atoi(v[3]);
  int n1_pucch = atoi(v[4]);

  /* size set to maximum possible */
  complex in[30720];
  complex in_f[2048*14];
  complex REs[14][100*12];
  complex z_p[14][12];
  int i, j;

  setup_parameters(&p, nb_rb);

  generate_7_5k(&p);

#if CHECK_7_5K
  if (nb_rb != 50) { printf("code below only for 50 RBs\n"); exit(1); }
  {
  int i;
  for (i = 0; i < 15360/2; i++)
    if ((int)floor(creal(seq_7_5k[i]) * 32767) != s50n_kHz_7_5[i*2] ||
        (int)floor(cimag(seq_7_5k[i]) * 32767) != s50n_kHz_7_5[i*2+1])
      printf("err %d [%d %d] [%d %d]\n", i,
          (int)floor(creal(seq_7_5k[i]) * 32767), (int)floor(cimag(seq_7_5k[i]) * 32767),
          s50n_kHz_7_5[i*2], s50n_kHz_7_5[i*2+1]);
  }
#endif

  file = fopen(filename, "r"); if (file==NULL) { perror(filename); return 1; }
  for (i = 0; i < p.samples_per_tti; i++) {
    short re, im;
    if (fread(&re, 2, 1, file) != 1) abort();
    if (fread(&im, 2, 1, file) != 1) abort();
    in[i] = re / 2048. + I * im / 2048.;
  }
  fclose(file);

#if 0
  if (nb_rb != 50) { printf("code below only for 50 RBs\n"); exit(1); }
  for (i = 0; i < 15360; i++) {
    in[i] = cexp(I * 2 * M_PI * (i - (80+72*2+1024*2)) * 20. / 1024.);
  }
#endif

  /* frequency shift by 7.5KHz */
  for (i = 0; i < p.samples_per_tti; i++) in[i] *= seq_7_5k[i];

  /* DFT */
  to_freq(in, in_f, &p);

  /* get REs from DFT */
  for (i = 0; i < 14; i++)
    for (j = 0; j < p.nb_re_per_symbol/2; j++) {
      REs[i][j]                      = in_f[i * p.fft_size + p.fft_size-p.nb_re_per_symbol/2 + j];
      REs[i][j+p.nb_re_per_symbol/2] = in_f[i * p.fft_size + j];
    }

#if 0
  for (i = 0; i < 14; i++)
    for (j = 0; j < p.nb_re_per_symbol; j++) printf("%f %f\n", creal(REs[i][j]), cimag(REs[i][j]));
exit(0);
#endif

#if 0
  if (nb_rb != 50) { printf("code below only for 50 RBs\n"); exit(1); }
  for (i = 0; i < 14; i++)
    for (j = 0; j < 600; j++) printf("%d %d %f\n", i, j, cabs(REs[i][j]));

  //for (i = 0; i < 1024*14; i++) printf("%d %f %f\n", i%1024, creal(in_f[i]), cimag(in_f[i]));
  //for (i = 1024*7; i < 1024*7 + 1024; i++) printf("%d %f\n", i%1024, cabs(in_f[i]));
  //for (i = 1024*2; i < 1024*2 + 1024; i++) printf("%f %f\n", creal(in_f[i]), cimag(in_f[i]));

  i = 20; printf("%g %g\n", creal(in_f[i]), cimag(in_f[i]));
  i+=1024; printf("%g %g\n", creal(in_f[i]), cimag(in_f[i]));
  i+=1024; printf("%g %g\n", creal(in_f[i]), cimag(in_f[i]));
  i+=1024; printf("%g %g\n", creal(in_f[i]), cimag(in_f[i]));
  i+=1024; printf("%g %g\n", creal(in_f[i]), cimag(in_f[i]));
  i+=1024; printf("%g %g\n", creal(in_f[i]), cimag(in_f[i]));
  i+=1024; printf("%g %g\n", creal(in_f[i]), cimag(in_f[i]));
  i+=1024; printf("%g %g\n", creal(in_f[i]), cimag(in_f[i]));
  i+=1024; printf("%g %g\n", creal(in_f[i]), cimag(in_f[i]));
  i+=1024; printf("%g %g\n", creal(in_f[i]), cimag(in_f[i]));
  i+=1024; printf("%g %g\n", creal(in_f[i]), cimag(in_f[i]));
  i+=1024; printf("%g %g\n", creal(in_f[i]), cimag(in_f[i]));
  i+=1024; printf("%g %g\n", creal(in_f[i]), cimag(in_f[i]));
  i+=1024; printf("%g %g\n", creal(in_f[i]), cimag(in_f[i]));
#endif

  pucch_compute(z_p, n1_pucch, subframe);

  /* compute channel estimate */

  complex chest0[12];  /* channel estimate for slot 0 */
  complex chest1[12];  /* channel estimate for slot 1 */

  for (i = 0; i < 12; i++) chest0[i] = chest1[i] = 0;
  for (int l = 2; l <= 4; l++) for (i = 0; i < 12; i++) {
    chest0[i] += REs[l][i] * conj(z_p[l][i]);
    chest1[i] += REs[7+l][i+p.nb_re_per_symbol-12] * conj(z_p[7+l][i]);
  }

  /* apply channel to received signal and extract soft data bits */

  complex slot0[7][12];
  complex slot1[7][12];

  for (i = 0; i < 7; i++) for (j = 0; j < 12; j++) {
    slot0[i][j] = REs[i][j] * conj(chest0[j]) *  conj(z_p[i][j]);
    slot1[i][j] = REs[7+i][j+p.nb_re_per_symbol-12] * conj(chest1[j]) *  conj(z_p[7+i][j]);
  }

  /* average soft data bits */

  complex pt = 0;
  for (i = 0; i < 7; i++) if (!(i == 2 || i == 3 || i == 4))
    for (j = 0; j < 12; j++) {
      pt += slot0[i][j];
      pt += slot1[i][j];
    }

  printf("p %f %f\n", creal(pt) / (4*12), cimag(pt) / (4*12));
  return 0;

for (i = 0; i < 7; i++) for (j = 0; j < 12; j++)  printf("slot0 %d %d %f %f\n", i, j, creal(slot0[i][j]), cimag(slot0[i][j]));
for (i = 0; i < 7; i++) for (j = 0; j < 12; j++)  printf("slot1 %d %d %f %f\n", i, j, creal(slot1[i][j]), cimag(slot1[i][j]));

exit(0);

  if (nb_rb != 50) { printf("code below only for 50 RBs\n"); exit(1); }

  /* compute channel for ref symbols */
  int ns, l, re;
  for (ns = 0; ns < 2; ns++) {
    for (l = 2; l <= 4; l++) { //printf(".");
      for (re = 0 + 588*ns; re < 12 + 588*ns; re++) {
        REs[ns*7+l][re] *= conj(z_p[ns*7+l][re - 588*ns]);
printf("%d %d %f\n", ns*3+l-2, re-588*ns, cabs(REs[ns*7+l][re]));
//printf("%d %d %f\n", ns*3+l-2, re-588*ns, cabs(z_p[ns*7+l][re - 588*ns]));
//printf("ns %d l %d re %d [%f %f]\n", ns, l, re, creal(REs[ns*7+l][re]), cimag(REs[ns*7+l][re]));
      }
    }
  }

  return 0;
}
