#include "gold.h"
#include <stdio.h>
#include <math.h>
#include <complex.h>

/* parameters */
int N_RB_sc            = 12;
int Delta_PUCCH_shift  = 1;    /* 1, 2 or 3 */
int N_1_cs             = 0;    /* nCS-An in RRC */
int N_2_RB             = 0;    /* nRB-CQI in RRC */
int cyclic_prefix_type = 0;    /* 0: normal, 1: extended */
int N_cell_ID          = 0;

int N_UL_symb;         /* 7 for normal prefix, 6 for extended */
int n_cell_cs[20][7];

void init_n_cs(gold *g, int N_cell_ID)
{
  int ns, l, i;
  for (ns = 0; ns < 20; ns++) {
    for (l = 0; l < N_UL_symb; l++) {
      int n_cs = 0;
      for (i = 0; i <= 7; i++)
        n_cs += gold_c(g, 8 * N_UL_symb * ns + 8 * l + i) << i;
      n_cell_cs[ns][l] = n_cs;
    }
  }
}

/* only format 1, 1a, 1b */
int get_m(int n1_pucch)
{
  int c = cyclic_prefix_type == 0 ? 3 : 2;
  int m;
  if (n1_pucch < c * N_1_cs / Delta_PUCCH_shift)
    m = N_2_RB;
  else
    m = (n1_pucch - c * N_1_cs / Delta_PUCCH_shift) /
        (c * N_RB_sc / Delta_PUCCH_shift)
        + N_2_RB + (N_1_cs + (8-1))/8;
  return m;
}

int get_N_prime(int n1_pucch)
{
  int c = cyclic_prefix_type == 0 ? 3 : 2;
  if (n1_pucch < c * N_1_cs / Delta_PUCCH_shift) return N_1_cs;
  else return N_RB_sc;
}

int get_n_prime(int n1_pucch, int ns)
{
  int h, d;
  int c = cyclic_prefix_type == 0 ? 3 : 2;
  int n_prime_1, N_prime;

  if ((ns & 1) == 0) {
    if (n1_pucch < c * N_1_cs / Delta_PUCCH_shift)
      return n1_pucch;
    else
      return (n1_pucch - c * N_1_cs / Delta_PUCCH_shift) %
             (c * N_RB_sc / Delta_PUCCH_shift);
  }

  n_prime_1 = get_n_prime(n1_pucch, ns-1);
  d = cyclic_prefix_type == 0 ? 2 : 0;
  N_prime = get_N_prime(n1_pucch);
  h = (n_prime_1 + d) % (c * N_prime / Delta_PUCCH_shift);

  if (n1_pucch >= c * N_1_cs / Delta_PUCCH_shift)
    return (c * (n_prime_1 + 1)) % (c * N_RB_sc / Delta_PUCCH_shift + 1) - 1;
  else
    return h/c + (h % c) * N_prime / Delta_PUCCH_shift;
}

int get_n_oc(int n1_pucch, int ns)
{
  int ret = get_n_prime(n1_pucch, ns) * Delta_PUCCH_shift /
            get_N_prime(n1_pucch);
  if (cyclic_prefix_type == 0) return ret;
  else return ret*2;
}

int get_n_cs(int n1_pucch, int ns, int l)
{
  return
   (n_cell_cs[ns][l]
    + (get_n_prime(n1_pucch, ns) * Delta_PUCCH_shift +
        (cyclic_prefix_type == 0 ?
           (get_n_oc(n1_pucch, ns) % Delta_PUCCH_shift)
         : (get_n_oc(n1_pucch, ns) / 2))) % get_N_prime(n1_pucch)
   ) % N_RB_sc;
}

double get_alpha_p(int n1_pucch, int ns, int l)
{
  return 2 * M_PI * get_n_cs(n1_pucch, ns, l) / N_RB_sc;
}

/* 36.211 5.5.1.3 */
int get_u(int N_cell_id, int ns, gold *g_u)
{
  int f_gs;
  int f_ss;
  int i;

  f_ss = N_cell_id % 30;

  /* group hopping is always enabled? */
  f_gs = 0;
  for (i = 0; i <= 7; i++)
    f_gs += gold_c(g_u, 8 * ns + i) << i;

  return (f_gs + f_ss) % 30;
}

/* 36.211 5.5.1 for r_uv_alpha_p and 36.211 5.5.1.2 for r_barre_uv */
complex get_r_uv_alpha_p(int N_cell_id, int ns, double alpha_p, int n,
    gold *g_u)
{
  int phi[30][12] = {
    { -1, 1, 3, -3, 3, 3, 1, 1, 3, 1, -3, 3 },
    { 1, 1, 3, 3, 3, -1, 1, -3, -3, 1, -3, 3 },
    { 1, 1, -3, -3, -3, -1, -3, -3, 1, -3, 1, -1 },
    { -1, 1, 1, 1, 1, -1, -3, -3, 1, -3, 3, -1 },
    { -1, 3, 1, -1, 1, -1, -3, -1, 1, -1, 1, 3 },
    { 1, -3, 3, -1, -1, 1, 1, -1, -1, 3, -3, 1 },
    { -1, 3, -3, -3, -3, 3, 1, -1, 3, 3, -3, 1 },
    { -3, -1, -1, -1, 1, -3, 3, -1, 1, -3, 3, 1 },
    { 1, -3, 3, 1, -1, -1, -1, 1, 1, 3, -1, 1 },
    { 1, -3, -1, 3, 3, -1, -3, 1, 1, 1, 1, 1 },
    { -1, 3, -1, 1, 1, -3, -3, -1, -3, -3, 3, -1 },
    { 3, 1, -1, -1, 3, 3, -3, 1, 3, 1, 3, 3 },
    { 1, -3, 1, 1, -3, 1, 1, 1, -3, -3, -3, 1 },
    { 3, 3, -3, 3, -3, 1, 1, 3, -1, -3, 3, 3 },
    { -3, 1, -1, -3, -1, 3, 1, 3, 3, 3, -1, 1 },
    { 3, -1, 1, -3, -1, -1, 1, 1, 3, 1, -1, -3 },
    { 1, 3, 1, -1, 1, 3, 3, 3, -1, -1, 3, -1 },
    { -3, 1, 1, 3, -3, 3, -3, -3, 3, 1, 3, -1 },
    { -3, 3, 1, 1, -3, 1, -3, -3, -1, -1, 1, -3 },
    { -1, 3, 1, 3, 1, -1, -1, 3, -3, -1, -3, -1 },
    { -1, -3, 1, 1, 1, 1, 3, 1, -1, 1, -3, -1 },
    { -1, 3, -1, 1, -3, -3, -3, -3, -3, 1, -1, -3 },
    { 1, 1, -3, -3, -3, -3, -1, 3, -3, 1, -3, 3 },
    { 1, 1, -1, -3, -1, -3, 1, -1, 1, 3, -1, 1 },
    { 1, 1, 3, 1, 3, 3, -1, 1, -1, -3, -3, 1 },
    { 1, -3, 3, 3, 1, 3, 3, 1, -3, -1, -1, 3 },
    { 1, 3, -3, -3, 3, -3, 1, -1, -1, 3, -1, -3 },
    { -3, -1, -3, -1, -3, 3, 1, -1, 1, 3, -3, -3 },
    { -1, 3, -3, 3, -1, 3, 3, -3, 3, 3, -1, -1 },
    { 3, -3, -3, -1, -1, -3, -1, 3, -3, 3, 1, -1 },
  };

  int u = get_u(N_cell_id, ns, g_u);
  /* v not used, is 0 */
  //int v = 0;
  complex r_barre_uv = cexp(I * phi[u][n] * M_PI / 4);
  return cexp(I * alpha_p * n) * r_barre_uv;
}

void pucch_compute(complex out[14][12], int n1_pucch, int subframe)
{
  gold g;
  gold g_u;
  int ns, l;

  if (cyclic_prefix_type == 0) N_UL_symb = 7; else N_UL_symb = 6;

  init_gold(&g, N_cell_ID);
  init_n_cs(&g, N_cell_ID);

  init_gold(&g_u, N_cell_ID/30);

  /* compute z_p for data and ref (beware: use conj to estimate channel!) */

  int m, n;

  //int N_PUCCH_SF = 4;
  int N_PUCCH_seq = 12;

  complex w_noc_ref_table[3][3] = { {1,1,1},
      {1,cexp(I*2*M_PI/3),cexp(I*4*M_PI/3)},
      {1,cexp(I*4*M_PI/3),cexp(I*2*M_PI/3)} };
  int w_noc_table[3][4] = { {1,1,1,1}, {1,-1,1,-1}, {1,-1,-1,1 } };

  int n_prime, n_oc;
  complex S_ns;
  complex w_noc;
  complex y_p;
  complex z_p;

  //n_prime = get_n_prime(n1_pucch, ns);
  //n_oc = get_n_oc(n1_pucch, ns);

  int ref;

  for (ns = subframe*2; ns <= subframe*2+1; ns++)
  for (l = 0; l < 7; l++) {
    n_prime = get_n_prime(n1_pucch, ns);
    n_oc = get_n_oc(n1_pucch, ns);
    if (l == 0 || l == 1 || l == 5 || l == 6) ref = 0; else ref = 1;
    m = (int[]){0, 1, 0, 1, 2, 2, 3}[l];
    for (n = 0; n < N_PUCCH_seq; n++) {
      S_ns = n_prime % 2 == 0 ? 1 : cexp(I * M_PI / 2);
      if (ref) S_ns = 1;
      if (ref) w_noc = w_noc_ref_table[n_oc][m];
      else     w_noc = w_noc_table[n_oc][m];
      y_p = get_r_uv_alpha_p(N_cell_ID, ns,
                             get_alpha_p(n1_pucch, ns, l), n, &g_u);
      z_p = S_ns * w_noc * y_p;
      out[(ns-subframe*2)*7+l][n] = z_p;
    }
  }

  free_gold(&g);
  free_gold(&g_u);
}
