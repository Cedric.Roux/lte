/* used to analyse traffic and latencies
 * 3 use case:
 *   - analyse UDP traffic on port 50000
 *   - analyse GTP-U traffic and extract UDP on port 50000
 *   - extract macpdu2wireshark traffic on port 9999 (to be processed by another tool)
 */

/* gcc -Wall -g -o pcap pcap.c -I include lib/libpcap.a /lib64/libdbus-1.so */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pcap/pcap.h>
#include <stdint.h>

int verbose;
int do_gtp;
int do_9999;

int src[4];
int dst[4];

int gtp_src[4];
int gtp_dst[4];
int gtp_src_port = 2152;
int gtp_dst_port = 2152;

typedef enum { TCP, UDP, ICMP, UNKNOWN, ERROR } ip_t;

typedef struct {
  int portsrc;
  int portdst;
  const unsigned char *data;
  int datalen;
} udp_t;

typedef struct {
  struct timeval ts;
  unsigned char ipsrc[4];
  unsigned char ipdst[4];
  ip_t          type;
  int           fragmented;
  union {
    udp_t udp;
  };
} packet_t;

void dump_buffer(const unsigned char *b, int s)
{
  int i;
  for (i = 0; i < s; i++) printf(" %2.2x", (unsigned char)b[i]);
  printf("\n");
}

packet_t parse_ip(const unsigned char *ip, int l)
{
  packet_t r;
  int iplen;
  if (l < 5*4) { printf("packet too short\n"); exit(1); }
  if (ip[0] != 0x45) goto bad;
  iplen = (ip[2] << 8) | ip[3];
  if (iplen > l) { printf("iplen %d len %d\n", iplen, l); goto bad; }

  r.fragmented = (ip[6] & ~0x40) || ip[7];
  if (r.fragmented) printf("ERROR: fragmented (protocol 0x%2.2x)\n", ip[9]);
  switch (ip[9]) {
  case 0x11 : r.type = UDP; break;
  case 0x01 : r.type = ICMP; break;
  case 0x84 : r.type = UNKNOWN; break; /* icmp - not handled */
  default: printf("unknown ip type 0x%2.2x\n", ip[9]); r.type = UNKNOWN; break;
  }

  r.ipsrc[0] = ip[12];
  r.ipsrc[1] = ip[13];
  r.ipsrc[2] = ip[14];
  r.ipsrc[3] = ip[15];

  r.ipdst[0] = ip[16];
  r.ipdst[1] = ip[17];
  r.ipdst[2] = ip[18];
  r.ipdst[3] = ip[19];

  if (r.type == UDP) {
    const unsigned char *udp = &ip[20];
    if (l < 20 + 8) { printf("too short for udp\n"); goto bad; }
    int src = (udp[0] << 8) | udp[1];
    int dst = (udp[2] << 8) | udp[3];
    int udplen = (udp[4] << 8) | udp[5];
    if (udplen < 8) goto bad;
    if (udplen > l - 20) { printf("bad udp len (%d, max is %d)\n", udplen, l-20); goto bad; }
    r.udp.portsrc = src;
    r.udp.portdst = dst;
    r.udp.data = udp + 8;
    r.udp.datalen = udplen - 8;
  }

  return r;

bad:
  printf("bad ip packet\n");
  dump_buffer(ip, l);
  exit(1);
}

uint32_t tdiff(struct timeval a, struct timeval b)
{
  uint64_t va = ((uint64_t)a.tv_sec) * 1000000 + a.tv_usec;
  uint64_t vb = ((uint64_t)b.tv_sec) * 1000000 + b.tv_usec;

  return va-vb;
}

void analyse_50000(packet_t *p)
{
  int sample_start;
  uint32_t timestamp;
  const unsigned char *b;
  int bad;

  if (p->type != UDP) return;

  static int prev_sample_start = 0;
  static uint32_t prev_timestamp = 0;
  static struct timeval prev_ts = { 0, 0 };

#if 0
  /* src is 172.21.15.11:50000 and dst is 12.1.1.24:50000 */
  if (p->ipsrc[0] != 172 || p->ipsrc[1] != 21 || p->ipsrc[2] != 15 || p->ipsrc[3] != 11) return;
  if (p->ipdst[0] !=  12 || p->ipdst[1] !=  1 || p->ipdst[2] !=  1 || p->ipdst[3] != 24) return;
#endif

  if (p->ipsrc[0] != src[0] || p->ipsrc[1] != src[1] || p->ipsrc[2] != src[2] || p->ipsrc[3] != src[3]) {
    if (verbose) printf("reject 50000 because ip src is %d.%d.%d.%d (expected %d.%d.%d.%d)\n",
                        p->ipsrc[0], p->ipsrc[1], p->ipsrc[2], p->ipsrc[3],
                        src[0], src[1], src[2], src[3]);
    return;
  }
  if (p->ipdst[0] != dst[0] || p->ipdst[1] != dst[1] || p->ipdst[2] != dst[2] || p->ipdst[3] != dst[3]) {
    if (verbose) printf("reject 50000 because ip dst is %d.%d.%d.%d (expected %d.%d.%d.%d)\n",
                        p->ipdst[0], p->ipdst[1], p->ipdst[2], p->ipdst[3],
                        dst[0], dst[1], dst[2], dst[3]);
    return;
  }
  if (p->udp.portsrc != 50000 || p->udp.portdst != 50000) {
    if (verbose) printf("reject 50000 because bad ports (src %d dst %d) (wanted 50000 50000)\n",
                        p->udp.portsrc, p->udp.portdst);
  }

  if (p->udp.datalen != 153 && p->udp.datalen != 297) { printf("ERROR: unsupported len %d, only 153 or 297 for the moment\n", p->udp.datalen); return; /*exit(1);*/ }

  b = p->udp.data;

  sample_start = ((b[0] & 1) << 16) | (b[1] << 8) | b[2];
  timestamp = (b[5] << 24) | (b[6] << 16) | (b[7] << 8) | b[9];

if (sample_start == prev_sample_start) return;

  int cycle153[4] = { 1000192, 999936, 999936, 999936 };
  int cycle297[2] = { 1999872, 2000128 };
  int *cycle;
  int cycle_size;
  int sample_count;
  static int cycle_pos = 0;

  if (p->udp.datalen == 153) {
    cycle = cycle153;
    cycle_size = 4;
    sample_count = 48;
  } else
  if (p->udp.datalen == 297) {
    cycle = cycle297;
    cycle_size = 2;
    sample_count = 96;
  } else
    abort();

printf("sample_start %d timestamp %d prev %d diff %d\n", sample_start, timestamp, prev_timestamp, timestamp-prev_timestamp);

  bad = 0;

  if (sample_start != (prev_sample_start + sample_count) % 48000) { bad = 1; printf("ERROR: missing data?\n"); }

  if (timestamp != (prev_timestamp + cycle[cycle_pos]) % 1000000000) {
    printf("ERROR: bad cycle detected, reset\n");
    cycle_pos = 0;
    bad = 1;
  } else
    cycle_pos = (cycle_pos + 1) % cycle_size;

  printf("F %ld %ld %d %d\n", p->ts.tv_sec, p->ts.tv_usec, sample_start, timestamp);
  printf("T %ld\n", p->ts.tv_usec - p->ts.tv_sec - 1660761372303117L);


//  if (!bad) {
printf("prev %ld %ld cur %ld %ld\n", prev_ts.tv_sec, prev_ts.tv_usec, p->ts.tv_sec, p->ts.tv_usec);
    uint32_t diff = tdiff(p->ts, prev_ts);
    printf("G %d\n", diff);
//  }

  //printf("%d %d (%d)\n", sample_start, timestamp, timestamp - prev_timestamp);

#if 0
  if (p->udp.datalen == 153) {
printf("sample_start %d timestamp %d\n", sample_start, timestamp);

    int cycle[4] = { 1000192, 999936, 999936, 999936 };
    static int cycle_pos = 0;

    bad = 0;

    if (sample_start != (prev_sample_start + 48) % 48000) { bad = 1; printf("ERROR: missing data?\n"); }

    if (timestamp != (prev_timestamp + cycle[cycle_pos]) % 1000000000) {
      printf("ERROR: bad cycle detected, reset\n");
      cycle_pos = 0;
      bad = 1;
    } else
      cycle_pos = (cycle_pos + 1) % 4;

    printf("F %ld %ld %d %d\n", p->ts.tv_sec, p->ts.tv_usec, sample_start, timestamp);
    printf("T %ld\n", p->ts.tv_sec - p->ts.tv_usec);


//  if (!bad) {
printf("prev %ld %ld cur %ld %ld\n", prev_ts.tv_sec, prev_ts.tv_usec, p->ts.tv_sec, p->ts.tv_usec);
      uint32_t diff = tdiff(p->ts, prev_ts);
      printf("G %d\n", diff);
//  }

    //printf("%d %d (%d)\n", sample_start, timestamp, timestamp - prev_timestamp);
  }

  if (p->udp.datalen == 297) {
printf("sample_start %d timestamp %d prev %d diff %d\n", sample_start, timestamp, prev_timestamp, timestamp-prev_timestamp);

    int cycle[2] = { 1999872, 2000128 };
    static int cycle_pos = 0;

    bad = 0;

    if (sample_start != (prev_sample_start + 96) % 48000) { bad = 1; printf("ERROR: missing data?\n"); }

    if (timestamp != (prev_timestamp + cycle[cycle_pos]) % 1000000000) {
      printf("ERROR: bad cycle detected, reset\n");
      cycle_pos = 0;
      bad = 1;
    } else
      cycle_pos = (cycle_pos + 1) % 2;

    printf("F %ld %ld %d %d\n", p->ts.tv_sec, p->ts.tv_usec, sample_start, timestamp);


//  if (!bad) {
printf("prev %ld %ld cur %ld %ld\n", prev_ts.tv_sec, prev_ts.tv_usec, p->ts.tv_sec, p->ts.tv_usec);
      uint32_t diff = tdiff(p->ts, prev_ts);
      printf("G %d\n", diff);
//  }

    //printf("%d %d (%d)\n", sample_start, timestamp, timestamp - prev_timestamp);
  }
#endif

  prev_sample_start = sample_start;
  prev_timestamp = timestamp;
  prev_ts = p->ts;
}

void dump_9999(packet_t *p)
{
  const unsigned char *b;
  int i;

  if (p->type != UDP) return;

  if (p->ipsrc[0] != src[0] || p->ipsrc[1] != src[1] || p->ipsrc[2] != src[2] || p->ipsrc[3] != src[3]) {
    if (verbose) printf("reject 9999 because ip src is %d.%d.%d.%d (expected %d.%d.%d.%d)\n",
                        p->ipsrc[0], p->ipsrc[1], p->ipsrc[2], p->ipsrc[3],
                        src[0], src[1], src[2], src[3]);
    return;
  }
  if (p->ipdst[0] != dst[0] || p->ipdst[1] != dst[1] || p->ipdst[2] != dst[2] || p->ipdst[3] != dst[3]) {
    if (verbose) printf("reject 9999 because ip dst is %d.%d.%d.%d (expected %d.%d.%d.%d)\n",
                        p->ipdst[0], p->ipdst[1], p->ipdst[2], p->ipdst[3],
                        dst[0], dst[1], dst[2], dst[3]);
    return;
  }
  if (/*p->udp.portsrc != 9999 ||*/ p->udp.portdst != 9999) {
    if (verbose) printf("reject 9999 because bad ports (src %d dst %d) (wanted 50000 50000)\n",
                        p->udp.portsrc, p->udp.portdst);
  }

  b = p->udp.data;
  printf("M %ld.%ld :", p->ts.tv_sec, p->ts.tv_usec);
  for (i = 0; i < p->udp.datalen; i++) printf("%2.2x", b[i]);
  printf("\n");
}

void analyse_gtp(packet_t *p)
{
  const unsigned char *b;
  int gtplen;
  const unsigned char *next_header;
  const unsigned char *pdcp;
  int pdcplen;
  //int nr_u_seq_num;
  int pdcp_sn;
  int pdcp_sn2;
  packet_t ip;

  if (p->type != UDP) return;

  /* src is 172.21.6.9:2152 and dst is 172.21.16.120:2152 */
  if (p->ipsrc[0] != gtp_src[0] || p->ipsrc[1] != gtp_src[1] || p->ipsrc[2] != gtp_src[2] || p->ipsrc[3] != gtp_src[3]) {
    if (verbose) printf("reject gtp because ip src is %d.%d.%d.%d (expected %d.%d.%d.%d)\n",
                        p->ipsrc[0], p->ipsrc[1], p->ipsrc[2], p->ipsrc[3],
                        gtp_src[0], gtp_src[1], gtp_src[2], gtp_src[3]);
    return;
  }
  if (p->ipdst[0] != gtp_dst[0] || p->ipdst[1] != gtp_dst[1] || p->ipdst[2] != gtp_dst[2] || p->ipdst[3] != gtp_dst[3]) {
    if (verbose) printf("reject gtp because ip dst is %d.%d.%d.%d (expected %d.%d.%d.%d)\n",
                        p->ipdst[0], p->ipdst[1], p->ipdst[2], p->ipdst[3],
                        gtp_dst[0], gtp_dst[1], gtp_dst[2], gtp_dst[3]);
    return;
  }
  if (p->udp.portsrc != gtp_src_port || p->udp.portdst != gtp_dst_port) {
    if (verbose) printf("reject gtp because bad ports (src %d dst %d) (wanted %d %d)\n",
                        p->udp.portsrc, p->udp.portdst, gtp_src_port, gtp_dst_port);
    return;
  }

  b = p->udp.data;
  if (p->udp.datalen < 12) goto bad;

  if (b[0] != 0x34) goto bad;    /* 0x34 means (among other things) that next ext present */
  if (b[1] != 0xff) goto bad;
  gtplen = (b[2] << 8) | b[3];
  if (gtplen < 12 + 4) goto bad;

//printf("udplen %d gtplen %d\n", p->udp.datalen, gtplen);

  next_header = &b[12];

  if (next_header[-1] == 0x85) {
    if (next_header[0] != 1) goto bad;
    if (next_header[3]) goto bad;
    ip = parse_ip(next_header + 4, gtplen - 4 - 4);

    ip.ts = p->ts;

    if (ip.type != UDP) { printf("gtp: not udp\n"); goto bad; }

    analyse_50000(&ip);

    return;
  }

  if (next_header[-1] != 0x84) goto bad;
  if (next_header[0] != 0x03 ||
      next_header[1] != 0x00 ||
      next_header[2] != 0x0c) goto bad;

  //nr_u_seq_num = (next_header[3] << 16) | (next_header[4] << 8) | next_header[5];
  pdcp_sn = (next_header[6] << 16) | (next_header[7] << 8) | next_header[8];

  if (next_header[11]) goto bad;  /* no more header wanted */

  pdcp = next_header + 12;
  pdcplen = gtplen - 12 - 4;

  if (pdcplen < 3) goto bad;

  pdcp_sn2 = ((pdcp[0] & 0x03) << 16) | (pdcp[1] << 8) | pdcp[2];

  if (pdcp_sn != pdcp_sn2) goto bad;

  ip = parse_ip(pdcp + 3, pdcplen - 3);

  ip.ts = p->ts;

  if (ip.type != UDP) { printf("gtp: not udp\n"); goto bad; }

#if 0
  /* src is 172.21.15.11:50000 and dst is 12.1.1.24:50000 */
  if (ip.ipsrc[0] != 172 || ip.ipsrc[1] != 21 || ip.ipsrc[2] != 15 || ip.ipsrc[3] !=  11) return;
  if (ip.ipdst[0] !=  12 || ip.ipdst[1] !=  1 || ip.ipdst[2] !=  1 || ip.ipdst[3] !=  24) return;
  if (ip.udp.portsrc != 50000 || ip.udp.portdst != 50000) return;

printf("yo\n");
#endif

  analyse_50000(&ip);

  return;

bad:
  printf("ERROR: bad gtp\n");
  //dump_buffer(b, p->udp.datalen);
}

void analyse(packet_t *p)
{
  if (do_9999)
    dump_9999(p);
  else if (do_gtp)
    analyse_gtp(p);
  else
    analyse_50000(p);
}

void dump_packet(packet_t *p)
{
  printf("ip src %d.%d.%d.%d dst %d.%d.%d.%d\n",
         p->ipsrc[0],
         p->ipsrc[1],
         p->ipsrc[2],
         p->ipsrc[3],
         p->ipdst[0],
         p->ipdst[1],
         p->ipdst[2],
         p->ipdst[3]);
  if (p->type == UDP) printf("p %d %d\n", p->udp.portsrc, p->udp.portdst);
}

packet_t parse_tcpdump(const unsigned char *b, int l)
{
//dump_buffer(b, l);
  if (l < 18) { printf("packet too short\n"); exit(1); }
  if (b[14] != 0x08 || b[15] != 0) goto bad;

  return parse_ip(&b[16], l-16);

bad:
  printf("ERROR: bad tcpdump packet\n");
  dump_buffer(b, l);
  packet_t ret;
  ret.type = ERROR;
  return ret;
//  exit(1);
}

#ifndef INCLUDE_PCAP

void usage(void)
{
  printf("usage: [options] <pcap file>\n");
  printf("options:\n");
  printf("    -v\n");
  printf("        be verbose\n");
  printf("    -gtp\n");
  printf("        dig for gtp, not udp port 50000\n");
  printf("    -9999\n");
  printf("        dump 9999 udp traffic (macpdu)\n");
  printf("    -src <source IP (default 127.0.0.1)>\n");
  printf("    -dst <destination IP (default 127.0.0.1)>\n");
  printf("    -gtp-src <source IP (default 127.0.0.1)>\n");
  printf("    -gtp-dst <destination IP (default 127.0.0.1)>\n");
  printf("    -gtp-src-port <port (default 2152)>\n");
  printf("    -gtp-dst-port <port (default 2152)>\n");
  exit(1);
}

int main(int n, char **v)
{
  char errbuf[PCAP_ERRBUF_SIZE];
  char *fn = NULL;
  pcap_t *p;
  int i;
  struct pcap_pkthdr *h;
  const unsigned char *d;

  src[0] = 127;
  src[1] = 0;
  src[2] = 0;
  src[3] = 1;

  dst[0] = 127;
  dst[1] = 0;
  dst[2] = 0;
  dst[3] = 1;

  gtp_src[0] = 127;
  gtp_src[1] = 0;
  gtp_src[2] = 0;
  gtp_src[3] = 1;

  gtp_dst[0] = 127;
  gtp_dst[1] = 0;
  gtp_dst[2] = 0;
  gtp_dst[3] = 1;

  for (i = 1; i < n; i++) {
    if (!strcmp(v[i], "-v"))            { verbose = 1; continue; }
    if (!strcmp(v[i], "-gtp"))          { do_gtp = 1; continue; }
    if (!strcmp(v[i], "-9999"))         { do_9999 = 1; continue; }
    if (!strcmp(v[i], "-src"))          { if (i > n-2) usage(); i++; if (sscanf(v[i], "%d.%d.%d.%d", &src[0], &src[1], &src[2], &src[3]) != 4) usage(); continue; }
    if (!strcmp(v[i], "-dst"))          { if (i > n-2) usage(); i++; if (sscanf(v[i], "%d.%d.%d.%d", &dst[0], &dst[1], &dst[2], &dst[3]) != 4) usage(); continue; }
    if (!strcmp(v[i], "-gtp-src"))      { if (i > n-2) usage(); i++; if (sscanf(v[i], "%d.%d.%d.%d", &gtp_src[0], &gtp_src[1], &gtp_src[2], &gtp_src[3]) != 4) usage(); continue; }
    if (!strcmp(v[i], "-gtp-dst"))      { if (i > n-2) usage(); i++; if (sscanf(v[i], "%d.%d.%d.%d", &gtp_dst[0], &gtp_dst[1], &gtp_dst[2], &gtp_dst[3]) != 4) usage(); continue; }
    if (!strcmp(v[i], "-gtp-src-port")) { if (i > n-2) usage(); i++; if (sscanf(v[i], "%d", &gtp_src_port) != 1) usage(); continue; }
    if (!strcmp(v[i], "-gtp-dst-port")) { if (i > n-2) usage(); i++; if (sscanf(v[i], "%d", &gtp_dst_port) != 1) usage(); continue; }
    if (fn == NULL) { fn = v[i]; continue; }
    usage();
  }
  if (fn == NULL) usage();

  p = pcap_open_offline(fn, errbuf);
  if (p == NULL) { printf("%s\n", errbuf); exit(1); }

  int type = pcap_datalink(p);
  if (type != 113) { printf("only linux ssl supported\n"); return 1; }

  n = 0;
  while (pcap_next_ex(p, &h, &d) == 1) {
    packet_t p;
    int len = h->len;
    if (h->caplen < h->len) { printf("ERROR: what? (len %d caplen %d)\n", h->len, h->caplen); return 1; }
//printf("caplen %d len %d\n", h->caplen, h->len);

    p = parse_tcpdump(d, len);
    p.ts = h->ts;
    n++;
    if (p.fragmented) { dump_buffer(d, len); dump_packet(&p); continue; }
    analyse(&p);
  }
  printf("%d packets\n", n);

  return 0;
}

#endif /* INCLUDE_PCAP */
