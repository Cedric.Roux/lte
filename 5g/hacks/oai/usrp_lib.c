/*
 * Licensed to the OpenAirInterface (OAI) Software Alliance under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The OpenAirInterface Software Alliance licenses this file to You under
 * the OAI Public License, Version 1.1  (the "License"); you may not use this file
 * except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.openairinterface.org/?page_id=698
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *-------------------------------------------------------------------------------
 * For more information about the OpenAirInterface (OAI) Software Alliance:
 *      contact@openairinterface.org
 */

/** usrp_lib.cpp
 *
 * \author: HongliangXU : hong-liang-xu@agilent.com
 */
#define _LARGEFILE_SOURCE
#define _FILE_OFFSET_BITS 64
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#include <stdio.h>
#include <time.h>
#include <uhd.h>
#include <math.h>
#include <float.h>
#include "common/utils/LOG/log.h"
#include "common_lib.h"
#include "assertions.h"

#include "common/utils/LOG/vcd_signal_dumper.h"

#include <sys/resource.h>

#include "openair1/PHY/sse_intrin.h"

/** @addtogroup _USRP_PHY_RF_INTERFACE_
 * @{
 */
int gpio789=0;
extern int usrp_tx_thread;


typedef struct {

  // --------------------------------
  // variables for USRP configuration
  // --------------------------------
  //! USRP device pointer
  uhd_usrp_handle usrp;

  //create a send streamer and a receive streamer
  //! USRP TX Stream
  uhd_tx_streamer_handle tx_stream;
  //! USRP RX Stream
  uhd_rx_streamer_handle rx_stream;

  //! USRP TX Metadata
  //uhd_tx_metadata_handle tx_md;
  //! USRP RX Metadata
  //uhd_rx_metadata_handle rx_md;
  int last_tx_eob;

  //! Sampling rate
  double sample_rate;

  //! TX forward samples. We use usrp_time_offset to get this value
  int tx_forward_nsamps; //166 for 20Mhz

  // --------------------------------
  // Debug and output control
  // --------------------------------
  int num_underflows;
  int num_overflows;
  int num_seq_errors;
  int64_t tx_count;
  int64_t rx_count;
  int wait_for_first_pps;
  int use_gps;
  //int first_tx;
  //int first_rx;
  //! timestamp of RX packet
  openair0_timestamp rx_timestamp;
} usrp_state_t;

#define R(x) \
  do { \
    if (x) { \
      char error_string[512]; \
      if (s->usrp != NULL) \
        uhd_usrp_last_error(s->usrp, error_string, 512); \
      else \
        strcpy(error_string, "unknown error"); \
      printf("%s:%d:%s: error (%s)\n", __FILE__, __LINE__, __FUNCTION__, error_string); \
      fflush(stdout); \
      exit(1); \
    } \
  } while (0)

//void print_notes(void)
//{
// Helpful notes
//  std::cout << boost::format("**************************************Helpful Notes on Clock/PPS Selection**************************************\n");
//  std::cout << boost::format("As you can see, the default 10 MHz Reference and 1 PPS signals are now from the GPSDO.\n");
//  std::cout << boost::format("If you would like to use the internal reference(TCXO) in other applications, you must configure that explicitly.\n");
//  std::cout << boost::format("You can no longer select the external SMAs for 10 MHz or 1 PPS signaling.\n");
//  std::cout << boost::format("****************************************************************************************************************\n");
//}

int check_ref_locked(usrp_state_t *s,size_t mboard) {
printf("TODO\n");
exit(1);
#if 0
  std::vector<std::string> sensor_names = s->usrp->get_mboard_sensor_names(mboard);
  bool ref_locked = false;

  if(std::find(sensor_names.begin(), sensor_names.end(), "ref_locked") != sensor_names.end()) {
    std::cout << "Waiting for reference lock..." << std::flush;

    for (int i = 0; i < 30 and not ref_locked; i++) {
      ref_locked = s->usrp->get_mboard_sensor("ref_locked", mboard).to_bool();

      if (not ref_locked) {
        std::cout << "." << std::flush;
        boost::this_thread::sleep(boost::posix_time::seconds(1));
      }
    }

    if(ref_locked) {
      std::cout << "LOCKED" << std::endl;
    } else {
      std::cout << "FAILED" << std::endl;
    }
  } else {
    std::cout << boost::format("ref_locked sensor not present on this board.\n");
  }

  return ref_locked;
#endif
}

static int sync_to_gps(openair0_device *device) {
printf("TODO\n");
exit(1);
#if 0
  //uhd::set_thread_priority_safe();
  //std::string args;
  //Set up program options
  //po::options_description desc("Allowed options");
  //desc.add_options()
  //("help", "help message")
  //("args", po::value<std::string>(&args)->default_value(""), "USRP device arguments")
  //;
  //po::variables_map vm;
  //po::store(po::parse_command_line(argc, argv, desc), vm);
  //po::notify(vm);
  //Print the help message
  //if (vm.count("help"))
  //{
  //  std::cout << boost::format("Synchronize USRP to GPS %s") % desc << std::endl;
  // return EXIT_FAILURE;
  //}
  //Create a USRP device
  //std::cout << boost::format("\nCreating the USRP device with: %s...\n") % args;
  //uhd::usrp::multi_usrp::sptr usrp = uhd::usrp::multi_usrp::make(args);
  //std::cout << boost::format("Using Device: %s\n") % usrp->get_pp_string();
  usrp_state_t *s = (usrp_state_t *)device->priv;

  try {
    size_t num_mboards = s->usrp->get_num_mboards();
    size_t num_gps_locked = 0;

    for (size_t mboard = 0; mboard < num_mboards; mboard++) {
      std::cout << "Synchronizing mboard " << mboard << ": " << s->usrp->get_mboard_name(mboard) << std::endl;
      bool ref_locked = check_ref_locked(s,mboard);

      if (ref_locked) {
        std::cout << boost::format("Ref Locked\n");
      } else {
        std::cout << "Failed to lock to GPSDO 10 MHz Reference. Exiting." << std::endl;
        exit(EXIT_FAILURE);
      }

      //Wait for GPS lock
      bool gps_locked = s->usrp->get_mboard_sensor("gps_locked", mboard).to_bool();

      if(gps_locked) {
        num_gps_locked++;
        std::cout << boost::format("GPS Locked\n");
      } else {
        LOG_W(HW,"WARNING:  GPS not locked - time will not be accurate until locked\n");
      }

      //Set to GPS time
      uhd::time_spec_t gps_time = uhd::time_spec_t(time_t(s->usrp->get_mboard_sensor("gps_time", mboard).to_int()));
      s->usrp->set_time_next_pps(gps_time+1.0, mboard);
      //s->usrp->set_time_next_pps(uhd::time_spec_t(0.0));
      
      //Wait for it to apply
      //The wait is 2 seconds because N-Series has a known issue where
      //the time at the last PPS does not properly update at the PPS edge
      //when the time is actually set.
      boost::this_thread::sleep(boost::posix_time::seconds(2));
      //Check times
      gps_time = uhd::time_spec_t(time_t(s->usrp->get_mboard_sensor("gps_time", mboard).to_int()));
      uhd::time_spec_t time_last_pps = s->usrp->get_time_last_pps(mboard);
      std::cout << "USRP time: " << (boost::format("%0.9f") % time_last_pps.get_real_secs()) << std::endl;
      std::cout << "GPSDO time: " << (boost::format("%0.9f") % gps_time.get_real_secs()) << std::endl;
      if (gps_time.get_real_secs() == time_last_pps.get_real_secs())
          std::cout << std::endl << "SUCCESS: USRP time synchronized to GPS time" << std::endl << std::endl;
      else
          std::cerr << std::endl << "ERROR: Failed to synchronize USRP time to GPS time" << std::endl << std::endl;
    }

    if (num_gps_locked == num_mboards and num_mboards > 1) {
      //Check to see if all USRP times are aligned
      //First, wait for PPS.
      uhd::time_spec_t time_last_pps = s->usrp->get_time_last_pps();

      while (time_last_pps == s->usrp->get_time_last_pps()) {
        boost::this_thread::sleep(boost::posix_time::milliseconds(1));
      }

      //Sleep a little to make sure all devices have seen a PPS edge
      boost::this_thread::sleep(boost::posix_time::milliseconds(200));
      //Compare times across all mboards
      bool all_matched = true;
      uhd::time_spec_t mboard0_time = s->usrp->get_time_last_pps(0);

      for (size_t mboard = 1; mboard < num_mboards; mboard++) {
        uhd::time_spec_t mboard_time = s->usrp->get_time_last_pps(mboard);

        if (mboard_time != mboard0_time) {
          all_matched = false;
          std::cerr << (boost::format("ERROR: Times are not aligned: USRP 0=%0.9f, USRP %d=%0.9f")
                        % mboard0_time.get_real_secs()
                        % mboard
                        % mboard_time.get_real_secs()) << std::endl;
        }
      }

      if (all_matched) {
        std::cout << "SUCCESS: USRP times aligned" << std::endl << std::endl;
      } else {
        std::cout << "ERROR: USRP times are not aligned" << std::endl << std::endl;
      }
    }
  } catch (std::exception &e) {
    std::cout << boost::format("\nError: %s") % e.what();
    std::cout << boost::format("This could mean that you have not installed the GPSDO correctly.\n\n");
    std::cout << boost::format("Visit one of these pages if the problem persists:\n");
    std::cout << boost::format(" * N2X0/E1X0: http://files.ettus.com/manual/page_gpsdo.html");
    std::cout << boost::format(" * X3X0: http://files.ettus.com/manual/page_gpsdo_x3x0.html\n\n");
    std::cout << boost::format(" * E3X0: http://files.ettus.com/manual/page_usrp_e3x0.html#e3x0_hw_gps\n\n");
    exit(EXIT_FAILURE);
  }

  return EXIT_SUCCESS;
#endif
}

/*! \brief Called to start the USRP transceiver. Return 0 if OK, < 0 if error
    @param device pointer to the device structure specific to the RF hardware target
*/
static int trx_usrp_start(openair0_device *device) {
  usrp_state_t *s = (usrp_state_t *)device->priv;

  if (device->type != USRP_X400_DEV) {
    // setup GPIO for TDD, GPIO(4) = ATR_RX
    //set data direction register (DDR) to output
    //s->usrp->set_gpio_attr("FP0", "DDR", 0xfff, 0xfff);
    R(uhd_usrp_set_gpio_attr(s->usrp, "FP0", "DDR", 0xfff, 0xfff, 0));
    //set lower 7 bits to be controlled automatically by ATR (the rest 5 bits are controlled manually)
    //s->usrp->set_gpio_attr("FP0", "CTRL", 0x7f,0xfff);
    R(uhd_usrp_set_gpio_attr(s->usrp, "FP0", "CTRL", 0x7f, 0xfff, 0));
    //set pins 4 (RX_TX_Switch) and 6 (Shutdown PA) to 1 when the radio is only receiving (ATR_RX)
    //s->usrp->set_gpio_attr("FP0", "ATR_RX", (1<<4)|(1<<6), 0x7f);
    R(uhd_usrp_set_gpio_attr(s->usrp, "FP0", "ATR_RX", (1<<4)|(1<<6), 0x7f, 0));
    // set pin 5 (Shutdown LNA) to 1 when the radio is transmitting and receiveing (ATR_XX)
    // (we use full duplex here, because our RX is on all the time - this might need to change later)
    //s->usrp->set_gpio_attr("FP0", "ATR_XX", (1<<5), 0x7f);
    R(uhd_usrp_set_gpio_attr(s->usrp, "FP0", "ATR_XX", (1<<5), 0x7f, 0));
    // set the output pins to 1
    //s->usrp->set_gpio_attr("FP0", "OUT", 7<<7, 0xf80);
    R(uhd_usrp_set_gpio_attr(s->usrp, "FP0", "OUT", 7<<7, 0xf80, 0));
  }

  s->wait_for_first_pps = 1;
  s->rx_count = 0;
  s->tx_count = 0;
  //s->first_tx = 1;
  //s->first_rx = 1;
  s->rx_timestamp = 0;

    //wait for next pps
#if 0
  uhd::time_spec_t last_pps = s->usrp->get_time_last_pps();
  uhd::time_spec_t current_pps = s->usrp->get_time_last_pps();
  while(current_pps == last_pps) {
    boost::this_thread::sleep(boost::posix_time::milliseconds(1));
    current_pps = s->usrp->get_time_last_pps();
  }
#endif
  int64_t last_pps_sec;
  double last_pps_frac;
  int64_t current_pps_sec;
  double current_pps_frac;
  R(uhd_usrp_get_time_last_pps(s->usrp, 0, &last_pps_sec, &last_pps_frac));
  R(uhd_usrp_get_time_last_pps(s->usrp, 0, &current_pps_sec, &current_pps_frac));
  while (last_pps_sec == current_pps_sec && last_pps_frac == current_pps_frac) {
    usleep(1000);
    R(uhd_usrp_get_time_last_pps(s->usrp, 0, &current_pps_sec, &current_pps_frac));
  }

  LOG_I(HW,"current pps at %f, starting streaming at %f\n",current_pps_sec + current_pps_frac, current_pps_sec + current_pps_frac + 1.0);

#if 0
  uhd::stream_cmd_t cmd(uhd::stream_cmd_t::STREAM_MODE_START_CONTINUOUS);
  cmd.time_spec = uhd::time_spec_t(current_pps+1.0);
  cmd.stream_now = false; // start at constant delay
  s->rx_stream->issue_stream_cmd(cmd);
#endif
  uhd_stream_cmd_t cmd;
  cmd.stream_mode = UHD_STREAM_MODE_START_CONTINUOUS;
  cmd.num_samps = 0;
  cmd.stream_now = false;
  cmd.time_spec_full_secs = current_pps_sec + 1;
  cmd.time_spec_frac_secs = current_pps_frac;
  R(uhd_rx_streamer_issue_stream_cmd(s->rx_stream, &cmd));

  return 0;
}

static void trx_usrp_send_end_of_burst(usrp_state_t *s)
{
  uhd_tx_metadata_handle h;

  // if last packet sent was end of burst no need to do anything. otherwise send end of burst packet
  if (s->last_tx_eob == 1)
    return;

#if 0
  s->tx_md.end_of_burst = true;
  s->tx_md.start_of_burst = false;
  s->tx_md.has_time_spec = false;
  s->tx_stream->send("", 0, s->tx_md);
#endif

  R(uhd_tx_metadata_make(&h,
                         false,    /* has_time_spec */
                         0,        /* full_secs */
                         0,        /* frac_secs */
                         false,    /* start_of_burst */
                         true));   /* end_of_burst */

  size_t ret;
  size_t n;
  R(uhd_tx_streamer_num_channels(s->tx_stream, &n));
  const void *buf[n];
  int i;
  for (i = 0; i < n; i++)
    buf[i] = "";
  R(uhd_tx_streamer_send(s->tx_stream, buf, 0, &h, 0.1, &ret));

  R(uhd_tx_metadata_free(&h));
}

static void trx_usrp_finish_rx(usrp_state_t *s)
{
  /* finish rx by sending STREAM_MODE_STOP_CONTINUOUS */
#if 0
  uhd::stream_cmd_t cmd(uhd::stream_cmd_t::STREAM_MODE_STOP_CONTINUOUS);
  s->rx_stream->issue_stream_cmd(cmd);

  /* collect all remaining samples (not sure if needed) */
  size_t samples;
  do {
    uint8_t buf[1024];
    samples = s->rx_stream->recv((void*)buf, sizeof(buf)/4, s->rx_md);
  } while (samples > 0);
#endif

  uhd_stream_cmd_t cmd;
  cmd.stream_mode = UHD_STREAM_MODE_STOP_CONTINUOUS;
  cmd.num_samps = 0;
  cmd.stream_now = true;
  cmd.time_spec_full_secs = 0;
  cmd.time_spec_frac_secs = 0;
  R(uhd_rx_streamer_issue_stream_cmd(s->rx_stream, &cmd));

  /* consume remaining samples */
  uint8_t _buf[1024];
  size_t ret;
  size_t n;
  uhd_rx_metadata_handle h;
  R(uhd_rx_metadata_make(&h));
  R(uhd_rx_streamer_num_channels(s->rx_stream, &n));
  void *buf[n];
  int i;
  for (i = 0; i < n; i++)
    buf[i] = _buf;
  do {
    R(uhd_rx_streamer_recv(s->rx_stream, buf, 1024/4, &h, 0.1, false, &ret));
  } while (ret > 0);
  R(uhd_rx_metadata_free(&h));
}

static void trx_usrp_write_reset(openair0_thread_t *wt);

#define time_spec_init(full, frac, _full_secs, _frac_secs) \
    {                                                      \
        const int64_t _full = (int64_t)(full);             \
        const double _frac  = (double)(frac);              \
        const int _frac_int = (int)(_frac);                \
        _full_secs          = _full + _frac_int;           \
        _frac_secs          = _frac - _frac_int;           \
        if (_frac_secs < 0) {                              \
            _full_secs -= 1;                               \
            _frac_secs += 1;                               \
        }                                                  \
    }

static void from_ticks(long long ticks, double tick_rate,
                       int64_t *ret_sec, double *ret_frac)
{
    const long long rate_i      = (long long)(tick_rate);
    const double rate_f         = tick_rate - rate_i;
    const int64_t secs_full     = ticks / rate_i;
    const long long ticks_error = ticks - secs_full * rate_i;
    const double ticks_frac     = ticks_error - secs_full * rate_f;
    time_spec_init(secs_full, ticks_frac / tick_rate, *ret_sec, *ret_frac);
}

static long long fast_llround(const double x)
{
    return (long long)(x + 0.5);
}

static long long to_ticks(int64_t full_secs, double frac_secs, double tick_rate)
{
    const long long rate_i     = (long long)(tick_rate);
    const double rate_f        = tick_rate - rate_i;
    const long long ticks_full = full_secs * rate_i;
    const double ticks_error   = full_secs * rate_f;
    const double ticks_frac    = frac_secs * tick_rate;
    return ticks_full + fast_llround(ticks_error + ticks_frac);
}

/*! \brief Terminate operation of the USRP transceiver -- free all associated resources
 * \param device the hardware to use
 */
static void trx_usrp_end(openair0_device *device) {
  if (device == NULL)
    return;

  usrp_state_t *s = (usrp_state_t *)device->priv;

  AssertFatal(s != NULL, "%s() called on uninitialized USRP\n", __func__);
  iqrecorder_end(device);

  LOG_I(HW, "releasing USRP\n");
  if (usrp_tx_thread != 0)
    trx_usrp_write_reset(&device->write_thread);

  /* finish tx and rx */
  trx_usrp_send_end_of_burst(s);
  trx_usrp_finish_rx(s);
  R(uhd_rx_streamer_free(&s->rx_stream));
  R(uhd_tx_streamer_free(&s->tx_stream));
  R(uhd_usrp_free(&s->usrp));
  s->tx_stream = NULL;
  s->rx_stream = NULL;
  s->usrp = NULL;
  free(s);
  device->priv = NULL;
  device->trx_start_func = NULL;
  device->trx_get_stats_func = NULL;
  device->trx_reset_stats_func = NULL;
  device->trx_end_func = NULL;
  device->trx_stop_func = NULL;
  device->trx_set_freq_func = NULL;
  device->trx_set_gains_func = NULL;
  device->trx_write_init = NULL;
}

/*! \brief Called to send samples to the USRP RF target
      @param device pointer to the device structure specific to the RF hardware target
      @param timestamp The timestamp at which the first sample MUST be sent
      @param buff Buffer which holds the samples
      @param nsamps number of samples to be sent
      @param antenna_id index of the antenna if the device has multiple antennas
      @param flags flags must be set to true if timestamp parameter needs to be applied
*/
static int trx_usrp_write(openair0_device *device,
			  openair0_timestamp timestamp,
			  void **buff,
			  int nsamps,
			  int cc,
			  int flags) {
  //int ret=0;
  usrp_state_t *s = (usrp_state_t *)device->priv;
  int nsamps2;  // aligned to upper 32 or 16 byte boundary

  int flags_lsb = flags&0xff;
  int flags_msb = (flags>>8)&0xff;

  int end;
  openair0_thread_t *write_thread = &device->write_thread;
  openair0_write_package_t *write_package = write_thread->write_package;

  AssertFatal( MAX_WRITE_THREAD_BUFFER_SIZE >= cc,"Do not support more than %d cc number\n", MAX_WRITE_THREAD_BUFFER_SIZE);

  bool first_packet_state=false,last_packet_state=false;

    if (flags_lsb == 2) { // start of burst
      //      s->tx_md.start_of_burst = true;
      //      s->tx_md.end_of_burst = false;
      first_packet_state = true;
      last_packet_state  = false;
    } else if (flags_lsb == 3) { // end of burst
      //s->tx_md.start_of_burst = false;
      //s->tx_md.end_of_burst = true;
      first_packet_state = false;
      last_packet_state  = true;
    } else if (flags_lsb == 4) { // start and end
    //  s->tx_md.start_of_burst = true;
    //  s->tx_md.end_of_burst = true;
      first_packet_state = true;
      last_packet_state  = true;
    } else if (flags_lsb==1) { // middle of burst
    //  s->tx_md.start_of_burst = false;
    //  s->tx_md.end_of_burst = false;
      first_packet_state = false;
      last_packet_state  = false;
    }
    else if (flags_lsb==10) { // fail safe mode
     // s->tx_md.has_time_spec = false;
     // s->tx_md.start_of_burst = false;
     // s->tx_md.end_of_burst = true;
     first_packet_state = false;
     last_packet_state  = true;
    }

  if(usrp_tx_thread == 0){
#if defined(__x86_64) || defined(__i386__)
      nsamps2 = (nsamps+7)>>3;
      __m256i buff_tx[cc<2?2:cc][nsamps2];
#elif defined(__arm__) || defined(__aarch64__)
    nsamps2 = (nsamps+3)>>2;
    int16x8_t buff_tx[cc<2?2:cc][nsamps2];
#else
#error Unsupported CPU architecture, USRP device cannot be built
#endif

    // bring RX data into 12 LSBs for softmodem RX
    for (int i=0; i<cc; i++) {
      for (int j=0; j<nsamps2; j++) {
#if defined(__x86_64__) || defined(__i386__)
        if ((((uintptr_t) buff[i])&0x1F)==0) {
          buff_tx[i][j] = simde_mm256_slli_epi16(((__m256i *)buff[i])[j],4);
        }
        else 
        {
          __m256i tmp = simde_mm256_loadu_si256(((__m256i *)buff[i])+j);
          buff_tx[i][j] = simde_mm256_slli_epi16(tmp,4);
        }
#elif defined(__arm__) || defined(__aarch64__)
        buff_tx[i][j] = vshlq_n_s16(((int16x8_t *)buff[i])[j],4);
#endif
      }
    }

#if 0
    s->tx_md.has_time_spec  = true;
    s->tx_md.start_of_burst = (s->tx_count==0) ? true : first_packet_state;
    s->tx_md.end_of_burst   = last_packet_state;
    s->tx_md.time_spec      = uhd::time_spec_t::from_ticks(timestamp, s->sample_rate);
    s->tx_count++;

    s->last_tx_eob = s->tx_md.end_of_burst;

VCD_SIGNAL_DUMPER_DUMP_FUNCTION_BY_NAME(VCD_SIGNAL_DUMPER_FUNCTIONS_BEAM_SWITCHING_GPIO,1);
    // bit 3 enables gpio (for backward compatibility)
    if (flags_msb&8) {
      // push GPIO bits 7-9 from flags_msb
      int gpio789=(flags_msb&7)<<7;
      s->usrp->set_command_time(s->tx_md.time_spec);
      s->usrp->set_gpio_attr("FP0", "OUT", gpio789, 0x380);
      s->usrp->clear_command_time();
    }
VCD_SIGNAL_DUMPER_DUMP_FUNCTION_BY_NAME(VCD_SIGNAL_DUMPER_FUNCTIONS_BEAM_SWITCHING_GPIO,0);

    if (cc>1) {
      std::vector<void *> buff_ptrs;

      for (int i=0; i<cc; i++)
        buff_ptrs.push_back(&(((int16_t *)buff_tx[i])[0]));

      ret = (int)s->tx_stream->send(buff_ptrs, nsamps, s->tx_md);
    }
    else {
      ret = (int)s->tx_stream->send(&(((int16_t *)buff_tx[0])[0]), nsamps, s->tx_md);
    }
#endif
    uhd_tx_metadata_handle h;

    int64_t full_secs;
    double frac_secs;
    from_ticks(timestamp, s->sample_rate, &full_secs, &frac_secs);

    R(uhd_tx_metadata_make(&h,
                           true,                                           /* has_time_spec */
                           full_secs,                                      /* full_secs */
                           frac_secs,                                      /* frac_secs */
                           (s->tx_count==0) ? true : first_packet_state,   /* start_of_burst */
                           last_packet_state));                            /* end_of_burst */

    size_t ret;
    size_t n;
    R(uhd_tx_streamer_num_channels(s->tx_stream, &n));
    AssertFatal(n == cc, "bad number of channels\n");
    const void *buf[n];
    int i;
    for (i = 0; i < n; i++)
      buf[i] = buff_tx[i];
#if 0
VCD_SIGNAL_DUMPER_DUMP_FUNCTION_BY_NAME(VCD_SIGNAL_DUMPER_FUNCTIONS_BEAM_SWITCHING_GPIO,1);
    // bit 3 enables gpio (for backward compatibility)
    if (flags_msb&8) {
      // push GPIO bits 7-9 from flags_msb
      int gpio789=(flags_msb&7)<<7;
      s->usrp->set_command_time(s->tx_md.time_spec);
      s->usrp->set_gpio_attr("FP0", "OUT", gpio789, 0x380);
      s->usrp->clear_command_time();
    }
VCD_SIGNAL_DUMPER_DUMP_FUNCTION_BY_NAME(VCD_SIGNAL_DUMPER_FUNCTIONS_BEAM_SWITCHING_GPIO,0);
#endif
    R(uhd_tx_streamer_send(s->tx_stream, buf, nsamps, &h, 0.1, &ret));

    R(uhd_tx_metadata_free(&h));

    s->tx_count++;

    s->last_tx_eob = last_packet_state;

    if (ret != nsamps) LOG_E(HW,"[xmit] tx samples %d != %d\n",(int)ret,nsamps);
    return ret;
  }
  else{
    pthread_mutex_lock(&write_thread->mutex_write);

    if(write_thread->count_write >= MAX_WRITE_THREAD_PACKAGE){
      LOG_W(HW,"Buffer overflow, count_write = %d, start = %d end = %d, resetting write package\n", write_thread->count_write, write_thread->start, write_thread->end);
      write_thread->end = write_thread->start;
      write_thread->count_write = 0;
    }

    end = write_thread->end;
    write_package[end].timestamp    = timestamp;
    write_package[end].nsamps       = nsamps;
    write_package[end].cc           = cc;
    write_package[end].first_packet = first_packet_state;
    write_package[end].last_packet  = last_packet_state;
    write_package[end].flags_msb    = flags_msb;
    for (int i = 0; i < cc; i++)
      write_package[end].buff[i]    = buff[i];
    write_thread->count_write++;
    write_thread->end = (write_thread->end + 1)% MAX_WRITE_THREAD_PACKAGE;
    LOG_D(HW,"Signaling TX TS %llu\n",(unsigned long long)timestamp);
    pthread_cond_signal(&write_thread->cond_write);
    pthread_mutex_unlock(&write_thread->mutex_write);
    return 0;
  }

}

//-----------------------start--------------------------
/*! \brief Called to send samples to the USRP RF target
      @param device pointer to the device structure specific to the RF hardware target
      @param timestamp The timestamp at which the first sample MUST be sent
      @param buff Buffer which holds the samples
      @param nsamps number of samples to be sent
      @param antenna_id index of the antenna if the device has multiple antennas
      @param flags flags must be set to true if timestamp parameter needs to be applied
*/
void *trx_usrp_write_thread(void * arg){
  //int ret=0;
  openair0_device *device=(openair0_device *)arg;
  openair0_thread_t *write_thread = &device->write_thread;
  openair0_write_package_t *write_package = write_thread->write_package;

  usrp_state_t *s;
  int nsamps2;  // aligned to upper 32 or 16 byte boundary
  int start;
  openair0_timestamp timestamp;
  void               **buff;
  int                nsamps;
  int                cc;
  signed char        first_packet;
  signed char        last_packet;
  int                flags_msb;

  while(1){
    pthread_mutex_lock(&write_thread->mutex_write);
    while (write_thread->count_write == 0) {
      pthread_cond_wait(&write_thread->cond_write,&write_thread->mutex_write); // this unlocks mutex_rxtx while waiting and then locks it again
    }
    if (write_thread->write_thread_exit)
      break;
    VCD_SIGNAL_DUMPER_DUMP_FUNCTION_BY_NAME( VCD_SIGNAL_DUMPER_FUNCTIONS_TRX_WRITE_THREAD, 1 );
    s = (usrp_state_t *)device->priv;
    start = write_thread->start;
    timestamp    = write_package[start].timestamp;
    buff         = write_package[start].buff;
    nsamps       = write_package[start].nsamps;
    cc           = write_package[start].cc;
    first_packet = write_package[start].first_packet;
    last_packet  = write_package[start].last_packet;
    flags_msb    = write_package[start].flags_msb;
    write_thread->start = (write_thread->start + 1)% MAX_WRITE_THREAD_PACKAGE;
    write_thread->count_write--;
    pthread_mutex_unlock(&write_thread->mutex_write);
    /*if(write_thread->count_write != 0){
      LOG_W(HW,"count write = %d, start = %d, end = %d\n", write_thread->count_write, write_thread->start, write_thread->end);
    }*/

    #if defined(__x86_64) || defined(__i386__)
        nsamps2 = (nsamps+7)>>3;
        __m256i buff_tx[cc<2?2:cc][nsamps2];
    #elif defined(__arm__) || defined(__aarch64__)
      nsamps2 = (nsamps+3)>>2;
      int16x8_t buff_tx[cc<2?2:cc][nsamps2];
    #else
    #error Unsupported CPU architecture, USRP device cannot be built
    #endif

    // bring RX data into 12 LSBs for softmodem RX
    for (int i=0; i<cc; i++) {
      for (int j=0; j<nsamps2; j++) {
        #if defined(__x86_64__) || defined(__i386__)
            if ((((uintptr_t) buff[i])&0x1F)==0) {
              buff_tx[i][j] = simde_mm256_slli_epi16(((__m256i *)buff[i])[j],4);
            }
            else
            {
              __m256i tmp = simde_mm256_loadu_si256(((__m256i *)buff[i])+j);
              buff_tx[i][j] = simde_mm256_slli_epi16(tmp,4);
            }
        #elif defined(__arm__) || defined(__aarch64__)
          buff_tx[i][j] = vshlq_n_s16(((int16x8_t *)buff[i])[j],4);
        #endif
      }
    }

#if 0
    s->tx_md.has_time_spec  = true;
    s->tx_md.start_of_burst = (s->tx_count==0) ? true : first_packet;
    s->tx_md.end_of_burst   = last_packet;
    s->tx_md.time_spec      = uhd::time_spec_t::from_ticks(timestamp, s->sample_rate);
    LOG_D(PHY,"usrp_tx_write: tx_count %llu SoB %d, EoB %d, TS %llu\n",(unsigned long long)s->tx_count,s->tx_md.start_of_burst,s->tx_md.end_of_burst,(unsigned long long)timestamp); 
    s->tx_count++;

    s->last_tx_eob = last_packet;

    // bit 3 enables gpio (for backward compatibility)
    if (flags_msb&8) {
      // push GPIO bits 7-9 from flags_msb
      int gpio789=(flags_msb&7)<<7;
      s->usrp->set_command_time(s->tx_md.time_spec);
      s->usrp->set_gpio_attr("FP0", "OUT", gpio789, 0x380);
      s->usrp->clear_command_time();
    }

    if (cc>1) {
      std::vector<void *> buff_ptrs;

      for (int i=0; i<cc; i++)
        buff_ptrs.push_back(&(((int16_t *)buff_tx[i])[0]));

      ret = (int)s->tx_stream->send(buff_ptrs, nsamps, s->tx_md);
    }
    else {
      ret = (int)s->tx_stream->send(&(((int16_t *)buff_tx[0])[0]), nsamps, s->tx_md);
    }
#endif
    uhd_tx_metadata_handle h;

    int64_t full_secs;
    double frac_secs;
    from_ticks(timestamp, s->sample_rate, &full_secs, &frac_secs);

    R(uhd_tx_metadata_make(&h,
                           true,                                     /* has_time_spec */
                           full_secs,                                /* full_secs */
                           frac_secs,                                /* frac_secs */
                           (s->tx_count==0) ? true : first_packet,   /* start_of_burst */
                           last_packet));                            /* end_of_burst */

    size_t ret;
    size_t n;
    R(uhd_tx_streamer_num_channels(s->tx_stream, &n));
    AssertFatal(n == cc, "bad number of channels\n");
    const void *buf[n];
    int i;
    for (i = 0; i < n; i++)
      buf[i] = buff_tx[i];
#if 0
VCD_SIGNAL_DUMPER_DUMP_FUNCTION_BY_NAME(VCD_SIGNAL_DUMPER_FUNCTIONS_BEAM_SWITCHING_GPIO,1);
    // bit 3 enables gpio (for backward compatibility)
    if (flags_msb&8) {
      // push GPIO bits 7-9 from flags_msb
      int gpio789=(flags_msb&7)<<7;
      s->usrp->set_command_time(s->tx_md.time_spec);
      s->usrp->set_gpio_attr("FP0", "OUT", gpio789, 0x380);
      s->usrp->clear_command_time();
    }
VCD_SIGNAL_DUMPER_DUMP_FUNCTION_BY_NAME(VCD_SIGNAL_DUMPER_FUNCTIONS_BEAM_SWITCHING_GPIO,0);
#endif
    if (flags_msb&8) printf("gcc shutup\n");
    R(uhd_tx_streamer_send(s->tx_stream, buf, nsamps, &h, 0.1, &ret));

    R(uhd_tx_metadata_free(&h));

    s->tx_count++;

    s->last_tx_eob = last_packet;

    if (ret != nsamps) LOG_E(HW,"[xmit] tx samples %d != %d\n",(int)ret,nsamps);
    VCD_SIGNAL_DUMPER_DUMP_VARIABLE_BY_NAME( VCD_SIGNAL_DUMPER_VARIABLES_USRP_SEND_RETURN, ret );
    VCD_SIGNAL_DUMPER_DUMP_FUNCTION_BY_NAME( VCD_SIGNAL_DUMPER_FUNCTIONS_TRX_WRITE_THREAD, 0 );

  }

  return NULL;
}

int trx_usrp_write_init(openair0_device *device){

  //uhd::set_thread_priority_safe(1.0);
  openair0_thread_t *write_thread = &device->write_thread;
  printf("initializing tx write thread\n");

  write_thread->start              = 0;
  write_thread->end                = 0;
  write_thread->count_write        = 0;
  write_thread->write_thread_exit  = false;
  printf("end of tx write thread\n");
  pthread_mutex_init(&write_thread->mutex_write, NULL);
  pthread_cond_init(&write_thread->cond_write, NULL);
  pthread_create(&write_thread->pthread_write,NULL,trx_usrp_write_thread,(void *)device);

  return(0);
}

static void trx_usrp_write_reset(openair0_thread_t *wt) {
  pthread_mutex_lock(&wt->mutex_write);
  wt->count_write = 1;
  wt->write_thread_exit = true;
  pthread_cond_signal(&wt->cond_write);
  pthread_mutex_unlock(&wt->mutex_write);
  void *retval = NULL;
  pthread_join(wt->pthread_write, &retval);
  LOG_I(HW, "stopped USRP write thread\n");
}

//---------------------end-------------------------

/*! \brief Receive samples from hardware.
 * Read \ref nsamps samples from each channel to buffers. buff[0] is the array for
 * the first channel. *ptimestamp is the time at which the first sample
 * was received.
 * \param device the hardware to use
 * \param[out] ptimestamp the time at which the first sample was received.
 * \param[out] buff An array of pointers to buffers for received samples. The buffers must be large enough to hold the number of samples \ref nsamps.
 * \param nsamps Number of samples. One sample is 2 byte I + 2 byte Q => 4 byte.
 * \param antenna_id Index of antenna for which to receive samples
 * \returns the number of sample read
*/
static int trx_usrp_read(openair0_device *device, openair0_timestamp *ptimestamp, void **buff, int nsamps, int cc) {
  usrp_state_t *s = (usrp_state_t *)device->priv;
  int samples_received=0;
  int nsamps2;  // aligned to upper 32 or 16 byte boundary
#if defined(__x86_64) || defined(__i386__)
  nsamps2 = (nsamps+7)>>3;
  __m256i buff_tmp[cc<2 ? 2 : cc][nsamps2];
#elif defined(__arm__) || defined(__aarch64__)
  nsamps2 = (nsamps+3)>>2;
  int16x8_t buff_tmp[cc<2 ? 2 : cc][nsamps2];
#endif

  int rxshift;
  switch (device->type) {
     case USRP_B200_DEV:
        rxshift=4;
        break;
     case USRP_X300_DEV:
     case USRP_N300_DEV:
     case USRP_X400_DEV:
        rxshift=2;
        break;
     default:
       AssertFatal(1==0,"Shouldn't be here\n");
  }

  uhd_rx_metadata_error_code_t error_code;
  int64_t full_secs;
  double frac_secs;
  uhd_rx_metadata_handle h;
  R(uhd_rx_metadata_make(&h));

  samples_received=0;
  while (samples_received != nsamps) {

#if 0
    if (cc>1) {
      // receive multiple channels (e.g. RF A and RF B)
      std::vector<void *> buff_ptrs;

      for (int i=0; i<cc; i++) buff_ptrs.push_back(buff_tmp[i]+samples_received);
      samples_received += s->rx_stream->recv(buff_ptrs, nsamps, s->rx_md);
    } else {
      // receive a single channel (e.g. from connector RF A)

      samples_received += s->rx_stream->recv((void*)((int32_t*)buff_tmp[0]+samples_received),
                                             nsamps-samples_received, s->rx_md);
    }
    if  ((s->wait_for_first_pps == 0) && (s->rx_md.error_code!=uhd::rx_metadata_t::ERROR_CODE_NONE))
      break;

    if ((s->wait_for_first_pps == 1) && (samples_received != nsamps)) {
      printf("sleep...\n"); //usleep(100);
    }
#endif

    size_t ret;
    size_t n;
    R(uhd_rx_streamer_num_channels(s->rx_stream, &n));
    AssertFatal(n == cc, "bad number of channels\n");
    void *buf[n];
    int i;
    for (i = 0; i < n; i++)
      buf[i] = buff_tmp[i] + samples_received;

    R(uhd_rx_streamer_recv(s->rx_stream, buf, nsamps-samples_received, &h, 0.1, false, &ret));
    R(uhd_rx_metadata_error_code(h, &error_code));
    R(uhd_rx_metadata_time_spec(h, &full_secs, &frac_secs));
    samples_received += ret;

    if  ((s->wait_for_first_pps == 0) && (error_code != UHD_RX_METADATA_ERROR_CODE_NONE))
      break;

    if ((s->wait_for_first_pps == 1) && (samples_received != nsamps)) {
      printf("sleep...\n"); //usleep(100);
    }
  }
  if (samples_received == nsamps) s->wait_for_first_pps=0;

  // bring RX data into 12 LSBs for softmodem RX
  for (int i=0; i<cc; i++) {
    for (int j=0; j<nsamps2; j++) {
#if defined(__x86_64__) || defined(__i386__)
      // FK: in some cases the buffer might not be 32 byte aligned, so we cannot use avx2

      if ((((uintptr_t) buff[i])&0x1F)==0) {
        ((__m256i *)buff[i])[j] = simde_mm256_srai_epi16(buff_tmp[i][j],rxshift);
      } else {
        __m256i tmp = simde_mm256_srai_epi16(buff_tmp[i][j],rxshift);
        simde_mm256_storeu_si256(((__m256i *)buff[i])+j, tmp);
      }
    }
#elif defined(__arm__) || defined(__aarch64__)
      for (int j=0; j<nsamps2; j++) 
        ((int16x8_t *)buff[i])[j] = vshrq_n_s16(buff_tmp[i][j],rxshift);
#endif
  }

  if (samples_received < nsamps) {
    LOG_E(HW,"[recv] received %d samples out of %d\n",samples_received,nsamps);
  }

  if (error_code != UHD_RX_METADATA_ERROR_CODE_NONE) {
    char err[1024];
    R(uhd_rx_metadata_to_pp_string(h, err, 1024));
    LOG_E(HW, "%s\n", err);
  }

  R(uhd_rx_metadata_free(&h));

  s->rx_count += nsamps;
  s->rx_timestamp = to_ticks(full_secs, frac_secs, s->sample_rate);
  *ptimestamp = s->rx_timestamp;

  // push GPIO bits 7-9 from flags_msb
   /*s->usrp->set_command_time(uhd::time_spec_t::from_ticks((s->rx_timestamp+(2*nsamps)),s->sample_rate));
   s->usrp->set_gpio_attr("FP0", "OUT", gpio789<<7, 0x380);
   s->usrp->clear_command_time();
   gpio789 = (gpio789+1)&7;*/
  recplay_state_t *recPlay=device->recplay_state;

  if ( recPlay != NULL) { // record mode
    // Copy subframes to memory (later dump on a file)
    if (recPlay->nbSamplesBlocks < device->openair0_cfg->recplay_conf->u_sf_max &&
        recPlay->maxSizeBytes > (recPlay->currentPtr-(uint8_t *)recPlay->ms_sample) +
        sizeof(iqrec_t) + nsamps*4 ) {
      iqrec_t *hdr=(iqrec_t *)recPlay->currentPtr;
      hdr->header = BELL_LABS_IQ_HEADER;
      hdr->ts = *ptimestamp;
      hdr->nbBytes=nsamps*4;
      memcpy(hdr+1, buff[0], nsamps*4);
      recPlay->currentPtr+=sizeof(iqrec_t)+nsamps*4;
      recPlay->nbSamplesBlocks++;
#if 0 // BMC: this is too verbose      
      LOG_D(HW,"recorded %d samples, for TS %lu, shift in buffer %ld\n", nsamps, hdr->ts, recPlay->currentPtr-(uint8_t *)recPlay->ms_sample);
#endif      
    } else
      exit_function(__FILE__, __FUNCTION__, __LINE__,"Recording reaches max iq limit\n");
  }

  return samples_received;
}

/*! \brief Compares two variables within precision
 * \param a first variable
 * \param b second variable
*/
static bool is_equal(double a, double b) {
  return fabs(a-b) < DBL_EPSILON;
}

void *freq_thread(void *arg) {
  openair0_device *device=(openair0_device *)arg;
  usrp_state_t *s = (usrp_state_t *)device->priv;
#if 0
  uhd::tune_request_t tx_tune_req(device->openair0_cfg[0].tx_freq[0],
                                  device->openair0_cfg[0].tune_offset);
  uhd::tune_request_t rx_tune_req(device->openair0_cfg[0].rx_freq[0],
                                  device->openair0_cfg[0].tune_offset);
  s->usrp->set_tx_freq(tx_tune_req);
  s->usrp->set_rx_freq(rx_tune_req);
#endif
  uhd_tune_request_t tx_tune_req = {
    .target_freq = device->openair0_cfg[0].tx_freq[0],
    .rf_freq_policy = UHD_TUNE_REQUEST_POLICY_MANUAL,
    .rf_freq = device->openair0_cfg[0].tx_freq[0] + device->openair0_cfg[0].tune_offset,
    .dsp_freq_policy = UHD_TUNE_REQUEST_POLICY_AUTO,
    .dsp_freq = 0,
    .args = NULL
  };
  uhd_tune_request_t rx_tune_req = {
    .target_freq = device->openair0_cfg[0].rx_freq[0],
    .rf_freq_policy = UHD_TUNE_REQUEST_POLICY_MANUAL,
    .rf_freq = device->openair0_cfg[0].rx_freq[0] + device->openair0_cfg[0].tune_offset,
    .dsp_freq_policy = UHD_TUNE_REQUEST_POLICY_AUTO,
    .dsp_freq = 0,
    .args = NULL
  };
  uhd_tune_result_t tune_result;

  R(uhd_usrp_set_tx_freq(s->usrp, &tx_tune_req, 0, &tune_result));
  R(uhd_usrp_set_rx_freq(s->usrp, &rx_tune_req, 0, &tune_result));

  return NULL;
}
/*! \brief Set frequencies (TX/RX). Spawns a thread to handle the frequency change to not block the calling thread
 * \param device the hardware to use
 * \param openair0_cfg RF frontend parameters set by application
 * \param dummy dummy variable not used
 * \returns 0 in success
 */
int trx_usrp_set_freq(openair0_device *device, openair0_config_t *openair0_cfg)
{
  usrp_state_t *s = (usrp_state_t *)device->priv;
  printf("Setting USRP TX Freq %f, RX Freq %f, tune_offset: %f\n", openair0_cfg[0].tx_freq[0], openair0_cfg[0].rx_freq[0], openair0_cfg[0].tune_offset);

#if 0
  uhd::tune_request_t tx_tune_req(openair0_cfg[0].tx_freq[0], openair0_cfg[0].tune_offset);
  uhd::tune_request_t rx_tune_req(openair0_cfg[0].rx_freq[0], openair0_cfg[0].tune_offset);
  s->usrp->set_tx_freq(tx_tune_req);
  s->usrp->set_rx_freq(rx_tune_req);
#endif

  uhd_tune_request_t tx_tune_req = {
    .target_freq = device->openair0_cfg[0].tx_freq[0],
    .rf_freq_policy = UHD_TUNE_REQUEST_POLICY_MANUAL,
    .rf_freq = device->openair0_cfg[0].tx_freq[0] + device->openair0_cfg[0].tune_offset,
    .dsp_freq_policy = UHD_TUNE_REQUEST_POLICY_AUTO,
    .dsp_freq = 0,
    .args = NULL
  };
  uhd_tune_request_t rx_tune_req = {
    .target_freq = device->openair0_cfg[0].rx_freq[0],
    .rf_freq_policy = UHD_TUNE_REQUEST_POLICY_MANUAL,
    .rf_freq = device->openair0_cfg[0].rx_freq[0] + device->openair0_cfg[0].tune_offset,
    .dsp_freq_policy = UHD_TUNE_REQUEST_POLICY_AUTO,
    .dsp_freq = 0,
    .args = NULL
  };
  uhd_tune_result_t tune_result;

  R(uhd_usrp_set_tx_freq(s->usrp, &tx_tune_req, 0, &tune_result));
  R(uhd_usrp_set_rx_freq(s->usrp, &rx_tune_req, 0, &tune_result));

  return(0);
}

/*! \brief Set RX frequencies
 * \param device the hardware to use
 * \param openair0_cfg RF frontend parameters set by application
 * \returns 0 in success
 */
int openair0_set_rx_frequencies(openair0_device *device, openair0_config_t *openair0_cfg) {
#if 0
  usrp_state_t *s = (usrp_state_t *)device->priv;
  uhd::tune_request_t rx_tune_req(openair0_cfg[0].rx_freq[0], openair0_cfg[0].tune_offset);
  printf("In openair0_set_rx_frequencies, freq: %f, tune offset: %f\n",
         openair0_cfg[0].rx_freq[0],  openair0_cfg[0].tune_offset);
  //rx_tune_req.rf_freq_policy = uhd::tune_request_t::POLICY_MANUAL;
  //rx_tune_req.rf_freq = openair0_cfg[0].rx_freq[0];
  s->usrp->set_rx_freq(rx_tune_req);
#endif
  usrp_state_t *s = (usrp_state_t *)device->priv;
  printf("In openair0_set_rx_frequencies, freq: %f, tune offset: %f\n",
         openair0_cfg[0].rx_freq[0],  openair0_cfg[0].tune_offset);

  uhd_tune_request_t rx_tune_req = {
    .target_freq = device->openair0_cfg[0].rx_freq[0],
    .rf_freq_policy = UHD_TUNE_REQUEST_POLICY_MANUAL,
    .rf_freq = device->openair0_cfg[0].rx_freq[0] + device->openair0_cfg[0].tune_offset,
    .dsp_freq_policy = UHD_TUNE_REQUEST_POLICY_AUTO,
    .dsp_freq = 0,
    .args = NULL
  };
  uhd_tune_result_t tune_result;

  R(uhd_usrp_set_rx_freq(s->usrp, &rx_tune_req, 0, &tune_result));

  return(0);
}

/*! \brief Set Gains (TX/RX)
 * \param device the hardware to use
 * \param openair0_cfg RF frontend parameters set by application
 * \returns 0 in success
 */
int trx_usrp_set_gains(openair0_device *device,
                       openair0_config_t *openair0_cfg) {
#if 0
  usrp_state_t *s = (usrp_state_t *)device->priv;
  ::uhd::gain_range_t gain_range_tx = s->usrp->get_tx_gain_range(0);
  s->usrp->set_tx_gain(gain_range_tx.stop()-openair0_cfg[0].tx_gain[0]);
  ::uhd::gain_range_t gain_range = s->usrp->get_rx_gain_range(0);

  // limit to maximum gain
  if (openair0_cfg[0].rx_gain[0]-openair0_cfg[0].rx_gain_offset[0] > gain_range.stop()) {
    LOG_E(HW,"RX Gain 0 too high, reduce by %f dB\n",
          openair0_cfg[0].rx_gain[0]-openair0_cfg[0].rx_gain_offset[0] - gain_range.stop());
    exit(-1);
  }

  s->usrp->set_rx_gain(openair0_cfg[0].rx_gain[0]-openair0_cfg[0].rx_gain_offset[0]);
  LOG_I(HW,"Setting USRP RX gain to %f (rx_gain %f,gain_range.stop() %f)\n",
        openair0_cfg[0].rx_gain[0]-openair0_cfg[0].rx_gain_offset[0],
        openair0_cfg[0].rx_gain[0],gain_range.stop());
  return(0);
#endif
  usrp_state_t *s = (usrp_state_t *)device->priv;
  double stop;
  uhd_meta_range_handle h;

  /* tx */
  R(uhd_meta_range_make(&h));
  R(uhd_usrp_get_tx_gain_range(s->usrp, "", 0, h));
  R(uhd_meta_range_stop(h, &stop));
  R(uhd_usrp_set_tx_gain(s->usrp, stop - openair0_cfg[0].tx_gain[0], 0, ""));
  R(uhd_meta_range_free(&h));

  /* rx */
  R(uhd_meta_range_make(&h));
  R(uhd_usrp_get_rx_gain_range(s->usrp, "", 0, h));
  R(uhd_meta_range_stop(h, &stop));
  /* limit to maximum gain */
  if (openair0_cfg[0].rx_gain[0]-openair0_cfg[0].rx_gain_offset[0] > stop) {
    LOG_E(HW,"RX Gain 0 too high, reduce by %f dB\n",
          openair0_cfg[0].rx_gain[0]-openair0_cfg[0].rx_gain_offset[0] - stop);
    exit(1);
  }
  R(uhd_usrp_set_rx_gain(s->usrp, openair0_cfg[0].rx_gain[0]-openair0_cfg[0].rx_gain_offset[0], 0, ""));
  R(uhd_meta_range_free(&h));

  LOG_I(HW,"Setting USRP RX gain to %f (rx_gain %f,gain_range.stop() %f)\n",
        openair0_cfg[0].rx_gain[0]-openair0_cfg[0].rx_gain_offset[0],
        openair0_cfg[0].rx_gain[0], stop);

  return(0);
}

/*! \brief Stop USRP
 * \param card refers to the hardware index to use
 */
int trx_usrp_stop(openair0_device *device) {
  return(0);
}

/*! \brief USRPB210 RX calibration table */
rx_gain_calib_table_t calib_table_b210[] = {
  {3500000000.0,44.0},
  {2660000000.0,49.0},
  {2300000000.0,50.0},
  {1880000000.0,53.0},
  {816000000.0,58.0},
  {-1,0}
};

/*! \brief USRPB210 RX calibration table */
rx_gain_calib_table_t calib_table_b210_38[] = {
  {3500000000.0,44.0},
  {2660000000.0,49.8},
  {2300000000.0,51.0},
  {1880000000.0,53.0},
  {816000000.0,57.0},
  {-1,0}
};

/*! \brief USRPx310 RX calibration table */
rx_gain_calib_table_t calib_table_x310[] = {
  {3500000000.0,77.0},
  {2660000000.0,81.0},
  {2300000000.0,81.0},
  {1880000000.0,82.0},
  {816000000.0,85.0},
  {-1,0}
};

/*! \brief USRPn3xf RX calibration table */
rx_gain_calib_table_t calib_table_n310[] = {
  {3500000000.0,0.0},
  {2660000000.0,0.0},
  {2300000000.0,0.0},
  {1880000000.0,0.0},
  {816000000.0, 0.0},
  {-1,0}
};

/*! \brief Empty RX calibration table */
rx_gain_calib_table_t calib_table_none[] = {
  {3500000000.0,0.0},
  {2660000000.0,0.0},
  {2300000000.0,0.0},
  {1880000000.0,0.0},
  {816000000.0, 0.0},
  {-1,0}
};


/*! \brief Set RX gain offset
 * \param openair0_cfg RF frontend parameters set by application
 * \param chain_index RF chain to apply settings to
 * \returns 0 in success
 */
void set_rx_gain_offset(openair0_config_t *openair0_cfg, int chain_index,int bw_gain_adjust) {
  int i=0;
  // loop through calibration table to find best adjustment factor for RX frequency
  double min_diff = 6e9,diff,gain_adj=0.0;

  if (bw_gain_adjust==1) {
    switch ((int)openair0_cfg[0].sample_rate) {
      case 46080000:
        break;

      case 30720000:
        break;

      case 23040000:
        gain_adj=1.25;
        break;

      case 15360000:
        gain_adj=3.0;
        break;

      case 7680000:
        gain_adj=6.0;
        break;

      case 3840000:
        gain_adj=9.0;
        break;

      case 1920000:
        gain_adj=12.0;
        break;

      default:
        LOG_E(HW,"unknown sampling rate %d\n",(int)openair0_cfg[0].sample_rate);
        //exit(-1);
        break;
    }
  }

  while (openair0_cfg->rx_gain_calib_table[i].freq>0) {
    diff = fabs(openair0_cfg->rx_freq[chain_index] - openair0_cfg->rx_gain_calib_table[i].freq);
    LOG_I(HW,"cal %d: freq %f, offset %f, diff %f\n",
          i,
          openair0_cfg->rx_gain_calib_table[i].freq,
          openair0_cfg->rx_gain_calib_table[i].offset,diff);

    if (min_diff > diff) {
      min_diff = diff;
      openair0_cfg->rx_gain_offset[chain_index] = openair0_cfg->rx_gain_calib_table[i].offset+gain_adj;
    }

    i++;
  }
}

/*! \brief print the USRP statistics
* \param device the hardware to use
* \returns  0 on success
*/
int trx_usrp_get_stats(openair0_device *device) {
  return(0);
}

/*! \brief Reset the USRP statistics
 * \param device the hardware to use
 * \returns  0 on success
 */
int trx_usrp_reset_stats(openair0_device *device) {
  return(0);
}

char *concat(char *a, char *b)
{
  int la = strlen(a);
  int lb = strlen(b);
  char *ret = malloc(la+lb+1);
  AssertFatal(ret != NULL, "out of memory\n");
  memcpy(ret, a, la);
  memcpy(ret + la, b, lb + 1);
  return ret;
}

#define ALL_MBOARDS ((size_t)(~0))

int device_init(openair0_device *device, openair0_config_t *openair0_cfg)
{
  LOG_I(HW, "openair0_cfg[0].sdr_addrs == '%s'\n", openair0_cfg[0].sdr_addrs);
  LOG_I(HW, "openair0_cfg[0].clock_source == '%d' (internal = %d, external = %d)\n", openair0_cfg[0].clock_source,internal,external);
  usrp_state_t *s ;
  int choffset = 0;

  if ( device->priv == NULL) {
    s=(usrp_state_t *)calloc(sizeof(usrp_state_t),1);
    device->priv=s;
    AssertFatal( s!=NULL,"USRP device: memory allocation failure\n");
  } else {
    LOG_E(HW, "multiple device init detected\n");
    return 0;
  }

  device->openair0_cfg = openair0_cfg;
  device->trx_start_func = trx_usrp_start;
  device->trx_get_stats_func = trx_usrp_get_stats;
  device->trx_reset_stats_func = trx_usrp_reset_stats;
  device->trx_end_func   = trx_usrp_end;
  device->trx_stop_func  = trx_usrp_stop;
  device->trx_set_freq_func = trx_usrp_set_freq;
  device->trx_set_gains_func   = trx_usrp_set_gains;
  device->trx_write_init = trx_usrp_write_init;


  // hotfix! to be checked later
  //uhd::set_thread_priority_safe(1.0);
  // Initialize USRP device
  int vers=0,subvers=0,subsubvers=0;
  int bw_gain_adjust=0;

  if (device->openair0_cfg->recplay_mode == RECPLAY_RECORDMODE) {
    LOG_I(HW, "USRP device initialized in subframes record mode\n");
  }

  {
    char version[256];
    R(uhd_get_version_string(version, 256));
    sscanf(version, "%d.%d.%d", &vers, &subvers, &subsubvers);
    LOG_I(HW,"UHD version %s (%d.%d.%d)\n",
          version, vers, subvers, subsubvers);
  }
  char *args, *tx_subdev, *rx_subdev;

  if (openair0_cfg[0].sdr_addrs == NULL) {
    args = "type=b200";
  } else {
    args = openair0_cfg[0].sdr_addrs;
    LOG_I(HW,"Checking for USRP with args %s\n",openair0_cfg[0].sdr_addrs);
  }
  /* args has to be malloced, it's freed later */
  args = concat(args, "");

#if 0
  uhd::device_addrs_t device_adds = uhd::device::find(args);
#endif
  uhd_string_vector_handle device_adds;
  size_t device_adds_size;
  R(uhd_string_vector_make(&device_adds));
  R(uhd_usrp_find(args, &device_adds));
  R(uhd_string_vector_size(device_adds, &device_adds_size));

#if 0
  if (device_adds.size() == 0) {
    LOG_E(HW,"No USRP Device Found.\n ");
    free(s);
    return -1;
  } else if (device_adds.size() > 1) {
    LOG_E(HW,"More than one USRP Device Found. Please specify device more precisely in config file.\n");
    free(s);
    return -1;
  }
#endif
  if (device_adds_size == 0) {
    LOG_E(HW,"No USRP Device Found.\n ");
    R(uhd_string_vector_free(&device_adds));
    free(s);
    return -1;
  } else if (device_adds_size > 1) {
    LOG_E(HW,"More than one USRP Device Found. Please specify device more precisely in config file.\n");
    R(uhd_string_vector_free(&device_adds));
    free(s);
    return -1;
  }

  char device_adds_str[1024];
  R(uhd_string_vector_at(device_adds, 0, device_adds_str, 1024));
  LOG_I(HW,"Found USRP %s\n", device_adds_str);

  double usrp_master_clock;

  if (strstr(device_adds_str, "type=b200")) {
    device->type = USRP_B200_DEV;
    usrp_master_clock = 30.72e6;
    char s[256];
    sprintf(s, ",master_clock_rate=%f", usrp_master_clock);
    char *n = concat(args, s);
    free(args);
    args = n;
    n = concat(args, ",num_send_frames=256,num_recv_frames=256, send_frame_size=7680, recv_frame_size=7680");
    free(args);
    args = n;
  }

  if (strstr(device_adds_str, "type=n3xx")) {
    device->type=USRP_N300_DEV;
    if (strstr(device_adds_str, "product=n320"))
      usrp_master_clock = 245.76e6; // N320 does not support 122.88e6 master clock rate
    else
      usrp_master_clock = 122.88e6;
    char s[256];
    sprintf(s, ",master_clock_rate=%f", usrp_master_clock);
    char *n = concat(args, s);
    free(args);
    args = n;
    if ( 0 != system("sysctl -w net.core.rmem_max=62500000 net.core.wmem_max=62500000") )
      LOG_W(HW,"Can't set kernel parameters for N3x0\n");
  }

  if (strstr(device_adds_str, "type=x300")) {
    printf("Found USRP x300\n");
    device->type=USRP_X300_DEV;
    usrp_master_clock = 184.32e6;
    char s[256];
    sprintf(s, ",master_clock_rate=%f", usrp_master_clock);
    char *n = concat(args, s);
    free(args);
    args = n;

    // USRP recommended: https://files.ettus.com/manual/page_usrp_x3x0_config.html
    if ( 0 != system("sysctl -w net.core.rmem_max=33554432 net.core.wmem_max=33554432") )
      LOG_W(HW,"Can't set kernel parameters for X3xx\n");
  }

  if (strstr(device_adds_str, "type=x4xx")) {
    printf("Found USRP x400\n");
    device->type = USRP_X400_DEV;
    usrp_master_clock = 245.76e6;
    char s[256];
    sprintf(s, ",master_clock_rate=%f", usrp_master_clock);
    char *n = concat(args, s);
    free(args);
    args = n;
  }

  R(uhd_string_vector_free(&device_adds));

  R(uhd_usrp_make(&s->usrp, args));

  if (!strstr(args, "clock_source")) {
    if (openair0_cfg[0].clock_source == internal) {
      R(uhd_usrp_set_clock_source(s->usrp, "internal", ALL_MBOARDS));
      LOG_I(HW,"Setting clock source to internal\n");
    }
    else if (openair0_cfg[0].clock_source == external ) {
      R(uhd_usrp_set_clock_source(s->usrp, "external", ALL_MBOARDS));
      LOG_I(HW,"Setting clock source to external\n");
    }
    else if (openair0_cfg[0].clock_source==gpsdo) {
      R(uhd_usrp_set_clock_source(s->usrp, "gpsdo", ALL_MBOARDS));
      LOG_I(HW,"Setting clock source to gpsdo\n");
    }
    else {
      LOG_W(HW,"Clock source set neither in usrp_args nor on command line, using default!\n");
    }
  }
  else {
    if (openair0_cfg[0].clock_source != unset) {
       LOG_W(HW,"Clock source set in both usrp_args and in clock_source, ingnoring the latter!\n");
    }
  }

  if (!strstr(args, "time_source")) {
    if (openair0_cfg[0].time_source == internal) {
      R(uhd_usrp_set_time_source(s->usrp, "internal", ALL_MBOARDS));
      LOG_I(HW,"Setting time source to internal\n");
    }
    else if (openair0_cfg[0].time_source == external ) {
      R(uhd_usrp_set_time_source(s->usrp, "external", ALL_MBOARDS));
      LOG_I(HW,"Setting time source to external\n");
    }
    else if (openair0_cfg[0].time_source==gpsdo) {
      R(uhd_usrp_set_time_source(s->usrp, "gpsdo", ALL_MBOARDS));
      LOG_I(HW,"Setting time source to gpsdo\n");
    }
    else {
      LOG_W(HW,"Time source set neither in usrp_args nor on command line, using default!\n");
    }
  }
  else {
    if (openair0_cfg[0].clock_source != unset) {
       LOG_W(HW,"Time source set in both usrp_args and in time_source, ingnoring the latter!\n");
    }
  }

  free(args);

  {
    char clock_source[128];
    R(uhd_usrp_get_clock_source(s->usrp, 0, clock_source, 128));
    if (!strcmp(clock_source, "gpsdo")) {
      s->use_gps = 1;

      if (sync_to_gps(device)==EXIT_SUCCESS) {
        LOG_I(HW,"USRP synced with GPS!\n");
      } else {
        LOG_I(HW,"USRP fails to sync with GPS. Exiting.\n");
        exit(EXIT_FAILURE);
      }
    } else {
      R(uhd_usrp_set_time_next_pps(s->usrp, 0, 0, ALL_MBOARDS));

      if (!strcmp(clock_source, "external")) {
        if (check_ref_locked(s,0)) {
          LOG_I(HW,"USRP locked to external reference!\n");
        } else {
          LOG_I(HW,"Failed to lock to external reference. Exiting.\n");
          exit(EXIT_FAILURE);
        }
      }
    }
  }

  R(uhd_usrp_set_time_now(s->usrp, 0, 0, ALL_MBOARDS));

  if (device->type==USRP_X300_DEV) {
    openair0_cfg[0].rx_gain_calib_table = calib_table_x310;
    LOG_I(HW, "-- Using calibration table: calib_table_x310\n");
  }

  if (device->type==USRP_N300_DEV) {
    openair0_cfg[0].rx_gain_calib_table = calib_table_n310;
    LOG_I(HW, "-- Using calibration table: calib_table_n310\n");
  }

  if (device->type == USRP_X400_DEV) {
    openair0_cfg[0].rx_gain_calib_table = calib_table_none;
    LOG_I(HW, "-- Using calibration table: calib_table_none\n");
  }


  if (device->type==USRP_N300_DEV || device->type==USRP_X300_DEV || device->type==USRP_X400_DEV) {
    LOG_I(HW,"%s() sample_rate:%u\n", __FUNCTION__, (int)openair0_cfg[0].sample_rate);

    switch ((int)openair0_cfg[0].sample_rate) {
      case 184320000:
        // from usrp_time_offset
        //openair0_cfg[0].samples_per_packet    = 2048;
        openair0_cfg[0].tx_sample_advance     = 15; //to be checked
        break;

      case 122880000:
        // from usrp_time_offset
        //openair0_cfg[0].samples_per_packet    = 2048;
        openair0_cfg[0].tx_sample_advance     = 15; //to be checked
        openair0_cfg[0].tx_bw                 = 80e6;
        openair0_cfg[0].rx_bw                 = 80e6;
        break;

      case 92160000:
        // from usrp_time_offset
        //openair0_cfg[0].samples_per_packet    = 2048;
        openair0_cfg[0].tx_sample_advance     = 15; //to be checked
        //openair0_cfg[0].tx_bw                 = 80e6;
        //openair0_cfg[0].rx_bw                 = 80e6;
        break;

      case 61440000:
        // from usrp_time_offset
        //openair0_cfg[0].samples_per_packet    = 2048;
        openair0_cfg[0].tx_sample_advance     = 15;
        openair0_cfg[0].tx_bw                 = 40e6;
        openair0_cfg[0].rx_bw                 = 40e6;
        break;

      case 46080000:
        //openair0_cfg[0].samples_per_packet    = 2048;
        openair0_cfg[0].tx_sample_advance     = 15;
        openair0_cfg[0].tx_bw                 = 40e6;
        openair0_cfg[0].rx_bw                 = 40e6;
        break;

      case 30720000:
        // from usrp_time_offset
        //openair0_cfg[0].samples_per_packet    = 2048;
        openair0_cfg[0].tx_sample_advance     = 15;
        openair0_cfg[0].tx_bw                 = 20e6;
        openair0_cfg[0].rx_bw                 = 20e6;
        break;

      case 23040000:
        //openair0_cfg[0].samples_per_packet    = 2048;
        openair0_cfg[0].tx_sample_advance     = 15;
        openair0_cfg[0].tx_bw                 = 20e6;
        openair0_cfg[0].rx_bw                 = 20e6;
        break;

      case 15360000:
        //openair0_cfg[0].samples_per_packet    = 2048;
        openair0_cfg[0].tx_sample_advance     = 45;
        openair0_cfg[0].tx_bw                 = 10e6;
        openair0_cfg[0].rx_bw                 = 10e6;
        break;

      case 7680000:
        //openair0_cfg[0].samples_per_packet    = 2048;
        openair0_cfg[0].tx_sample_advance     = 50;
        openair0_cfg[0].tx_bw                 = 5e6;
        openair0_cfg[0].rx_bw                 = 5e6;
        break;

      case 1920000:
        //openair0_cfg[0].samples_per_packet    = 2048;
        openair0_cfg[0].tx_sample_advance     = 50;
        openair0_cfg[0].tx_bw                 = 1.25e6;
        openair0_cfg[0].rx_bw                 = 1.25e6;
        break;

      default:
        LOG_E(HW,"Error: unknown sampling rate %f\n",openair0_cfg[0].sample_rate);
        exit(-1);
        break;
    }
  }

  if (device->type == USRP_B200_DEV) {
    if ((vers == 3) && (subvers == 9) && (subsubvers>=2)) {
      openair0_cfg[0].rx_gain_calib_table = calib_table_b210;
      bw_gain_adjust=0;
      LOG_I(HW, "-- Using calibration table: calib_table_b210\n"); // Bell Labs info
    } else {
      openair0_cfg[0].rx_gain_calib_table = calib_table_b210_38;
      bw_gain_adjust=1;
      LOG_I(HW, "-- Using calibration table: calib_table_b210_38\n"); // Bell Labs info
    }

    switch ((int)openair0_cfg[0].sample_rate) {
      case 46080000:
        R(uhd_usrp_set_master_clock_rate(s->usrp, 46.08e6, ALL_MBOARDS));
        //openair0_cfg[0].samples_per_packet    = 1024;
        openair0_cfg[0].tx_sample_advance     = 115;
        openair0_cfg[0].tx_bw                 = 40e6;
        openair0_cfg[0].rx_bw                 = 40e6;
        break;

      case 30720000:
        R(uhd_usrp_set_master_clock_rate(s->usrp, 30.72e6, ALL_MBOARDS));
        //openair0_cfg[0].samples_per_packet    = 1024;
        openair0_cfg[0].tx_sample_advance     = 115;
        openair0_cfg[0].tx_bw                 = 20e6;
        openair0_cfg[0].rx_bw                 = 20e6;
        break;

      case 23040000:
        R(uhd_usrp_set_master_clock_rate(s->usrp, 23.04e6, ALL_MBOARDS)); // to be checked
        //openair0_cfg[0].samples_per_packet    = 1024;
        openair0_cfg[0].tx_sample_advance     = 113;
        openair0_cfg[0].tx_bw                 = 20e6;
        openair0_cfg[0].rx_bw                 = 20e6;
        break;

      case 15360000:
        R(uhd_usrp_set_master_clock_rate(s->usrp, 30.72e06, ALL_MBOARDS));
        //openair0_cfg[0].samples_per_packet    = 1024;
        openair0_cfg[0].tx_sample_advance     = 103;
        openair0_cfg[0].tx_bw                 = 20e6;
        openair0_cfg[0].rx_bw                 = 20e6;
        break;

      case 7680000:
        R(uhd_usrp_set_master_clock_rate(s->usrp, 30.72e06, ALL_MBOARDS));
        //openair0_cfg[0].samples_per_packet    = 1024;
        openair0_cfg[0].tx_sample_advance     = 80;
        openair0_cfg[0].tx_bw                 = 20e6;
        openair0_cfg[0].rx_bw                 = 20e6;
        break;

      case 1920000:
        R(uhd_usrp_set_master_clock_rate(s->usrp, 30.72e06, ALL_MBOARDS));
        //openair0_cfg[0].samples_per_packet    = 1024;
        openair0_cfg[0].tx_sample_advance     = 40;
        openair0_cfg[0].tx_bw                 = 20e6;
        openair0_cfg[0].rx_bw                 = 20e6;
        break;

      default:
        LOG_E(HW,"Error: unknown sampling rate %f\n",openair0_cfg[0].sample_rate);
        exit(-1);
        break;
    }
  }

  /* device specific */
  //openair0_cfg[0].txlaunch_wait = 1;//manage when TX processing is triggered
  //openair0_cfg[0].txlaunch_wait_slotcount = 1; //manage when TX processing is triggered
  openair0_cfg[0].iq_txshift = 4;//shift
  openair0_cfg[0].iq_rxrescale = 15;//rescale iqs

  if(openair0_cfg[0].tx_subdev!=NULL){
    LOG_I(HW, "openair0_cfg[0].tx_subdev == %s\n", openair0_cfg[0].tx_subdev);
    tx_subdev = openair0_cfg[0].tx_subdev;
    uhd_subdev_spec_handle h;
    R(uhd_subdev_spec_make(&h, tx_subdev));
    R(uhd_usrp_set_tx_subdev_spec(s->usrp, h, ALL_MBOARDS));
    R(uhd_subdev_spec_free(&h));
  }

  if(openair0_cfg[0].rx_subdev!=NULL){
    LOG_I(HW, "openair0_cfg[0].rx_subdev == %s\n", openair0_cfg[0].rx_subdev);
    rx_subdev = openair0_cfg[0].rx_subdev;
    uhd_subdev_spec_handle h;
    R(uhd_subdev_spec_make(&h, rx_subdev));
    R(uhd_usrp_set_rx_subdev_spec(s->usrp, h, ALL_MBOARDS));
    R(uhd_subdev_spec_free(&h));
  }

  size_t rx_num_channels;
  R(uhd_usrp_get_rx_num_channels(s->usrp, &rx_num_channels));

  for(int i = 0; i < rx_num_channels; i++) {
    if (i < openair0_cfg[0].rx_num_channels) {
      R(uhd_usrp_set_rx_rate(s->usrp, openair0_cfg[0].sample_rate, i + choffset));
      uhd_tune_request_t rx_tune_req = {
        .target_freq = device->openair0_cfg[0].rx_freq[i],
        .rf_freq_policy = UHD_TUNE_REQUEST_POLICY_MANUAL,
        .rf_freq = device->openair0_cfg[0].rx_freq[i] + device->openair0_cfg[0].tune_offset,
        .dsp_freq_policy = UHD_TUNE_REQUEST_POLICY_AUTO,
        .dsp_freq = 0,
        .args = NULL
      };
      uhd_tune_result_t tune_result;
      R(uhd_usrp_set_rx_freq(s->usrp, &rx_tune_req, i + choffset, &tune_result));
      set_rx_gain_offset(&openair0_cfg[0],i,bw_gain_adjust);
      double stop;
      uhd_meta_range_handle h;
      R(uhd_meta_range_make(&h));
      R(uhd_usrp_get_rx_gain_range(s->usrp, "", i + choffset, h));
      R(uhd_meta_range_stop(h, &stop));
      R(uhd_meta_range_free(&h));
      /* limit to maximum gain */
      double gain = openair0_cfg[0].rx_gain[i] - openair0_cfg[0].rx_gain_offset[i];
      if (gain > stop) {
        LOG_E(HW,"RX Gain too high, lower by %f dB\n", gain - stop);
        gain = stop;
      }
      R(uhd_usrp_set_rx_gain(s->usrp, gain, i + choffset, ""));
      LOG_I(HW,"RX Gain %d %f (%f) => %f (max %f)\n",i,
            openair0_cfg[0].rx_gain[i],openair0_cfg[0].rx_gain_offset[i],
            openair0_cfg[0].rx_gain[i]-openair0_cfg[0].rx_gain_offset[i],
            stop);
    }
  }

  size_t tx_num_channels;
  R(uhd_usrp_get_tx_num_channels(s->usrp, &tx_num_channels));

  LOG_D(HW, "usrp->get_tx_num_channels() == %zd\n", tx_num_channels);
  LOG_D(HW, "openair0_cfg[0].tx_num_channels == %d\n", openair0_cfg[0].tx_num_channels);

  for(int i = 0; i < tx_num_channels; i++) {
    double stop;
    uhd_meta_range_handle h;
    R(uhd_meta_range_make(&h));
    R(uhd_usrp_get_tx_gain_range(s->usrp, "", i, h));
    R(uhd_meta_range_stop(h, &stop));
    R(uhd_meta_range_free(&h));

    if (i < openair0_cfg[0].tx_num_channels) {
      R(uhd_usrp_set_tx_rate(s->usrp, openair0_cfg[0].sample_rate, i + choffset));
      uhd_tune_request_t tx_tune_req = {
        .target_freq = openair0_cfg[0].tx_freq[i],
        .rf_freq_policy = UHD_TUNE_REQUEST_POLICY_MANUAL,
        .rf_freq = openair0_cfg[0].tx_freq[i] + openair0_cfg[0].tune_offset,
        .dsp_freq_policy = UHD_TUNE_REQUEST_POLICY_AUTO,
        .dsp_freq = 0,
        .args = NULL
      };
      uhd_tune_result_t tune_result;

      R(uhd_usrp_set_tx_freq(s->usrp, &tx_tune_req, i + choffset, &tune_result));
      R(uhd_usrp_set_tx_gain(s->usrp, stop - openair0_cfg[0].tx_gain[i], i + choffset, ""));
      LOG_I(HW,"USRP TX_GAIN:%3.2lf gain_range:%3.2lf tx_gain:%3.2lf\n", stop - openair0_cfg[0].tx_gain[i], stop, openair0_cfg[0].tx_gain[i]);
    }
  }

  /* display USRP settings */
  double master_clock_rate;
  R(uhd_usrp_get_master_clock_rate(s->usrp, 0, &master_clock_rate));
  LOG_I(HW,"Actual master clock: %fMHz...\n", master_clock_rate/1e6);
  {
    char source[128];
    R(uhd_usrp_get_clock_source(s->usrp, 0, source, 128));
    LOG_I(HW,"Actual clock source %s...\n", source);
    R(uhd_usrp_get_time_source(s->usrp, 0, source, 128));
    LOG_I(HW,"Actual time source %s...\n", source);
  }

  // create tx & rx streamer
  size_t channel = 0;
  uhd_stream_args_t stream_args_rx = {
    cpu_format: "sc16",
    otw_format: "sc16",
    args: "",
    channel_list: &channel,
    n_channels: 1
  };

  size_t channels_rx[openair0_cfg[0].rx_num_channels];
  stream_args_rx.n_channels = openair0_cfg[0].rx_num_channels;
  stream_args_rx.channel_list = channels_rx;

  for (int i = 0; i<openair0_cfg[0].rx_num_channels; i++) {
    LOG_I(HW,"setting rx channel %d\n",i+choffset);
    channels_rx[i] = i + choffset;
  }

  R(uhd_rx_streamer_make(&s->rx_stream));
  R(uhd_usrp_get_rx_stream(s->usrp, &stream_args_rx, s->rx_stream));

  int samples=openair0_cfg[0].sample_rate;
  size_t max;
  R(uhd_rx_streamer_max_num_samps(s->rx_stream, &max));
  samples/=10000;
  LOG_I(HW,"RF board max packet size %u, size for 100µs jitter %d \n", (int)max, samples);

  char rx_spp[64];
  if ( samples < max ) {
    sprintf(rx_spp, "spp=%d", samples);
    stream_args_rx.args = rx_spp;
  }

  {
    size_t max_num_samples;
    R(uhd_rx_streamer_max_num_samps(s->rx_stream, &max_num_samples));
    LOG_I(HW,"rx_max_num_samps %zu\n", max_num_samples);
  }

  size_t channels_tx[openair0_cfg[0].tx_num_channels];
  uhd_stream_args_t stream_args_tx = {
    cpu_format: "sc16",
    otw_format: "sc16",
    args: "",
    channel_list: channels_tx,
    n_channels: openair0_cfg[0].tx_num_channels
  };

  for (int i = 0; i<openair0_cfg[0].tx_num_channels; i++)
    channels_tx[i] = i + choffset;

  R(uhd_tx_streamer_make(&s->tx_stream));
  R(uhd_usrp_get_tx_stream(s->usrp, &stream_args_tx, s->tx_stream));

  /* Setting TX/RX BW after streamers are created due to USRP calibration issue */
  // N310 with UHD >= 4.2.0 has issues with changing the BW, which is a NOP on N310 in earlier versions
  // see also: https://github.com/EttusResearch/uhd/issues/644
  if (device->type != USRP_N300_DEV) {
    size_t tx_num_channels;
    size_t rx_num_channels;
    R(uhd_usrp_get_tx_num_channels(s->usrp, &tx_num_channels));
    R(uhd_usrp_get_rx_num_channels(s->usrp, &rx_num_channels));

    for (int i = 0; i < tx_num_channels && i < openair0_cfg[0].tx_num_channels; i++)
      R(uhd_usrp_set_tx_bandwidth(s->usrp, openair0_cfg[0].tx_bw, i + choffset));

    for(int i = 0; i < rx_num_channels && i < openair0_cfg[0].rx_num_channels; i++)
      R(uhd_usrp_set_rx_bandwidth(s->usrp, openair0_cfg[0].rx_bw, i + choffset));
  }

  for (int i = 0; i < openair0_cfg[0].rx_num_channels; i++) {
    double rx_rate;
    double rx_freq;
    double rx_gain;
    double rx_bandwidth;
    char rx_antenna[128];
    R(uhd_usrp_get_rx_rate(s->usrp, i + choffset, &rx_rate));
    R(uhd_usrp_get_rx_freq(s->usrp, i + choffset, &rx_freq));
    R(uhd_usrp_get_rx_gain(s->usrp, i + choffset, "", &rx_gain));
    R(uhd_usrp_get_rx_bandwidth(s->usrp, i + choffset, &rx_bandwidth));
    R(uhd_usrp_get_rx_antenna(s->usrp, i + choffset, rx_antenna, 128));

    LOG_I(HW, "RX Channel %d\n", i);
    LOG_I(HW, "  Actual RX sample rate: %fMSps...\n", rx_rate / 1e6);
    LOG_I(HW, "  Actual RX frequency: %fGHz...\n", rx_freq / 1e9);
    LOG_I(HW, "  Actual RX gain: %f...\n", rx_gain);
    LOG_I(HW, "  Actual RX bandwidth: %fM...\n", rx_bandwidth / 1e6);
    LOG_I(HW, "  Actual RX antenna: %s...\n", rx_antenna);
  }

  for (int i = 0; i < openair0_cfg[0].tx_num_channels; i++) {
    double tx_rate;
    double tx_freq;
    double tx_gain;
    double tx_bandwidth;
    char tx_antenna[128];
    size_t max_num_samps;
    R(uhd_usrp_get_tx_rate(s->usrp, i + choffset, &tx_rate));
    R(uhd_usrp_get_tx_freq(s->usrp, i + choffset, &tx_freq));
    R(uhd_usrp_get_tx_gain(s->usrp, i + choffset, "", &tx_gain));
    R(uhd_usrp_get_tx_bandwidth(s->usrp, i + choffset, &tx_bandwidth));
    R(uhd_usrp_get_tx_antenna(s->usrp, i + choffset, tx_antenna, 128));
    R(uhd_tx_streamer_max_num_samps(s->tx_stream, &max_num_samps));

    LOG_I(HW, "TX Channel %d\n", i);
    LOG_I(HW, "  Actual TX sample rate: %fMSps...\n", tx_rate / 1e6);
    LOG_I(HW, "  Actual TX frequency: %fGHz...\n", tx_freq / 1e9);
    LOG_I(HW, "  Actual TX gain: %f...\n", tx_gain);
    LOG_I(HW, "  Actual TX bandwidth: %fM...\n", tx_bandwidth / 1e6);
    LOG_I(HW, "  Actual TX antenna: %s...\n", tx_antenna);
    LOG_I(HW, "  Actual TX packet size: %lu\n", max_num_samps);
  }

  {
    char pp[4096];
    R(uhd_usrp_get_pp_string(s->usrp, pp, 4096));
    LOG_I(HW, "Using Device: %s\n", pp);
  }
  {
    int64_t full;
    double frac;
    R(uhd_usrp_get_time_now(s->usrp, 0, &full, &frac));
    LOG_I(HW,"Device timestamp: %f...\n", full + frac);
  }

  device->trx_write_func = trx_usrp_write;
  device->trx_read_func  = trx_usrp_read;
  s->sample_rate = openair0_cfg[0].sample_rate;

  // TODO:
  // init tx_forward_nsamps based usrp_time_offset ex
  if(is_equal(s->sample_rate, (double)30.72e6))
    s->tx_forward_nsamps  = 176;

  if(is_equal(s->sample_rate, (double)15.36e6))
    s->tx_forward_nsamps = 90;

  if(is_equal(s->sample_rate, (double)7.68e6))
    s->tx_forward_nsamps = 50;

  recplay_state_t *recPlay=device->recplay_state;

  if (recPlay != NULL) { // record mode
    recPlay->maxSizeBytes=openair0_cfg[0].recplay_conf->u_sf_max *
                            (sizeof(iqrec_t)+BELL_LABS_IQ_BYTES_PER_SF);
    recPlay->ms_sample = (iqrec_t *) malloc(recPlay->maxSizeBytes);
    recPlay->currentPtr= (uint8_t *)recPlay->ms_sample;

    if (recPlay->ms_sample == NULL) {
      LOG_E(HW, "Memory allocation failed for subframe record or replay mode.\n");
      exit(1);
    }
  }
  return 0;
}
