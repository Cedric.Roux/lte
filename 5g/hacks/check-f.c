#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

typedef struct {
  unsigned long s, mus, sample, ts;
} data_t;

int get_data(data_t *r, FILE *f)
{
  return fscanf(f, "F %ld %ld %ld %ld\n", &r->s, &r->mus, &r->sample, &r->ts) != 4;
}

int older(int a, int b)
{
  int diff1 = (a - b + 48000) % 48000;
  int diff2 = (b - a + 48000) % 48000;
  if (diff1 > diff2) return 1;
  return 0;
}

int diff(long as, long amus, long bs, long bmus)
{
  int64_t a = (int64_t)as * 1000000 + (int64_t)amus;
  int64_t b = (int64_t)bs * 1000000 + (int64_t)bmus;
  return a-b;
}

void usage(void)
{
  printf("gimme <ue file> <meduse file>\n");
  exit(1);
}

int main(int n, char **v)
{
  char *fue = NULL, *fmeduse = NULL;
  FILE *ue, *meduse;
  int i;

  for (i = 1; i < n; i++) {
    if (fue == NULL) { fue = v[i]; continue; }
    if (fmeduse == NULL) { fmeduse = v[i]; continue; }
    usage();
  }

  if (fue == NULL || fmeduse == NULL) usage();

  ue = fopen(fue, "r");
  meduse = fopen(fmeduse, "r");
  if (ue == NULL || meduse == NULL) { printf("error with %s or %s\n", fue, fmeduse); return 1; }

  int has_sync = 0;
  int delta;

  while (1) {
    data_t due, dmeduse;
    if (get_data(&due, ue) || get_data(&dmeduse, meduse)) break;
    while (due.sample != dmeduse.sample || due.ts != dmeduse.ts) {
      printf("unsynch ue %ld %ld meduse %ld %ld [%ld.%ld %ld.%ld %d]\n", due.sample, due.ts, dmeduse.sample, dmeduse.ts, due.s, due.mus, dmeduse.s, dmeduse.mus, diff(due.s, due.mus, dmeduse.s, dmeduse.mus));
      if (older(due.sample, dmeduse.sample)) {
        if (get_data(&due, ue)) goto over;
      } else {
        if (get_data(&dmeduse, meduse)) goto over;
      }
    }
    if (!has_sync) {
      has_sync = 1;
      delta = diff(due.s, due.mus, dmeduse.s, dmeduse.mus);
    }
    int ndelta = diff(due.s, due.mus, dmeduse.s, dmeduse.mus);
    //printf("ok delta %d ndelta %d diff %d\n", delta, ndelta, ndelta-delta);
    printf("%d\n", ndelta-delta);
  }

over:
  return 0;
}
