#include <stdio.h>
#include <stdlib.h>

char *get_l(void)
{
  static char *l = NULL;
  static int maxsize = 0;
  int size = 0;
  while (1) {
    int c = getchar(); if (c == '\n' || c == EOF) break;
    if (size == maxsize) { maxsize += 1024; l = realloc(l, maxsize); if (l==NULL) abort(); }
    l[size] = c;
    size++;
  }
  if (size == maxsize) { maxsize += 1024; l = realloc(l, maxsize); if (l==NULL) abort(); }
  if (size == 0) return NULL;
  l[size] = 0;
  return l;
}

int main(void)
{
  unsigned long prev = 0;

  while (1) {
    char *l = get_l(); if (l == NULL) break;
    unsigned int h, m, s, ns, t, socket;
#if 0
    /* gtp */
    if (sscanf(l, "%d:%d:%d.%d [%d]: JITTER_GTP_IN socket %d", &h, &m, &s, &ns, &t, &socket) != 6) continue;
#endif
#if 0
    /* pdcp */
    if (sscanf(l, "%d:%d:%d.%d [%d]: JITTER_PDCP_IN entity_id %d", &h, &m, &s, &ns, &t, &socket) != 6) continue;
    if (socket != 1546086992) continue;
#endif
#if 1
    /* rlc */
    if (sscanf(l, "%d:%d:%d.%d [%d]: JITTER_RLC_IN entity_id %d", &h, &m, &s, &ns, &t, &socket) != 6) continue;
    if (socket != 1544115904) continue;
#endif
    unsigned long cur = (unsigned long)t * (unsigned long)1000000000 + ns;
//if (cur - prev > 10000000) { prev = cur; continue; }
    printf("%ld\n", cur - prev);
    //printf("%ld\t[%2.2d:%2.2d:%2.2d.%2.2d [%9.9d]\n", cur - prev, h, m, s, ns, t);
    prev = cur;
  }
}
