/* extract UDP data from macpdu2wireshark trace - uses pcap.c for that */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#define INCLUDE_PCAP
#include "pcap.c"

int do_frame_time;
int do_time_diff;

#define GETLEN(x) do { \
    if (f == 0 && pos > payload_size - 1) goto err; \
    if (f == 1 && pos > payload_size - 2) goto err; \
    if (f == 0) { len = payload[pos]; pos++; } \
    else        { len = (payload[pos] << 8) | payload[pos+1]; pos+=2; } \
    if (pos + len > payload_size) goto err; \
  } while (0)

int diff(long as, long amus, long bs, long bmus)
{
  int64_t a = (int64_t)as * 1000000 + (int64_t)amus;
  int64_t b = (int64_t)bs * 1000000 + (int64_t)bmus;
  return a-b;
}

void pdcp(unsigned long s, unsigned long mus, int hfn, int frame, int slot, unsigned char *payload, int len)
{
  static int last_sn = -1;
  int sn;
  int sn_size = 18;
  int sn_modulus;

  if (len < 4) { printf("bad len pdcp\n"); exit(1); }
  if ((payload[0] & 0x80) == 0) { printf("ERROR: pdcp control not done\n"); return; }

  sn = ((payload[0] & 0x03) << 16) | (payload[1] << 8) | payload[2];
  if (sn_size != 18) abort();

  sn_modulus = 256 * 1024;

  if (sn != (last_sn + 1) % sn_modulus) { printf("ERROR: bad pdcp sn %d (last %d)\n", sn, last_sn); exit(1); }
  last_sn = sn;

  packet_t ip = parse_ip(payload + 3, len - 3);
  if (do_time_diff) {
    uint64_t n = hfn * 1024 * 20 + frame * 20 + slot;
    ip.ts.tv_sec = n * 500;
    ip.ts.tv_usec = s * 1000000 + mus;
  } else if (do_frame_time) {
    uint64_t n = hfn * 1024 * 20 + frame * 20 + slot;
printf("n %ld\n", n);
    ip.ts.tv_sec = n / 1000000;
    ip.ts.tv_usec = n % 1000000;
printf("s %ld us %ld\n", ip.ts.tv_sec, ip.ts.tv_usec);
  } else {
    ip.ts.tv_sec = s;
    ip.ts.tv_usec = mus;
  }
  analyse_50000(&ip);
}

typedef struct {
  int rnti;
  int frame;
  int slot;
  int sn;
  int so;
  int si;
  char *data;
  int size;
} rlc_pdu_t;

rlc_pdu_t rlc_list[512];
int rlc_list_size;
int last_sn = -1;

void add_list(unsigned char *payload, int len, int sn_size, int rnti, int frame, int slot)
{
  int sn_modulo = sn_size == 12 ? 4096 : 64;
  int si = payload[0] >> 6;
printf("add_list rlc_list_size %d si %d rnti %d frame %d slot %d\n", rlc_list_size, si, rnti, frame, slot);
  rlc_list[rlc_list_size].si = si;
  if (si & 0x02) {
    /* not head - has so */
    if (!rlc_list_size) abort();
    if (sn_size == 12) {
      if (len < 5) { printf("ERROR: rlc bad len\n"); exit(1); }
      rlc_list[rlc_list_size].sn = ((payload[0] & 0x0f) << 4) | payload[1];
      if (rlc_list[rlc_list_size].sn != rlc_list[rlc_list_size-1].sn) { printf("rlc bad sn\n"); exit(1); }
      rlc_list[rlc_list_size].so = (payload[2] << 8) | payload[3];
      rlc_list[rlc_list_size].data = malloc(len - 4); if (rlc_list[rlc_list_size].data == NULL) exit(1);
      memcpy(rlc_list[rlc_list_size].data, payload+4, len-4);
      rlc_list[rlc_list_size].size = len-4;
    } else {
      if (len < 4) { printf("ERROR: rlc bad len\n"); exit(1); }
      rlc_list[rlc_list_size].sn = payload[0] & 0x3f;
      rlc_list[rlc_list_size].so = (payload[1] << 8) | payload[2];
      rlc_list[rlc_list_size].data = malloc(len - 3); if (rlc_list[rlc_list_size].data == NULL) exit(1);
      memcpy(rlc_list[rlc_list_size].data, payload+3, len-3);
      rlc_list[rlc_list_size].size = len-3;
    }
    if (rlc_list[rlc_list_size].sn != last_sn) { printf("ERROR: bad sn\n"); exit(1); }
  } else {
    /* head - no so */
    if (rlc_list_size) abort();
    if (sn_size == 12) {
      if (len < 3) { printf("ERROR: rlc bad len\n"); exit(1); }
      rlc_list[rlc_list_size].sn = ((payload[0] & 0x0f) << 4) | payload[1];
      rlc_list[rlc_list_size].data = malloc(len - 2); if (rlc_list[rlc_list_size].data == NULL) exit(1);
      memcpy(rlc_list[rlc_list_size].data, payload+2, len-2);
      rlc_list[rlc_list_size].size = len-2;
    } else {
      if (len < 2) { printf("ERROR: rlc bad len\n"); exit(1); }
      rlc_list[rlc_list_size].sn = payload[0] & 0x3f;
      rlc_list[rlc_list_size].data = malloc(len - 1); if (rlc_list[rlc_list_size].data == NULL) exit(1);
      memcpy(rlc_list[rlc_list_size].data, payload+1, len-1);
      rlc_list[rlc_list_size].size = len-1;
    }
    if (rlc_list[rlc_list_size].sn != (last_sn + 1) % sn_modulo) { printf("ERROR: bad sn\n"); exit(1); }
  }
  last_sn = rlc_list[rlc_list_size].sn;
  rlc_list[rlc_list_size].rnti = rnti;
  rlc_list_size++;
}

void rlc(unsigned long s, unsigned long mus, int rnti, int harq_id, int hfn, int frame, int slot, unsigned char *payload, int len, int drb_count)
{
  int si;
  int sn_size = 12;

  if (len == 0) { printf("ERROR: empty rlc\n"); return; }
  if (len < 4+4) { printf("ERROR: rlc too small\n"); return; }

  si = payload[0] >> 6;

  if (si == 0x01) { /* head */
printf("head drb_count %d\n", drb_count);
    if (rlc_list_size) { printf("ERROR: head but list not empty frame %d slot %d rnti %d\n", frame, slot, rnti); exit(1); }
    add_list(payload, len, sn_size, rnti, frame, slot);
    printf("DEBUG: rlc added head pdu sn %d\n", rlc_list[rlc_list_size-1].sn);
    return;
  }

  if (si == 0x03) { /* middle */
printf("middle\n");
    if (!rlc_list_size) { printf("ERROR: middle but list empty\n"); exit(1); }
    add_list(payload, len, sn_size, rnti, frame, slot);
    printf("DEBUG: rlc added middle pdu sn %d so %d\n", rlc_list[rlc_list_size-1].sn, rlc_list[rlc_list_size-1].so);
    return;
  }

  if (si == 0x02) { /* end */
printf("end\n");
    int i;
    int pdcp_len;
    unsigned char *x;
    if (!rlc_list_size) { printf("ERROR: end but list empty\n"); exit(1); }
    add_list(payload, len, sn_size, rnti, frame, slot);
    printf("DEBUG: rlc added end pdu sn %d so %d\n", rlc_list[rlc_list_size-1].sn, rlc_list[rlc_list_size-1].so);
    pdcp_len = 0;
    for (i = 0; i < rlc_list_size; i++) pdcp_len += rlc_list[i].size;
    x = payload = malloc(pdcp_len); if (payload == NULL) exit(1);
    for (i = 0; i < rlc_list_size; i++) {
      memcpy(x, rlc_list[i].data, rlc_list[i].size);
      free(rlc_list[i].data);
      x += rlc_list[i].size;
    }
    rlc_list_size = 0;
    pdcp(s, mus, hfn, frame, slot, payload, pdcp_len);
    free(payload);
    return;
  }

  /* full */
  pdcp(s, mus, hfn, frame, slot, payload + 1, len - 1);

#if 0
  static long last_s = -1;
  static long last_mus = -1;
  static int last_frame = -1;
  static int last_slot = -1;

  printf("G %d\n", diff(s, mus, last_s, last_mus));
  if (last_frame == frame && last_slot == slot) printf("dup\n");

  last_frame = frame;
  last_slot = slot;
  last_s = s;
  last_mus = mus;
#endif
}

void mac(long s, long mus, unsigned char *payload, int payload_size, int direction, int rnti, int ueid, int harq_id, int frame, int slot)
{
  static int hfn = 0;
  static int last_frame = -1;

//{ int i; for (i = 0; i < payload_size; i++) printf("%2.2x ", payload[i]); printf("\n"); }
  int pos = 0;

  int drb_count = 0;

  /* only dl */
  if (direction != 1) return;

  if (frame < last_frame) hfn++;
  last_frame = frame;
printf("SF %d %d %d\n", hfn, frame, slot);

  while (pos != payload_size) {
    int f = (payload[pos] >> 6) & 1;
    int lcid = payload[pos] & 0x3f;
    int len;
    pos++;
    switch (lcid) {
    case 0: /* ccch - don't care */
      GETLEN(len);
      pos += len;
      break;
    case 1:
    case 2: /* SRBs - don't care */
      GETLEN(len);
      pos += len;
      break;
    case 7:
      GETLEN(len);
      drb_count++;
      rlc(s, mus, rnti, harq_id, hfn, frame, slot, payload + pos, len, drb_count);
      pos += len;
      break;
    case 61: /* timing advance command - don't care */
      if (pos > payload_size - 1) goto err;
      pos += 1;
      break;
    case 62: /* contention resolution - don't care */
      if (pos > payload_size - 6) goto err;
      pos += 6;
      break;
    case 63: /* padding - to the end */
      pos = payload_size;
      break;
    default: printf("not handled lcid %d\n", lcid); goto err;
    }
  }

  printf("M %d\n", drb_count);

  return;

err:
  printf("ERROR: bad MAC packet\n");
  exit(1);
}

void macpdu(int s, int mus, unsigned char *b, int size)
{
  unsigned char *start = b;
  unsigned char *payload;
  int payload_size;
  int direction;
  int rnti_type;
  int rnti;
  int ueid;
  int harq_id;
  int frame;
  int slot;

  /* accept (and simply ignore) mac-lte stuff */
  unsigned char *a = (unsigned char *)"mac-lte";
  unsigned char *c = b;
  while (*a && *c && *a == *c) { a++; c++; }
  if (!*a) { printf("WARN: skip mac-lte packet\n"); return; }

  /* mac-nr */
  a = (unsigned char *)"mac-nr";
  while (*a && *b && *a == *b) { a++; b++; }
  if (*a || b - start >= size) goto err;

  /* fdd/tdd (don't care) */
  b++; if (b - start >= size) goto err;

  /* direction */
  direction = *b;
  b++; if (b - start >= size) goto err;

  /* rnti_type */
  rnti_type = *b;
  b++; if (b - start >= size) goto err;

  rnti = -1;
  ueid = -1;
  harq_id = -1;
  frame = -1;
  slot = -1;

  while (1) {
    if (b - start + 1 > size) goto err;
    switch (b[0]) {
    case 1: /* payload */
      payload = b+1;
      payload_size = size - (payload - start);
      if (payload_size <= 0) goto err;
      goto got_data;
    case 2: /* rnti */
      if (b - start + 3 > size) goto err;
      rnti = (b[1] << 8) | b[2];
      b += 3;
      break;
    case 3: /* ueid */
      if (b - start + 3 > size) goto err;
      ueid = (b[1] << 8) | b[2];
      b += 3;
      break;
    case 6: /* harq id */
      if (b - start + 2 > size) goto err;
      harq_id= b[1];
      b += 2;
      break;
    case 7: /* frame slot */
      if (b - start + 5 > size) goto err;
      frame = (b[1] << 8) | b[2];
      slot  = (b[3] << 8) | b[4];
      b += 5;
      break;
    }
  }

got_data:
  if (rnti_type != 3) return;
  mac(s, mus, payload, payload_size, direction, rnti, ueid, harq_id, frame, slot);
  return;

err:
  printf("ERROR: bad macpdu\n");
  exit(1);
}

char *get_l(void)
{
  static char *l = NULL;
  static int maxsize = 0;
  int size = 0;
  while (1) {
    int c = getchar(); if (c == '\n' || c == EOF) break;
    if (size == maxsize) { maxsize += 1024; l = realloc(l, maxsize); if (l==NULL) abort(); }
    l[size] = c;
    size++;
  }
  if (size == maxsize) { maxsize += 1024; l = realloc(l, maxsize); if (l==NULL) abort(); }
  if (size == 0) return NULL;
  l[size] = 0;
  return l;
}

int val(char v)
{
  if (v >= '0' && v <= '9') return v - '0';
  if (v >= 'a' && v <= 'f') return v - 'a' + 10;
  if (v >= 'A' && v <= 'F') return v - 'A' + 10;
  printf("ERROR: bad val\n");
  exit(1);
}

void usage(void)
{
  printf("usage: [options] < file.txt\n");
  printf("options:\n");
  printf("    -v\n");
  printf("        be verbose\n");
  printf("    -f\n");
  printf("        use hfn/frame/slot for time, not wireshark timestamps\n");
  printf("    -t\n");
  printf("        diff (in mus) of hfn/frame/slot theoretical time and wireshark timestamps\n");
  printf("    -src <source IP (default 127.0.0.1)>\n");
  printf("    -dst <destination IP (default 127.0.0.1)>\n");
  exit(1);
}

int main(int n, char **v)
{
  long s, mus;
  int size;
  int i;

  src[0] = 127;
  src[1] = 0;
  src[2] = 0;
  src[3] = 1;

  dst[0] = 127;
  dst[1] = 0;
  dst[2] = 0;
  dst[3] = 1;

  for (i = 1; i < n; i++) {
    if (!strcmp(v[i], "-v"))            { verbose = 1; continue; }
    if (!strcmp(v[i], "-f"))            { do_frame_time = 1; continue; }
    if (!strcmp(v[i], "-t"))            { do_time_diff = 1; continue; }
    if (!strcmp(v[i], "-src"))          { if (i > n-2) usage(); i++; if (sscanf(v[i], "%d.%d.%d.%d", &src[0], &src[1], &src[2], &src[3]) != 4) usage(); continue; }
    if (!strcmp(v[i], "-dst"))          { if (i > n-2) usage(); i++; if (sscanf(v[i], "%d.%d.%d.%d", &dst[0], &dst[1], &dst[2], &dst[3]) != 4) usage(); continue; }
    usage();
  }

  if (do_time_diff && do_frame_time) {
    printf("-t and -f cannot be used together\n");
    return 1;
  }

  while (1) {
    unsigned char *l = (unsigned char *)get_l();
    char *p;
    if (l == NULL) break;
    if (sscanf((char *)l, "M %ld.%ld", &s, &mus) != 2) { printf("ERROR; bad input\n"); return 1; }
    p = strchr((char *)l, ':');
    if (p == NULL) { printf("ERROR: bad input\n"); return 1; }
    p++;
    size = 0;
    while (*p) {
      char a = *p++;
      char b = *p++;
      l[size] = val(a) << 4 | val(b);
      size++;
    }
    macpdu(s, mus, l, size);
  }

  return 0;
}
