/* udp client/server - server sends packet of fixed size every 2ms (can be
 * changed with commad line)
 * port 50001 (command line option to change)
 */
#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <time.h>
#include <inttypes.h>
#include <sched.h>

void run_server(int port)
{
  char b[65536];
  int len;
  int id = 0;
  int s = socket(AF_INET, SOCK_DGRAM, 0);
  if (s == -1) { perror("socket"); exit(1); }
  struct sockaddr_in a;
  a.sin_family = AF_INET;
  a.sin_port   = htons(port);
  a.sin_addr.s_addr = inet_addr("0.0.0.0");
  if (bind(s, (struct sockaddr *)&a, sizeof(a))) { perror("bind"); exit(1); }
  struct timespec t;
  struct timespec last;
  if (clock_gettime(CLOCK_REALTIME, &last)) { perror("clock_gettime"); exit(1); }
  int first = 1;
  while (1) {
    socklen_t addrlen = sizeof(a);
    if ((len = recvfrom(s, b, 65536, 0, (struct sockaddr *)&a, &addrlen)) < 1) {
      printf("recvfrom fails\n");
      exit(1);
    }
    if (len < 4) { printf("ERROR: bad packet len %d\n", len); continue; }
    int recv_id = *(int *)b;
    if (recv_id != id) {
      if (recv_id > id)
        printf("ERROR: expect %d get %d\n", id, recv_id);
      else
        printf("ERROR: LOWER expect %d get %d\n", id, recv_id);
      /* reset id */
      if (recv_id > id)
        id = recv_id;
      else
        id--;
      /* special case: recv_id = 0 means restart */
//      if (recv_id == 0) id = 0;
    }
    id++;
    if (clock_gettime(CLOCK_REALTIME, &t)) { perror("clock_gettime"); exit(1); }
    int64_t diff = ((int64_t)t.tv_sec * 1000000 + (int64_t)t.tv_nsec/1000)
                 - ((int64_t)last.tv_sec * 1000000 + (int64_t)last.tv_nsec/1000);
    if (!first) printf("%"PRId64"\n", diff);
    first = 0;
    last = t;
  }
}

struct timespec last;

void time_start(void)
{
  if (clock_gettime(CLOCK_REALTIME, &last)) { perror("clock_gettime"); exit(1); }
}

void delay(int delay_mus)
{
  struct timespec t;
  struct timespec delay;

  if (delay_mus >= 1000 * 1000) { fprintf(stderr, "delay >= 1000*1000\n"); exit(1); }

  last.tv_nsec += delay_mus * 1000;
  if (last.tv_nsec > 1000000000) {
    last.tv_sec++;
    last.tv_nsec -= 1000000000;
  }

  if (clock_gettime(CLOCK_REALTIME, &t)) { perror("clock_gettime"); exit(1); }

  if (last.tv_nsec < t.tv_nsec) {
    delay.tv_nsec = last.tv_nsec - t.tv_nsec + 1000000000;
    delay.tv_sec = last.tv_sec - t.tv_sec - 1;
  } else {
    delay.tv_nsec = last.tv_nsec - t.tv_nsec;
    delay.tv_sec = last.tv_sec - t.tv_sec;
  }

  if (delay.tv_sec < 0 || (delay.tv_sec == 0 && delay.tv_nsec == 0)) { printf("ERROR: we late\n"); return; /* exit(1); */ }

  if (pselect(0, NULL, NULL, NULL, &delay, NULL)) { printf("ERROR: pselect fails\n"); exit(1); }
}

void run_client(char *server, int delay_mus, int size, int port)
{
  char buf[65536];
  int len;
  int id = 0;
  int s = socket(AF_INET, SOCK_DGRAM, 0);
  if (s == -1) { perror("socket"); exit(1); }
  struct sockaddr_in a;
  a.sin_family = AF_INET;
  a.sin_port   = htons(port);
  a.sin_addr.s_addr = inet_addr(server);
  memset(buf, 0, 1024);
  printf("client settings: server %s delay_mus %d size %d\n",
         server, delay_mus, size);
  time_start();
  while (1) {
    *(unsigned int *)buf = id;
    if ((len = sendto(s, buf, size, 0, (struct sockaddr *)&a, sizeof(a))) != size) { printf("ERROR: error sending\n"); exit(1); }
    id++;
    delay(delay_mus);
  }
}

void usage(void)
{
  printf("usage: <options>\n");
  printf("options:\n");
  printf("    -to <ip address>\n");
  printf("        run as client, connect to <ip address>\n");
  printf("    -delay <delay in microseconds (default 2000, ie 2ms)>\n");
  printf("    -size <udp payload size (default 297)>\n");
  printf("    -port <port (default 50001)>\n");
  printf("    -rt <cpu>\n");
  printf("        run in SCHED_RR on given cpu (-1 to not bind to a cpu)\n");
  exit(1);
}

int main(int n, char **v)
{
  int i;
  int server = 1;
  char *ip = NULL;
  int delay = 2 * 1000;
  int size = 297;
  int port = 50001;
  int realtime = 0;
  int cpu = -1;

  for (i = 1; i < n; i++) {
    if (!strcmp(v[i], "-to")) { if (i>n-2) usage(); ip = v[++i]; continue; }
    if (!strcmp(v[i], "-delay")) { if (i>n-2) usage(); delay = atoi(v[++i]); continue; }
    if (!strcmp(v[i], "-delay")) { if (i>n-2) usage(); delay = atoi(v[++i]); continue; }
    if (!strcmp(v[i], "-size")) { if (i>n-2) usage(); size = atoi(v[++i]); continue; }
    if (!strcmp(v[i], "-port")) { if (i>n-2) usage(); port = atoi(v[++i]); continue; }
    if (!strcmp(v[i], "-rt")) { if (i>n-2) usage(); realtime = 1; cpu = atoi(v[++i]); continue; }
    usage();
  }
  if (ip != NULL) server = 0;

  if (realtime) {
    struct sched_param params;
    params.sched_priority = sched_get_priority_max(SCHED_RR);
    if (sched_setscheduler(0, SCHED_RR, &params) == -1) {
      printf("sched_setscheduler failed, you root?\n");
      exit(1);
    }
    cpu_set_t cpumask;
    CPU_ZERO(&cpumask);
    CPU_SET(cpu, &cpumask);
    if (sched_setaffinity(0, sizeof(cpumask), &cpumask) == -1) {
      printf("sched_setaffinity failed\n");
      exit(1);
    }
  }

  if (server) run_server(port);

  run_client(ip, delay, size, port);

  return 0;
}
