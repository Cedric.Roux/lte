#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void usage(void)
{
  printf("to decode: <encoded value>\n");
  printf("to encode: <RBstart> <L>\n");
  exit(1);
}

void decode(int lbw)
{
  int rb_start;
  int l;

  l = 1 + lbw / 275;
  rb_start = lbw - 275 * (l - 1);

  printf("rb_start %d l %d (if l-1 <= 137)\n", rb_start, l);

  l = -(lbw / 275 - 275 - 1);
  rb_start = -(lbw - 275 * (275 - l + 1) - 275 + 1);

  printf("rb_start %d l %d (if l-1 > 137)\n", rb_start, l);
}

void encode(int rb_start, int l)
{
  if (l-1 <= 275/2)
    printf("%d\n", 275 * (l - 1) + rb_start);
  else
    printf("%d\n", 275 * (275 - l + 1) + 275 - 1 - rb_start);
}

int main(int n, char **v)
{
  char *arg1 = NULL;
  char *arg2 = NULL;
  int i;

  for (i = 1; i < n; i++) {
    if (!strcmp(v[i], "-h") || !strcmp(v[i], "--help")) usage();
    if (arg1 == NULL) { arg1 = v[i]; continue; }
    if (arg2 == NULL) { arg2 = v[i]; continue; }
    usage();
  }

  if (arg1 == NULL && arg2 == NULL) usage();

  if (arg2 == NULL) decode(atoi(arg1));
  else              encode(atoi(arg1), atoi(arg2));

  return 0;
}
