/* milenage defined in 35.206 */
/* most of code copied from 35.206 */
/* gcc -Wall -o nsa-keys nsa-keys.c -lcrypto */

#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <openssl/hmac.h>
#include <openssl/evp.h>

/*-------------------------------------------------------------------
 *          Example algorithms f1, f1*, f2, f3, f4, f5, f5*
 *-------------------------------------------------------------------
 *
 * A sample implementation of the example 3GPP authentication and
 * key agreement functions f1, f1*, f2, f3, f4, f5 and f5*. This is
 * a byte-oriented implementation of the functions, and of the block
 * cipher kernel function Rijndael.
 *
 * This has been coded for clarity, not necessarily for efficiency.
 *
 * The functions f2, f3, f4 and f5 share the same inputs and have
 * been coded together as a single function. f1, f1* and f5* are
 * all coded separately.
 *
 *-----------------------------------------------------------------*/
typedef uint8_t u8;
/*--------- Operator Variant Algorithm Configuration Field --------*/
            /*------- Insert your value of OP here -------*/
u8 OP[16] = {0x63, 0xbf, 0xa5, 0x0e, 0xe6, 0x52, 0x33, 0x65,
             0xff, 0x14, 0xc1, 0xf4, 0x5f, 0x88, 0x73, 0x7d};
            /*------- Insert your value of OP here -------*/
/*--------------------------- prototypes --------------------------*/
void f1    ( u8 k[16], u8 rand[16], u8 sqn[6], u8 amf[2],
             u8 mac_a[8] );
void f2345 ( u8 k[16], u8 rand[16],
             u8 res[8], u8 ck[16], u8 ik[16], u8 ak[6] );
void f1star( u8 k[16], u8 rand[16], u8 sqn[6], u8 amf[2],
             u8 mac_s[8] );
void f5star( u8 k[16], u8 rand[16],
             u8 ak[6] );
void ComputeOPc( u8 op_c[16] );
void RijndaelKeySchedule( u8 key[16] );
void RijndaelEncrypt( u8 input[16], u8 output[16] );
/*-------------------------------------------------------------------
 *                            Algorithm f1
 *-------------------------------------------------------------------
 *
 * Computes network authentication code MAC-A from key K, random
 * challenge RAND, sequence number SQN and authentication management
 * field AMF.
 *
 *-----------------------------------------------------------------*/
void f1    ( u8 k[16], u8 rand[16], u8 sqn[6], u8 amf[2],
             u8 mac_a[8] )
{
  u8 op_c[16];
  u8 temp[16];
  u8 in1[16];
  u8 out1[16];
  u8 rijndaelInput[16];
  u8 i;
  RijndaelKeySchedule( k );
  ComputeOPc( op_c );
  for (i=0; i<16; i++)
    rijndaelInput[i] = rand[i] ^ op_c[i];
  RijndaelEncrypt( rijndaelInput, temp );
  for (i=0; i<6; i++)
  {
    in1[i]    = sqn[i];
    in1[i+8]  = sqn[i];
  }
  for (i=0; i<2; i++)
  {
    in1[i+6] = amf[i];
    in1[i+14] = amf[i];
  }
  /* XOR op_c and in1, rotate by r1=64, and XOR *
   * on the constant c1 (which is all zeroes)   */
  for (i=0; i<16; i++)
    rijndaelInput[(i+8) % 16] = in1[i] ^ op_c[i];
  /* XOR on the value temp computed before */
  for (i=0; i<16; i++)
    rijndaelInput[i] ^= temp[i];
  RijndaelEncrypt( rijndaelInput, out1 );
  for (i=0; i<16; i++)
    out1[i] ^= op_c[i];
  for (i=0; i<8; i++)
    mac_a[i] = out1[i];
  return;
} /* end of function f1 */
/*-------------------------------------------------------------------
 *                            Algorithms f2-f5
 *-------------------------------------------------------------------
 *
 * Takes key K and random challenge RAND, and returns response RES,
 * confidentiality key CK, integrity key IK and anonymity key AK.
 *
 *-----------------------------------------------------------------*/
void f2345 ( u8 k[16], u8 rand[16],
             u8 res[8], u8 ck[16], u8 ik[16], u8 ak[6] )
{
  u8 op_c[16];
  u8 temp[16];
  u8 out[16];
  u8 rijndaelInput[16];
  u8 i;
  RijndaelKeySchedule( k );
  ComputeOPc( op_c );
  for (i=0; i<16; i++)
    rijndaelInput[i] = rand[i] ^ op_c[i];
  RijndaelEncrypt( rijndaelInput, temp );
  /* To obtain output block OUT2: XOR OPc and TEMP,    *
   * rotate by r2=0, and XOR on the constant c2 (which *
   * is all zeroes except that the last bit is 1).     */
  for (i=0; i<16; i++)
    rijndaelInput[i] = temp[i] ^ op_c[i];
  rijndaelInput[15] ^= 1;
  RijndaelEncrypt( rijndaelInput, out );
  for (i=0; i<16; i++)
    out[i] ^= op_c[i];
  for (i=0; i<8; i++)
    res[i] = out[i+8];
  for (i=0; i<6; i++)
    ak[i] = out[i];
  /* To obtain output block OUT3: XOR OPc and TEMP,        *
   * rotate by r3=32, and XOR on the constant c3 (which    *
   * is all zeroes except that the next to last bit is 1). */
  for (i=0; i<16; i++)
    rijndaelInput[(i+12) % 16] = temp[i] ^ op_c[i];
  rijndaelInput[15] ^= 2;
  RijndaelEncrypt( rijndaelInput, out );
  for (i=0; i<16; i++)
    out[i] ^= op_c[i];
  for (i=0; i<16; i++)
    ck[i] = out[i];
  /* To obtain output block OUT4: XOR OPc and TEMP,         *
   * rotate by r4=64, and XOR on the constant c4 (which     *
   * is all zeroes except that the 2nd from last bit is 1). */
  for (i=0; i<16; i++)
    rijndaelInput[(i+8) % 16] = temp[i] ^ op_c[i];
  rijndaelInput[15] ^= 4;
  RijndaelEncrypt( rijndaelInput, out );
  for (i=0; i<16; i++)
    out[i] ^= op_c[i];
  for (i=0; i<16; i++)
    ik[i] = out[i];
  return;
} /* end of function f2345 */
/*-------------------------------------------------------------------
 *                            Algorithm f1*
 *-------------------------------------------------------------------
 *
 * Computes resynch authentication code MAC-S from key K, random
 * challenge RAND, sequence number SQN and authentication management
 * field AMF.
 *
 *-----------------------------------------------------------------*/
void f1star( u8 k[16], u8 rand[16], u8 sqn[6], u8 amf[2],
             u8 mac_s[8] )
{
  u8 op_c[16];
  u8 temp[16];
  u8 in1[16];
  u8 out1[16];
  u8 rijndaelInput[16];
  u8 i;
  RijndaelKeySchedule( k );
  ComputeOPc( op_c );
  for (i=0; i<16; i++)
    rijndaelInput[i] = rand[i] ^ op_c[i];
  RijndaelEncrypt( rijndaelInput, temp );
  for (i=0; i<6; i++)
  {
    in1[i]    = sqn[i];
    in1[i+8] = sqn[i];
  }
  for (i=0; i<2; i++)
  {
    in1[i+6] = amf[i];
    in1[i+14] = amf[i];
  }
  /* XOR op_c and in1, rotate by r1=64, and XOR *
   * on the constant c1 (which is all zeroes)   */
  for (i=0; i<16; i++)
    rijndaelInput[(i+8) % 16] = in1[i] ^ op_c[i];
  /* XOR on the value temp computed before */
  for (i=0; i<16; i++)
    rijndaelInput[i] ^= temp[i];
  RijndaelEncrypt( rijndaelInput, out1 );
  for (i=0; i<16; i++)
    out1[i] ^= op_c[i];
  for (i=0; i<8; i++)
    mac_s[i] = out1[i+8];
  return;
} /* end of function f1star */
/*-------------------------------------------------------------------
 *                            Algorithm f5*
 *-------------------------------------------------------------------
 *
 * Takes key K and random challenge RAND, and returns resynch
 * anonymity key AK.
 *
 *-----------------------------------------------------------------*/
void f5star( u8 k[16], u8 rand[16],
             u8 ak[6] )
{
  u8 op_c[16];
  u8 temp[16];
  u8 out[16];
  u8 rijndaelInput[16];
  u8 i;
  RijndaelKeySchedule( k );
  ComputeOPc( op_c );
  for (i=0; i<16; i++)
    rijndaelInput[i] = rand[i] ^ op_c[i];
  RijndaelEncrypt( rijndaelInput, temp );
  /* To obtain output block OUT5: XOR OPc and TEMP,         *
   * rotate by r5=96, and XOR on the constant c5 (which     *
   * is all zeroes except that the 3rd from last bit is 1). */
  for (i=0; i<16; i++)
    rijndaelInput[(i+4) % 16] = temp[i] ^ op_c[i];
  rijndaelInput[15] ^= 8;
  RijndaelEncrypt( rijndaelInput, out );
  for (i=0; i<16; i++)
    out[i] ^= op_c[i];
  for (i=0; i<6; i++)
    ak[i] = out[i];
  return;
} /* end of function f5star */
/*-------------------------------------------------------------------
 * Function to compute OPc from OP and K. Assumes key schedule has
    already been performed.
 *-----------------------------------------------------------------*/
void ComputeOPc( u8 op_c[16] )
{
  u8 i;
  RijndaelEncrypt( OP, op_c );
  for (i=0; i<16; i++)
    op_c[i] ^= OP[i];
  return;
} /* end of function ComputeOPc */

/*-------------------- Rijndael round subkeys ---------------------*/
u8 roundKeys[11][4][4];
/*--------------------- Rijndael S box table ----------------------*/
u8 S[256] = {
 99,124,119,123,242,107,111,197, 48, 1,103, 43,254,215,171,118,
202,130,201,125,250, 89, 71,240,173,212,162,175,156,164,114,192,
183,253,147, 38, 54, 63,247,204, 52,165,229,241,113,216, 49, 21,
  4,199, 35,195, 24,150, 5,154, 7, 18,128,226,235, 39,178,117,
  9,131, 44, 26, 27,110, 90,160, 82, 59,214,179, 41,227, 47,132,
 83,209, 0,237, 32,252,177, 91,106,203,190, 57, 74, 76, 88,207,
208,239,170,251, 67, 77, 51,133, 69,249, 2,127, 80, 60,159,168,
 81,163, 64,143,146,157, 56,245,188,182,218, 33, 16,255,243,210,
205, 12, 19,236, 95,151, 68, 23,196,167,126, 61,100, 93, 25,115,
 96,129, 79,220, 34, 42,144,136, 70,238,184, 20,222, 94, 11,219,
224, 50, 58, 10, 73, 6, 36, 92,194,211,172, 98,145,149,228,121,
231,200, 55,109,141,213, 78,169,108, 86,244,234,101,122,174, 8,
186,120, 37, 46, 28,166,180,198,232,221,116, 31, 75,189,139,138,
112, 62,181,102, 72, 3,246, 14, 97, 53, 87,185,134,193, 29,158,
225,248,152, 17,105,217,142,148,155, 30,135,233,206, 85, 40,223,
140,161,137, 13,191,230, 66,104, 65,153, 45, 15,176, 84,187, 22,
};
/*------- This array does the multiplication by x in GF(2^8) ------*/
u8 Xtime[256] = {
  0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30,
 32, 34, 36, 38, 40, 42, 44, 46, 48, 50, 52, 54, 56, 58, 60, 62,
 64, 66, 68, 70, 72, 74, 76, 78, 80, 82, 84, 86, 88, 90, 92, 94,
 96, 98,100,102,104,106,108,110,112,114,116,118,120,122,124,126,
128,130,132,134,136,138,140,142,144,146,148,150,152,154,156,158,
160,162,164,166,168,170,172,174,176,178,180,182,184,186,188,190,
192,194,196,198,200,202,204,206,208,210,212,214,216,218,220,222,
224,226,228,230,232,234,236,238,240,242,244,246,248,250,252,254,
 27, 25, 31, 29, 19, 17, 23, 21, 11, 9, 15, 13, 3, 1, 7, 5,
 59, 57, 63, 61, 51, 49, 55, 53, 43, 41, 47, 45, 35, 33, 39, 37,
 91, 89, 95, 93, 83, 81, 87, 85, 75, 73, 79, 77, 67, 65, 71, 69,
123,121,127,125,115,113,119,117,107,105,111,109, 99, 97,103,101,
155,153,159,157,147,145,151,149,139,137,143,141,131,129,135,133,
187,185,191,189,179,177,183,181,171,169,175,173,163,161,167,165,
219,217,223,221,211,209,215,213,203,201,207,205,195,193,199,197,
251,249,255,253,243,241,247,245,235,233,239,237,227,225,231,229
};
/*-------------------------------------------------------------------
 * Rijndael key schedule function. Takes 16-byte key and creates
 * all Rijndael's internal subkeys ready for encryption.
 *-----------------------------------------------------------------*/
void RijndaelKeySchedule( u8 key[16] )
{
  u8 roundConst;
  int i, j;
  /* first round key equals key */
  for (i=0; i<16; i++)
    roundKeys[0][i & 0x03][i>>2] = key[i];
  roundConst = 1;
  /* now calculate round keys */
  for (i=1; i<11; i++)
  {
    roundKeys[i][0][0] = S[roundKeys[i-1][1][3]]
                         ^ roundKeys[i-1][0][0] ^ roundConst;
    roundKeys[i][1][0] = S[roundKeys[i-1][2][3]]
                         ^ roundKeys[i-1][1][0];
    roundKeys[i][2][0] = S[roundKeys[i-1][3][3]]
                         ^ roundKeys[i-1][2][0];
    roundKeys[i][3][0] = S[roundKeys[i-1][0][3]]
                         ^ roundKeys[i-1][3][0];
    for (j=0; j<4; j++)
    {
      roundKeys[i][j][1] = roundKeys[i-1][j][1] ^ roundKeys[i][j][0];
      roundKeys[i][j][2] = roundKeys[i-1][j][2] ^ roundKeys[i][j][1];
      roundKeys[i][j][3] = roundKeys[i-1][j][3] ^ roundKeys[i][j][2];
    }
    /* update round constant */
    roundConst = Xtime[roundConst];
  }
  return;
} /* end of function RijndaelKeySchedule */
/* Round key addition function */
void KeyAdd(u8 state[4][4], u8 roundKeys[11][4][4], int round)
{
  int i, j;
  for (i=0; i<4; i++)
    for (j=0; j<4; j++)
      state[i][j] ^= roundKeys[round][i][j];
  return;
}
/* Byte substitution transformation */
int ByteSub(u8 state[4][4])
{
  int i, j;
  for (i=0; i<4; i++)
    for (j=0; j<4; j++)
      state[i][j] = S[state[i][j]];
  return 0;
}
/* Row shift transformation */
void ShiftRow(u8 state[4][4])
{
  u8 temp;
  /* left rotate row 1 by 1 */
  temp = state[1][0];
  state[1][0] = state[1][1];
  state[1][1] = state[1][2];
  state[1][2] = state[1][3];
  state[1][3] = temp;
  /* left rotate row 2 by 2 */
  temp = state[2][0];
  state[2][0] = state[2][2];
  state[2][2] = temp;
  temp = state[2][1];
  state[2][1] = state[2][3];
  state[2][3] = temp;
  /* left rotate row 3 by 3 */
  temp = state[3][0];
  state[3][0] = state[3][3];
  state[3][3] = state[3][2];
  state[3][2] = state[3][1];
  state[3][1] = temp;
  return;
}
/* MixColumn transformation*/
void MixColumn(u8 state[4][4])
{
  u8 temp, tmp, tmp0;
  int i;
  /* do one column at a time */
  for (i=0; i<4;i++)
  {
    temp = state[0][i] ^ state[1][i] ^ state[2][i] ^ state[3][i];
    tmp0 = state[0][i];
    /* Xtime array does multiply by x in GF2^8 */
    tmp = Xtime[state[0][i] ^ state[1][i]];
    state[0][i] ^= temp ^ tmp;
    tmp = Xtime[state[1][i] ^ state[2][i]];
    state[1][i] ^= temp ^ tmp;
    tmp = Xtime[state[2][i] ^ state[3][i]];
    state[2][i] ^= temp ^ tmp;
    tmp = Xtime[state[3][i] ^ tmp0];
    state[3][i] ^= temp ^ tmp;
  }
  return;
}
/*-------------------------------------------------------------------
 * Rijndael encryption function. Takes 16-byte input and creates
 * 16-byte output (using round keys already derived from 16-byte
 * key).
 *-----------------------------------------------------------------*/
void RijndaelEncrypt( u8 input[16], u8 output[16] )
{
  u8 state[4][4];
  int i, r;
  /* initialise state array from input byte string */
  for (i=0; i<16; i++)
    state[i & 0x3][i>>2] = input[i];
  /* add first round_key */
  KeyAdd(state, roundKeys, 0);
  /* do lots of full rounds */
  for (r=1; r<=9; r++)
  {
    ByteSub(state);
    ShiftRow(state);
    MixColumn(state);
    KeyAdd(state, roundKeys, r);
  }
  /* final round */
  ByteSub(state);
  ShiftRow(state);
  KeyAdd(state, roundKeys, r);
  /* produce output byte string from state array */
  for (i=0; i<16; i++)
  {
    output[i] = state[i & 0x3][i>>2];
  }
  return;
} /* end of function RijndaelEncrypt */

/****************************************************************************/
/* code below is original                                                   */
/****************************************************************************/

/* k, rand, op -> ck, ik, ak (35.206 defines it all) */
/* 33.401 annex A derives kASME from MCC/MNC, SQN XOR AK with key CK|IK */
/* 33.401 annex A derives kENB from NAS COUNT with key kASME */

int ishex(int v)
{
  return (v >= '0' && v <= '9') ||
         (v >= 'a' && v <= 'f') ||
         (v >= 'A' && v <= 'F');
}

int unhex(int v)
{
  if (v >= '0' && v <= '9') return v - '0';
  if (v >= 'a' && v <= 'f') return v + 10 - 'a';
  return v + 10 - 'A';
}

unsigned char *add(char *_in, unsigned char *out, int *outlen)
{
printf("adding %s\n", _in);
  unsigned char *in = (unsigned char *)_in;
  while (*in) {
    if (!ishex(in[0]) || !ishex(in[1]))
      { printf("bad input %s\n", _in); exit(1); }
    *outlen = *outlen + 1;
    out = realloc(out, *outlen); if (out == NULL) abort();
    out[*outlen-1] = unhex(in[0]) * 16 + unhex(in[1]);
    in += 2;
  }
  return out;
}

unsigned char *addu(unsigned char *in, int inlen, unsigned char *out, int *outlen)
{
  int i;

  printf("addu: ");
  for (i = 0; i < inlen; i++) printf("%2.2x", in[i]);
  printf("\n");

  for (i = 0; i < inlen; i++) {
    *outlen = *outlen + 1;
    out = realloc(out, *outlen); if (out == NULL) abort();
    out[*outlen-1] = in[i];
  }
  return out;
}

unsigned char *addp(unsigned char *in, int inlen, unsigned char *out, int *outlen)
{
  out = addu(in, inlen, out, outlen);
  unsigned char l[2];
  l[0] = (inlen >> 8) & 255;
  l[1] = inlen & 255;
  out = addu(l, 2, out, outlen);
  return out;
}

#if 0
int main_kdf(int n, char **v)
{
  char *key = NULL;
  char *fc = NULL;
  char *l[n];
  int nl = 0;
  int i;

  unsigned char *k = NULL;
  int klen = 0;
  unsigned char out[256/8];
  unsigned char *data = NULL;
  int datalen = 0;

  for (i = 1; i < n; i++) {
    if (!strcmp(v[i], "-h") || !strcmp(v[i], "--help")) usage();
    if (key == NULL) { key = v[i]; continue; }
    if (fc == NULL) { fc = v[i]; continue; }
    l[nl] = v[i];
    nl++;
  }

  if (key == NULL || fc == NULL || nl == 0) usage();

  k = add(key, k, &klen);

  data = add(fc, data, &datalen);
  for (i = 0; i < nl; i++) {
    char *h = "0123456789abcdef";
    char pp[5];
    int p;
    data = add(l[i], data, &datalen);
    p = strlen(l[i]) / 2;
    if (p > 65535) { printf("too long\n"); exit(1); }
    pp[0] = h[(p >> (3*4)) & 0x0f];
    pp[1] = h[(p >> (2*4)) & 0x0f];
    pp[2] = h[(p >> (1*4)) & 0x0f];
    pp[3] = h[(p >> (0*4)) & 0x0f];
    pp[4] = 0;
    data = add(pp, data, &datalen);
  }

  HMAC(EVP_sha256(), k, klen, data, datalen, out, NULL);

printf("klen %d datalen %d\n", klen, datalen);

  printf("k: ");
  for (i = 0; i < klen; i++) {
    printf("%2.2x", k[i]);
  }
  printf("\n");

  for (i = 0; i < 256/8; i++) {
    printf("%2.2x", out[i]);
  }
  printf("\n");

  return 0;
}
#endif

void usage(void)
{
  printf("gimme, all hex-encoded: op k rand sqn_xor_ak mccmnc nascount skcounter\n");
    printf("lengths (in bytes) are:\n");
    printf("  op:         16\n");
    printf("  k:          16\n");
    printf("  rand:       16\n");
    printf("  sqn_xor_ak: 6\n");
    printf("  mccmnc:     3  (eg. 20892f for 208 92)\n");
    printf("  nascount:   4\n");
    printf("  skcounter:  2\n");
  exit(0);
}

int main(int n, char **v)
{
  unsigned char res[8];
  unsigned char ck[16];
  unsigned char ik[16];
  unsigned char ak[6];
  unsigned char sqn[6];

  char          *_op = NULL;
  unsigned char *op = NULL;
  int           oplen = 0;
  char          *_k = NULL;
  unsigned char *k = NULL;
  int           klen = 0;
  char          *_rand = NULL;
  unsigned char *rand = NULL;
  int           randlen = 0;
  char          *_sqn_xor_ak = NULL;
  unsigned char *sqn_xor_ak = NULL;
  int           sqn_xor_aklen = 0;
  char          *_mccmnc = NULL;
  unsigned char *mccmnc = NULL;
  int           mccmnclen = 0;
  char          *_nascount = NULL;
  unsigned char *nascount = NULL;
  int           nascountlen = 0;
  char          *_skcounter = NULL;
  unsigned char *skcounter = NULL;
  int           skcounterlen = 0;

  int i;

  for (i = 1; i < n; i++) {
    if (!strcmp(v[i], "-h") || !strcmp(v[i], "--help")) usage();
    if (_op == NULL)         { _op         = v[i]; continue; }
    if (_k == NULL)          { _k          = v[i]; continue; }
    if (_rand == NULL)       { _rand       = v[i]; continue; }
    if (_sqn_xor_ak == NULL) { _sqn_xor_ak = v[i]; continue; }
    if (_mccmnc == NULL)     { _mccmnc     = v[i]; continue; }
    if (_nascount == NULL)   { _nascount   = v[i]; continue; }
    if (_skcounter == NULL)  { _skcounter  = v[i]; continue; }
    usage();
  }

  if (_op == NULL || _k == NULL || _rand == NULL || _sqn_xor_ak == NULL ||
      _mccmnc == NULL || _nascount == NULL || _skcounter == NULL) usage();

  op         = add(_op, op, &oplen);
  k          = add(_k, k, &klen);
  rand       = add(_rand, rand, &randlen);
  sqn_xor_ak = add(_sqn_xor_ak, sqn_xor_ak, &sqn_xor_aklen);
  mccmnc     = add(_mccmnc, mccmnc, &mccmnclen);
  nascount   = add(_nascount, nascount, &nascountlen);
  skcounter  = add(_skcounter, skcounter, &skcounterlen);

  if (oplen != 16 || klen != 16 || randlen != 16 || sqn_xor_aklen != 6 ||
      mccmnclen != 3 || nascountlen != 4 || skcounterlen != 2) {
    printf("bad length, we want:\n");
    printf("  op:         16\n");
    printf("  k:          16\n");
    printf("  rand:       16\n");
    printf("  sqn_xor_ak: 6\n");
    printf("  mccmnc:     3  (eg. 20892f for 208 92)\n");
    printf("  nascount:   4\n");
    printf("  skcounter:  2\n");
    exit(1);
  }

  /* reorder mcc/mnc as wanted for computation of kASME */
  {
    unsigned char mcc[3], mnc[3];
    mcc[0] = (mccmnc[0] >> 4) & 0x0f;
    mcc[1] =  mccmnc[0] & 0x0f;
    mcc[2] = (mccmnc[1] >> 4) & 0x0f;
    mnc[0] =  mccmnc[1] & 0x0f;
    mnc[1] = (mccmnc[2] >> 4) & 0x0f;
    mnc[2] =  mccmnc[2] & 0x0f;

    /* SN id = MCC1|MCC0  MNC2|MCC2  MNC1|MNC0 */
    mccmnc[0] = (mcc[1] << 4) | mcc[0];
    mccmnc[1] = (mnc[2] << 4) | mcc[2];
    mccmnc[2] = (mnc[1] << 4) | mnc[0];
  }

  memcpy(OP, op, 16);
  f2345(k, rand, res, ck, ik, ak);

  printf("res: ");
  for (i = 0; i < 8; i++) printf("%2.2x", res[i]);
  printf("\n");
  printf("ck: ");
  for (i = 0; i < 16; i++) printf("%2.2x", ck[i]);
  printf("\n");
  printf("ik: ");
  for (i = 0; i < 16; i++) printf("%2.2x", ik[i]);
  printf("\n");
  printf("ak: ");
  for (i = 0; i < 6; i++) printf("%2.2x", ak[i]);
  printf("\n");

  for (i = 0; i < 6; i++) sqn[i] = sqn_xor_ak[i] ^ ak[i];
  printf("sqn: ");
  for (i = 0; i < 6; i++) printf("%2.2x", sqn[i]);
  printf("\n");

  unsigned char key[32];
  unsigned char kASME[32];
  unsigned char kENB[32];
  unsigned char skGNB[32];
  unsigned char n_up_enc_key[32];
  unsigned char *data = NULL;
  unsigned char fc;
  int datalen;

  /* kASME */
  free(data); data = NULL; datalen = 0;
  memcpy(key, ck, 16);
  memcpy(key+16, ik, 16);
  fc = 0x10;
  data = addu(&fc, 1, data, &datalen);
  data = addp(mccmnc, 3, data, &datalen);
  data = addp(sqn_xor_ak, 6, data, &datalen);
  HMAC(EVP_sha256(), key, 32, data, datalen, kASME, NULL);

  printf("kASME: ");
  for (i = 0; i < 32; i++) printf("%2.2x", kASME[i]);
  printf("\n");

  /* kENB */
  free(data); data = NULL; datalen = 0;
  memcpy(key, kASME, 32);
  fc = 0x11;
  data = addu(&fc, 1, data, &datalen);
  data = addp(nascount, 4, data, &datalen);
  HMAC(EVP_sha256(), key, 32, data, datalen, kENB, NULL);

  printf("kENB: ");
  for (i = 0; i < 32; i++) printf("%2.2x", kENB[i]);
  printf("\n");

  /* skGNB */
  free(data); data = NULL; datalen = 0;
  memcpy(key, kENB, 32);
  fc = 0x1c;
  data = addu(&fc, 1, data, &datalen);
  data = addp(skcounter, 2, data, &datalen);
  HMAC(EVP_sha256(), key, 32, data, datalen, skGNB, NULL);

  printf("skGNB: ");
  for (i = 0; i < 32; i++) printf("%2.2x", skGNB[i]);
  printf("\n");

  /* N-UP-enc-key (33.501 annex A.8) */
  free(data); data = NULL; datalen = 0;
  memcpy(key, skGNB, 32);
  fc = 0x69;
  data = addu(&fc, 1, data, &datalen);
  fc = 0x05;                                /* algorithm type distinguisher */
  data = addp(&fc, 1, data, &datalen);
  /* nea0 = 0, 128-nea1 = 1, 128-nea2 = 2, 128-nea3 = 3 */
  printf("    key computed for: 128-nea2, change code if needed\n");
  fc = 0x02;                                        /* algorithm identifier */
  data = addp(&fc, 1, data, &datalen);
  HMAC(EVP_sha256(), key, 32, data, datalen, n_up_enc_key, NULL);

  printf("N-UP-enc-key: ");
  for (i = 0; i < 32; i++) printf("%2.2x", n_up_enc_key[i]);
  printf("\n");
  printf("       start: ");
  for (i = 0; i < 16; i++) printf("%2.2x", n_up_enc_key[i]);
  printf("\n");
  printf("         end: ");
  for (i = 0; i < 16; i++) printf("%2.2x", n_up_enc_key[i+16]);
  printf("\n");
  printf("         (\"end\" is the one to use)\n");

  return 0;
}
