/* reads lines from textlog, analyses GNB_MAC_DL_PDU_WITH_DATA and
 * GNB_MAC_UL_PDU_WITH_DATA for DL iperf UDP traffic
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdint.h>
#include <arpa/inet.h>

/* options */
int has_sdap   = 0;
int has_iperf3 = 0;

char *getl(void)
{
  static char *l = NULL;
  static int maxsize = 0;
  int size = 0;

#define PUT(c) \
  do { \
    if (size == maxsize) { \
      maxsize += 1024; \
      l = realloc(l, maxsize); \
      if (l == NULL) exit(1); \
    } \
    l[size] = c; \
    size++; \
  } while (0)

  while (1) {
    int c = getchar();
    if (c == EOF && size == 0) return NULL;
    if (c == EOF || c == '\n') break;
    PUT(c);
  }
  PUT(0);
  return l;
}

int unhex(int x)
{
  if (x >= '0' && x <= '9') return x - '0';
  if (x >= 'A' && x <= 'F') return x - 'A' + 10;
  if (x >= 'a' && x <= 'f') return x - 'a' + 10;
  printf("unhex fails\n");
  exit(1);
}

unsigned char *get_buffer(char *s, int *len)
{
  static unsigned char *l = NULL;
  static int maxsize = 0;
  int size = 0;
  int a, b;

  while (*s && *s != '[') s++;
  if (!*s) return NULL;
  s++;
  while (1) {
    while (*s && isspace(*s)) s++;
    if (!*s) return NULL;
    if (*s == ']') break;
    a = *s;
    s++; if (!*s) return NULL;
    b = *s;
    s++;
    a = unhex(a);
    b = unhex(b);
    a = a * 16 + b;
    PUT(a);
  }

  *len = size;
  return l;
}

void iperf(unsigned char *b, int blen)
{
  static uint32_t next_seq = 0;
  uint32_t seq;

{int i; printf("iperf [%d] ", blen);
 for (i = 0; i < 32; i++) printf("%2.2x ", b[i]);
 printf("... ");
 for (i = blen-10; i < blen; i++) printf("%2.2x ", b[i]);
 printf("\n"); }

  seq = (b[0] << 24) | (b[1] << 16) | (b[2] << 8) | b[3];
  if (seq != next_seq) {
    if (seq & 0x80000000) {
      printf("iperf bad last packet marker (%d [%d]), reset next_seq to 0 (was %d\n", seq, seq & 0x7fffffff, next_seq);
      next_seq = 0;
      return;
    }
    printf("iperf bad seq detected, wanted %d, got %d\n", next_seq, seq);
    /*exit(1);*/
  }

  unsigned char v = 0x30;
  int i;
  for (i = 20; i < blen; i++) {
    if (b[i] != v) { printf("iperf dead data\n"); } //exit(1); }
    v++;
    if (v == 0x3a) v = 0x30;
  }

  next_seq++;
}

void iperf3(unsigned char *b, int blen)
{
  static uint32_t next_seq = 1;
  uint32_t seq;
  static uint64_t last_t = 0;
  uint64_t t;

{int i; printf("iperf3 [%d] ", blen);
 for (i = 0; i < 32; i++) printf("%2.2x ", b[i]);
 printf("... ");
 for (i = blen-10; i < blen; i++) printf("%2.2x ", b[i]);
 } //printf("\n"); }

  seq = (b[8] << 24) | (b[9] << 16) | (b[10] << 8) | b[11];
  printf(" seq %d\n", seq);
  if (seq != next_seq) {
    if (seq & 0x80000000) {
      printf("iperf3 bad last packet marker (%d [%d]), reset next_seq to 0 (was %d\n", seq, seq & 0x7fffffff, next_seq);
      next_seq = 0;
      return;
    }
    printf("iperf3 bad seq detected, wanted %d, got %d\n", next_seq, seq);
    /*exit(1);*/
  }

  t = ((uint64_t)b[0] << (32+24)) |
      ((uint64_t)b[1] << (32+16)) |
      ((uint64_t)b[2] << (32+ 8)) |
      ((uint64_t)b[3] << (32+ 0)) |
      ((uint64_t)b[4] << (   24)) |
      ((uint64_t)b[5] << (   16)) |
      ((uint64_t)b[6] << (    8)) |
      ((uint64_t)b[7] << (    0));
  if (t <= last_t) printf("iperf3 bad t (%16.16lx, prev %16.16lx)\n", t, last_t);
  last_t = t;

#if 0
  unsigned char v = 0x30;
  int i;
  for (i = 20; i < blen; i++) {
    if (b[i] != v) { printf("iperf dead data\n"); } //exit(1); }
    v++;
    if (v == 0x3a) v = 0x30;
  }
#endif

  next_seq++;
}

uint16_t udp_checksum(void *_a, size_t len, uint32_t src_addr, uint32_t dest_addr)
{
  const uint16_t *buf = _a;
  uint16_t *ip_src = (void*)&src_addr, *ip_dst = (void*)&dest_addr;
  uint32_t sum;
  size_t length = len;

  // Calculate the sum
  sum = 0;
  while (len > 1)
  {
    sum += *buf++;
    if (sum & 0x80000000)
      sum = (sum & 0xFFFF) + (sum >> 16);
    len -= 2;
  }

  if (len & 1)
    // Add the padding if the packet lenght is odd
    sum += *((uint8_t*)buf);

  // Add the pseudo-header
  sum += *(ip_src++);
  sum += *ip_src;

  sum += *(ip_dst++);
  sum += *ip_dst;

  //sum += htons(IPPROTO_UDP);
  sum += htons(17);
  sum += htons(length);

  // Add the carries
  while (sum >> 16)
    sum = (sum & 0xFFFF) + (sum >> 16);

  // Return the one's complement of sum
  return (uint16_t)~sum;
}

void udp(unsigned char *b, int blen, uint32_t src, uint32_t dst)
{
{int i; printf("udp [%d] ", blen);
 for (i = 0; i < 32; i++) printf("%2.2x ", b[i]);
 printf("... ");
 for (i = blen-10; i < blen; i++) printf("%2.2x ", b[i]);
 printf("\n"); }
  int src_port, dst_port, len;

  src_port = (b[0] << 8) | b[1];
  dst_port = (b[2] << 8) | b[3];
  len = (b[4] << 8) | b[5];

  printf("udp src %d dst %d len %d\n", src_port, dst_port, len);

  printf("udp checksum %d\n", udp_checksum(b, blen, src, dst));

  if (has_iperf3)
    iperf3(b+8, blen-8);
  else
    iperf(b+8, blen-8);
}

static unsigned short compute_checksum(void *_a, unsigned int count) {
 unsigned short *addr = _a;
  register unsigned long sum = 0;
  while (count > 1) {
    sum += * addr++;
    count -= 2;
  }
  //if any bytes left, pad the bytes and add
  if(count > 0) {
abort();
//    sum += ((*addr)&htons(0xFF00));
  }
  //Fold sum to 16 bits: add carrier to result
  while (sum>>16) {
      sum = (sum & 0xffff) + (sum >> 16);
  }
  //one's complement
  sum = ~sum;
  return ((unsigned short)sum);
}

void ip(unsigned char *b, int blen)
{
{int i; printf("ip [%d] ", blen);
 for (i = 0; i < 32; i++) printf("%2.2x ", b[i]);
 printf("... ");
 for (i = blen-10; i < blen; i++) printf("%2.2x ", b[i]);
 printf("\n"); }

  uint32_t src, dst;

  /* hackish */
  if (blen < 50) return;
  if (b[9] != 17) { printf("ip bad protocol %d, only 17 supported\n", b[9]); return; /* ip.proto = UDP */ }

  printf("ip src %d.%d.%d.%d dst %d.%d.%d.%d\n", b[12], b[13], b[14], b[15],
         b[16], b[17], b[18], b[19]);

  printf("ip checksum %d\n", compute_checksum(b, 20));
  if (compute_checksum(b, 20)) { printf("ip bad checksum\n"); exit(1); }

  /* big endian expected by udp checksum */
  src = (b[15] << 24) | (b[14] << 16) | (b[13] << 8) | b[12];
  dst = (b[19] << 24) | (b[18] << 16) | (b[17] << 8) | b[16];

  udp(b+20, blen-20, src, dst);
}

void pdcp(unsigned char *b, int blen, int pdcp_sn_size)
{
  static int next_sn = 0;
  int sn;
  int dc;

  if (pdcp_sn_size != 18) abort();

  if (blen < 3) abort();

  dc = b[0] >> 7;
  if (dc == 0) { printf("pdcp failure: dc not 1\n"); exit(1); }

  sn = ((b[0] & 0x3) << 16) | (b[1] << 8) | b[2];

  if (sn != next_sn) { printf("pdcp bad sn %d, expected %d\n", sn, next_sn); exit(1); }
  next_sn++;

  blen -= 3;
  b += 3;

  printf("pdcp data size %d sn %d\n", blen, sn);

  if (has_sdap) {
    /* hardcoded: sdap eats 1 byte */
    if (blen < 1) abort();
    if (*b != 1) abort();
    blen--;
    b++;
  }

  ip(b, blen);
}

#define BAD do { printf("%s:%d: bad packet\n", __FUNCTION__, __LINE__); exit(1); } while (0)

typedef struct {
  int start;
  int len;
} segment_t;

typedef struct {
  int next_sn;
  unsigned char data[65536];
  segment_t s[65536];
  int ssize;
} rlc_am_t;

void rlc_am_status(unsigned char *b, int blen, int rlc_sn_size, int is_ul)
{
  int cpt;
  int ack_sn;
  int e1;

  if (!is_ul) { printf("bad: dl rlc am status not handled\n"); return; exit(1); }

  if (rlc_sn_size != 18) abort();

  cpt = (b[0] >> 4) &0x07;
  ack_sn = ((b[0] & 0x0f) << 14) | (b[1] << 6) | (b[2] >> 2);
  e1 = (b[2] >> 1) & 1;

  printf("rlc am ack cpt %d ack_sn %d e1 %d\n", cpt, ack_sn, e1);
}

void rlc_am_add_segment(rlc_am_t *rlc, int start, int len)
{
  int i;
  if (rlc->ssize == 65536) abort();
  rlc->s[rlc->ssize].start = start;
  rlc->s[rlc->ssize].len = len;
  /* order segment list */
  for (i = rlc->ssize - 1; i >= 0; i--) {
    segment_t t;
    if (rlc->s[i].start < start) break;
    t = rlc->s[i];
    rlc->s[i] = rlc->s[i+1];
    rlc->s[i+1] = t;
  }

  rlc->ssize++;
}

void rlc_am_deliver(rlc_am_t *rlc, unsigned char *b, int blen, int pdcp_sn_size)
{
  int start = -1;
  int new_start;
  int i;
  /* check no hole in segment list */
  for (i = 0; i < rlc->ssize; i++) {
    if (rlc->s[i].start > start + 1) { printf("rlc segment missing\n"); exit(1); }
    new_start = rlc->s[i].start + rlc->s[i].len;
    if (new_start > start) start = new_start;
  }
  /* full, deliver to pdcp */
  pdcp(rlc->data, new_start, pdcp_sn_size);
}

static rlc_am_t rlc;
void rlc_am(unsigned char *b, int blen, int pdcp_sn_size, int rlc_sn_size, int is_ul)
{
  int dc;
  int p;
  int si;
  int sn;
  int so = 0;

  dc = (b[0] >> 7) & 1;

  if (dc == 0) return rlc_am_status(b, blen, rlc_sn_size, is_ul);

  if (is_ul) { printf("bad: ul data traffic not handled\n"); return; exit(1); }

  p = (b[0] >> 6) & 1;
  si = (b[0] >> 4) & 3;

  if (rlc_sn_size == 12) {
    if (blen < 2) BAD;
    sn = ((b[0] & 0x0f) << 8) | b[1];
    b += 2;
    blen -= 2;
  } else {
    if (blen < 3) BAD;
    sn = ((b[0] & 0x03) << 16) | (b[1] << 8) | b[2];
    b += 3;
    blen -= 3;
  }

  if (sn != rlc.next_sn) { printf("rlc am bad sn %d, expected %d [dc %d p %d si %d sn %d]\n", sn, rlc.next_sn, dc, p, si, sn); return; exit(1); }

  /* si: 0  full sdu
   * si: 1  start of sdu
   * si: 2  end of sdu
   * si: 3  middle of sdu
   */
  printf("rlc am dc %d p %d si %d sn %d\n", dc, p, si, sn);

  if (si == 2 || si == 3) {
    if (blen < 2) BAD;
    so = (b[0] << 8) | b[1];
    b += 2;
    blen -= 2;
  }

  if (so + blen > 65536) { printf("bad rlc am\n"); exit(1); }
  memcpy(rlc.data + so, b, blen);
  rlc_am_add_segment(&rlc, so, blen);

  if (si == 0 || si == 2) {
    rlc_am_deliver(&rlc, b, blen, pdcp_sn_size);
    rlc.next_sn++;
    rlc.ssize = 0;
  }
}

#define SIZE(x) do { \
    int bcount; \
    if (f == 0) bcount = 1; else bcount = 2; \
    if (blen < bcount) BAD; \
    if (f == 0) { \
      x = b[0]; \
      b++; \
      blen--; \
    } else { \
      x = (b[0] << 8) | b[1]; \
      b += 2; \
      blen -= 2; \
    } \
printf("SIZE: blen %d bcount %d size %d b[0] %x b[1] %x\n", blen, bcount, x, b[0], b[1]); \
    if (blen < x) BAD; \
  } while (0)

void mac_dl(unsigned char *b, int blen, int pdcp_sn_size, int rlc_sn_size)
{
printf("--------------------\n");
printf("dl packet size %d\n", blen);

  int f;
  int lcid;
  int plen;
  while (blen > 0) {
printf("start of while: blen %d\n", blen);
    f = (b[0] >> 6) & 1;
    lcid = b[0] & 0x3f;
    printf("lcid %d\n", lcid);
    switch (lcid) {
    case 63: /* padding (til the end) */ blen = 0; break;
    case 62: /* ue contention resolution */ blen -= 7; b += 7; break;
    case 61: /* timing advance */ blen -= 2; b += 2; break;
    case 0: /* ccch */ b++; blen--; SIZE(plen); b += plen; blen -= plen; break;
    case 1: /* srb 1 */ b++; blen--; SIZE(plen); b += plen; blen -= plen; break;
    case 4: /* drb 1 */ b++; blen--; SIZE(plen); rlc_am(b, plen, pdcp_sn_size, rlc_sn_size, 0); b += plen; blen -= plen; break;

    default: printf("unsupported dl lcid %d\n", lcid); exit(1);
    }
  }
  if (blen < 0) { printf("bad dl packet\n"); exit(1); }
}

void mac_ul(unsigned char *b, int blen, int pdcp_sn_size, int rlc_sn_size)
{
printf("--------------------\n");
printf("ul packet size %d\n", blen);

  int f;
  int lcid;
  int plen;
  while (blen > 0) {
printf("start of while: blen %d\n", blen);
    f = (b[0] >> 6) & 1;
    lcid = b[0] & 0x3f;
    printf("lcid %d\n", lcid);
    switch (lcid) {
    case 63: /* padding (til the end) */ blen = 0; break;
    case 62: /* long bsr */ {
      int v;
      int i;
      b++; blen--;
      if (blen < 1) BAD;
      v = *b; b++; blen--;
      printf("long bsr 1st byte %2.2x\n", v);
      for (i = 0; i < 8; i++, v >>= 1) if (v & 1) { blen--; b++; }
      break;
    }
    case 61: /* short bsr */ b += 2; blen -= 2; break;
    case 57: /* singer entry phr */ b += 3; blen -= 3; break;
    case 0: /* ccch 64 bits */ b++; blen--; b += 64/8; blen -= 64/8; break;
    case 1: /* srb 1 */ b++; blen--; SIZE(plen); b += plen; blen -= plen; break;
    case 4: /* drb 1 */ b++; blen--; SIZE(plen); rlc_am(b, plen, pdcp_sn_size, rlc_sn_size, 1); b += plen; blen -= plen; break;
    case 52: /* ccch 48 bits */ b++; blen--; b += 48/8; blen -= 48/8; break;
    default: printf("unsupported ul lcid %d\n", lcid); exit(1);
    }
  }
  if (blen < 0) { printf("bad ul packet\n"); return; exit(1); }
}

void usage(void)
{
  printf("options:\n");
  printf("    -sdap\n");
  printf("        PDCP data has the SDAP 1 byte header\n");
  printf("    -iperf3\n");
  printf("        trace is with iperf 3, not iperf 2\n");
  exit(1);
}

int main(int n, char **v)
{
  int pdcp_sn_size = 18;
  int rlc_sn_size = 18;

  char *l;
  unsigned char *b;
  int blen;

  int i;

  for (i = 1; i < n; i++) {
    if (!strcmp(v[i], "-sdap")) { has_sdap = 1; continue; }
    if (!strcmp(v[i], "-iperf3")) { has_iperf3 = 1; continue; }
    usage();
  }

  while (1) {
    l = getl();
    if (l == NULL) break;
    if (strstr(l, "GNB_MAC_DL_PDU_WITH_DATA") != NULL
        || strstr(l, "GNB_MAC_RETRANSMISSION_DL_PDU_WITH_DATA") != NULL) {
      b = get_buffer(l, &blen);
      if (b == NULL || blen == 0) { printf("bad GNB_MAC_DL_PDU_WITH_DATA or GNB_MAC_RETRANSMISSION_DL_PDU_WITH_DATA\n"); exit(1); }
      mac_dl(b, blen, pdcp_sn_size, rlc_sn_size);
    } else
    if (strstr(l, "GNB_MAC_UL_PDU_WITH_DATA") != NULL) {
      b = get_buffer(l, &blen);
      if (b == NULL || blen == 0) { printf("bad GNB_MAC_UL_PDU_WITH_DATA\n"); exit(1); }
      mac_ul(b, blen, pdcp_sn_size, rlc_sn_size);
    }
  }

  return 0;
}
