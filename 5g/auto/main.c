#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <poll.h>
#include <fcntl.h>

#include "scenario.h"
#include "input.h"
#include "shell.h"
#include "util.h"
#include <sys/wait.h>

void throw(task_t *task, input_t *input, int len)
{
  fwrite(input->buffer, len, 1, task->out);
  fflush(task->out);
//fwrite(input->buffer, len, 1, stdout);
  memmove(input->buffer, input->buffer + len, input->size - len);
  input->size -= len;

  task->logsize += len;
  /* limit log size to 1,000,000 */
  if (task->logsize > 1000000) {
//  if (task->logsize > 10000) {
    char log[1024 + 1024];
    sprintf(log, "cp %s/%s %s/%s.0", task->logdir, task->name, task->logdir, task->name);
    /* todo: check return value of system() */
    system(log);
    if (ftruncate(fileno(task->out), 0)) { perror("ftruncate"); exit(1); }
    rewind(task->out);
    task->logsize = 0;
  }
}

char *get_prompt(task_t *task, input_t *input, int fd, char *PS1)
{
  char *ret;
  while (1) {
    int l = get_input(input, fd, 4096);
    if (l == -1) return NULL;
//printf("before memmem (%d) [", input->size);
//for (int i = 0; i < input->size; i++) printf(" %2.2x", (unsigned char)input->buffer[i]);
//printf("\n");
    ret = memmem(input->buffer, input->size, PS1, strlen(PS1)); if (ret != NULL) break;
    int i;
    for (i = input->size-1; i >= 0; i--) if (input->buffer[i] == '\n') break;
    if (i >= 0) throw(task, input, i+1);
//printf("ret %p\n", ret);
  }
//printf("GOT PROMPT!!\n");
  return ret;
}

typedef struct { task_t *t; char *sudo_password; char *PS1; shell_t *s; } thread_data_t;
void *run_task_thread(void *_td)
{
  thread_data_t *td = _td;
  task_t *t           = td->t;
  char *sudo_password = td->sudo_password;
  char *PS1           = td->PS1;
  shell_t *s          = td->s;
  input_t *input = new_input();
  bool sudo;
  char *sudo_prompt = "AUTOSUDO:";

  bool nofail = false;

  for (int i = 0; i < t->commands_count; i++) {
//printf("***** start loop i %d command '%s'\n", i, t->commands[i].command); fflush(stdout);
    char *p = get_prompt(t, input, s->fd, PS1);
    if (p == NULL) goto error;
    throw(t, input, p - input->buffer + strlen(PS1));
    if (t->commands[i].options.nofail != nofail) {
      nofail = t->commands[i].options.nofail;
      if (nofail) { if (!my_fullwrite(s->fd, "set -e\n")) goto error; }
      else { if (!my_fullwrite(s->fd, "set +e\n")) goto error; }
      char *p = get_prompt(t, input, s->fd, PS1);
      if (p == NULL) goto error;
      throw(t, input, p - input->buffer + strlen(PS1));
    }
    /* sudo: force -k and use -p for reliable prompt */
    if (!my_strncmp(t->commands[i].command, "sudo")) sudo = true; else sudo = false;
    if (sudo) {
      char t[1024];
      /* hack to hide prompt */
      sprintf(t, "sudo -k -p %c\"\"%s", sudo_prompt[0], sudo_prompt + 1);
      if (!my_fullwrite(s->fd, t)) goto error;
    }
    if (!my_fullwrite(s->fd, t->commands[i].command + (sudo ? 4 : 0))) goto error;
    if (!my_fullwrite(s->fd, "\n")) goto error;
    if (sudo) {
      /* send sudo password */
      p = get_prompt(t, input, s->fd, sudo_prompt);
      if (p == NULL) goto error;
      throw(t, input, p - input->buffer + strlen(sudo_prompt));
//printf("sending sudo password '%s'\n", sudo_password);
//printf("%d\n", __LINE__); fflush(stdout);
      if (!my_fullwrite(s->fd, sudo_password)) goto error;
//printf("%d\n", __LINE__); fflush(stdout);
      if (!my_fullwrite(s->fd, "\n")) goto error;
//printf("%d\n", __LINE__); fflush(stdout);
    }
  }

  while (1) {
    throw(t, input, input->size);
    if (get_input(input, s->fd, 4096) == -1) goto over;
  }

//printf("%d\n", __LINE__); fflush(stdout);
  goto over;

error:
  printf("task '%s' in error, let's finish input\n", t->name);
  while (1) {
    throw(t, input, input->size);
    if (get_input(input, s->fd, 4096) == -1) goto over;
  }

over:
//printf("%d\n", __LINE__); fflush(stdout);
  throw(t, input, input->size);
  fclose(t->out);
  free_input(input);
  free(_td);

  t->dead = true;

  return (void*)1;
}

void run_task(task_t *t, char *sudo_password, char *PS1)
{
  thread_data_t *td = malloc(sizeof(*td)); if (td == NULL) abort();
  char log[1024];
  if (strlen(t->logdir) + strlen(t->name) > 1000) { fprintf(stderr, "error: name for logfile %s/%s is too long\n", t->logdir, t->name); exit(1); }
  sprintf(log, "%s/%s", t->logdir, t->name);
  t->out = fopen(log, "w"); if (t->out == NULL) { perror(log); exit(1); }
  td->t = t;
  td->sudo_password = sudo_password;
  td->PS1 = PS1;
  shell_t *s = new_shell(PS1);
  td->s = s;
  t->pid = s->pid;
  t->pthread_id = new_thread(run_task_thread, td);
}

void usage(void)
{
  printf("usage: [options] <scenario file>\n");
  printf("options:\n");
  printf("    -t [task]\n");
  printf("        execute this task only\n");
  printf("    -d [task]\n");
  printf("        disable this task\n");
  printf("    -s [password]\n");
  printf("        sudo password\n");
  printf("        WARNING: this is not secure, don't use in sensitive environments\n");
  exit(0);
}

static bool disabled_task(char *name, char **disabled_tasks, int disabled_tasks_count)
{
  /* todo: optimize? */
  for (int i = 0; i < disabled_tasks_count; i++)
    if (!strcmp(name, disabled_tasks[i])) return true;
  return false;
}

/* return -1 on timeout, n if log[n] is found */
int wait_log(task_t *t, char **log, int log_count, int timeout_ms)
{
  int ret = -1;

  printf("[auto] wait for some logs from task '%s'\n", t->name);
  printf("       waiting on those %d logs;\n", log_count);
  for(int i = 0; i < log_count; i++)
    printf("       '%s'\n", log[i]);

  char logfile[1024];
  if (strlen(t->name) + strlen(t->logdir) > 1000) abort();
  sprintf(logfile, "%s/%s", t->logdir, t->name);
  int f = open(logfile, O_RDONLY);
  if (f == -1) usleep(1000);
  char in[4096];
  int in_size = 0;
  int advance_len = 0;
  for (int i = 0; i < log_count; i++)
    if (strlen(log[i]) > advance_len) advance_len = strlen(log[i]);
  struct timespec start, cur;
  if (clock_gettime(CLOCK_REALTIME, &start)) abort();
  while (1) {
    if (clock_gettime(CLOCK_REALTIME, &cur)) abort();
    int delta_t_ms = 1000 * (cur.tv_sec - start.tv_sec);
    if (cur.tv_nsec < start.tv_nsec)
      delta_t_ms += -1000 + (1000000000 - start.tv_nsec + cur.tv_nsec) / 1000000;
    else
      delta_t_ms += (cur.tv_nsec - start.tv_nsec) / 1000000;
    delta_t_ms = timeout_ms - delta_t_ms;
    if (delta_t_ms <= 0) { printf("[auto] timeout\n"); break; }
    struct pollfd fds[1];
    fds[0].fd = f;
    fds[0].events = POLLIN;
    int r = poll(fds, 1, delta_t_ms);
    if (r == -1) { perror("poll"); exit(1); }
    if (r == 0) continue;
    if (fds[0].revents != POLLIN) { printf("[auto] bad return from poll, revents %d\n", fds[0].revents); exit(1); }
    r = read(f, in + in_size, 4096 - in_size);
    if (r == 0) { usleep(1000); continue; }
    if (r < 0) { printf("[auto] read fails, returns %d\n", r); exit(1); }
    in_size += r;
    if (in_size < advance_len) continue;
    for (int i = 0; i < log_count; i++)
      if (memmem(in, in_size, log[i], strlen(log[i])) != NULL) { ret = i; goto done; }
    /* advance */
    memmove(in, in + in_size - advance_len, advance_len);
    in_size = advance_len;
  }

done:
  printf("[auto] wait done, returns %d\n", ret);
  return ret;
}

int main(int n, char **v)
{
  char *sudo_password = NULL;
  char *logdir = "/tmp";
  char *scenario_file = NULL;
  scenario_t *scenario;
  char *task = NULL;
  char *disabled_tasks[n];
  int disabled_tasks_count = 0;
  char **log = NULL;
  int log_count = 0;
  int timeout_ms = 3000;
  bool sudo_from_stdin = false;

  for (int i = 1; i < n; i++) {
    if (!strcmp(v[i], "-h")) usage();
    if (!strcmp(v[i], "-t")) { if(task!=NULL)usage(); if (i>n-2) usage(); task = v[++i]; continue; }
    if (!strcmp(v[i], "-d")) { if (i>n-2) usage(); disabled_tasks[disabled_tasks_count++] = v[++i]; continue; }
    if (!strcmp(v[i], "-s")) { if (i>n-2) usage();
      sudo_password = strdup(v[++i]); if (sudo_password == NULL) abort();
      memset(v[i-1], 0, strlen(v[i-1]));
      memset(v[i], 0, strlen(v[i]));
      continue; }
    if (scenario_file == NULL) { scenario_file = v[i]; continue; }
    usage();
  }
  if (scenario_file == NULL) usage();

  scenario = read_scenario(scenario_file);

  /* check that tasks in -d exist */
  for (int i = 0; i < disabled_tasks_count; i++) get_task(scenario, disabled_tasks[i]);

  if (sudo_password == NULL) {
    sudo_password = strdup(getpass("sudo password: ")); if (sudo_password == NULL) abort();
    sudo_from_stdin = true;
  }

  if (task != NULL) {
    /* run task */
    task_t *t = get_task(scenario, task);
    t->logdir = logdir;
    printf("[auto] start task '%s' log file: %s/%s\n", t->name, t->logdir, t->name);
    run_task(t, sudo_password, "AUTOSHELL:");
    /* wait for end of task */
    int status;
    if (waitpid(t->pid, &status, 0) == -1) { perror("waitpid"); exit(1); }
    printf("[auto] task '%s' exit status %d\n", t->name, status);
    void *r = wait_thread(t->pthread_id);
    printf("[auto] thread returns %p\n", r);
    if (status) exit(1);
    return 0;
  }

  for (int i = 0; i < scenario->actions_count; i++) {
    action_t *a = &scenario->actions[i];
    switch (a->type) {
    case ACTION_START: {
      task_t *t = get_task(scenario, a->start.name);
      if (disabled_task(t->name, disabled_tasks, disabled_tasks_count)) {
        printf("[auto] skip start of task '%s'\n", t->name);
        break;
      }
      t->logdir = logdir;
      printf("[auto] start task '%s' log file: %s/%s\n", t->name, t->logdir, t->name);
      run_task(t, sudo_password, "AUTOSHELL:");
      break;
    }
    case ACTION_WAIT: {
      task_t *t = get_task(scenario, a->start.name);
      int status;
      if (disabled_task(t->name, disabled_tasks, disabled_tasks_count)) {
        printf("[auto] skip wait of task '%s'\n", t->name);
        break;
      }
      if (waitpid(t->pid, &status, 0) == -1) { perror("waitpid"); exit(1); }
      printf("[auto] task '%s' exit status %d\n", t->name, status);
      void *r = wait_thread(t->pthread_id);
      printf("[auto] thread returns %p\n", r);
      if (status) exit(1);
      break;
    }
    case ACTION_USER_INTERRUPT: {
      printf("press enter to continue\n");
      getchar();
      break;
    }
    case ACTION_LOG: {
      log_count++;
      log = realloc(log, log_count * sizeof(char *)); if (log == NULL) abort();
      log[log_count - 1] = a->log.s;
      break;
    }
    case ACTION_TIMEOUT: {
      timeout_ms = a->timeout.t_ms;
      break;
    }
    case ACTION_WAITLOG: {
      task_t *t = get_task(scenario, a->waitlog.name);
      if (disabled_task(t->name, disabled_tasks, disabled_tasks_count)) {
        printf("[auto] skip waitlog of task '%s'\n", t->name);
        break;
      }
      int r = wait_log(t, log, log_count, timeout_ms);
      if (r == -1) { printf("[auto] error: timeout waiting for some logs\n"); exit(1); }
      free(log);
      log = NULL;
      log_count = 0;
      break;
    }
    }
  }

  if (sudo_from_stdin) {
    memset(sudo_password, 0, strlen(sudo_password));
    free(sudo_password);
  }

  return 0;
}
