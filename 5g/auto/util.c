#include "util.h"

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h>
#include <pthread.h>

static char *line;
static int line_size;
static int line_maxsize;

static void put(char c)
{
  if (line_size == line_maxsize) {
    line_maxsize += 512;
    line = realloc(line, line_maxsize); if (line == NULL) abort();
  }
  line[line_size] = c;
  line_size++;
}

char *next_line(void *in)
{
  int c;
  int leading_space = 1;

  line_size = 0;

  while (1) {
    c = fgetc(in);
    if (c == EOF || c == '\n') break;
    if (leading_space && isspace(c)) continue;
    leading_space = 0;
    put(c);
  }

  if (c == EOF && line_size == 0) return NULL;

  put(0);
  return line;
}

void clear_line(void)
{
  free(line);
  line = NULL;
  line_size = line_maxsize = 0;
}

bool fullwrite(int fd, char *buf, int count)
{
  int l;
  while (count) {
    l = write(fd, buf, count);
    if (l <= 0) return false;
    count -= l;
    buf += l;
  }
  return true;
}

pthread_t new_thread(void *(*f)(void *), void *data)
{
  pthread_t t;
  pthread_attr_t att;

  if (pthread_attr_init(&att))
    { fprintf(stderr, "pthread_attr_init err\n"); exit(1); }
//  if (pthread_attr_setdetachstate(&att, PTHREAD_CREATE_DETACHED))
//    { fprintf(stderr, "pthread_attr_setdetachstate err\n"); exit(1); }
  if (pthread_attr_setstacksize(&att, 10000000))
    { fprintf(stderr, "pthread_attr_setstacksize err\n"); exit(1); }
  if (pthread_create(&t, &att, f, data))
    { fprintf(stderr, "pthread_create err\n"); exit(1); }
  if (pthread_attr_destroy(&att))
    { fprintf(stderr, "pthread_attr_destroy err\n"); exit(1); }

  return t;
}

void *wait_thread(pthread_t id)
{
  void *ret;
  if (pthread_join(id, &ret) == -1)
    { fprintf(stderr, "pthread_join err\n"); exit(1); }
  return ret;
}
