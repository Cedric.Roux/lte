#include "shell.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pty.h>

#include "util.h"

shell_t *new_shell(char *PS1)
{
  char init[1024];
  if (strlen(PS1) < 2) { fprintf(stderr, "error: PS1 '%s' is too short\n", PS1); exit(1); }
  if (strlen(PS1) > 200) { fprintf(stderr, "error: PS1 '%s' is too big\n", PS1); exit(1); }
  sprintf(init, "set +e -o pipefail\n"
                "export LC_ALL=C\n"
                /* a little hack to avoid parsing PS1 here */
                "export PS1=%c\"\"%s\n",
          *PS1, PS1+1);
  struct winsize w = { ws_row:25, ws_col:80, ws_xpixel:80*8, ws_ypixel:25*16 };
  shell_t *ret = malloc(sizeof(*ret)); if (ret == NULL) abort();
  ret->pid = forkpty(&ret->fd, NULL, NULL, &w);
  if (ret->pid == -1) { perror("forkpty"); exit(1); }
  if (ret->pid == 0) { execl("/bin/bash", "bash", NULL); exit(1); }
  if (!fullwrite(ret->fd, init, strlen(init))) abort();
  return ret;
}

void end_shell(shell_t *shell)
{
}
