#include "input.h"

#include <stdlib.h>
#include <unistd.h>

input_t *new_input(void)
{
  input_t *ret = malloc(sizeof(input_t)); if (ret == NULL) abort();
  ret->buffer = malloc(1024); if (ret->buffer == NULL) abort();
  ret->size = 0;
  ret->maxsize = 1024;
  return ret;
}

void free_input(input_t *input)
{
  free(input->buffer);
  free(input);
}

#include <stdio.h>

int get_input(input_t *input, int fd, int len)
{
  if (input->maxsize - input->size < len) {
    input->maxsize += len;
    input->buffer = realloc(input->buffer, input->maxsize); if (input->buffer == NULL) abort();
  }
  len = read(fd, input->buffer + input->size, len);
//printf("got %d\n", len);
  if (len <= 0) return -1;
//printf(" [");
//for (int i = 0; i < len; i++) printf(" %2.2x", (unsigned char)input->buffer[input->size + i]);
//printf("]\n");
//printf("'");
//fwrite(input->buffer + input->size, len, 1, stdout);
//printf("'\n");
//fflush(stdout);
  input->size += len;
  return len;
}
