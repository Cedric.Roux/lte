#include "scenario.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "util.h"

static char *the_line;
static void save_line(char *line)
{
  free(the_line);
  if (line == NULL) { the_line = NULL; return; }
  the_line = strdup(line); if (the_line == NULL) abort();
}

static char *no_leading_space(char *s)
{
  while (*s && isspace(*s)) s++;
  if (!*s) { fprintf(stderr, "error: bad line '%s'\n", the_line); exit(1); }
  return s;
}

static void remove_trailing_space(char *name)
{
  char *end = name;
  while (*end && !isspace(*end)) end++;
  if (end == name) { fprintf(stderr, "error: bad line '%s'\n", the_line); exit(1); }
  *end = 0;
}

static bool exist_task(scenario_t *scenario, char *name)
{
  for (int i = 0; i < scenario->tasks_count; i++)
    if (!strcmp(scenario->tasks[i].name, name)) return true;
  return false;
}

static task_t *new_task(scenario_t *scenario, char *line)
{
  char *name = &line[5];
  task_t *ret;

  name = no_leading_space(name); remove_trailing_space(name);
  if (exist_task(scenario, name)) { fprintf(stderr, "error: task '%s' defined more than once\n", name); exit(1); }
  scenario->tasks_count++;
  scenario->tasks = realloc(scenario->tasks, scenario->tasks_count * sizeof(task_t)); if (scenario->tasks == NULL) abort();
  ret = &scenario->tasks[scenario->tasks_count-1];
  memset(ret, 0, sizeof(task_t));
  ret->name = strdup(name); if (ret->name == NULL) abort();
  ret->pid = -1;
  ret->dead = false;
  return ret;
}

static void add_command(task_t *t, char *command, command_option_t options)
{
  t->commands_count++;
  t->commands = realloc(t->commands, t->commands_count * sizeof(command_t)); if (t->commands == NULL) abort();
  command_t *c = &t->commands[t->commands_count - 1];
  c->options = options;
  c->command = strdup(command); if (c->command == NULL) abort();
}

static void add_action(scenario_t *scenario, char *line)
{
  scenario->actions_count++;
  scenario->actions = realloc(scenario->actions, scenario->actions_count * sizeof(action_t)); if (scenario->actions == NULL) abort();
  action_t *action = &scenario->actions[scenario->actions_count - 1];
  if (!my_strncmp(line, ":start")) {
    char *name = no_leading_space(line+6); remove_trailing_space(name);
    if (!exist_task(scenario, name)) { fprintf(stderr, "error: task '%s' not defined\n", name); exit(1); }
    action->type = ACTION_START;
    action->start.name = strdup(name); if (action->start.name == NULL) abort();
    return;
  }
  if (!my_strncmp(line, ":user-interrupt")) {
    action->type = ACTION_USER_INTERRUPT;
    return;
  }
  if (!my_strncmp(line, ":waitlog")) {
    char *name = no_leading_space(line+8); remove_trailing_space(name);
    if (!exist_task(scenario, name)) { fprintf(stderr, "error: task '%s' not defined\n", name); exit(1); }
    action->type = ACTION_WAITLOG;
    action->waitlog.name = strdup(name); if (action->waitlog.name == NULL) abort();
    return;
  }
  if (!my_strncmp(line, ":timeout")) {
    int t;
    if (sscanf(line + 8, "%d", &t) != 1) { fprintf(stderr, "error: bad line '%s'\n", line); exit(1); }
    action->type = ACTION_TIMEOUT;
    action->timeout.t_ms = t;
    return;
  }
  if (!my_strncmp(line, ":log")) {
    char *str = no_leading_space(line+4);
    char separator = *str;
    char *end = str+1;
    while (*end && *end != separator) end++;
    if (!*end) { fprintf(stderr, "error: bad line '%s'\n", line); exit(1); }
    action->type = ACTION_LOG;
    action->log.s = malloc(end-str); if (action->log.s == NULL) abort();
    memcpy(action->log.s, str + 1, end - str - 1);
    action->log.s[end - str - 1] = 0;
    return;
  }
  if (!my_strncmp(line, ":wait")) {
    char *name = no_leading_space(line+5); remove_trailing_space(name);
    if (!exist_task(scenario, name)) { fprintf(stderr, "error: task '%s' not defined\n", name); exit(1); }
    action->type = ACTION_WAIT;
    action->wait.name = strdup(name); if (action->wait.name == NULL) abort();
    return;
  }
  fprintf(stderr, "bad action '%s'\n", line); exit(1);
}

scenario_t *read_scenario(char *scenario_file)
{
#  define DEFAULT() do {       \
    options.nofail = false;    \
    options.repeat = false;    \
    options.repeat_count = 0;  \
    options.repeat_delay = 0;  \
  } while (0)
  command_option_t options;

  scenario_t *ret = calloc(1, sizeof(*ret));   if (ret == NULL) abort();
  FILE *in = fopen(scenario_file, "r");
  if (in == NULL) { perror(scenario_file); exit(1); }
  task_t *curtask = NULL;

  DEFAULT();

  while (1) {
    char *line = next_line(in); save_line(line);
    if (line == NULL) goto end;
task:
    /* ignore empty lines (next_line() remove leading spaces) and comment lines */
    if (line[0] == 0 || line[0] == '#') continue;
    if (line[0] == ':') {
      if (!my_strncmp(line, ":scenario")) goto scenario;
      if (!my_strncmp(line, ":task")) { curtask = new_task(ret, line); DEFAULT(); continue; }
      if (curtask == NULL) { fprintf(stderr, "error: illegal line '%s'\n", the_line); exit(1); }
      if (!my_strncmp(line, ":nofail")) { options.nofail = true; continue; }
      if (!my_strncmp(line, ":mayfail")) { options.nofail = false; continue; }
      if (!my_strncmp(line, ":repeat")) {
        options.repeat = true;
        if (sscanf(line, ":repeat %d %d", &options.repeat_count, &options.repeat_delay) != 2) { fprintf(stderr, "error: bad '%s'\n", the_line); exit(1); }
      }
    }
    if (curtask == NULL) { fprintf(stderr, "error: command '%s' without task defined\n", the_line); exit(1); }
    add_command(curtask, line, options);
    options.repeat = false;
//    DEFAULT();
  }

scenario:
  while (1) {
    char *line = next_line(in); save_line(line);
    if (line == NULL) goto end;
    /* ignore empty lines (next_line() remove leading spaces) and comment lines */
    if (line[0] == 0 || line[0] == '#') continue;
    if (line[0] == ':') {
      if (!my_strncmp(line, ":task")) { curtask = NULL; DEFAULT(); goto task; }
      add_action(ret, line);
      continue;
    }
    fprintf(stderr, "error: bad '%s' in :scenario part\n", the_line); exit(1);
  }

end:
  fclose(in);
  free(the_line); the_line = NULL;
  clear_line();

  return ret;
}

char *action_name(action_type_t t)
{
  switch (t) {
  default: return "[unknown]";
  case ACTION_START: return "start";
  case ACTION_WAIT: return "wait";
  case ACTION_USER_INTERRUPT: return "user-interrupt";
  }
}

task_t *get_task(scenario_t *scenario, char *name)
{
  for (int i = 0; i < scenario->tasks_count; i++)
    if (!strcmp(scenario->tasks[i].name, name))
      return &scenario->tasks[i];
  fprintf(stderr, "taks '%s' not found\n", name);
  exit(1);
}
