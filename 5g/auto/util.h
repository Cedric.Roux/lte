#ifndef _UTIL_H_
#define _UTIL_H_

#include <stdbool.h>
#include <pthread.h>

#define my_strncmp(a, b) strncmp(a, b, strlen(b))
#define my_fullwrite(a, b) fullwrite(a, b, strlen(b))

char *next_line(void *in);
void clear_line(void);

bool fullwrite(int fd, char *buf, int size);

pthread_t new_thread(void *(*f)(void *), void *data);
void *wait_thread(pthread_t id);

#endif /* _UTIL_H_ */
