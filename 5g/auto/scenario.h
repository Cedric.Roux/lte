#ifndef _SCENARIO_H_
#define _SCENARIO_H_

#include <stdio.h>
#include <stdbool.h>
#include <pthread.h>

/* task & command management */

typedef struct {
  bool repeat;    int repeat_count;    int repeat_delay;
  bool nofail;
} command_option_t;

typedef struct {
  command_option_t options;
  char *command;
} command_t;

typedef struct {
  char *name;
  command_t *commands;  int commands_count;
  int pid;
  pthread_t pthread_id;
  bool dead;
  /* for logging */
  char *logdir;
  FILE *out;
  int logsize;
} task_t;

/* action management */

typedef enum {
  ACTION_START, ACTION_WAIT, ACTION_USER_INTERRUPT,
  ACTION_LOG, ACTION_TIMEOUT, ACTION_WAITLOG
} action_type_t;

typedef struct {
  action_type_t type;
  union {
    struct {
      char *name;
    } start;
    struct {
      char *name;
    } wait;
    struct {
      char *s;
    } log;
    struct {
      int t_ms;
    } timeout;
    struct {
      char *name;
    } waitlog;
  };
} action_t;

typedef struct {
  task_t *tasks;        int tasks_count;
  char *sudo;
  action_t *actions;    int actions_count;
} scenario_t;

scenario_t *read_scenario(char *scenario_file);
char *action_name(action_type_t t);
task_t *get_task(scenario_t *scenario, char *name);

#endif /* _SCENARIO_H_ */
