#ifndef _SHELL_H_
#define _SHELL_H_

#include <stdbool.h>

typedef struct {
  int fd;
  int pid;
  bool alive;
} shell_t;

shell_t *new_shell(char *PS1);

#endif /* _SHELL_H_ */
