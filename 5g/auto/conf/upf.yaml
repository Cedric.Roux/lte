################################################################################
# Licensed to the OpenAirInterface (OAI) Software Alliance under one or more
# contributor license agreements.  See the NOTICE file distributed with
# this work for additional information regarding copyright ownership.
# The OpenAirInterface Software Alliance licenses this file to You under
# the OAI Public License, Version 1.1  (the "License"); you may not use this file
# except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.openairinterface.org/?page_id=698
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#-------------------------------------------------------------------------------
# For more information about the OpenAirInterface (OAI) Software Alliance:
#      contact@openairinterface.org
################################################################################

# OAI CN Configuration File
### This file can be used by all OAI NFs
### Some fields are specific to an NF and will be ignored by other NFs
### The {{ env['ENV_NAME'] }} syntax lets you define these values in a docker-compose file
### If you intend to mount this file or use a bare-metal deployment, please refer to README.md
### The README.md also defines default values and allowed values for each configuration parameter

############# Common configuration

# Log level for all the NFs
log_level:
  general: debug

# If you enable registration, the other NFs will use the NRF discovery mechanism
register_nf:
  general: yes
  
http_version: 2

############## SBI Interfaces
### Each NF takes its local SBI interfaces and remote interfaces from here, unless it gets them using NRF discovery mechanisms
nfs:
  smf:
    host: oai-smf
    sbi:
      port: 8080
      api_version: v1
      interface_name: eth0
    n4:
      interface_name: eth0
      port: 8805
  upf:
    host: 33.0.0.7
    sbi:
      port: 8080
      api_version: v1
      interface_name: eth0
    n3:
      interface_name: eth0
      port: 2152
    n4:
      interface_name: eth0
      port: 8805
    n6:
      interface_name: eth0
    n9:
      interface_name: eth0
      port: 2152
  nrf:
    host: 33.0.0.1
    sbi:
      port: 8080
      api_version: v1
      interface_name: eth0

## general single_nssai configuration
## Defines YAML anchors, which are reused in the config file
snssais:
  - &embb_slice1
    sst: 1
  - &embb_slice2
    sst: 1
    sd: 000001 # in hex
  - &custom_slice
    sst: 222
    sd: 00007B # in hex

upf:
  support_features:
    enable_bpf_datapath: no  # If "on": BPF is used as datapath else simpleswitch is used, DEFAULT= off
    enable_snat: no          # If "on": Source natting is done for UE, DEFAULT= off
  remote_n6_gw: 33.0.0.100
  smfs:
    - host: 33.0.0.3            # To be used for PFCP association in case of no-NRF
  upf_info:
    sNssaiUpfInfoList:
      - sNssai: *embb_slice1
        dnnUpfInfoList:
          - dnn: "internet"
      - sNssai: *embb_slice2
        dnnUpfInfoList:
          - dnn: "oai"
      - sNssai: *custom_slice
        dnnUpfInfoList:
          - dnn: "default"

### DNN configuration
dnns:
  - dnn: "internet"
    pdu_session_type: "IPV4"
    ipv4_subnet: "13.3.1.0/24"
  - dnn: "oai"
    pdu_session_type: "IPV4"
    ipv4_pool: "12.2.1.0/24"
  - dnn: "default"
    pdu_session_type: "IPV4"
    ipv4_pool: "12.3.1.0/26"
  - dnn: "ims"
    pdu_session_type: "IPV4V6"
    ipv4_pool: "14.1.1.0/24"
