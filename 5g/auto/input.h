#ifndef _INPUT_H_
#define _INPUT_H_

typedef struct {
  char *buffer;
  int size;
  int maxsize;
} input_t;

input_t *new_input(void);
void free_input(input_t *input);
int get_input(input_t *input, int fd, int len);

#endif /* _INPUT_H_ */
