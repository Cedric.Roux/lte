/* gcc -Wall -o pdcp_security pdcp_security.c ../snow3g.c -lcrypto */
/* echo 80 00 00 4a b5 a5 [...]|./pdcp_security  -c <key> -i <key> -no-integrity -count 4 -header-size 3 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <openssl/cmac.h>
#include "../snow3g.h"

#define UPLINK   0
#define DOWNLINK 1

void compute_t(unsigned char *t, uint32_t count, int bearer, int direction)
{
  t[0] = (count >> 24) & 255;
  t[1] = (count >> 16) & 255;
  t[2] = (count >>  8) & 255;
  t[3] = (count      ) & 255;
  t[4] = ((bearer-1) << 3) | (direction << 2);
  memset(&t[5], 0, 8-5);
}

void do_integrity(void *integrity_context,
               unsigned char *out,
               unsigned char *buffer, int length,
               int bearer, int count, int direction)
{
  CMAC_CTX *ctx = integrity_context;
  unsigned char t[8];
  unsigned char mac[16];
  size_t maclen;

  /* see 33.401 B.2.3 for the input to 128-EIA2
   * (which is identical to 128-NIA2, see 33.501 D.3.1.3) */
  compute_t(t, count, bearer, direction);

  CMAC_Init(ctx, NULL, 0, NULL, NULL);
  CMAC_Update(ctx, t, 8);
  CMAC_Update(ctx, buffer, length);
  CMAC_Final(ctx, mac, &maclen);

  /* AES CMAC (RFC 4493) outputs 128 bits but NR PDCP PDUs have a MAC-I of
   * 32 bits (see 38.323 6.2). RFC 4493 2.1 says to truncate most significant
   * bit first (so seems to say 33.401 B.2.3)
   */
  memcpy(out, mac, 4);
}

/****************************************************************************/
/* process user input                                                       */
/****************************************************************************/

char *data;
int size;

int get_hex(void)
{
  while (1) {
    int c = getchar();
    if (c == EOF) return -1;
    if (c >= '0' && c <= '9') return c - '0';
    if (c >= 'a' && c <= 'f') return c - 'a' + 10;
    if (c >= 'A' && c <= 'F') return c - 'A' + 10;
    if (!isspace(c)) { printf("error: bad input\n"); exit(1); }
  }
}

void put(int x)
{
  if (!(size & 1023)) {
    data = realloc(data, size + 1024);
    if (data == NULL) { printf("error: out of memory\n"); exit(1); }
  }
  data[size] = x;
  size++;
}

void get_data(void)
{
  while (1) {
    int a = get_hex(); if (a == -1) break;
    int b = get_hex(); if (b == -1) { printf("error: bad input\n"); exit(1); }
    put(a * 16 + b);
  }
}

int unhex(int c)
{
  if (c >= '0' && c <= '9') return c - '0';
  if (c >= 'a' && c <= 'f') return c - 'a' + 10;
  if (c >= 'A' && c <= 'F') return c - 'A' + 10;
  return -1;
}

void get_key(unsigned char *k, char *s)
{
  int i;
  for (i = 0; i < 16; i++) {
    int a = unhex(s[i*2]);   if (a == -1) { printf("error: bad key, length must be 16\n"); exit(1); }
    int b = unhex(s[i*2+1]); if (b == -1) { printf("error: bad key, length must be 16\n"); exit(1); }
    k[i] = a*16 + b;
  }
}

void usage(void)
{
  printf("options:\n");
  printf("    -c <ciphering key> (mandatory option)\n");
  printf("    -i <integrity key> (mandatory option)\n");
  printf("    -dl\n");
  printf("        to use downlink packet, default: uplink\n");
  printf("    -rb <rb>                   (default: 1)\n");
  printf("    -count <count>             (default: 0)\n");
  printf("    -header-size <header size> (default: 2)\n");
  printf("    -no-ciphering\n");
  printf("        disable de-ciphering\n");
  printf("    -no-integrity\n");
  printf("        disable integrity\n");
  printf("    -ialgo [algo]\n");
  printf("        use this algo (default: 2 for nia2)\n");
  printf("    -calgo [algo]\n");
  printf("        use this algo (default: 2 for nea2)\n");
  exit(1);
}

int main(int n, char **v)
{
  int rb_id = 1;
  int count = 0;
  int direction = UPLINK;
  int header_size = 2;
  int decipher = 1;
  int integrity = 1;
  int i;
  unsigned char key_ciphering[16] = { 0 };
  unsigned char key_integrity[16] = { 0 };
  int has_key_ciphering = 0;
  int has_key_integrity = 0;
  int ialgo = 2;
  int calgo = 2;

  for (i = 1; i < n; i++) {
    if (!strcmp(v[i], "-dl")) { direction = DOWNLINK; continue; }
    if (!strcmp(v[i], "-rb")) { if (i > n-2) usage(); rb_id = atoi(v[++i]); continue; }
    if (!strcmp(v[i], "-count")) { if (i > n-2) usage(); count = atoi(v[++i]); continue; }
    if (!strcmp(v[i], "-header-size")) { if (i > n-2) usage(); header_size = atoi(v[++i]); continue; }
    if (!strcmp(v[i], "-no-ciphering")) { decipher = 0; continue; }
    if (!strcmp(v[i], "-no-integrity")) { integrity = 0; continue; }
    if (!strcmp(v[i], "-c")) { if (i > n-2) usage(); has_key_ciphering = 1; get_key(key_ciphering, v[++i]); continue; }
    if (!strcmp(v[i], "-i")) { if (i > n-2) usage(); has_key_integrity = 1; get_key(key_integrity, v[++i]); continue; }
    if (!strcmp(v[i], "-calgo")) { if (i > n-2) usage(); calgo = atoi(v[++i]); continue; }
    if (!strcmp(v[i], "-ialgo")) { if (i > n-2) usage(); ialgo = atoi(v[++i]); continue; }
    usage();
  }

  if (ialgo != 1 && ialgo != 2) {
    printf("unknown integrity algo, only 1 and 2 supported\n");
    exit(1);
  }
  if (calgo != 1 && calgo != 2) {
    printf("unknown ciphering algo, only 1 and 2 supported\n");
    exit(1);
  }

  if (has_key_ciphering == 0 || has_key_integrity == 0) usage();

  printf("using: rb_id %d count %d direction %d (0: uplink, 1: downlink) header_size %d do deciphering %d do integrity %d\n",
         rb_id, count, direction, header_size, decipher, integrity);

  get_data();
  if (size <= 4) { printf("bad input, too short, min 5 bytes\n"); exit(1); }

  unsigned char *unciphered_data = malloc(size); if (unciphered_data == NULL) { printf("error: out of memory\n"); exit(1); }

  memcpy(unciphered_data, data, size);

  /* de-ciphering */
  if (decipher == 0)
    goto after_ciphering;

  if (calgo == 1) {
    /* nea1 */
    snow3g_ciphering(count, rb_id - 1, direction,
                     key_ciphering,
                     size - header_size,
                     (uint8_t *)data + header_size,
                     unciphered_data + header_size);
    goto after_ciphering;
  }

  /* nea2 */
  unsigned char iv[16];
  iv[0] = (count >> 24) & 255;
  iv[1] = (count >> 16) & 255;
  iv[2] = (count >>  8) & 255;
  iv[3] = (count      ) & 255;
  iv[4] = ((rb_id-1) << 3) | (direction << 2);
  memset(&iv[5], 0, 16-5);

  EVP_CIPHER_CTX* ctx = EVP_CIPHER_CTX_new();
  int rc = EVP_EncryptInit_ex(ctx, EVP_aes_128_ctr(), NULL, key_ciphering, iv);
  if (rc != 1) abort();

  int len_ev = 0;
  rc = EVP_EncryptUpdate(ctx, unciphered_data + header_size, &len_ev, (unsigned char *)data + header_size, size - header_size);
  if (len_ev > size) abort();
  if (rc != 1) abort();

  rc = EVP_EncryptFinal_ex(ctx, unciphered_data + len_ev, &len_ev);
  if (rc != 1) abort();

  EVP_CIPHER_CTX_free(ctx);

after_ciphering:;

  /* integrity */
  if (integrity == 0) goto after_integrity;

  unsigned char iout[4];

  if (calgo == 1) {
    snow3g_integrity(count, rb_id - 1, direction, key_integrity, size-4, unciphered_data, iout);
  }

  if (calgo == 2) {
    CMAC_CTX *ictx = CMAC_CTX_new(); if (ictx == NULL) abort();
    CMAC_Init(ictx, key_integrity, 16, EVP_aes_128_cbc(), NULL);

    do_integrity(ictx, iout, unciphered_data, size-4, rb_id, count, direction);
  }

  if (memcmp(iout, unciphered_data+size-4, 4)) {
    printf("bad integrity %2.2x%2.2x%2.2x%2.2x\n", iout[0], iout[1], iout[2], iout[3]);
  } else {
    printf("good integrity %2.2x%2.2x%2.2x%2.2x\n", iout[0], iout[1], iout[2], iout[3]);
  }

after_integrity:
  printf("de-ciphered data:");
  for (i = 0; i < size; i++) printf(" %2.2x", unciphered_data[i]);
  printf("\n");

  return 0;
}
