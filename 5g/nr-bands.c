/* https://www.linkedin.com/pulse/how-calculate-global-synchronisation-channel-number-gscn-fathi-farouk */
/* https://media.licdn.com/dms/image/C4E12AQHm3ChfbmJHbQ/article-inline_image-shrink_1500_2232/0/1617302102711?e=1688601600&v=beta&t=S5edpBPpL2uQvMm2u1B3ExNZL3a3GDHLSNs03GIqJXk */
/* https://5g-tools.com/5g-nr-gscn-calculator/ */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdint.h>
#include <inttypes.h>

#define MHZ ((uint64_t)1000000)
#define KHZ ((uint64_t)1000)

char *mhz(uint64_t freq, char *out)
{
  char *o = out;
  char *last;
  uint64_t m = freq / MHZ;
  int r = freq % MHZ;
  char ms[32];
  char rs[10];
  int i = 0;
  sprintf(ms, "%"PRIu64"", m);
  sprintf(rs, "%6.6d", r);
  while (ms[i]) *o++ = ms[i++];
  last = o;
  *o++ = '.';
  for (i = 0; i < 6; i++) {
    *o++ = rs[i];
    if (rs[i] != '0') last = o;
  }
  *last = 0;
  return out;
}

typedef struct {
  char *band;
  int delta_f_raster;
  int ul_nref_first;
  int ul_nref_step;
  int ul_nref_last;
  int dl_nref_first;
  int dl_nref_step;
  int dl_nref_last;
} arfcn_t;

/* 38.104 table 5.4.2.3-1 */
arfcn_t bands_arfcn[] = {
  { "n1", 100, 384000, 20, 396000, 422000, 20, 434000 },
  { "n2", 100, 370000, 20, 382000, 386000, 20, 398000 },
  { "n3", 100, 342000, 20, 357000, 361000, 20, 376000 },
  { "n5", 100, 164800, 20, 169800, 173800, 20, 178800 },
  { "n7", 100, 500000, 20, 514000, 524000, 20, 538000 },
  { "n8", 100, 176000, 20, 183000, 185000, 20, 192000 },
  { "n12", 100, 139800, 20, 143200, 145800, 20, 149200 },
  { "n13", 100, 155400, 20, 157400, 149200, 20, 151200 },
  { "n14", 100, 157600, 20, 159600, 151600, 20, 153600 },
  { "n18", 100, 163000, 20, 166000, 172000, 20, 175000 },
  { "n20", 100, 166400, 20, 172400, 158200, 20, 164200 },
  { "n25", 100, 370000, 20, 383000, 386000, 20, 399000 },
  { "n24", 100, 325300, 20, 332100, 305000, 20, 311800 },
  { "n26", 100, 162800, 20, 169800, 171800, 20, 178800 },
  { "n28", 100, 140600, 20, 149600, 151600, 20, 160600 },
  { "n28", 100, 144608, 0, 144608, 155608, 0, 155608 },
  { "n29", 100, 0, 0, 0, 143400, 20, 145600 },
  { "n30", 100, 461000, 20, 463000, 470000, 20, 472000 },
  { "n34", 100, 402000, 20, 405000, 402000, 20, 405000 },
  { "n38", 100, 514000, 20, 524000, 514000, 20, 524000 },
  { "n39", 100, 376000, 20, 384000, 376000, 20, 384000 },
  { "n40", 100, 460000, 20, 480000, 460000, 20, 480000 },
  { "n41", 15, 499200, 3, 537999, 499200, 3, 537999 },
  { "n41", 30, 499200, 6, 537996, 499200, 6, 537996 },
  { "n46", 15, 743334, 1, 795000, 743334, 1, 795000 /* special band */ },
  { "n48", 15, 636667, 1, 646666, 636667, 1, 646666 },
  { "n48", 30, 636668, 2, 646666, 636668, 2, 646666 },
  { "n50", 100, 286400, 20, 303400, 286400, 20, 303400 },
  { "n51", 100, 285400, 20, 286400, 285400, 20, 286400 },
  { "n53", 100, 496700, 20, 499000, 496700, 20, 499000 },
  { "n65", 100, 384000, 20, 402000, 422000, 20, 440000 },
  { "n66", 100, 342000, 20, 356000, 422000, 20, 440000 },
  { "n67", 100, 0, 0, 0, 147600, 20, 151600 },
  { "n70", 100, 339000, 20, 342000, 399000, 20, 404000 },
  { "n71", 100, 132600, 20, 139600, 123400, 20, 130400 },
  { "n74", 100, 285400, 20, 294000, 295000, 20, 303600 },
  { "n75", 100, 0, 0, 0, 286400, 20, 303400 },
  { "n76", 100, 0, 0, 0, 285400, 20, 286400 },
  { "n77", 15, 620000, 1, 680000, 620000, 1, 680000 },
  { "n77", 30, 620000, 2, 680000, 620000, 2, 680000 },
  { "n78", 15, 620000, 1, 653333, 620000, 1, 653333 },
  { "n78", 30, 620000, 2, 653332, 620000, 2, 653332 },
  { "n79", 15, 693334, 1, 733333, 693334, 1, 733333 },
  { "n79", 30, 693334, 2, 733332, 693334, 2, 733332 },
  { "n80", 100, 342000, 20, 357000, 0, 0, 0 },
  { "n81", 100, 176000, 20, 183000, 0, 0, 0 },
  { "n82", 100, 166400, 20, 172400, 0, 0, 0 },
  { "n83", 100, 140600, 20, 149600, 0, 0, 0 },
  { "n84", 100, 384000, 20, 396000, 0, 0, 0 },
  { "n85", 100, 139600, 20, 143200, 145600, 20, 149200 },
  { "n86", 100, 342000, 20, 356000, 0, 0, 0 },
  { "n89", 100, 164800, 20, 169800, 0, 0, 0 },
  { "n89", 15, 499200, 3, 537999, 499200, 3, 537999 },
  { "n90", 30, 499200, 6, 537996, 499200, 6, 537996 },
  { "n90", 100, 499200, 20, 538000, 499200, 20, 538000 },
  { "n91", 100, 166400, 20, 172400, 285400, 20, 286400 },
  { "n92", 100, 166400, 20, 172400, 286400, 20, 303400 },
  { "n93", 100, 176000, 20, 183000, 285400, 20, 286400 },
  { "n94", 100, 176000, 20, 183000, 286400, 20, 303400 },
  { "n95", 100, 402000, 20, 405000, 0, 0, 0 },
  { "n96", 15, 795000, 1, 875000, 795000, 1, 875000 }, /* special band */
  { "n97", 100, 460000, 20, 480000, 0, 0, 0 },
  { "n98", 100, 376000, 20, 384000, 0, 0, 0 },
  { "n99", 100, 325300, 20, 332100, 0, 0, 0 },
  { "n100", 100, 174880, 20, 176000, 183880, 20, 185000 },
  { "n101", 100, 380000, 20, 382000, 380000, 20, 382000 },
  { "n102", 5, 796334, 1, 828333, 796334, 1, 828333 }, /* special band */
  { "n104", 15, 828334, 1, 875000, 828334, 1, 875000 },
  { "n104", 30, 828334, 2, 875000, 828334, 2, 875000 },
};

typedef struct {
  char *band;
  int scs;
  char *ssb_pattern;
  int first;
  int step;
  int last;
} gscn_t;

/* 38.104 table 5.4.3.3-1 */
gscn_t bands_gscn[] = {
  { "n1", 15, "Case A", 5279, 1, 5419 },
  { "n2", 15, "Case A", 4829, 1, 4969 },
  { "n3", 15, "Case A", 4517, 1, 4693 },
  { "n5", 15, "Case A", 2177, 1, 2230 },
  { "n5", 30, "Case B", 2183, 1, 2224 },
  { "n7", 15, "Case A", 6554, 1, 6718 },
  { "n8", 15, "Case A", 2318, 1, 2395 },
  { "n12", 15, "Case A", 1828, 1, 1858 },
  { "n13", 15, "Case A", 1871, 1, 1885 },
  { "n14", 15, "Case A", 1901, 1, 1915 },
  { "n18", 15, "Case A", 2156, 1, 2182 },
  { "n20", 15, "Case A", 1982, 1, 2047 },
  { "n24", 15, "Case A", 3818, 1, 3892 },
  { "n24", 30, "Case B", 3824, 1, 3886 },
  { "n25", 15, "Case A", 4829, 1, 4981 },
  { "n26", 15, "Case A", 2153, 1, 2230 },
  { "n28", 15, "Case A", 1901, 1, 2002 },
  { "n29", 15, "Case A", 1798, 1, 1813 },
  { "n30", 15, "Case A", 5879, 1, 5893 },
  { "n34", 15, "Case A", 0, 0, 0 },
  { "n34", 30, "Case C", 5036, 1, 5050 },
  { "n38", 15, "Case A", 0, 0, 0 },
  { "n38", 30, "Case C", 6437, 1, 6538 },
  { "n39", 15, "Case A", 0, 0, 0 },
  { "n39", 30, "Case C", 4712, 1, 4789 },
  { "n40", 30, "Case C", 5762, 1, 5989 },
  { "n41", 15, "Case A", 6246, 3, 6717 },
  { "n41", 30, "Case C", 6252, 3, 6714 },
  { "n46", 30, "Case C", 8993, 1, 9530 },
  { "n48", 30, "Case C", 7884, 1, 7982 },
  { "n50", 30, "Case C", 3590, 1, 3781 },
  { "n51", 15, "Case A", 3572, 1, 3574 },
  { "n53", 15, "Case A", 6215, 1, 6232 },
  { "n65", 15, "Case A", 5279, 1, 5494 },
  { "n66", 15, "Case A", 5279, 1, 5494 },
  { "n66", 30, "Case B", 5285, 1, 5488 },
  { "n67", 15, "Case A", 1850, 1, 1888 },
  { "n70", 15, "Case A", 4993, 1, 5044 },
  { "n71", 15, "Case A", 1547, 1, 1624 },
  { "n74", 15, "Case A", 3692, 1, 3790 },
  { "n75", 15, "Case A", 3584, 1, 3787 },
  { "n76", 15, "Case A", 3572, 1, 3574 },
  { "n77", 30, "Case C", 7711, 1, 8329 },
  { "n78", 30, "Case C", 7711, 1, 8051 },
  { "n79", 30, "Case C", 8480, 16, 8880 },
  { "n79", 30, "Case C", 8475, 1, 8884 },
  { "n85", 15, "Case A", 1826, 1, 1858 },
  { "n90", 15, "Case A", 6246, 1, 6717 },
  { "n90", 15, "Case A", 6245, 1, 6718 },
  { "n90", 30, "Case C", 6252, 1, 6714 },
  { "n91", 15, "Case A", 3572, 1, 3574 },
  { "n92", 15, "Case A", 3584, 1, 3787 },
  { "n93", 15, "Case A", 3572, 1, 3574 },
  { "n94", 15, "Case A", 3584, 1, 3787 },
  { "n96", 30, "Case C", 9531, 1, 10363 },
  { "n100", 15, "Case A", 2303, 1, 2307 },
  { "n101", 15, "Case A", 4754, 1, 4768 },
  { "n101", 30, "Case C", 4760, 1, 4764 },
  { "n102", 30, "Case C", 9531, 1, 9877 },
  { "n104", 30, "Case C", 9882, 7, 10358 },
};

int get_band(char *band, int raster)
{
  int i;
  for (i = 0; i < sizeof(bands_arfcn) / sizeof(bands_arfcn[0]); i++) {
    if (!strcmp(bands_arfcn[i].band, band) && bands_arfcn[i].delta_f_raster == raster)
      return i;
  }
  printf("band %s raster %d not found\n", band, raster);
  exit(1);
}

uint64_t arfcn_to_freq_hz(int arfcn)
{
  /* sub-3ghz supposed */
  int delta_f_global_khz;
  uint64_t fref_offs;
  int nref_offs;
  int nref = arfcn;

  if (arfcn < 600000) {
    delta_f_global_khz = 5;
    fref_offs = 0;
    nref_offs = 0;
  } else if (arfcn < 2016667) {
    delta_f_global_khz = 15;
    fref_offs = 3000 * MHZ;
    nref_offs = 600000;
  } else {
    delta_f_global_khz = 60;
    fref_offs = 24250 * MHZ + 80 * KHZ;
    nref_offs = 2016667;
  }

  return fref_offs + delta_f_global_khz * KHZ * (nref - nref_offs);
}

int freq_hz_to_arfcn(uint64_t f)
{
  int delta_f_global_khz;
  uint64_t fref_offs;
  int nref_offs;
  if (f < 3000 * MHZ) {
    delta_f_global_khz = 5;
    fref_offs = 0;
    nref_offs = 0;
  } else if (f <= 24250 * MHZ) {
    delta_f_global_khz = 15;
    fref_offs = 3000 * MHZ;
    nref_offs = 600000;
  } else {
    delta_f_global_khz = 60;
    fref_offs = 24250 * MHZ + 80 * KHZ;
    nref_offs = 2016667;
  }
  return (f - fref_offs) / (delta_f_global_khz * KHZ) + nref_offs;
}

uint64_t gscn_to_freq_hz(int gscn)
{
  if (gscn >= 2 && gscn <= 7498) {
    int n, m;

    n = gscn / 3;
    m = gscn - 3 * n;
    if (m == 2) n++;
    m = (gscn - 3 * n) * 2 + 3;
    //printf("n %d m %d gscn %d\n", n, m, 3 * n + (m - 3) / 2);

    return n * 1200 * KHZ + m * 50 * KHZ;
  }
  if (gscn >= 7499 && gscn <= 22255) {
    int n = gscn - 7499;
    return  3000 * MHZ + n * (1 * MHZ + 440 * KHZ);
  }
  if (gscn >= 22256 && gscn <= 26639) {
    int n = gscn - 22256;
    return 24250 * MHZ + 80 * KHZ + n * (17 * MHZ + 280 * KHZ);
  }

  printf("gscn %d not handled\n", gscn);
  exit(1);
}

int freq_hz_to_gscn(uint64_t freq)
{
  char mm[64];
  if (freq < 3000 * MHZ) {
    int n = freq / (1200 * KHZ);
    int m = (freq - n * 1200 * KHZ) / (50 * KHZ);
    if (m != 1 && m != 3 && m != 5)
      printf("error: freq %s gives m %d for gscn, should be 1, 3, or 5\n",
             mhz(freq, mm), m);
    int gscn = (m - 3) / 2 + 3 * n;
    return gscn;
  } else if (freq < 24250 * MHZ + 80 * KHZ) {
     int n = (freq - 3000 * MHZ) / (1 * MHZ + 440 * KHZ);
     return n + 7499;
  } else {
     int n = (freq - (24250 * MHZ + 80 * KHZ)) / (17 * MHZ + 280 * KHZ);
     return n + 22256;
  }
}

/* returns nearest arfcn of point A with ssb at center of band */
int gscn_nearest_arfcn(int gscn, char *band, int raster, int scs, int rb)
{
  int b = get_band(band, raster);
  int i;
  uint64_t f_gscn = gscn_to_freq_hz(gscn) - rb * 12 * scs * KHZ / 2;
  int nearest = -1000;
  int64_t mindiff = -1;
  for (i = bands_arfcn[b].dl_nref_first;
       i <= bands_arfcn[b].dl_nref_last;
       i += bands_arfcn[b].dl_nref_step) {
    uint64_t f = arfcn_to_freq_hz(i);
    int64_t diff = f - f_gscn; if (diff < 0) diff = -diff;
    if (diff % (scs * KHZ)) continue;
    if (diff < mindiff || mindiff == -1) {
      mindiff = diff;
      nearest = i;
    }
  }
  if (nearest == -1000) {
    printf("error: no valid arfcn found for gscn %d for band %s with raster %d\n",
           gscn, band, raster);
    exit(1);
  }
  return nearest;
}

int *get_gscn_of_band(char *band, int scs)
{
  static int ret[65536];
  int i;
  int count;
  int gscn;
  for (i = 0; i < sizeof(bands_gscn) / sizeof(bands_gscn[0]); i++) {
    if (!strcmp(band, bands_gscn[i].band) && scs == bands_gscn[i].scs)
      break;
  }
  if (i == sizeof(bands_gscn) / sizeof(bands_gscn[0])) {
    printf("error: band %s scs %d not found\n", band, scs);
    exit(1);
  }
  if (!strcmp(band, "n34")
      || (!strcmp(band, "n38") && scs == 15)
      || !strcmp(band, "n39")
      || !strcmp(band, "n46")
      || !strcmp(band, "n79")
      || !strcmp(band, "n90")
      || !strcmp(band, "n96")
      || !strcmp(band, "n102"))
    { printf("%s:%d: TODO\n", __FUNCTION__, __LINE__); exit(1); }
  for (count = 0,       gscn = bands_gscn[i].first;
       count < 65536 && gscn <= bands_gscn[i].last;
       count++,         gscn += bands_gscn[i].step)
    ret[count] = gscn;
  if (count == 65536) { printf("you should not read this\n"); exit(1); }
  ret[count] = -1;
  return ret;
}

void usage(void)
{
  printf("<action> <options>\n");
  printf("actions:\n");
  printf("    bands          - print known bands\n");
  printf("    gscn           - print valid gscn for a band\n");
  //printf("    arfcn          - dump arfcn of a band\n");
  printf("    freq-to-arfcn  - print arfcn from freq\n");
  printf("    freq-to-gscn   - print gscn from freq\n");
  printf("    nearest-arfcn  - print nearest arfcn to gscn\n");
  printf("    gscn-to-freq   - print freq of gscn\n");
  printf("    gscn-to-arfcn  - print arfcn of gscn\n");
  printf("    arfcn-to-freq  - print freq of arfcn\n");
  printf("    nearest        - find valid arfcn+gscn nearest to given freq\n");
  printf("    ssb            - get value to pass to nr-uesoftmodem --ssb\n");
  printf("options:\n");
  printf("    -b <band> (default n78)\n");
  printf("    -r <raster> (default 30)\n");
  printf("    -f <freq (MHz)>\n");
  printf("    -gscn <gscn>\n");
  printf("    -arfcn <arfcn>\n");
  printf("    -scs <scs> (default 30)\n");
  printf("    -rb <rbs> (default 106)\n");
  exit(1);
}

uint64_t parse_freq_mhz(char *f)
{
  char *s = f;
  uint64_t ret = 0;
  uint64_t mul = MHZ;
  int div_mul = 0;

  while (*s) {
    switch (*s) {
      case '0'...'9':
        if (div_mul) { mul /= 10; }
        ret *= 10;
        ret += *s - '0';
        break;
      case '.':
        div_mul = 1;
        break;
      default: printf("bad freq %s\n", f); exit(1);
      }
    s++;
  }

  if (mul == 0) { printf("can't parse freq %s\n", f); exit(1); }

  return ret * mul;
}

int main(int n, char **v)
{
  int i;
  int arfcn = 0;
  char *action = NULL;
  char *band = "n78";
  int raster = 30;
  uint64_t freq = 0;
  int gscn = 0;
  int scs = 30;
  int rb = 106;

  for (i = 1; i < n; i++) {
    if (!strcmp(v[i], "-b")) { if (i > n-2) usage(); band = v[++i]; continue; }
    if (!strcmp(v[i], "-r")) { if (i > n-2) usage(); raster = atoi(v[++i]); continue; }
    if (!strcmp(v[i], "-f")) { if (i > n-2) usage(); freq = parse_freq_mhz(v[++i]); continue; }
    if (!strcmp(v[i], "-gscn")) { if (i > n-2) usage(); gscn = atoi(v[++i]); continue; }
    if (!strcmp(v[i], "-arfcn")) { if (i > n-2) usage(); arfcn = atoi(v[++i]); continue; }
    if (!strcmp(v[i], "-scs")) { if (i > n-2) usage(); scs = atoi(v[++i]); continue; }
    if (!strcmp(v[i], "-rb")) { if (i > n-2) usage(); rb = atoi(v[++i]); continue; }
    if (action == NULL) { action = v[i]; continue; }
    usage();
  }

  if (action == NULL) usage();

  if (!strcmp(action, "bands")) {
    printf("known bands:\n");
    for (i = 0; i < sizeof(bands_arfcn) / sizeof(bands_arfcn[0]); i++) {
      char m1[64];
      char m2[64];
      printf("    %s (delta_F_raster (kHz) %d [arfcn %d - %d] [feq MHz %s - %s]\n",
             bands_arfcn[i].band,
             bands_arfcn[i].delta_f_raster,
             bands_arfcn[i].dl_nref_first,
             bands_arfcn[i].dl_nref_last,
             mhz(arfcn_to_freq_hz(bands_arfcn[i].dl_nref_first), m1),
             mhz(arfcn_to_freq_hz(bands_arfcn[i].dl_nref_last), m2));
    }
    return 0;
  }

  if (!strcmp(action, "gscn")) {
    int *gscn_array = get_gscn_of_band(band, scs);
    for (int j = 0; gscn_array[j] != -1; j++) {
      freq = gscn_to_freq_hz(gscn_array[j]);
      arfcn = freq_hz_to_arfcn(freq);
      printf("band %s scs %d gscn %d arfcn %d freq %ld\n",
             band, scs, gscn_array[j], arfcn, freq);
    }
    return 0;
  }

  if (!strcmp(action, "freq-to-arfcn")) {
    int b = get_band(band, raster);
    int arfcn = freq_hz_to_arfcn(freq);
    char m[64];
    printf("freq %s arfcn %d\n", mhz(freq, m), arfcn);
    if (bands_arfcn[b].dl_nref_first > arfcn
        || bands_arfcn[b].dl_nref_last < arfcn)
      printf("error: arfcn %d out of band %s\n", arfcn, band);
    if ((arfcn - bands_arfcn[b].dl_nref_first) % bands_arfcn[b].dl_nref_step)
      printf("error: arfcn %d not aligned on raster for band %s raster %d\n",
             arfcn, band, raster);
    return 0;
  }

  if (!strcmp(action, "freq-to-gscn")) {
    int gscn = freq_hz_to_gscn(freq);
    char m[64];
    printf("freq %s gscn %d\n", mhz(freq, m), gscn);
    uint64_t rfreq = gscn_to_freq_hz(gscn);
    printf("(real freq from gscn %s)\n", mhz(rfreq, m));
    if (rfreq != freq)
      printf("error: freq %s is not correct for gscn %d\n", mhz(freq, m), gscn);
    return 0;
  }

  if (!strcmp(action, "nearest-arfcn")) {
    int nearest = gscn_nearest_arfcn(gscn, band, raster, scs, rb);
    printf("gscn %d has nearest point A arfcn %d for band %s and raster %d and scs %d and rb %d\n",
           gscn, nearest, band, raster, scs, rb);
    return 0;
  }

  if (!strcmp(action, "gscn-to-freq")) {
    char m[64];
    printf("gscn %d freq %s\n", gscn, mhz(gscn_to_freq_hz(gscn), m));
    return 0;
  }

  if (!strcmp(action, "gscn-to-arfcn")) {
    uint64_t freq = gscn_to_freq_hz(gscn);
    int arfcn = freq_hz_to_arfcn(freq);
    char m[64];
    printf("gscn %d freq %s arfcn %d\n", gscn, mhz(freq, m), arfcn);
    return 0;
  }

  if (!strcmp(action, "arfcn-to-freq")) {
    char m[64];
    printf("arfcn %d freq %s\n", arfcn, mhz(arfcn_to_freq_hz(arfcn), m));
    return 0;
  }

  if (!strcmp(action, "nearest")) {
    int *gscn_array = get_gscn_of_band(band, scs);
    int nearest_gscn = gscn_array[0];
    uint64_t nearest_freq = gscn_to_freq_hz(nearest_gscn);
    int64_t nearest_diff = freq - nearest_freq;
    if (nearest_diff < 0) nearest_diff = -nearest_diff;
    for (int j = 1; gscn_array[j] != -1; j++) {
      int64_t diff = freq - gscn_to_freq_hz(gscn_array[j]);
      if (diff < 0) diff = -diff;
      if (diff < nearest_diff) {
        nearest_diff = diff;
        nearest_freq = gscn_to_freq_hz(gscn_array[j]);
        nearest_gscn = gscn_array[j];
      }
    }
    int nearest_arfcn = gscn_nearest_arfcn(nearest_gscn, band, raster, scs, rb);
    char m1[64];
    char m2[64];
    char m3[64];
    printf("freq %s has nearest gscn %d (freq %s arfcn %d) point A arfcn %d (freq %s) for band %s, raster %d, scs %d and rb %d\n",
           mhz(freq, m1), nearest_gscn, mhz(gscn_to_freq_hz(nearest_gscn), m2), freq_hz_to_arfcn(gscn_to_freq_hz(nearest_gscn)),
           nearest_arfcn, mhz(arfcn_to_freq_hz(nearest_arfcn), m3),
           band, raster, scs, rb);
    return 0;
  }

  if (!strcmp(action, "ssb")) {
    uint64_t freq_pointA = arfcn_to_freq_hz(arfcn);
    uint64_t freq_ssb = gscn_to_freq_hz(gscn);
    int64_t delta = freq_ssb - freq_pointA - 10 * 12 * scs * KHZ;
    char m1[64];
    char m2[64];
    printf("freq_pointA %s freq_ssb %s delta %ld\n", mhz(freq_pointA, m1), mhz(freq_ssb, m2), delta);
    if (delta % (scs * KHZ))
      printf("error: arfcn pointA %d and gscn ssb %d are not aligned to scs %d\n",
             arfcn, gscn, scs);
    printf("ssb %ld for arfcn %d gscn %d scs %d\n", delta / (scs * KHZ), arfcn, gscn, scs);
    return 0;
  }

  usage();

  return 0;
}
