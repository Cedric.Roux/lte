#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *ul_lcid_to_string(int lcid)
{
  static char b[64];

  if (lcid == 0) return "CCCH of size 64 bits";
  if (lcid >= 1 && lcid <= 32) {
    sprintf(b, "lcid %d", lcid);
    return b;
  }
  if (lcid >= 33 && lcid <= 51) {
    sprintf(b, "reserved %d", lcid);
    return b;
  }
  static char *name[] = {
    "CCCH of size 48 bits",
    "Recommended bit rate query",
    "Multiple Entry PHR (four octet Ci)",
    "Configured Grant Confirmation",
    "Multiple Entry PHR (one octet Ci)",
    "Single Entry PHR",
    "C-RNTI",
    "Short Truncated BSR",
    "Long Truncated BSR",
    "Short BSR",
    "Long BSR",
    "Padding",
  };
  if (lcid >= 52 && lcid <= 63) return name[lcid - 52];

  printf("FATAL: bad lcid %d (not in [0..63])\n", lcid);
  exit(1);
}

char *dl_lcid_to_string(int lcid)
{
  static char b[64];

  if (lcid == 0) return "CCCH";
  if (lcid >= 1 && lcid <= 32) {
    sprintf(b, "lcid %d", lcid);
    return b;
  }
  if (lcid >= 33 && lcid <= 46) {
    sprintf(b, "reserved %d", lcid);
    return b;
  }
  static char *name[] = {
    "Recommended bit rate",
    "SP ZP CSI-RS Resource Set Activation/Deactivation",
    "PUCCH spatial relation Activation/Deactivation",
    "SP SRS Activation/Deactivation",
    "SP CSI reporting on PUCCH Activation/Deactivation",
    "TCI State Indication for UE-specific PDCCH",
    "TCI States Activation/Deactivation for UE-specific PDSCH",
    "Aperiodic CSI Trigger State Subselection",
    "SP CSI-RS / CSI-IM Resource Set Activation/Deactivation",
    "Duplication Activation/Deactivation",
    "SCell Activation/Deactivation (four octet)",
    "SCell Activation/Deactivation (one octet)",
    "Long DRX Command",
    "DRX Command",
    "Timing Advance Command",
    "UE Contention Resolution Identity",
    "Padding",
  };
  if (lcid >= 47 && lcid <= 63) return name[lcid - 47];

  printf("FATAL: bad lcid %d (not in [0..63])\n", lcid);
  exit(1);
}

char *long_bsr(int bsr)
{
  static int v[] = {
0, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 22, 23, 25, 26, 28, 30,
32, 34, 36, 38, 40, 43, 46, 49, 52, 55, 59, 62, 66, 71, 75, 80, 85, 91,
97, 103, 110, 117, 124, 132, 141, 150, 160, 170, 181, 193, 205, 218, 233,
248, 264, 281, 299, 318, 339, 361, 384, 409, 436, 464, 494, 526, 560, 597,
635, 677, 720, 767, 817, 870, 926, 987, 1051, 1119, 1191, 1269, 1351, 1439,
1532, 1631, 1737, 1850, 1970, 2098, 2234, 2379, 2533, 2698, 2873, 3059, 3258,
3469, 3694, 3934, 4189, 4461, 4751, 5059, 5387, 5737, 6109, 6506, 6928, 7378,
7857, 8367, 8910, 9488, 10104, 10760, 11458, 12202, 12994, 13838, 14736,
15692, 16711, 17795, 18951, 20181, 21491, 22885, 24371, 25953, 27638, 29431,
31342, 33376, 35543, 37850, 40307, 42923, 45709, 48676, 51836, 55200, 58784,
62599, 66663, 70990, 75598, 80505, 85730, 91295, 97221, 103532, 110252,
117409, 125030, 133146, 141789, 150992, 160793, 171231, 182345, 194182,
206786, 220209, 234503, 249725, 265935, 283197, 301579, 321155, 342002,
364202, 387842, 413018, 439827, 468377, 498780, 531156, 565634, 602350,
641449, 683087, 727427, 774645, 824928, 878475, 935498, 996222, 1060888,
1129752, 1203085, 1281179, 1364342, 1452903, 1547213, 1647644, 1754595,
1868488, 1989774, 2118933, 2256475, 2402946, 2558924, 2725027, 2901912,
3090279, 3290873, 3504487, 3731968, 3974215, 4232186, 4506902, 4799451,
5110989, 5442750, 5796046, 6172275, 6572925, 6999582, 7453933, 7937777,
8453028, 9001725, 9586039, 10208280, 10870913, 11576557, 12328006, 13128233,
13980403, 14887889, 15854280, 16883401, 17979324, 19146385, 20389201,
21712690, 23122088, 24622972, 26221280, 27923336, 29735875, 31666069,
33721553, 35910462, 38241455, 40723756, 43367187, 46182206, 49179951,
52372284, 55771835, 59392055, 63247269, 67352729, 71724679, 76380419,
81338368, 81338368,
  };
  static char b[64];

  if (bsr < 0 || bsr > 255) {
    sprintf(b, "invalid bsr %d", bsr);
    return b;
  }
  if (bsr == 0) return "bsr = 0";
  if (bsr == 254) return "bsr > 81338368";
  if (bsr == 255) return "reserved (255)";

  sprintf(b, "%d < bsr <= %d", v[bsr-1], v[bsr]);
  return b;
}

char *short_bsr(int bsr)
{
  static int v[] = {
    0, 10, 14, 20, 28, 38, 53, 74,
    102, 142, 198, 276, 384, 535, 745, 1038,
    1446, 2014, 2806, 3909, 5446, 7587, 10570, 14726,
    20516, 28581, 39818, 55474, 77284, 107669, 150000, 150000
  };
  static char b[64];

  if (bsr < 0 || bsr > 31) {
    sprintf(b, "invalid bsr %d", bsr);
    return b;
  }
  if (bsr == 0) return "bsr = 0";
  if (bsr == 31) return "bsr > 150000";

  sprintf(b, "%d < bsr <= %d", v[bsr-1], v[bsr]);
  return b;
}

#define S(s) do { \
    if (p + (s) > len) { \
      printf(" FATAL: bad input data\n"); \
      exit(1); \
    } \
    p += (s); \
  } while (0)

#define TODO printf("%s:%d:%s: TODO\n", __FILE__, __LINE__, __FUNCTION__); exit(1)

#define LEN do { \
    if (f) { \
      S(2); \
      l = (b[p-2] << 8) | b[p-1]; \
    } else { \
      S(1); \
      l = b[p-1]; \
    } \
  } while (0)

void mac_uplink(unsigned char *b, int len)
{
  int f;
  int lcid;
  int p = 0;
  int i;
  int l;
  int ph;
  int pc_max;
  int lcg_id;
  int lcg_mask;
  int bsr;

  while (p < len) {
    f = (b[p] >> 6) & 1;
    lcid = b[p] & 0x3f;
    p++;

    printf("[at %d] %s:", p-1, ul_lcid_to_string(lcid));

    switch (lcid) {
    case 0:          /* CCCH of size 64 bits */
      S(8);
      for (i = p-8; i < p; i++)
        printf(" %2.2x", b[i]);
      break;

    case 1 ... 32:   /* Identity of the logical channel */
      LEN;
      S(l);
      printf(" [len %d]", l);
      for (i = p-l; i < p; i++)
        printf(" %2.2x", b[i]);
      break;

    case 33 ... 51:  /* Reserved */
      printf(" FATAL: 'reserved' should not be used\n");
      exit(1);

    case 52:         /* CCCH of size 48 bits */
      S(6);
      for (i = p-6; i < p; i++)
        printf(" %2.2x", b[i]);
      break;

    case 53:         /* Recommended bit rate query */
      TODO;
      break;
    case 54:         /* Multiple Entry PHR (four octet Ci) */
      TODO;
      break;
    case 55:         /* Configured Grant Confirmation */
      TODO;
      break;
    case 56:         /* Multiple Entry PHR (one octet Ci) */
      TODO;
      break;

    case 57:         /* Single Entry PHR */
      S(2);
      ph = b[p-2] & 0x3f;
      pc_max = b[p-1] & 0x3f;
      /* 38.311 10.1.17 */
      printf(" phr %d pc_max %d", ph, pc_max);
      if (ph == 0) printf(" [PH < -32]");
      else if (ph == 63) printf(" [PH >= 38]");
      else if (ph <= 54) printf(" [%d <= PH < %d]", ph-33, ph-32);
      else printf(" [%d <= PH < %d]", 22 + (ph-55) * 2, 24 + (ph-55) * 2);
      if (pc_max == 0) printf(" [pc_max < -29]");
      else if (pc_max == 63) printf(" [33 <= pc_max]");
      else printf(" [%d <= pc_max < %d]", pc_max-30, pc_max-29);
      break;

    case 58:         /* C-RNTI */
      TODO;
      break;
    case 59:         /* Short Truncated BSR */
      TODO;
      break;
    case 60:         /* Long Truncated BSR */
      TODO;
      break;

    case 61:         /* Short BSR */
      S(1);
      lcg_id = (b[p-1] >> 5) &0x7;
      bsr = b[p-1] & 0x1f;
      printf(" lcg id %d %s", lcg_id, short_bsr(bsr));
      break;

    case 62:         /* Long BSR */
      LEN;
      S(1);
      l--;
      lcg_mask = b[p-1];
      if (lcg_mask == 0) printf(" [all empty]");
      for (i = 0; i < 8; i++, lcg_mask >>= 1) {
        if (!(lcg_mask & 1)) continue;
        S(1);
        l--;
        printf(" [lcg id %d %s]", i, long_bsr(b[p-1]));
      }
      if (l != 0) { printf(" FATAL: bad length for long BSR\n"); exit(1); }
      break;

    case 63:         /* Padding */
      for (i = p; i < len; i++)
        printf(" %2.2x", b[i]);
      p = len;
      break;
    }

    printf("\n");
  }
}

void mac_downlink(unsigned char *b, int len)
{
  int f;
  int lcid;
  int p = 0;
  int i;
  int l;
  int tag_id;
  int ta_command;

  while (p < len) {
    f = (b[p] >> 6) & 1;
    lcid = b[p] & 0x3f;
    p++;

    printf("[at %d] %s:", p-1, dl_lcid_to_string(lcid));

    switch (lcid) {
    case 0:          /* CCCH */
      TODO;
      break;

    case 1 ... 32:   /* Identity of the logical channel */
      LEN;
      S(l);
      printf(" [len %d]", l);
      for (i = p-l; i < p; i++)
        printf(" %2.2x", b[i]);
      break;

    case 33 ... 46:  /* Reserved */
      TODO;
      break;

    case 47:         /* Recommended bit rate */
      TODO;
      break;

    case 48:         /* SP ZP CSI-RS Resource Set Activation/Deactivation */
      TODO;
      break;

    case 49:         /* PUCCH spatial relation Activation/Deactivation */
      TODO;
      break;

    case 50:         /* SP SRS Activation/Deactivation */
      TODO;
      break;

    case 51:         /* SP CSI reporting on PUCCH Activation/Deactivation */
      TODO;
      break;

    case 52:         /* TCI State Indication for UE-specific PDCCH */
      TODO;
      break;

    case 53:         /* TCI States Activation/Deactivation for UE-specific PDSCH */
      TODO;
      break;

    case 54:         /* Aperiodic CSI Trigger State Subselection */
      TODO;
      break;

    case 55:         /* SP CSI-RS / CSI-IM Resource Set Activation/Deactivation */
      TODO;
      break;

    case 56:         /* Duplication Activation/Deactivation */
      TODO;
      break;

    case 57:         /* SCell Activation/Deactivation (four octet) */
      TODO;
      break;

    case 58:         /* SCell Activation/Deactivation (one octet) */
      TODO;
      break;

    case 59:         /* Long DRX Command */
      TODO;
      break;

    case 60:         /* DRX Command */
      TODO;
      break;

    case 61:         /* Timing Advance Command */
      S(1);
      tag_id = (b[p-1] >> 6) & 0x03;
      ta_command = b[p-1] & 0x3f;
      printf(" tag %d ta_command %d", tag_id, ta_command);
      break;

    case 62:         /* UE Contention Resolution Identity */
      TODO;
      break;

    case 63:         /* Padding */
      for (i = p; i < len; i++)
        printf(" %2.2x", b[i]);
      p = len;
      break;

    }

    printf("\n");
  }
}

int read_input(unsigned char **data, int line_mode)
{
  unsigned char *buf = NULL;
  int n = 0;

  while (1) {
    int v;
    int c;
no_digit_1:
    c = getchar();
    if (c == EOF) break;
    if (line_mode && c == '\n') break;
    if (c >= '0' && c <= '9')      v = c - '0';
    else if (c >= 'a' && c <= 'f') v = c + 10 - 'a';
    else if (c >= 'A' && c <= 'F') v = c + 10 - 'A';
    else goto no_digit_1;
    v <<= 4;
no_digit_2:
    c = getchar();
    if (c == EOF) { printf("FATAL: bad input\n"); exit(1); }
    if (c >= '0' && c <= '9')      v += c - '0';
    else if (c >= 'a' && c <= 'f') v += c + 10 - 'a';
    else if (c >= 'A' && c <= 'F') v += c + 10 - 'A';
    else goto no_digit_2;

    n++;
    buf = realloc(buf, n); if (buf == NULL) { perror("realloc"); exit(1); }
    buf[n-1] = v;
  }

  *data = buf;
  return n;
}

void usage(void)
{
  printf("options:\n");
  printf("    -ul          uplink packet (default is downlink)\n");
  printf("    -v           more verbose (print input and a line a of -)\n");
  printf("    -l           line mode: process line by line\n");
  exit(1);
}

int main(int n, char **v)
{
  int do_uplink = 0;
  int verbose = 0;
  int line_mode = 0;
  int i;
  unsigned char *data;
  int length;

  for (i = 1; i < n; i++) {
    if (!strcmp(v[i], "-h") || !strcmp(v[i], "--help")) usage();
    if (!strcmp(v[i], "-ul")) { do_uplink = 1; continue; }
    if (!strcmp(v[i], "-v")) { verbose = 1; continue; }
    if (!strcmp(v[i], "-l")) { line_mode = 1; continue; }
    usage();
  }

next_line:
  if (verbose)
    printf("------------------------------------------------------------------------------\n");

  length = read_input(&data, line_mode);

  if (verbose) {
    printf("input:");
    for (i = 0; i < length; i++) printf(" %2.2x", data[i]);
    printf("\n");
  }

  if (do_uplink) mac_uplink(data, length);
  else           mac_downlink(data, length);

  free(data);
  if (line_mode && length) goto next_line;

  return 0;
}
