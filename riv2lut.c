/* 36.211 6.2.3.2 - Virtual resource blocks of distributed type */
/* this takes format 1C RIVs, translates to format 1A and shows
 * the allocation
 */
#include <stdio.h>
#include <stdlib.h>

#define min(a, b) ((a) < (b) ? (a) : (b))

int get_prb(int N_DL_RB, int n_VRB, int slot)
{
  int N_gap1;
  int N_gap;
  int N_DL_VRB;
  int N_tilde_DL_VRB;
  int P;
  int N_row;
  int N_null;
  int n_tilde_VRB;
  int n_tilde_prime_PRB;
  int n_tilde_prime_prime_PRB;
  int n_tilde_PRB_ns;
  int n_PRB_ns;

  if (N_DL_RB >= 6 && N_DL_RB <= 10)        N_gap1 = (N_DL_RB+1) / 2;
  else if (N_DL_RB == 11)                   N_gap1 = 4;
  else if (N_DL_RB >= 12 && N_DL_RB <= 19)  N_gap1 = 8;
  else if (N_DL_RB >= 20 && N_DL_RB <= 26)  N_gap1 = 12;
  else if (N_DL_RB >= 27 && N_DL_RB <= 44)  N_gap1 = 18;
  else if (N_DL_RB >= 45 && N_DL_RB <= 49)  N_gap1 = 27;
  else if (N_DL_RB >= 50 && N_DL_RB <= 63)  N_gap1 = 27;
  else if (N_DL_RB >= 64 && N_DL_RB <= 79)  N_gap1 = 32;
  else if (N_DL_RB >= 80 && N_DL_RB <= 110) N_gap1 = 48;
  else { printf("unsupported N_DL_RB (%d)\n", N_DL_RB); exit(1); }

  /* only gap1 case */
  N_gap = N_gap1;

  N_DL_VRB = 2 * min(N_gap, N_DL_RB - N_gap);
  N_tilde_DL_VRB = N_DL_VRB;

  if (N_DL_RB <= 10) P = 1;
  else if (N_DL_RB >= 11 && N_DL_RB <= 26) P = 2;
  else if (N_DL_RB >= 27 && N_DL_RB <= 63) P = 3;
  else if (N_DL_RB >= 64 && N_DL_RB <= 110) P = 4;
  else { printf("unsupported N_DL_RB (%d)\n", N_DL_RB); exit(1); }

  N_row = ((N_tilde_DL_VRB + 4*P-1) / (4*P)) * P;
  N_null = 4 * N_row - N_tilde_DL_VRB;

  n_tilde_VRB = n_VRB % N_tilde_DL_VRB;
  n_tilde_prime_PRB =
      2 * N_row * (n_tilde_VRB % 2) +
      (n_tilde_VRB / 2) +
      N_tilde_DL_VRB * (n_VRB / N_tilde_DL_VRB);
  n_tilde_prime_prime_PRB =
      N_row * (n_tilde_VRB % 4) +
      (n_tilde_VRB / 4) +
      N_tilde_DL_VRB * (n_VRB / N_tilde_DL_VRB);

  /* even case */
  n_tilde_PRB_ns =
      (N_null != 0 &&
       n_tilde_VRB >= N_tilde_DL_VRB - N_null && n_tilde_VRB % 2 == 1) ?
          n_tilde_prime_PRB - N_row :
      (N_null != 0 &&
       n_tilde_VRB >= N_tilde_DL_VRB - N_null && n_tilde_VRB % 2 == 0) ?
          n_tilde_prime_PRB - N_row + N_null / 2 :
      (N_null != 0 &&
       n_tilde_VRB < N_tilde_DL_VRB - N_null && n_tilde_VRB % 4 >= 2) ?
          n_tilde_prime_prime_PRB - N_null / 2 :
          n_tilde_prime_prime_PRB;

  /* odd case */
  if (slot & 1)
    n_tilde_PRB_ns =
        (n_tilde_PRB_ns + N_tilde_DL_VRB / 2) % N_tilde_DL_VRB +
        N_tilde_DL_VRB * (n_VRB / N_tilde_DL_VRB);

  n_PRB_ns = n_tilde_PRB_ns < N_tilde_DL_VRB / 2 ?
      n_tilde_PRB_ns :
      n_tilde_PRB_ns + N_gap - N_tilde_DL_VRB / 2;

  return n_PRB_ns;
}

void decode_riv(int N_DL_RB, int RIV, int *_start, int *_len)
{
  int N_step_RB;
  int Nprime_DL_VRB;
  int start, len;
  int N_gap, N_gap1;
  int N_DL_VRB;

  if (N_DL_RB >= 6 && N_DL_RB <= 10)        N_gap1 = (N_DL_RB+1) / 2;
  else if (N_DL_RB == 11)                   N_gap1 = 4;
  else if (N_DL_RB >= 12 && N_DL_RB <= 19)  N_gap1 = 8;
  else if (N_DL_RB >= 20 && N_DL_RB <= 26)  N_gap1 = 12;
  else if (N_DL_RB >= 27 && N_DL_RB <= 44)  N_gap1 = 18;
  else if (N_DL_RB >= 45 && N_DL_RB <= 49)  N_gap1 = 27;
  else if (N_DL_RB >= 50 && N_DL_RB <= 63)  N_gap1 = 27;
  else if (N_DL_RB >= 64 && N_DL_RB <= 79)  N_gap1 = 32;
  else if (N_DL_RB >= 80 && N_DL_RB <= 110) N_gap1 = 48;
  else { printf("unsupported N_DL_RB (%d)\n", N_DL_RB); exit(1); }

  /* only gap1 case */
  N_gap = N_gap1;

  N_DL_VRB = 2 * min(N_gap, N_DL_RB - N_gap);

  if (N_DL_RB >= 6 && N_DL_RB <= 49)        N_step_RB = 2;
  else if (N_DL_RB >= 50 && N_DL_RB <= 110) N_step_RB = 4;

  Nprime_DL_VRB = N_DL_VRB / N_step_RB;

  start = RIV % Nprime_DL_VRB;
  len   = RIV / Nprime_DL_VRB + 1;
  if (start + len > Nprime_DL_VRB) {
    start = Nprime_DL_VRB - 1 - start;
    len   = Nprime_DL_VRB + 1 - len + 1;
  }

  *_start = start * N_step_RB;
  *_len = len * N_step_RB;
}

int main(void)
{
  int N_DL_RB;
  int slot;
  int RIV, RIV_1A;
  int start, len;
  int vrb, prb;
  unsigned long alloc;

  N_DL_RB = 25;

  for (slot = 0; slot < 2; slot++) {
    for (RIV = 0; RIV < 32; RIV++) {
      decode_riv(N_DL_RB, RIV, &start, &len);
      RIV_1A = len - 1 <= N_DL_RB/2 ?
                   N_DL_RB * (len - 1) + start :
                   N_DL_RB * (N_DL_RB - len + 1) + (N_DL_RB - 1 - start);
      alloc = 0;
      for (vrb = start; vrb < start+len; vrb++) {
        prb = get_prb(N_DL_RB, vrb, slot);
        alloc |= 1 << prb;
      }
      printf("RIV %d|%d start %d len %d alloc %ld\n", RIV, RIV_1A,
             start, len, alloc);
    }
  }

  return 0;
}
