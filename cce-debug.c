/* prints CCEs to monitor in UE spec. space */
#include <stdio.h>
#include <stdlib.h>

unsigned int get_nCCE_offset(
                    const unsigned char L,
                    const int nCCE,
                    const unsigned short rnti,
                    const unsigned char subframe)
{

  int search_space_free,m,nb_candidates = 0,l,i;
  unsigned int Yk;
    // according to procedure in Section 9.1.1 of 36.213 (v. 8.6)
    // compute Yk
    Yk = (unsigned int)rnti;

    for (i=0; i<=subframe; i++)
      Yk = (Yk*39827)%65537;

return Yk;

#if 0
    Yk = Yk % (nCCE/L);


    switch (L) {
    case 1:
    case 2:
      nb_candidates = 6;
      break;

    case 4:
    case 8:
      nb_candidates = 2;
      break;

    default:
      DevParam(L, nCCE, rnti);
      break;
    }


    LOG_D(MAC,"rnti %x, Yk = %d, nCCE %d (nCCE/L %d),nb_cand %d\n",rnti,Yk,nCCE,nCCE/L,nb_candidates);

    for (m = 0 ; m < nb_candidates ; m++) {
      search_space_free = 1;

      for (l=0; l<L; l++) {
        int cce = (((Yk+m)%(nCCE/L))*L) + l;
        if (cce >= nCCE || CCE_table[cce] == 1) {
          search_space_free = 0;
          break;
        }
      }

      if (search_space_free == 1) {
        for (l=0; l<L; l++)
          CCE_table[(((Yk+m)%(nCCE/L))*L)+l]=1;

        return(((Yk+m)%(nCCE/L))*L);
      }
    }
#endif
}

int nb_candidates[9] = {
  -1,
  6,
  6,
  -1,
  2,
  -1,
  -1,
  -1,
  2
};

int main(int n, char **v)
{
  if (n != 5)
  { printf("gimme <rnti> <aggr. level (L)> <subframe> <N_CCE>\n"); return 1; }

  int rnti  = atoi(v[1]);
  int L     = atoi(v[2]);   /* aggr. level */
  int k     = atoi(v[3]) + 1;
  int N_CCE = atoi(v[4]);

  int sf = k-1;

  unsigned int A = 39827;
  unsigned int D = 65537;

  unsigned int Yk = rnti;
  while (k) { Yk = (A * Yk) % D; k--; }

if (Yk != get_nCCE_offset(L, N_CCE, rnti, sf)) { printf("err\n"); exit(1); }

  printf("Yk = %d\n", Yk);
  printf("L = %d, nb candidates %d\n", L, nb_candidates[L]);

  int m, n_cce;
  for (m = 0; m < nb_candidates[L]; m++) {
    n_cce = L * ((Yk+m) % (N_CCE/L));
    printf("%d\n", n_cce);
  }

  return 0;
}
