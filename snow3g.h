#ifndef _SNOW3G_H_
#define _SNOW3G_H_

#include <stdint.h>

typedef struct {
  uint32_t s[16];
  uint32_t r1, r2, r3;
  uint32_t zt;
  int zt_size;
} snow3g_t;

void snow3g_init(snow3g_t *s, char *k, char *iv);
uint8_t snow3g_get_next_keystream_byte(snow3g_t *s);
void snow3g_ciphering(uint32_t count, int bearer, int direction, uint8_t *key, int length, uint8_t *in, uint8_t *out);
void snow3g_integrity(uint32_t count, int bearer, int direction, uint8_t *key, int length, uint8_t *in, uint8_t *out);

#endif /* _SNOW3G_H_ */
