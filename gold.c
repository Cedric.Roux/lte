/* literal inefficient gold implementation */
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

typedef struct {
  unsigned char *x;
  int length;
} bit_vector;

typedef struct {
  bit_vector x1, x2;
} gold;

static int get_bit(bit_vector *v, int bit)
{
  if (bit >= v->length) abort();
  return v->x[bit];
}

static void set_bit(bit_vector *v, int bit, int val)
{
  if (bit != v->length) abort();
  if ((v->length & 32767) == 0)
    { v->x = realloc(v->x, v->length+32768); if (v->x == NULL) abort(); }
  v->x[v->length] = val;
  v->length++;
}

static int compute_x1(gold *g, int bit)
{
  int n = bit-31;
  return (get_bit(&g->x1, n+3) + get_bit(&g->x1, n)) & 1;
}

static int compute_x2(gold *g, int bit)
{
  int n = bit-31;
  return (get_bit(&g->x2, n+3) + get_bit(&g->x2, n+2) +
          get_bit(&g->x2, n+1) + get_bit(&g->x2, n)) & 1;
}

int gold_c(gold *g, int n)
{
  int bit = n + 1600;
  int i;
  for (i = g->x1.length; i <= bit; i++) set_bit(&g->x1, i, compute_x1(g, i));
  for (i = g->x2.length; i <= bit; i++) set_bit(&g->x2, i, compute_x2(g, i));
  return (get_bit(&g->x1, bit) + get_bit(&g->x2, bit)) & 1;
}

void init_gold(gold *g, uint32_t c_init)
{
  int i;

  memset(g, 0, sizeof(*g));

  set_bit(&g->x1, 0, 1);
  for (i = 1; i <= 30; i++) set_bit(&g->x1, i, 0);
  //for (; i <= 1600; i++) set_bit(&g->x1, i, compute_x1(g, i));

  for (i = 0; i <= 30; i++) set_bit(&g->x2, i, (c_init >> i) & 1);
  //for (; i <= 1600; i++) set_bit(&g->x2, i, compute_x2(g, i));
}

void free_gold(gold *g)
{
  free (g->x1.x);
  free (g->x2.x);
  memset(g, 0, sizeof(*g));
}

#ifdef TEST

/* see 36.211 5.4 (for rel 10, p. 21) */
void print_pucch_n_cs(gold *g)
{
  int N_UL_symb = 7;         /* normal prefix */
  int ns, l, i;
  printf("      l 0   1   2   3   4   5   6\n");
  for (ns = 0; ns < 20; ns++) {
    printf("ns %2d ", ns);
    for (l = 0; l < N_UL_symb; l++) {
      int n_cs = 0;
      for (i = 0; i <= 7; i++)
        n_cs += gold_c(g, 8 * N_UL_symb * ns + 8 * l + i) << i;
      printf(" %3d", n_cs);
    }
    printf("\n");
  }
}

int main(void)
{
  gold g;
  memset(&g, 0, sizeof(g));
  init_gold(&g, 0 /* N_cell_ID */);
  print_pucch_n_cs(&g);
  return 0;
}

#endif /* TEST */
