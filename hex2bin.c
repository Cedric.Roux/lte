#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

int main(void)
{
  int a, b, c;
  unsigned char o;
  while (1) {
    c = getchar(); if (c == EOF) break;
    if      (c >= '0' && c <= '9') a = c - '0';
    else if (c >= 'a' && c <= 'f') a = c - 'a' + 10;
    else if (c >= 'A' && c <= 'F') a = c - 'A' + 10;
    else if (isspace(c)) continue;
    else abort();

    c = getchar(); if (c == EOF) abort();
    if      (c >= '0' && c <= '9') b = c - '0';
    else if (c >= 'a' && c <= 'f') b = c - 'a' + 10;
    else if (c >= 'A' && c <= 'F') b = c - 'A' + 10;
    else abort();

    o = (a << 4) | b;
    printf("%c", o);
  }
  fflush(stdout);
  return 0;
}
