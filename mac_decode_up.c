/* uplink only */

#include <stdio.h>
#include <stdlib.h>

unsigned char pdu[65536];
int pdu_size;

int uplink_lcid_fixed_size[32] = {
  /*  0 */  0,                          /* CCCH */
  /*  1 */  0, 0, 0, 0, 0, 0, 0,        /* lcids */
  /*  8 */  0, 0, 0,                    /* lcids */
  /* 11 */  0, 0, 0, 0, 0,              /* reserved */
  /* 16 */  0, 0, 0, 0, 0, 0, 0, 0,     /* reserved */
  /* 24 */  0,                          /* reserved */
  /* 25 */  0,                          /* extended power headroom */
  /* 26 */  1,                          /* power headroom */
  /* 27 */  2,                          /* crnti */
  /* 28 */  1,                          /* truncated bsr */
  /* 29 */  1,                          /* short bsr */
  /* 30 */  3,                          /* long bsr */
  /* 31 */  1,                          /* padding */
};

struct mac_header {
  int lcid;
  int size;
};

char *lcid_name[32] = {
  "CCCH",
  "lcid 1",
  "lcid 2",
  "lcid 3",
  "lcid 4",
  "lcid 5",
  "lcid 6",
  "lcid 7",
  "lcid 8",
  "lcid 9",
  "lcid 10",
  "reserved 11",
  "reserved 12",
  "reserved 13",
  "reserved 14",
  "reserved 15",
  "reserved 16",
  "reserved 17",
  "reserved 18",
  "reserved 19",
  "reserved 20",
  "reserved 21",
  "reserved 22",
  "reserved 23",
  "reserved 24",
  "extended power headroom",
  "power headroom",
  "crnti",
  "truncated bsr",
  "short bsr",
  "long bsr",
  "padding",
};

int main(void)
{
  unsigned char *s;
  struct mac_header h[128];
  int n = 0;
  int i;

  /* read MAC PDU from stdin */
  while (1) {
    int c = getchar();
    if (c == EOF) break;
    if (pdu_size == 65536) abort();
    pdu[pdu_size] = c;
    pdu_size++;
  }

  /* decode header */
  s = pdu;
  while (1) {
    int e = (*s >> 5) & 1;
    int lcid = *s & 0x1f;
    int size = uplink_lcid_fixed_size[lcid];
    if (!e) size = -1;
    if (!size) {
      s++; if (s >= pdu + pdu_size) goto error;
      int f = (*s >> 7) & 1;
      size = *s & 0x7f;
      if (f) {
        s++; if (s >= pdu + pdu_size) goto error;
        size <<= 8;
        size |= *s;
      }
    }
    if (n == 128) goto error;
    h[n].lcid = lcid;
    h[n].size = size;
    n++;
    if (!e) break;
    s++; if (s >= pdu + pdu_size) goto error;
  }
  s++;

  /* dump sdus */
  for (i = 0; i < n; i++) {
    int j;
    if (h[i].lcid == 31) { printf("PADDING\n"); continue; }
    if (h[i].size == -1) {
      h[i].size = pdu + pdu_size - s;
      printf("LCID %d (%s) LAST size %d: [", h[i].lcid, lcid_name[h[i].lcid],
             h[i].size);
    } else
      printf("LCID %d (%s) size %d: [", h[i].lcid, lcid_name[h[i].lcid],
             h[i].size);
    for (j = 0; j < h[i].size; j++) {
      if (s >= pdu + pdu_size) goto error;
      printf(" %2.2x", *s);
      s++;
    }
    printf(" ]\n");
  }

  return 0;

error:
  printf("malformed mac pdu\n");
  return 1;
}
