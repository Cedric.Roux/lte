/* prints CCEs to monitor in UE spec. space */
#include <stdio.h>
#include <stdlib.h>

int nb_candidates[9] = {
  -1,
  6,
  6,
  -1,
  2,
  -1,
  -1,
  -1,
  2
};

int main(int n, char **v)
{
  if (n != 5)
  { printf("gimme <rnti> <aggr. level (L)> <subframe> <N_CCE>\n"); return 1; }

  int rnti  = atoi(v[1]);
  int L     = atoi(v[2]);   /* aggr. level */
  int k     = atoi(v[3]) + 1;
  int N_CCE = atoi(v[4]);

  unsigned int A = 39827;
  unsigned int D = 65537;

  unsigned int Yk = rnti;
  while (k) { Yk = (A * Yk) % D; k--; }

  printf("Yk = %d\n", Yk);
  printf("L = %d, nb candidates %d\n", L, nb_candidates[L]);

  int m, n_cce;
  for (m = 0; m < nb_candidates[L]; m++) {
    n_cce = L * ((Yk+m) % (N_CCE/L));
    printf("%d\n", n_cce);
  }

  return 0;
}
