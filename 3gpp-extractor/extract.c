#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *line;
int size;
int maxsize;

void put(int c)
{
  if (size == maxsize)
    { line = realloc(line, maxsize+=1024); if (line == NULL) abort(); }
  line[size] = c;
  size++;
}

void p(void)
{
  int col = 0;
  char *l = line;
  while (*l) {
    if (*l == '\t') {
      do {
        printf(" ");
        col++;
      } while (col % 4);
    } else {
      printf("%c", *l);
      col++;
    }
    l++;
  }
  printf("\n");
}

void l(void)
{
  int c;
  size = 0;
  while (1) {
    c = getchar();
    if (c == EOF || c == '\n') break;
    put(c);
  }
  if (c != EOF || size) put(0);
}

int main(void)
{
again:
  while (1) {
    l();
    if (size == 0) return 0;
    if (!strncmp(line, "-- ASN1START", 12)) goto getit;
  }
getit:
  while (1) {
    l();
    if (size == 0) abort();
    if (!strncmp(line, "-- ASN1STOP", 11)) goto again;
    p();
  }
}
