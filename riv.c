/* tool to check decoding of RIV from DCI format 1C (also prints for 1A) */
/* openair is wrong (in develop 82b0b3bad3d04e93175a58b80f34b80e7066fcec) */ 
#include <stdio.h>
#include <stdlib.h>

#define min(a, b) ((a) < (b) ? (a) : (b))

void print_table_1c(int N_DL_RB)
{
  int N_DL_VRB;
  int N_step_RB;
  int N_gap1;
  int N_gap;
  int Nprime_DL_VRB;
  int Lprime_CRBs;
  int RBprime_start;
  int RIV;
  int RIV_choice;
  int RIV_max = 0;

  printf("table for format 1C\n");

  if (N_DL_RB >= 6 && N_DL_RB <= 49) N_step_RB = 2;
  else if (N_DL_RB >= 50 && N_DL_RB <= 110) N_step_RB = 4;
  else { printf("unsupported N_DL_RB value %d\n", N_DL_RB); exit(1); }

  /* only gap1 case (see 36.211 6.2.3.2 for gap2) */
  if (N_DL_RB >= 6 && N_DL_RB <= 10)        N_gap1 = (N_DL_RB+1) / 2;
  else if (N_DL_RB == 11)                   N_gap1 = 4;
  else if (N_DL_RB >= 12 && N_DL_RB <= 19)  N_gap1 = 8;
  else if (N_DL_RB >= 20 && N_DL_RB <= 26)  N_gap1 = 12;
  else if (N_DL_RB >= 27 && N_DL_RB <= 44)  N_gap1 = 18;
  else if (N_DL_RB >= 45 && N_DL_RB <= 49)  N_gap1 = 27;
  else if (N_DL_RB >= 50 && N_DL_RB <= 63)  N_gap1 = 27;
  else if (N_DL_RB >= 64 && N_DL_RB <= 79)  N_gap1 = 32;
  else if (N_DL_RB >= 80 && N_DL_RB <= 110) N_gap1 = 48;
  else { printf("unsupported N_DL_RB (%d)\n", N_DL_RB); exit(1); }

  N_gap = N_gap1;

  N_DL_VRB = 2 * min(N_gap, N_DL_RB - N_gap);
  Nprime_DL_VRB = N_DL_VRB / N_step_RB;

  printf("N_DL_RB %d N_DL_VRB %d Nprime_DL_VRB %d\n",
         N_DL_RB, N_DL_VRB, Nprime_DL_VRB);

  printf("table for Nprime_DL_VRB %d\n", Nprime_DL_VRB);

  for (RBprime_start=0; RBprime_start <= Nprime_DL_VRB-1; RBprime_start++) {
    for (Lprime_CRBs=1; Lprime_CRBs <= Nprime_DL_VRB-RBprime_start;
         Lprime_CRBs++) {
      /* compute RIV as of 36.213 7.1.6.3 */
      if (Lprime_CRBs - 1 <= Nprime_DL_VRB/2) {
        RIV = Nprime_DL_VRB * (Lprime_CRBs - 1) + RBprime_start;
        RIV_choice = 1;
      } else {
        RIV = Nprime_DL_VRB * (Nprime_DL_VRB - Lprime_CRBs+1) +
              (Nprime_DL_VRB - 1 - RBprime_start);
        RIV_choice = 2;
      }

      if (RIV > RIV_max) RIV_max = RIV;

      /* BEGIN: decode RIV as done in openair */
      int rballoc = RIV;
      int NpDLVRB = Nprime_DL_VRB;
      int LpCRBsm1;
      int RBpstart;
      int openair_choice;

      // This is the 1C part from 7.1.6.3 in 36.213
      LpCRBsm1 = rballoc/NpDLVRB;
      //  printf("LpCRBs = %d\n",LpCRBsm1+1);

      if (LpCRBsm1 <= (NpDLVRB/2)) {
        RBpstart = rballoc % NpDLVRB;
        openair_choice = 1;
      }
      else {
        LpCRBsm1 = NpDLVRB-LpCRBsm1;
        RBpstart = NpDLVRB-(rballoc%NpDLVRB);
        openair_choice = 2;
      }
      /* END: decode RIV as done in openair */

      /* decode RIV the right way */
      int start = RIV % Nprime_DL_VRB;
      int len   = RIV / Nprime_DL_VRB + 1;
      if (start + len > Nprime_DL_VRB) {
        start = Nprime_DL_VRB - 1 - start;
        len   = Nprime_DL_VRB + 1 - len + 1;
      }

      printf("RBprime_start %d|%d Lprime_CRBs %d|%d RIV %d (RIV_choice %d) "
             "[openair RBpstart %d LpCRBsm1 %d choice %d]"
             "[f1: start/len %d/%d][f2: start/len %d/%d]\n",
             RBprime_start, start, Lprime_CRBs, len, RIV, RIV_choice,
             RBpstart, LpCRBsm1, openair_choice,
             RIV % Nprime_DL_VRB,
             RIV / Nprime_DL_VRB + 1,
             Nprime_DL_VRB - 1 - RIV % Nprime_DL_VRB,
             Nprime_DL_VRB + 1 - RIV / Nprime_DL_VRB);

      /* this will not happen */
      if (start != RBprime_start || len != Lprime_CRBs)
        { printf("wrong computation!\n"); exit(1); }
    }
  }

  printf("RIV_max %d\n", RIV_max);
}

void print_table_1a(int N_DL_RB)
{
  int N_DL_VRB;
  int N_gap1;
  int N_gap;
  int L_CRBs;
  int RB_start;
  int RIV;
  int RIV_choice;
  int RIV_max = 0;

  printf("table for format 1A\n");

  /* only gap1 case (see 36.211 6.2.3.2 for gap2) */
  if (N_DL_RB >= 6 && N_DL_RB <= 10)        N_gap1 = (N_DL_RB+1) / 2;
  else if (N_DL_RB == 11)                   N_gap1 = 4;
  else if (N_DL_RB >= 12 && N_DL_RB <= 19)  N_gap1 = 8;
  else if (N_DL_RB >= 20 && N_DL_RB <= 26)  N_gap1 = 12;
  else if (N_DL_RB >= 27 && N_DL_RB <= 44)  N_gap1 = 18;
  else if (N_DL_RB >= 45 && N_DL_RB <= 49)  N_gap1 = 27;
  else if (N_DL_RB >= 50 && N_DL_RB <= 63)  N_gap1 = 27;
  else if (N_DL_RB >= 64 && N_DL_RB <= 79)  N_gap1 = 32;
  else if (N_DL_RB >= 80 && N_DL_RB <= 110) N_gap1 = 48;
  else { printf("unsupported N_DL_RB (%d)\n", N_DL_RB); exit(1); }

  N_gap = N_gap1;

  N_DL_VRB = 2 * min(N_gap, N_DL_RB - N_gap);

  printf("N_DL_RB %d N_DL_VRB %d\n", N_DL_RB, N_DL_VRB);

  printf("table for N_DL_VRB %d\n", N_DL_VRB);

  for (RB_start=0; RB_start <= N_DL_RB-1; RB_start++) {
    /* in openair N_DL_RB is used instead of N_DL_VRB here */
    for (L_CRBs=1; L_CRBs <= N_DL_VRB-RB_start; L_CRBs++) {
      /* compute RIV as of 36.213 7.1.6.3 */
      if (L_CRBs - 1 <= N_DL_RB/2) {
        RIV = N_DL_RB * (L_CRBs - 1) + RB_start;
        RIV_choice = 1;
      } else {
        RIV = N_DL_RB * (N_DL_RB - L_CRBs+1) +
              (N_DL_RB - 1 - RB_start);
        RIV_choice = 2;
      }

      if (RIV > RIV_max) RIV_max = RIV;

      /* decoding to be done, the code below is just a copy of case 1C
       * (maybe it's fine)
       */
      /* decode RIV the right way */
      int start = RIV % N_DL_RB;
      int len   = RIV / N_DL_RB + 1;
      if (start + len > N_DL_RB) {
        start = N_DL_RB - 1 - start;
        len   = N_DL_RB + 1 - len + 1;
      }

      printf("RB_start %d|%d L_CRBs %d|%d RIV %d (RIV_choice %d)\n",
             RB_start, start, L_CRBs, len, RIV, RIV_choice);

      /* this will not happen */
      if (start != RB_start || len != L_CRBs)
        { printf("wrong computation!\n"); exit(1); }
    }
  }

  printf("RIV_max %d\n", RIV_max);
}

int main(void)
{
  print_table_1c(6); //3);
  print_table_1c(25); //12);
  print_table_1c(50); //11);
  print_table_1c(100); //24);

  print_table_1a(6);
  print_table_1a(25);
  print_table_1a(50);
  print_table_1a(100);

  return 0;
}
