/* gcc -Wall eea2-128.c -lcrypto */
/* usage: echo <crypted data as hex> | hex2bin | ./eea2-128 <key> | hd */

#include <stdio.h>
#include <openssl/aes.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>

void compute_t(unsigned char *t, uint32_t count, int bearer, int direction)
{
  t[0] = (count >> 24) & 255;
  t[1] = (count >> 16) & 255;
  t[2] = (count >>  8) & 255;
  t[3] = (count      ) & 255;
  t[4] = (bearer << 3) | (direction << 2);
  memset(&t[5], 0, 16-5);
}

void inc_t(unsigned char *t)
{
  int i = 15;
  while (i != 7) {
    t[i]++;
    if (t[i]) break;
    i--;
  }
  /* let's die if last 64 bits wrap around */
  if (i == 7) abort();
}

int ishex(int v)
{
  return (v >= '0' && v <= '9') ||
         (v >= 'a' && v <= 'f') ||
         (v >= 'A' && v <= 'F');
}

int unhex(int v)
{
  if (v >= '0' && v <= '9') return v - '0';
  if (v >= 'a' && v <= 'f') return v + 10 - 'a';
  return v + 10 - 'A';
}

unsigned char *add(char *_in, unsigned char *out, int *outlen)
{
  unsigned char *in = (unsigned char *)_in;
  while (*in) {
    if (!ishex(in[0]) || !ishex(in[1]))
      { printf("bad input %s\n", _in); exit(1); }
    *outlen = *outlen + 1;
    out = realloc(out, *outlen); if (out == NULL) abort();
    out[*outlen-1] = unhex(in[0]) * 16 + unhex(in[1]);
    in += 2;
  }
  return out;
}

void usage(void)
{
  printf("gimme: key [bearer] [direction] [count]\n");
  printf("     key:       hex encoded, 16 bytes (i.e. 128 bits)\n");
  printf("     bearer:    RB identity - 1 (eg. for drb 1 pass 0)\n");
  printf("                default: 0\n");
  printf("     direction: 0 for uplink, 1 for downlink\n");
  printf("                default: 0\n");
  printf("     count:     pdcp sequence number (with HFN in front)\n");
  printf("                default: 0\n");
  exit(0);
}

int main(int n, char **v)
{
  unsigned char t[16];
  unsigned char tc[16];
  uint32_t count = 0;
  int bearer     = 0;  /* is "RB identity - 1" (RB identity is drb-Identity in RRCConnectionReconfiguration) */
  int direction  = 0;  /* 0: uplink, 1: downlink */
  int insize = 0;
  AES_KEY key;
  char *_k         = NULL;
  unsigned char *k = NULL;
  int klen         = 0;
  int i;
  int o = 0;  /* counter for command line's options */

  for (i = 1; i < n; i++) {
    if (!strcmp(v[i], "-h") || !strcmp(v[i], "--help")) usage();
    if (_k == NULL) { _k = v[i]; continue; }
    if (o == 0) { o++; bearer    = atoi(v[i]); continue; }
    if (o == 1) { o++; direction = atoi(v[i]); continue; }
    if (o == 2) { o++; count     = atoi(v[i]); continue; }
    usage();
  }

  if (_k == NULL) usage();

  k = add(_k, k, &klen);

  if (klen != 16) usage();

  compute_t(t, count, bearer, direction);

  AES_set_encrypt_key(k, 128, &key);
  AES_encrypt(t, tc, &key);

  while (1) {
    int c = getchar();
    if (c == EOF) break;
    if (insize == 16) {
      inc_t(t);
      AES_encrypt(t, tc, &key);
      insize = 0;
    }
    putchar((unsigned char)c ^ tc[insize]);
    insize++;
  }

  return 0;
}
