#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

enum format {
  FORMAT_0
};

void err(void)
{
  printf("bad dci\n");
  exit(1);
}

int getbit(int *bits, int nbits, int n)
{
  if (n >= nbits) err();
  return bits[n];
}

void decode0(int *bits, int nbits, int nb_rb)
{
  int cur = 0;
  int i, n, b, v;

  /* carrier indication (0 or 3 bits) not done */

  /* format0/format1a flag (1 bit) */
  if (getbit(bits, nbits, cur) != 0) err();
  cur++;

  /* hopping (1 bit) */
  printf("hopping flag %d\n", getbit(bits, nbits, cur));
  cur++;

  /* resource block assignment (not decoded) (n bits) */
  n = ceil(log2(nb_rb * (nb_rb+1) / 2));
  printf("resource block assignment ");
  v = 0;
  for (i = 0; i < n; i++, cur++) {
    b = getbit(bits, nbits, cur);
    v <<= 1; v |= b;
    printf("%d", b);
  }
  printf(" (%d)\n", v);

  /* mcs (5 bits) */
  printf("mcs ");
  v = 0;
  for (i = 0; i < 5; i++, cur++) {
    b = getbit(bits, nbits, cur);
    v <<= 1; v |= b;
    printf("%d", b);
  }
  printf(" (%d)\n", v);

  /* ndi */
  printf("ndi %d\n", getbit(bits, nbits, cur));
  cur++;

  /* tpc for pusch (2 bits) */
  printf("tpc for pusch %d%d\n",
         getbit(bits, nbits, cur), getbit(bits, nbits, cur+1));
  cur+=2;

  /* cyclic shift for dm rs and occ index (3 bits) */
  printf("cyclic shift for dm rs and occ index %d%d%d\n",
         getbit(bits, nbits, cur), getbit(bits, nbits, cur+1),
         getbit(bits, nbits, cur+2));
  cur+=3;

  /* ul index - TDD (2 bits) NOT DONE */

  /* DAI - TDD (2 bits) NOT DONE */

  /* CSI request (1 or 2 bits), only 1 bit case done*/
  printf("csi request %d\n", getbit(bits, nbits, cur));
  cur++;

  /* SRS request (0 or 1 bit), not done */

  /* resource allocation type (1 bit) */
  printf("resource allocation type %d\n", getbit(bits, nbits, cur));
  cur++;
}

void decode(int *bits, int nbits, int format, int nb_rb)
{
  switch (format) {
  case 0: decode0(bits, nbits, nb_rb); return;
  }
}

void u(void)
{
  printf("usage: [options] <dci (binary)>\n");
  printf("options:\n");
  printf("    -f <format> (default 0)\n");
  printf("        format one of: 0\n");
  printf("    -b <number of resource blocks> (default 25)\n");
  exit(0);
}

int main(int n, char **v)
{
  char *dci = NULL;
  int format = FORMAT_0;
  int nb_rb = 25;
  int bits[128];
  int i;

  for (i = 1; i < n; i++) {
    if (!strcmp(v[i], "-f")) { if (i > n - 2) u(); i++;
      if (!strcmp(v[i], "0")) format = FORMAT_0; else u();
      continue;
    }
    if (!strcmp(v[i], "-b")) { if (i > n - 2) u(); i++;
      nb_rb = atoi(v[i]);
      if (nb_rb != 25 && nb_rb != 50) u();
      continue;
    }
    if (dci != NULL) u();
    dci = v[i];
  }

  if (dci == NULL) u();

  for (i = 0; dci[i]; i++) bits[i] = dci[i]=='0'?0 : dci[i]=='1'?1 : (u(),0);

  decode(bits, i, format, nb_rb);

  return 0;
}
